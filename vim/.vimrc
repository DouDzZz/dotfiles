execute pathogen#infect()
syntax on
set clipboard=unnamedplus
colorscheme desert
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set mouse=a
set number
set ruler
set splitright
set splitbelow
map <F5> <Esc>:tabedit 
map <F6> <Esc>:tabprevious
nnoremap <C-J> <Esc><C-W><C-J>
nnoremap <C-K> <Esc><C-W><C-K>
nnoremap <C-H> <Esc><C-W><C-H>
nnoremap <C-L> <Esc><C-W><C-L>
map <C-S> <Esc>:sp 
map <C-S><C-D> <Esc>:vsp 
map <C-Q> <Esc>:set relativenumber! 
set directory^=$HOME/.vim/tmp//
set noswapfile
nnoremap e ea
map <C-n> :NERDTreeToggle<CR>
let g:airline_theme='luna'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'
" let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
