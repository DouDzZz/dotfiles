" Insert a newline after each specified string (or before if use '!').
" If no arguments, use previous search
command! -bang -nargs=* -range LineBreakAt <line1>,<line2>call LineBreakAt('<bang>', <f-args>)
function! LineBreakAt(bang, ...) range
	let save_search = @/
	if empty(a:bang)
		let before = ''
		let after = '\ze.'
		let repl = '&\r'
	else
		let before = '.\zs'
		let after = ''
		let repl = '\r$'
	endif
	let pa_list = map(deepcopy(a:000), "escape(v:val, '/\\.*$^~[')")
	let find = empty(pa_list) ? @/ : join(pa_list, '\|')
	let find = before . '\%(' . find .'\)' . after
	" Example: 10,20s/\%(arg1\|arg2\|arg3|)\ze./&\r/ge
	execute a:firstline . ',' . a:lastline . 's/' . find . '/' . repl . '/ge'
	let @/ = save_search
endfunction
