" **************************************************************************** "
"                                                                              "
"                                                         :::      ::::::::    "
"    init.vim                                           :+:      :+:    :+:    "
"                                                     +:+ +:+         +:+      "
"    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         "
"                                                 +#+#+#+#+#+   +#+            "
"    Created: 2019/04/02 15:54:31 by edjubert          #+#    #+#              "
"    Updated: 2019/04/02 16:10:50 by edjubert         ###   ########.fr        "
"                                                                              "
" **************************************************************************** "

" **************************************************************************** "
"                                                                              "
"                                     ZAZRC                                    "
"                                                                              "
" **************************************************************************** "

"---------------------------- Activate indentation --------------------------- "
"filetype off
"filetype plugin indent on
"set smartindent

"----------------------- Non-expanded, 4-wide tabulations -------------------- "
set tabstop=4
set shiftwidth=4
set noexpandtab
set autoread

"-------------------------- Disable vi-compatibility ------------------------- "
"set nocompatible


"---------------------------- Real-world encoding ---------------------------- "
set encoding=utf-8

"------------------------ Interpret modelines in files ----------------------- "
"set modelines=1

"-------------------------- Do not abandon buffers --------------------------- "
set hidden

"------------------------ Don't bother throttling tty ------------------------ "
"set ttyfast

"---------------------- More useful backspace behavior ----------------------- "
set backspace=indent,eol,start

"---------------------- Use statusbar on all windows ------------------------- "
"set laststatus=2

"----------------------------- Better search --------------------------------- "
set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch

"---------------- Prevent backups when editing system files ----------------- "
"au BufWrite /private/tmp/crontab.* set nowritebackup
"au BufWrite /private/etc/pw.* set nowritebackup

"------------------------ Source user configuration ------------------------- "
" if filereadable(expand("~/.myvimrc"))
" 	    source ~/.myvimrc
" 	endif

"--------------------------- END OF ZAZ<3 VIMRC ----------------------------- "



" *************************************************************************** "
"                                                                             "
"                                  SETTERS                                    "
"                                                                             "
" *************************************************************************** "

"--------------------------------- STYLING ---------------------------------- "
syntax on
colorscheme desert

"---------------------------------- TOOLS ----------------------------------- "
set clipboard=unnamed
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set mouse=a
set number
set ruler
set splitright
set splitbelow

"--------------------------------- KEY MAP ---------------------------------- "
map <F5> <Esc>:tabedit 
map <F6> <Esc>:tabprevious<CR>
map <F7> <Esc>:tabnext<Cr>
map <C-n> :NERDTreeToggle<CR>
map <C-S> <Esc>:sp 
map <C-S><C-D> <Esc>:vsp 
map <C-Q> <Esc>:set relativenumber!<CR>
nnoremap <C-J> <Esc><C-W><C-J>
nnoremap <C-K> <Esc><C-W><C-K>
nnoremap <C-H> <Esc><C-W><C-H>
nnoremap <C-L> <Esc><C-W><C-L>
nnoremap e ea

"----------------------------- HANDLE SWAP FILES ---------------------------- "
set directory^=$HOME/.vim/tmp//
set noswapfile


" *************************************************************************** "
"                                                                             "
"                                 VIM AIRLINE                                 "
"                                                                             "
" *************************************************************************** "

let g:airline_theme='luna'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'
let g:airline#extensions#tabline#buffer_nr_show = 1
" let g:airline#extensions#tabline#buffer_idx_mode = 1


" *************************************************************************** "
"                                                                             "
"                               PLUGIN MANAGER                                "
"                                                                             "
" *************************************************************************** "

call plug#begin()
	Plug 'neomake/neomake'
	Plug 'mattn/emmet-vim'
	Plug '2072/PHP-Indenting-for-VIm'
	Plug 'tpope/vim-commentary'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'scrooloose/nerdtree'
	Plug 'pbondoer/vim-42header'
	Plug 'tmux-plugins/vim-tmux-focus-events'
"	Plug 'junegunn/goyo.vim'
call plug#end()

let g:deoplete#enable_at_startup = 1

" *************************************************************************** "
"                                                                             "
"                                  NEOMAKE                                    "
"                                                                             "
" *************************************************************************** "
autocmd BufWinEnter,BufWritePost *.c :Neomake gcc

call neomake#configure#automake('w')
let g:neomake_verbose = 3
let g:neomake_error_sign = {'text': '❌'}
let g:neomake_warning_sign = {'text': '⚠️'}
let g:neomake_style_warning_sign = {'text': '💩'}
let g:neomake_place_signs = 1
let g:neomake_style_error_sign = {'text': '⁉️'}
let g:neomake_open_list=0
let g:c_syntax_for_h=1
let g:neomake_c_enabled_makers=['gcc']
let g:neomake_gcc_args=[
			\ '-fsyntax-only',
			\ '-Wall',
			\ '-Werror',
			\ '-Wextra',
			\ '-Wconversion',
			\ '-Wunreachable-code',
			\ '-Winit-self',
			\ '-I../includes/',
			\ '-I../include/',
			\ '-I.',
			\ ]
"	\ '-Wfloat-equal',
"	\ '-Wshadow',
"	\ '-Wpointer-arith',
"	\ '-Wcast-align',
"	\ '-Wstrict-prototypes',
"	\ '-Wwrite-strings',
"	\ '-Waggregate-return',

