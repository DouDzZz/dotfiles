(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('styled-components')) :
	typeof define === 'function' && define.amd ? define('styled-is', ['exports', 'styled-components'], factory) :
	(factory((global['styled-is'] = {}),global.styledComponents));
}(this, (function (exports,styledComponents) { 'use strict';

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var styledIf = function styledIf(method, condition) {
  return function () {
    for (var _len = arguments.length, names = Array(_len), _key = 0; _key < _len; _key++) {
      names[_key] = arguments[_key];
    }

    return function () {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return function (props) {
        return names[method](function (name) {
          return Boolean(props[name]) === condition;
        }) && styledComponents.css.apply(undefined, args);
      };
    };
  };
};

var is = styledIf('every', true);
var isNot = styledIf('every', false);
var isOr = styledIf('some', true);
var isSomeNot = styledIf('some', false);

exports.default = is;
exports.isNot = isNot;
exports.isOr = isOr;
exports.isSomeNot = isSomeNot;

Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRoaXMgU291cmNlIENvZGUgRm9ybSBpcyBzdWJqZWN0IHRvIHRoZSB0ZXJtcyBvZiB0aGUgTW96aWxsYSBQdWJsaWNcbi8vIExpY2Vuc2UsIHYuIDIuMC4gSWYgYSBjb3B5IG9mIHRoZSBNUEwgd2FzIG5vdCBkaXN0cmlidXRlZCB3aXRoIHRoaXNcbi8vIGZpbGUsIFlvdSBjYW4gb2J0YWluIG9uZSBhdCBodHRwOi8vbW96aWxsYS5vcmcvTVBMLzIuMC8uXG5cbmltcG9ydCB7IGNzcyB9IGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3Qgc3R5bGVkSWYgPSAobWV0aG9kLCBjb25kaXRpb24pID0+ICguLi5uYW1lcykgPT4gKC4uLmFyZ3MpID0+IHByb3BzID0+XG4gIG5hbWVzW21ldGhvZF0obmFtZSA9PiBCb29sZWFuKHByb3BzW25hbWVdKSA9PT0gY29uZGl0aW9uKSAmJiBjc3MoLi4uYXJncyk7XG5cbmNvbnN0IGlzID0gc3R5bGVkSWYoJ2V2ZXJ5JywgdHJ1ZSk7XG5jb25zdCBpc05vdCA9IHN0eWxlZElmKCdldmVyeScsIGZhbHNlKTtcbmNvbnN0IGlzT3IgPSBzdHlsZWRJZignc29tZScsIHRydWUpO1xuY29uc3QgaXNTb21lTm90ID0gc3R5bGVkSWYoJ3NvbWUnLCBmYWxzZSk7XG5cbmV4cG9ydCBkZWZhdWx0IGlzO1xuZXhwb3J0IHsgaXNOb3QsIGlzT3IsIGlzU29tZU5vdCB9O1xuIl0sIm5hbWVzIjpbInN0eWxlZElmIiwibWV0aG9kIiwiY29uZGl0aW9uIiwibmFtZXMiLCJhcmdzIiwiQm9vbGVhbiIsInByb3BzIiwibmFtZSIsImNzcyIsImlzIiwiaXNOb3QiLCJpc09yIiwiaXNTb21lTm90Il0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7OztBQUlBLEFBRUEsSUFBTUEsV0FBVyxTQUFYQSxRQUFXLENBQUNDLE1BQUQsRUFBU0MsU0FBVDtTQUF1QjtzQ0FBSUMsS0FBSjtXQUFBOzs7V0FBYzt5Q0FBSUMsSUFBSjtZQUFBOzs7YUFBYTtlQUNqRUQsTUFBTUYsTUFBTixFQUFjO2lCQUFRSSxRQUFRQyxNQUFNQyxJQUFOLENBQVIsTUFBeUJMLFNBQWpDO1NBQWQsS0FBNkRNLHNDQUFPSixJQUFQLENBREk7T0FBYjtLQUFkO0dBQXZCO0NBQWpCOztBQUdBLElBQU1LLEtBQUtULFNBQVMsT0FBVCxFQUFrQixJQUFsQixDQUFYO0FBQ0EsSUFBTVUsUUFBUVYsU0FBUyxPQUFULEVBQWtCLEtBQWxCLENBQWQ7QUFDQSxJQUFNVyxPQUFPWCxTQUFTLE1BQVQsRUFBaUIsSUFBakIsQ0FBYjtBQUNBLElBQU1ZLFlBQVlaLFNBQVMsTUFBVCxFQUFpQixLQUFqQixDQUFsQjs7Ozs7Ozs7Ozs7Ozs7OyJ9