import { css } from 'styled-components';

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var styledIf = function styledIf(method, condition) {
  return function () {
    for (var _len = arguments.length, names = Array(_len), _key = 0; _key < _len; _key++) {
      names[_key] = arguments[_key];
    }

    return function () {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return function (props) {
        return names[method](function (name) {
          return Boolean(props[name]) === condition;
        }) && css.apply(undefined, args);
      };
    };
  };
};

var is = styledIf('every', true);
var isNot = styledIf('every', false);
var isOr = styledIf('some', true);
var isSomeNot = styledIf('some', false);

export default is;
export { isNot, isOr, isSomeNot };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRoaXMgU291cmNlIENvZGUgRm9ybSBpcyBzdWJqZWN0IHRvIHRoZSB0ZXJtcyBvZiB0aGUgTW96aWxsYSBQdWJsaWNcbi8vIExpY2Vuc2UsIHYuIDIuMC4gSWYgYSBjb3B5IG9mIHRoZSBNUEwgd2FzIG5vdCBkaXN0cmlidXRlZCB3aXRoIHRoaXNcbi8vIGZpbGUsIFlvdSBjYW4gb2J0YWluIG9uZSBhdCBodHRwOi8vbW96aWxsYS5vcmcvTVBMLzIuMC8uXG5cbmltcG9ydCB7IGNzcyB9IGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3Qgc3R5bGVkSWYgPSAobWV0aG9kLCBjb25kaXRpb24pID0+ICguLi5uYW1lcykgPT4gKC4uLmFyZ3MpID0+IHByb3BzID0+XG4gIG5hbWVzW21ldGhvZF0obmFtZSA9PiBCb29sZWFuKHByb3BzW25hbWVdKSA9PT0gY29uZGl0aW9uKSAmJiBjc3MoLi4uYXJncyk7XG5cbmNvbnN0IGlzID0gc3R5bGVkSWYoJ2V2ZXJ5JywgdHJ1ZSk7XG5jb25zdCBpc05vdCA9IHN0eWxlZElmKCdldmVyeScsIGZhbHNlKTtcbmNvbnN0IGlzT3IgPSBzdHlsZWRJZignc29tZScsIHRydWUpO1xuY29uc3QgaXNTb21lTm90ID0gc3R5bGVkSWYoJ3NvbWUnLCBmYWxzZSk7XG5cbmV4cG9ydCBkZWZhdWx0IGlzO1xuZXhwb3J0IHsgaXNOb3QsIGlzT3IsIGlzU29tZU5vdCB9O1xuIl0sIm5hbWVzIjpbInN0eWxlZElmIiwibWV0aG9kIiwiY29uZGl0aW9uIiwibmFtZXMiLCJhcmdzIiwiQm9vbGVhbiIsInByb3BzIiwibmFtZSIsImNzcyIsImlzIiwiaXNOb3QiLCJpc09yIiwiaXNTb21lTm90Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7O0FBSUEsQUFFQSxJQUFNQSxXQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsTUFBRCxFQUFTQyxTQUFUO1NBQXVCO3NDQUFJQyxLQUFKO1dBQUE7OztXQUFjO3lDQUFJQyxJQUFKO1lBQUE7OzthQUFhO2VBQ2pFRCxNQUFNRixNQUFOLEVBQWM7aUJBQVFJLFFBQVFDLE1BQU1DLElBQU4sQ0FBUixNQUF5QkwsU0FBakM7U0FBZCxLQUE2RE0scUJBQU9KLElBQVAsQ0FESTtPQUFiO0tBQWQ7R0FBdkI7Q0FBakI7O0FBR0EsSUFBTUssS0FBS1QsU0FBUyxPQUFULEVBQWtCLElBQWxCLENBQVg7QUFDQSxJQUFNVSxRQUFRVixTQUFTLE9BQVQsRUFBa0IsS0FBbEIsQ0FBZDtBQUNBLElBQU1XLE9BQU9YLFNBQVMsTUFBVCxFQUFpQixJQUFqQixDQUFiO0FBQ0EsSUFBTVksWUFBWVosU0FBUyxNQUFULEVBQWlCLEtBQWpCLENBQWxCOzs7OzsifQ==