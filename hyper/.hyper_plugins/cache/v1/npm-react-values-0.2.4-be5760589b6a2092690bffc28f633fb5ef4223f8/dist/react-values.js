(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react')) :
	typeof define === 'function' && define.amd ? define(['exports', 'react'], factory) :
	(factory((global.ReactValues = {}),global.React));
}(this, (function (exports,React) { 'use strict';

React = React && React.hasOwnProperty('default') ? React['default'] : React;

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();







var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};



var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var AnyValue = function (_React$Component) {
  inherits(AnyValue, _React$Component);

  function AnyValue(props, context, empty) {
    classCallCheck(this, AnyValue);

    var _this = possibleConstructorReturn(this, (AnyValue.__proto__ || Object.getPrototypeOf(AnyValue)).call(this, props));

    var controlled = props.value !== undefined;
    var initial = empty;
    if (props.defaultValue !== undefined) initial = props.defaultValue;
    if (props.value !== undefined) initial = props.value;

    _this.state = { controlled: controlled, value: initial };
    _this.transforms = {};
    _this.computeds = {};
    _this.mounted = false;

    _this.define('set', function (v, next) {
      return next;
    });
    _this.define('reset', function () {
      return _this.clone(initial);
    });
    _this.define('clear', function () {
      return _this.clone(empty);
    });
    return _this;
  }

  createClass(AnyValue, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.mounted = false;
    }
  }, {
    key: 'clone',
    value: function clone(value) {
      return value;
    }
  }, {
    key: 'transform',
    value: function transform(fn, options) {
      var _this2 = this;

      var _props = this.props,
          disabled = _props.disabled,
          value = _props.value,
          onChange = _props.onChange;

      if (!this.mounted) return;
      if (disabled) return;

      if (this.state.controlled) {
        var next = this.apply(value, fn, options);
        if (onChange) onChange(next);
      } else {
        this.setState(function (existing) {
          var next = _this2.apply(existing.value, fn, options);
          return { value: next };
        }, function () {
          if (onChange) onChange(_this2.state.value);
        });
      }
    }
  }, {
    key: 'apply',
    value: function apply(value, fn) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var _options$mutates = options.mutates,
          mutates = _options$mutates === undefined ? false : _options$mutates;

      var current = mutates ? this.clone(value) : value;
      var next = typeof fn === 'function' ? fn(current) : fn;
      return next;
    }
  }, {
    key: 'define',
    value: function define(name, fn, options) {
      var _this3 = this;

      this.transforms[name] = function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        _this3.transform(function (v) {
          return fn.apply(undefined, [v].concat(args));
        }, options);
      };
    }
  }, {
    key: 'compute',
    value: function compute(name, fn) {
      var _this4 = this;

      this.computeds[name] = function () {
        return fn(_this4.value);
      };
    }
  }, {
    key: 'proxy',
    value: function proxy(method) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var _options$alias = options.alias,
          alias = _options$alias === undefined ? method : _options$alias,
          _options$mutates2 = options.mutates,
          mutates = _options$mutates2 === undefined ? false : _options$mutates2;


      this.define(alias, function (v) {
        for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
          args[_key2 - 1] = arguments[_key2];
        }

        var ret = v[method].apply(v, args);
        return mutates ? v : ret;
      }, options);
    }
  }, {
    key: 'render',
    value: function render() {
      var props = this.props,
          transforms = this.transforms,
          computeds = this.computeds,
          value = this.value;
      var children = props.children,
          render = props.render,
          _props$disabled = props.disabled,
          disabled = _props$disabled === undefined ? false : _props$disabled;

      var fn = children || render;
      if (fn === null) return null;

      var renderProps = _extends({ value: value, disabled: disabled }, transforms);

      for (var key in computeds) {
        renderProps[key] = computeds[key]();
      }

      var ret = typeof fn === 'function' ? fn(renderProps) : fn;
      return ret;
    }
  }, {
    key: 'value',
    get: function get$$1() {
      var state = this.state,
          props = this.props;

      return state.controlled ? props.value : state.value;
    }
  }]);
  return AnyValue;
}(React.Component);

var ArrayValue = function (_AnyValue) {
  inherits(ArrayValue, _AnyValue);

  function ArrayValue() {
    var _ref;

    classCallCheck(this, ArrayValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = ArrayValue.__proto__ || Object.getPrototypeOf(ArrayValue)).call.apply(_ref, [this].concat(args, [[]])));

    _this.compute('first', function (v) {
      return v[0];
    });
    _this.compute('last', function (v) {
      return v[Math.max(0, v.length - 1)];
    });

    _this.proxy('concat');
    _this.proxy('fill', { mutates: true });
    _this.proxy('filter');
    _this.proxy('flat');
    _this.proxy('flatMap');
    _this.proxy('map');
    _this.proxy('pop', { mutates: true });
    _this.proxy('push', { mutates: true });
    _this.proxy('reverse', { mutates: true });
    _this.proxy('shift', { mutates: true });
    _this.proxy('slice');
    _this.proxy('sort', { mutates: true });
    _this.proxy('splice', { mutates: true });
    _this.proxy('unshift', { mutates: true });
    return _this;
  }

  createClass(ArrayValue, [{
    key: 'clone',
    value: function clone(value) {
      return value.slice();
    }
  }]);
  return ArrayValue;
}(AnyValue);

var BooleanValue = function (_AnyValue) {
  inherits(BooleanValue, _AnyValue);

  function BooleanValue() {
    var _ref;

    classCallCheck(this, BooleanValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = BooleanValue.__proto__ || Object.getPrototypeOf(BooleanValue)).call.apply(_ref, [this].concat(args, [false])));

    _this.define('toggle', function (v) {
      return !v;
    });
    return _this;
  }

  return BooleanValue;
}(AnyValue);

var SECONDS = 1000;
var MINUTES = 1000 * 60;
var HOURS = 1000 * 60 * 60;

var DateValue = function (_AnyValue) {
  inherits(DateValue, _AnyValue);

  function DateValue() {
    var _ref;

    classCallCheck(this, DateValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = DateValue.__proto__ || Object.getPrototypeOf(DateValue)).call.apply(_ref, [this].concat(args, [new Date()])));

    _this.compute('date', function (v) {
      return v.getDate();
    });
    _this.compute('hours', function (v) {
      return v.getHours();
    });
    _this.compute('milliseconds', function (v) {
      return v.getMilliseconds();
    });
    _this.compute('minutes', function (v) {
      return v.getMinutes();
    });
    _this.compute('month', function (v) {
      return v.getMonth();
    });
    _this.compute('seconds', function (v) {
      return v.getSeconds();
    });
    _this.compute('year', function (v) {
      return v.getFullYear();
    });

    _this.define('setMonth', setMonth);
    _this.proxy('setDate', { mutates: true });
    _this.proxy('setFullYear', { alias: 'setYear', mutates: true });
    _this.proxy('setFullYear', { mutates: true });
    _this.proxy('setHours', { mutates: true });
    _this.proxy('setMilliseconds', { mutates: true });
    _this.proxy('setMinutes', { mutates: true });
    _this.proxy('setSeconds', { mutates: true });

    _this.define('incrementDate', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incDate(v, n);
    });
    _this.define('incrementHours', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, n * HOURS);
    });
    _this.define('incrementMilliseconds', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, n);
    });
    _this.define('incrementMinutes', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, n * MINUTES);
    });
    _this.define('incrementMonth', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMonth(v, n);
    });
    _this.define('incrementSeconds', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, n * SECONDS);
    });
    _this.define('incrementYear', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMonth(v, n * 12);
    });

    _this.define('decrementDate', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incDate(v, 0 - n);
    });
    _this.define('decrementHours', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, 0 - n * HOURS);
    });
    _this.define('decrementMilliseconds', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, 0 - n);
    });
    _this.define('decrementMinutes', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, 0 - n * MINUTES);
    });
    _this.define('decrementMonth', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMonth(v, 0 - n);
    });
    _this.define('decrementSeconds', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMil(v, 0 - n * SECONDS);
    });
    _this.define('decrementYear', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return incMonth(v, 0 - n * 12);
    });
    return _this;
  }

  createClass(DateValue, [{
    key: 'clone',
    value: function clone(value) {
      return new Date(value.getTime());
    }
  }]);
  return DateValue;
}(AnyValue);

function incMil(v, n) {
  return new Date(v.getTime() + n);
}

function incDate(v, n) {
  var d = new Date(v.getTime());
  d.setDate(d.getDate() + n);
  return d;
}

function incMonth(v, n) {
  var year = v.getFullYear();
  var desiredMonth = v.getMonth() + n;
  var desired = new Date(0);
  desired.setFullYear(year, desiredMonth, 1);
  desired.setHours(0, 0, 0, 0);
  var max = days(desired);
  v.setMonth(desiredMonth, Math.min(max, v.getDate()));
  return v;
}

function setMonth(v, m) {
  var d = new Date(v.getTime());
  var year = d.getFullYear();
  var day = d.getDate();
  var desired = new Date(0);
  desired.setFullYear(year, m, 15);
  desired.setHours(0, 0, 0, 0);
  var max = days(desired);
  d.setMonth(m, Math.min(day, max));
  return d;
}

function days(v) {
  var year = v.getFullYear();
  var monthIndex = v.getMonth();
  var lastDayOfMonth = new Date(0);
  lastDayOfMonth.setFullYear(year, monthIndex + 1, 0);
  lastDayOfMonth.setHours(0, 0, 0, 0);
  return lastDayOfMonth.getDate();
}

var MapValue = function (_AnyValue) {
  inherits(MapValue, _AnyValue);

  function MapValue() {
    var _ref;

    classCallCheck(this, MapValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = MapValue.__proto__ || Object.getPrototypeOf(MapValue)).call.apply(_ref, [this].concat(args, [new Map()])));

    _this.define('set', function (v) {
      for (var _len2 = arguments.length, a = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        a[_key2 - 1] = arguments[_key2];
      }

      var first = a[0];
      return a.length === 1 ? typeof first === 'function' ? first(v) : first : v.set.apply(v, a);
    });

    _this.proxy('clear', { mutates: true });
    _this.proxy('delete', { mutates: true });
    _this.proxy('delete', { alias: 'unset', mutates: true });
    return _this;
  }

  createClass(MapValue, [{
    key: 'clone',
    value: function clone(value) {
      return new Map(value);
    }
  }]);
  return MapValue;
}(AnyValue);

var NumberValue = function (_AnyValue) {
  inherits(NumberValue, _AnyValue);

  function NumberValue() {
    var _ref;

    classCallCheck(this, NumberValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = NumberValue.__proto__ || Object.getPrototypeOf(NumberValue)).call.apply(_ref, [this].concat(args, [0])));

    _this.define('increment', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

      var num = typeof n !== 'number' ? 1 : n;
      return Math.min(v + num, _this.props.max);
    });

    _this.define('decrement', function (v) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

      var num = typeof n !== 'number' ? 1 : n;
      return Math.max(v - num, _this.props.min);
    });
    return _this;
  }

  return NumberValue;
}(AnyValue);

NumberValue.defaultProps = {
  max: Number.MAX_SAFE_INTEGER,
  min: Number.MIN_SAFE_INTEGER
};

var ObjectValue = function (_AnyValue) {
  inherits(ObjectValue, _AnyValue);

  function ObjectValue() {
    var _ref;

    classCallCheck(this, ObjectValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = ObjectValue.__proto__ || Object.getPrototypeOf(ObjectValue)).call.apply(_ref, [this].concat(args, [{}])));

    _this.define('set', function (v) {
      for (var _len2 = arguments.length, a = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        a[_key2 - 1] = arguments[_key2];
      }

      var first = a[0];

      if (a.length === 1) {
        return typeof first === 'function' ? first(v) : first;
      }

      var key = a[0],
          val = a[1];

      var clone = _extends({}, v);
      clone[key] = val;
      return clone;
    });

    _this.define('assign', function (v) {
      var val = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return _extends({}, v, val);
    });
    _this.define('clear', function () {
      return {};
    });
    _this.define('delete', unset);
    _this.define('unset', unset);
    return _this;
  }

  createClass(ObjectValue, [{
    key: 'clone',
    value: function clone(value) {
      return _extends({}, value);
    }
  }]);
  return ObjectValue;
}(AnyValue);

function unset(v, key) {
  var clone = _extends({}, v);
  delete clone[key];
  return clone;
}

var SetValue = function (_AnyValue) {
  inherits(SetValue, _AnyValue);

  function SetValue() {
    var _ref;

    classCallCheck(this, SetValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = SetValue.__proto__ || Object.getPrototypeOf(SetValue)).call.apply(_ref, [this].concat(args, [new Set()])));

    _this.define('toggle', function (v, val, boolean) {
      if (boolean) {
        return v.add(val);
      } else {
        v.delete(val);
        return v;
      }
    }, { mutates: true });

    _this.proxy('add');
    _this.proxy('clear', { mutates: true });
    _this.proxy('delete', { mutates: true });
    _this.proxy('delete', { alias: 'remove', mutates: true });
    return _this;
  }

  createClass(SetValue, [{
    key: 'clone',
    value: function clone(value) {
      return new Set(value);
    }
  }]);
  return SetValue;
}(AnyValue);

var StringValue = function (_AnyValue) {
  inherits(StringValue, _AnyValue);

  function StringValue() {
    var _ref;

    classCallCheck(this, StringValue);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = possibleConstructorReturn(this, (_ref = StringValue.__proto__ || Object.getPrototypeOf(StringValue)).call.apply(_ref, [this].concat(args, [''])));

    _this.proxy('concat');
    _this.proxy('normalize');
    _this.proxy('padEnd');
    _this.proxy('padStart');
    _this.proxy('repeat');
    _this.proxy('replace');
    _this.proxy('slice');
    _this.proxy('substr');
    _this.proxy('substring');
    _this.proxy('toLowerCase');
    _this.proxy('toUpperCase');
    _this.proxy('trim');
    _this.proxy('trimEnd');
    _this.proxy('trimStart');
    return _this;
  }

  return StringValue;
}(AnyValue);

exports.AnyValue = AnyValue;
exports.ArrayValue = ArrayValue;
exports.BooleanValue = BooleanValue;
exports.DateValue = DateValue;
exports.MapValue = MapValue;
exports.NumberValue = NumberValue;
exports.ObjectValue = ObjectValue;
exports.SetValue = SetValue;
exports.StringValue = StringValue;

Object.defineProperty(exports, '__esModule', { value: true });

})));
