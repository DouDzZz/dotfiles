import { StyledIcon, StyledIconProps } from '..';
export declare const Type: StyledIcon<any>;
export declare const TypeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
