import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crosshair: StyledIcon<any>;
export declare const CrosshairDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
