import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MessageCircle: StyledIcon<any>;
export declare const MessageCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
