import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Upload: StyledIcon<any>;
export declare const UploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
