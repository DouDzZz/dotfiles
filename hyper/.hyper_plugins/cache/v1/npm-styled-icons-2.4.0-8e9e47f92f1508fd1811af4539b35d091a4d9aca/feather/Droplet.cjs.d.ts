import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Droplet: StyledIcon<any>;
export declare const DropletDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
