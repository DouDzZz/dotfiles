import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDownCircle: StyledIcon<any>;
export declare const ArrowDownCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
