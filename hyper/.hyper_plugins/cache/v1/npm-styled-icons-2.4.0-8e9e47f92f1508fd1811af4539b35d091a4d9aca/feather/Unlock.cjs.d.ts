import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unlock: StyledIcon<any>;
export declare const UnlockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
