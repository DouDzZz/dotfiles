import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Briefcase: StyledIcon<any>;
export declare const BriefcaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
