import { StyledIcon, StyledIconProps } from '..';
export declare const HardDrive: StyledIcon<any>;
export declare const HardDriveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
