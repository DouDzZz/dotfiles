import { StyledIcon, StyledIconProps } from '..';
export declare const AlignCenter: StyledIcon<any>;
export declare const AlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
