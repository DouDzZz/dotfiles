import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RefreshCw: StyledIcon<any>;
export declare const RefreshCwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
