import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudOff: StyledIcon<any>;
export declare const CloudOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
