import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SkipBack: StyledIcon<any>;
export declare const SkipBackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
