import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Film: StyledIcon<any>;
export declare const FilmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
