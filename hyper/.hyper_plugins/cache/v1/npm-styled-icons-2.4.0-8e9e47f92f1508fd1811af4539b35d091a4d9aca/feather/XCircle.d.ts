import { StyledIcon, StyledIconProps } from '..';
export declare const XCircle: StyledIcon<any>;
export declare const XCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
