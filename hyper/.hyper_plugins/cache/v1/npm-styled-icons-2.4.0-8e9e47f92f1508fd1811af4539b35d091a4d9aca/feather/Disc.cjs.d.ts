import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Disc: StyledIcon<any>;
export declare const DiscDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
