import { StyledIcon, StyledIconProps } from '..';
export declare const Home: StyledIcon<any>;
export declare const HomeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
