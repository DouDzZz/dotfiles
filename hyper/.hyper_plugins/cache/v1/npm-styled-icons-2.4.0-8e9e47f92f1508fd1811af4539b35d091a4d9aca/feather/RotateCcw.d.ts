import { StyledIcon, StyledIconProps } from '..';
export declare const RotateCcw: StyledIcon<any>;
export declare const RotateCcwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
