import { StyledIcon, StyledIconProps } from '..';
export declare const RotateCw: StyledIcon<any>;
export declare const RotateCwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
