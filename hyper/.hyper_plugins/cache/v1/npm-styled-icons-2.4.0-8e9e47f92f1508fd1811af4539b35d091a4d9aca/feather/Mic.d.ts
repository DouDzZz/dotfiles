import { StyledIcon, StyledIconProps } from '..';
export declare const Mic: StyledIcon<any>;
export declare const MicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
