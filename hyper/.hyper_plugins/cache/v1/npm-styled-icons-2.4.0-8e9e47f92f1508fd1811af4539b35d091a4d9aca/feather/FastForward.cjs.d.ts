import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FastForward: StyledIcon<any>;
export declare const FastForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
