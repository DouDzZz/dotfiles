"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Film = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Film-title" }, props.title), react_1.default.createElement("rect", { width: 20, height: 20, x: 2, y: 2, rx: 2.18, ry: 2.18, key: "k0" }),
            react_1.default.createElement("line", { x1: 7, x2: 7, y1: 2, y2: 22, key: "k1" }),
            react_1.default.createElement("line", { x1: 17, x2: 17, y1: 2, y2: 22, key: "k2" }),
            react_1.default.createElement("line", { x1: 2, x2: 22, y1: 12, y2: 12, key: "k3" }),
            react_1.default.createElement("line", { x1: 2, x2: 7, y1: 7, y2: 7, key: "k4" }),
            react_1.default.createElement("line", { x1: 2, x2: 7, y1: 17, y2: 17, key: "k5" }),
            react_1.default.createElement("line", { x1: 17, x2: 22, y1: 17, y2: 17, key: "k6" }),
            react_1.default.createElement("line", { x1: 17, x2: 22, y1: 7, y2: 7, key: "k7" })
        ]
        : [react_1.default.createElement("rect", { width: 20, height: 20, x: 2, y: 2, rx: 2.18, ry: 2.18, key: "k0" }),
            react_1.default.createElement("line", { x1: 7, x2: 7, y1: 2, y2: 22, key: "k1" }),
            react_1.default.createElement("line", { x1: 17, x2: 17, y1: 2, y2: 22, key: "k2" }),
            react_1.default.createElement("line", { x1: 2, x2: 22, y1: 12, y2: 12, key: "k3" }),
            react_1.default.createElement("line", { x1: 2, x2: 7, y1: 7, y2: 7, key: "k4" }),
            react_1.default.createElement("line", { x1: 2, x2: 7, y1: 17, y2: 17, key: "k5" }),
            react_1.default.createElement("line", { x1: 17, x2: 22, y1: 17, y2: 17, key: "k6" }),
            react_1.default.createElement("line", { x1: 17, x2: 22, y1: 7, y2: 7, key: "k7" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Film.displayName = 'Film';
exports.FilmDimensions = { height: 24, width: 24 };
var templateObject_1;
