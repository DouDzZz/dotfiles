import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Video: StyledIcon<any>;
export declare const VideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
