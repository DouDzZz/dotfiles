var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var TrendingUp = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "TrendingUp-title" }, props.title), React.createElement("polyline", { points: "23 6 13.5 15.5 8.5 10.5 1 18", key: "k0" }),
            React.createElement("polyline", { points: "17 6 23 6 23 12", key: "k1" })
        ]
        : [React.createElement("polyline", { points: "23 6 13.5 15.5 8.5 10.5 1 18", key: "k0" }),
            React.createElement("polyline", { points: "17 6 23 6 23 12", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
TrendingUp.displayName = 'TrendingUp';
export var TrendingUpDimensions = { height: 24, width: 24 };
var templateObject_1;
