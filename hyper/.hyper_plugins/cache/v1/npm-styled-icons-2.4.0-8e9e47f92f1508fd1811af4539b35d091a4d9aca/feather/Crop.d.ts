import { StyledIcon, StyledIconProps } from '..';
export declare const Crop: StyledIcon<any>;
export declare const CropDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
