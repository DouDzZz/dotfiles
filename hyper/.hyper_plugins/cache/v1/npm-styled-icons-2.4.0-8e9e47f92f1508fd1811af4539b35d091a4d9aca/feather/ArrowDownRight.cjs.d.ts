import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDownRight: StyledIcon<any>;
export declare const ArrowDownRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
