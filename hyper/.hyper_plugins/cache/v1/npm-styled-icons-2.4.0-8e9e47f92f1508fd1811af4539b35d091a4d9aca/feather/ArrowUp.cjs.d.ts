import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowUp: StyledIcon<any>;
export declare const ArrowUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
