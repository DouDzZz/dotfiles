import { StyledIcon, StyledIconProps } from '..';
export declare const TrendingDown: StyledIcon<any>;
export declare const TrendingDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
