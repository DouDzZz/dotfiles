import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryCharging: StyledIcon<any>;
export declare const BatteryChargingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
