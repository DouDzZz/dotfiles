import { StyledIcon, StyledIconProps } from '..';
export declare const Facebook: StyledIcon<any>;
export declare const FacebookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
