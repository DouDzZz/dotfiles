import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignLeft: StyledIcon<any>;
export declare const AlignLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
