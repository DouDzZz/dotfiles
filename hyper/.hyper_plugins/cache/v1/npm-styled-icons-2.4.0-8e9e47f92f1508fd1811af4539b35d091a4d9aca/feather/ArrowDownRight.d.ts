import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDownRight: StyledIcon<any>;
export declare const ArrowDownRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
