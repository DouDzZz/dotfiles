import { StyledIcon, StyledIconProps } from '..';
export declare const CornerLeftUp: StyledIcon<any>;
export declare const CornerLeftUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
