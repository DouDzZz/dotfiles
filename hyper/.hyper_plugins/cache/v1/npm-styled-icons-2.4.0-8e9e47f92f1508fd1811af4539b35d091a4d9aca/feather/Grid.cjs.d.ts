import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Grid: StyledIcon<any>;
export declare const GridDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
