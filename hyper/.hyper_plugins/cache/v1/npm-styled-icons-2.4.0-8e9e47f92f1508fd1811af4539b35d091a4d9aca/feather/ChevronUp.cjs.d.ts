import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronUp: StyledIcon<any>;
export declare const ChevronUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
