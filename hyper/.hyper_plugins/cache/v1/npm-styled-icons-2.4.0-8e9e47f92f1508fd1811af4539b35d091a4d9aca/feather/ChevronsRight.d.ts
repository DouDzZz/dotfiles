import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronsRight: StyledIcon<any>;
export declare const ChevronsRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
