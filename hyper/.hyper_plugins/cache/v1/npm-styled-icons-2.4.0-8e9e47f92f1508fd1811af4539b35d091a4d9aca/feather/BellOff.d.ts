import { StyledIcon, StyledIconProps } from '..';
export declare const BellOff: StyledIcon<any>;
export declare const BellOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
