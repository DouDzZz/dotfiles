var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Layers = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Layers-title" }, props.title), React.createElement("polygon", { points: "12 2 2 7 12 12 22 7 12 2", key: "k0" }),
            React.createElement("polyline", { points: "2 17 12 22 22 17", key: "k1" }),
            React.createElement("polyline", { points: "2 12 12 17 22 12", key: "k2" })
        ]
        : [React.createElement("polygon", { points: "12 2 2 7 12 12 22 7 12 2", key: "k0" }),
            React.createElement("polyline", { points: "2 17 12 22 22 17", key: "k1" }),
            React.createElement("polyline", { points: "2 12 12 17 22 12", key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Layers.displayName = 'Layers';
export var LayersDimensions = { height: 24, width: 24 };
var templateObject_1;
