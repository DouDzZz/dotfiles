import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronsDown: StyledIcon<any>;
export declare const ChevronsDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
