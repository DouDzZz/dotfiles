import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StopCircle: StyledIcon<any>;
export declare const StopCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
