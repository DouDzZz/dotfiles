import { StyledIcon, StyledIconProps } from '..';
export declare const Cpu: StyledIcon<any>;
export declare const CpuDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
