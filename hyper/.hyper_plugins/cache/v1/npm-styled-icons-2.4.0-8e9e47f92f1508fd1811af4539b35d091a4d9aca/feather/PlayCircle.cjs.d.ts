import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayCircle: StyledIcon<any>;
export declare const PlayCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
