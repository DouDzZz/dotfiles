import { StyledIcon, StyledIconProps } from '..';
export declare const Slack: StyledIcon<any>;
export declare const SlackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
