import { StyledIcon, StyledIconProps } from '..';
export declare const CloudRain: StyledIcon<any>;
export declare const CloudRainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
