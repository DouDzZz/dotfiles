import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerUpLeft: StyledIcon<any>;
export declare const CornerUpLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
