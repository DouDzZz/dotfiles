import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowLeftCircle: StyledIcon<any>;
export declare const ArrowLeftCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
