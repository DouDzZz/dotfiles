"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Move = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Move-title" }, props.title), react_1.default.createElement("polyline", { points: "5 9 2 12 5 15", key: "k0" }),
            react_1.default.createElement("polyline", { points: "9 5 12 2 15 5", key: "k1" }),
            react_1.default.createElement("polyline", { points: "15 19 12 22 9 19", key: "k2" }),
            react_1.default.createElement("polyline", { points: "19 9 22 12 19 15", key: "k3" }),
            react_1.default.createElement("line", { x1: 2, x2: 22, y1: 12, y2: 12, key: "k4" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 22, key: "k5" })
        ]
        : [react_1.default.createElement("polyline", { points: "5 9 2 12 5 15", key: "k0" }),
            react_1.default.createElement("polyline", { points: "9 5 12 2 15 5", key: "k1" }),
            react_1.default.createElement("polyline", { points: "15 19 12 22 9 19", key: "k2" }),
            react_1.default.createElement("polyline", { points: "19 9 22 12 19 15", key: "k3" }),
            react_1.default.createElement("line", { x1: 2, x2: 22, y1: 12, y2: 12, key: "k4" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 22, key: "k5" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Move.displayName = 'Move';
exports.MoveDimensions = { height: 24, width: 24 };
var templateObject_1;
