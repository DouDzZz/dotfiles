import { StyledIcon, StyledIconProps } from '..';
export declare const Speaker: StyledIcon<any>;
export declare const SpeakerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
