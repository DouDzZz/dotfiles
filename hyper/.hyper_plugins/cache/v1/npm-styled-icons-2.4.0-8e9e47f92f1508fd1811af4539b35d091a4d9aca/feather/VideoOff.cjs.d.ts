import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideoOff: StyledIcon<any>;
export declare const VideoOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
