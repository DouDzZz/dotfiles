import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Speaker: StyledIcon<any>;
export declare const SpeakerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
