import { StyledIcon, StyledIconProps } from '..';
export declare const Minimize: StyledIcon<any>;
export declare const MinimizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
