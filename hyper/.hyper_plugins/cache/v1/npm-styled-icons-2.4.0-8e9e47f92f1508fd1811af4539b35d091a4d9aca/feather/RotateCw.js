var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var RotateCw = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "RotateCw-title" }, props.title), React.createElement("polyline", { points: "23 4 23 10 17 10", key: "k0" }),
            React.createElement("path", { d: "M20.49 15a9 9 0 1 1-2.12-9.36L23 10", key: "k1" })
        ]
        : [React.createElement("polyline", { points: "23 4 23 10 17 10", key: "k0" }),
            React.createElement("path", { d: "M20.49 15a9 9 0 1 1-2.12-9.36L23 10", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
RotateCw.displayName = 'RotateCw';
export var RotateCwDimensions = { height: 24, width: 24 };
var templateObject_1;
