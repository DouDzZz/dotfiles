import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneIncoming: StyledIcon<any>;
export declare const PhoneIncomingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
