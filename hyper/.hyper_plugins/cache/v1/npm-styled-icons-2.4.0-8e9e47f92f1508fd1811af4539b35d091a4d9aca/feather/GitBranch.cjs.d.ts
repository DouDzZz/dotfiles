import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitBranch: StyledIcon<any>;
export declare const GitBranchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
