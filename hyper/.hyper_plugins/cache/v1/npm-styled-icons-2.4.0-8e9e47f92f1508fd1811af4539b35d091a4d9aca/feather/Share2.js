var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Share2 = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Share2-title" }, props.title), React.createElement("circle", { cx: 18, cy: 5, r: 3, key: "k0" }),
            React.createElement("circle", { cx: 6, cy: 12, r: 3, key: "k1" }),
            React.createElement("circle", { cx: 18, cy: 19, r: 3, key: "k2" }),
            React.createElement("line", { x1: 8.59, x2: 15.42, y1: 13.51, y2: 17.49, key: "k3" }),
            React.createElement("line", { x1: 15.41, x2: 8.59, y1: 6.51, y2: 10.49, key: "k4" })
        ]
        : [React.createElement("circle", { cx: 18, cy: 5, r: 3, key: "k0" }),
            React.createElement("circle", { cx: 6, cy: 12, r: 3, key: "k1" }),
            React.createElement("circle", { cx: 18, cy: 19, r: 3, key: "k2" }),
            React.createElement("line", { x1: 8.59, x2: 15.42, y1: 13.51, y2: 17.49, key: "k3" }),
            React.createElement("line", { x1: 15.41, x2: 8.59, y1: 6.51, y2: 10.49, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Share2.displayName = 'Share2';
export var Share2Dimensions = { height: 24, width: 24 };
var templateObject_1;
