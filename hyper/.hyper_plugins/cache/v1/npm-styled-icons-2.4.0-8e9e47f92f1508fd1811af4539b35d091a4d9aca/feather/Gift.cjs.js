"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Gift = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Gift-title" }, props.title), react_1.default.createElement("polyline", { points: "20 12 20 22 4 22 4 12", key: "k0" }),
            react_1.default.createElement("rect", { width: 20, height: 5, x: 2, y: 7, key: "k1" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 7, key: "k2" }),
            react_1.default.createElement("path", { d: "M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7zM12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z", key: "k3" })
        ]
        : [react_1.default.createElement("polyline", { points: "20 12 20 22 4 22 4 12", key: "k0" }),
            react_1.default.createElement("rect", { width: 20, height: 5, x: 2, y: 7, key: "k1" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 7, key: "k2" }),
            react_1.default.createElement("path", { d: "M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7zM12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z", key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Gift.displayName = 'Gift';
exports.GiftDimensions = { height: 24, width: 24 };
var templateObject_1;
