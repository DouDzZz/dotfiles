import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowLeftCircle: StyledIcon<any>;
export declare const ArrowLeftCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
