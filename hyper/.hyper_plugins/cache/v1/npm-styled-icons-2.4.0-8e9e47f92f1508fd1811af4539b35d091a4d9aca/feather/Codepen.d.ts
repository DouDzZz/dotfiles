import { StyledIcon, StyledIconProps } from '..';
export declare const Codepen: StyledIcon<any>;
export declare const CodepenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
