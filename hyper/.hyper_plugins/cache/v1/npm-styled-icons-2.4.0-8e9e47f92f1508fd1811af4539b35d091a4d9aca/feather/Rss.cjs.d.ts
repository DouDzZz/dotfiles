import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rss: StyledIcon<any>;
export declare const RssDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
