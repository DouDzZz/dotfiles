import { StyledIcon, StyledIconProps } from '..';
export declare const UserPlus: StyledIcon<any>;
export declare const UserPlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
