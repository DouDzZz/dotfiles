import { StyledIcon, StyledIconProps } from '..';
export declare const Slash: StyledIcon<any>;
export declare const SlashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
