import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Square: StyledIcon<any>;
export declare const SquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
