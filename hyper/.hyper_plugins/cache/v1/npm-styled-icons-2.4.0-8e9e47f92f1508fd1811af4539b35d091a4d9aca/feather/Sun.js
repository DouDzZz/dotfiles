var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Sun = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Sun-title" }, props.title), React.createElement("circle", { cx: 12, cy: 12, r: 5, key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 1, y2: 3, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 23, key: "k2" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 4.22, y2: 5.64, key: "k3" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 18.36, y2: 19.78, key: "k4" }),
            React.createElement("line", { x1: 1, x2: 3, y1: 12, y2: 12, key: "k5" }),
            React.createElement("line", { x1: 21, x2: 23, y1: 12, y2: 12, key: "k6" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 19.78, y2: 18.36, key: "k7" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 5.64, y2: 4.22, key: "k8" })
        ]
        : [React.createElement("circle", { cx: 12, cy: 12, r: 5, key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 1, y2: 3, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 23, key: "k2" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 4.22, y2: 5.64, key: "k3" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 18.36, y2: 19.78, key: "k4" }),
            React.createElement("line", { x1: 1, x2: 3, y1: 12, y2: 12, key: "k5" }),
            React.createElement("line", { x1: 21, x2: 23, y1: 12, y2: 12, key: "k6" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 19.78, y2: 18.36, key: "k7" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 5.64, y2: 4.22, key: "k8" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Sun.displayName = 'Sun';
export var SunDimensions = { height: 24, width: 24 };
var templateObject_1;
