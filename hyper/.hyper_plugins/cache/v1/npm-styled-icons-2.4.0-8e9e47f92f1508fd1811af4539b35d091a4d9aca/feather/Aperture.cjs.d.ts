import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Aperture: StyledIcon<any>;
export declare const ApertureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
