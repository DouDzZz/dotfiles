import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Zap: StyledIcon<any>;
export declare const ZapDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
