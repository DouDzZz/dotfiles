import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Activity: StyledIcon<any>;
export declare const ActivityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
