import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Trash2: StyledIcon<any>;
export declare const Trash2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
