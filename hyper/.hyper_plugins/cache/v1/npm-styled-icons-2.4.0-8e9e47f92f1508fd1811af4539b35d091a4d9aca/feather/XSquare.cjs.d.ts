import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const XSquare: StyledIcon<any>;
export declare const XSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
