import { StyledIcon, StyledIconProps } from '..';
export declare const Thermometer: StyledIcon<any>;
export declare const ThermometerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
