import { StyledIcon, StyledIconProps } from '..';
export declare const Maximize2: StyledIcon<any>;
export declare const Maximize2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
