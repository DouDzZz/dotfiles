import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronRight: StyledIcon<any>;
export declare const ChevronRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
