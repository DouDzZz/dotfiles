import { StyledIcon, StyledIconProps } from '..';
export declare const Activity: StyledIcon<any>;
export declare const ActivityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
