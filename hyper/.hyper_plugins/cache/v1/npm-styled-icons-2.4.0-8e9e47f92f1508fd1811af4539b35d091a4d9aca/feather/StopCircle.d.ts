import { StyledIcon, StyledIconProps } from '..';
export declare const StopCircle: StyledIcon<any>;
export declare const StopCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
