import { StyledIcon, StyledIconProps } from '..';
export declare const Sunset: StyledIcon<any>;
export declare const SunsetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
