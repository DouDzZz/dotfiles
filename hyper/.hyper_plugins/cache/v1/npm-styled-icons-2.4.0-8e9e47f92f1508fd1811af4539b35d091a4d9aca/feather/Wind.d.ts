import { StyledIcon, StyledIconProps } from '..';
export declare const Wind: StyledIcon<any>;
export declare const WindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
