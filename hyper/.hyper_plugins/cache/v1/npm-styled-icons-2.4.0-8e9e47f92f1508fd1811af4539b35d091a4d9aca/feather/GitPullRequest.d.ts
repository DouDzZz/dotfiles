import { StyledIcon, StyledIconProps } from '..';
export declare const GitPullRequest: StyledIcon<any>;
export declare const GitPullRequestDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
