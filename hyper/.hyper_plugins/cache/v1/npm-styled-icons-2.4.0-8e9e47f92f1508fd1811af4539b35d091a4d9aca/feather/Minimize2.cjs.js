"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Minimize2 = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Minimize2-title" }, props.title), react_1.default.createElement("polyline", { points: "4 14 10 14 10 20", key: "k0" }),
            react_1.default.createElement("polyline", { points: "20 10 14 10 14 4", key: "k1" }),
            react_1.default.createElement("line", { x1: 14, x2: 21, y1: 10, y2: 3, key: "k2" }),
            react_1.default.createElement("line", { x1: 3, x2: 10, y1: 21, y2: 14, key: "k3" })
        ]
        : [react_1.default.createElement("polyline", { points: "4 14 10 14 10 20", key: "k0" }),
            react_1.default.createElement("polyline", { points: "20 10 14 10 14 4", key: "k1" }),
            react_1.default.createElement("line", { x1: 14, x2: 21, y1: 10, y2: 3, key: "k2" }),
            react_1.default.createElement("line", { x1: 3, x2: 10, y1: 21, y2: 14, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Minimize2.displayName = 'Minimize2';
exports.Minimize2Dimensions = { height: 24, width: 24 };
var templateObject_1;
