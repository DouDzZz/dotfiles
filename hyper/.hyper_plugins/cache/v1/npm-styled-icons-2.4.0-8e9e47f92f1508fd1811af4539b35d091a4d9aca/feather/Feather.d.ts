import { StyledIcon, StyledIconProps } from '..';
export declare const Feather: StyledIcon<any>;
export declare const FeatherDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
