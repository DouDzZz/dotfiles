import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hash: StyledIcon<any>;
export declare const HashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
