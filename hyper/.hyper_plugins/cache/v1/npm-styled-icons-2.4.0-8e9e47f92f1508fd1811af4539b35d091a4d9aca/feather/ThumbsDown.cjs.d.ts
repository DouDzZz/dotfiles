import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbsDown: StyledIcon<any>;
export declare const ThumbsDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
