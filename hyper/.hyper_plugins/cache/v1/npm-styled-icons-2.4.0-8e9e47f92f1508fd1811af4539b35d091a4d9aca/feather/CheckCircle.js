var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var CheckCircle = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "CheckCircle-title" }, props.title), React.createElement("path", { d: "M22 11.08V12a10 10 0 1 1-5.93-9.14", key: "k0" }),
            React.createElement("polyline", { points: "22 4 12 14.01 9 11.01", key: "k1" })
        ]
        : [React.createElement("path", { d: "M22 11.08V12a10 10 0 1 1-5.93-9.14", key: "k0" }),
            React.createElement("polyline", { points: "22 4 12 14.01 9 11.01", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
CheckCircle.displayName = 'CheckCircle';
export var CheckCircleDimensions = { height: 24, width: 24 };
var templateObject_1;
