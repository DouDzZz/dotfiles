import { StyledIcon, StyledIconProps } from '..';
export declare const Layers: StyledIcon<any>;
export declare const LayersDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
