var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Minimize2 = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Minimize2-title" }, props.title), React.createElement("polyline", { points: "4 14 10 14 10 20", key: "k0" }),
            React.createElement("polyline", { points: "20 10 14 10 14 4", key: "k1" }),
            React.createElement("line", { x1: 14, x2: 21, y1: 10, y2: 3, key: "k2" }),
            React.createElement("line", { x1: 3, x2: 10, y1: 21, y2: 14, key: "k3" })
        ]
        : [React.createElement("polyline", { points: "4 14 10 14 10 20", key: "k0" }),
            React.createElement("polyline", { points: "20 10 14 10 14 4", key: "k1" }),
            React.createElement("line", { x1: 14, x2: 21, y1: 10, y2: 3, key: "k2" }),
            React.createElement("line", { x1: 3, x2: 10, y1: 21, y2: 14, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Minimize2.displayName = 'Minimize2';
export var Minimize2Dimensions = { height: 24, width: 24 };
var templateObject_1;
