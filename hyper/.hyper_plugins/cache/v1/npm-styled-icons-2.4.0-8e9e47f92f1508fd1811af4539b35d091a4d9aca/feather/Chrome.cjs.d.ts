import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Chrome: StyledIcon<any>;
export declare const ChromeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
