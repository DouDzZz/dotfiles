import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShoppingCart: StyledIcon<any>;
export declare const ShoppingCartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
