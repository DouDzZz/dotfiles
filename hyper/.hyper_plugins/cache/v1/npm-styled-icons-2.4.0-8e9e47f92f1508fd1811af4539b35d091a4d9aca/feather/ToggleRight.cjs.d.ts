import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ToggleRight: StyledIcon<any>;
export declare const ToggleRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
