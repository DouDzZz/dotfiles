"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Slack = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Slack-title" }, props.title), react_1.default.createElement("path", { d: "M22.08 9C19.81 1.41 16.54-.35 9 1.92S-.35 7.46 1.92 15 7.46 24.35 15 22.08 24.35 16.54 22.08 9z", key: "k0" }),
            react_1.default.createElement("line", { x1: 12.57, x2: 16.15, y1: 5.99, y2: 16.39, key: "k1" }),
            react_1.default.createElement("line", { x1: 7.85, x2: 11.43, y1: 7.61, y2: 18.01, key: "k2" }),
            react_1.default.createElement("line", { x1: 16.39, x2: 5.99, y1: 7.85, y2: 11.43, key: "k3" }),
            react_1.default.createElement("line", { x1: 18.01, x2: 7.61, y1: 12.57, y2: 16.15, key: "k4" })
        ]
        : [react_1.default.createElement("path", { d: "M22.08 9C19.81 1.41 16.54-.35 9 1.92S-.35 7.46 1.92 15 7.46 24.35 15 22.08 24.35 16.54 22.08 9z", key: "k0" }),
            react_1.default.createElement("line", { x1: 12.57, x2: 16.15, y1: 5.99, y2: 16.39, key: "k1" }),
            react_1.default.createElement("line", { x1: 7.85, x2: 11.43, y1: 7.61, y2: 18.01, key: "k2" }),
            react_1.default.createElement("line", { x1: 16.39, x2: 5.99, y1: 7.85, y2: 11.43, key: "k3" }),
            react_1.default.createElement("line", { x1: 18.01, x2: 7.61, y1: 12.57, y2: 16.15, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Slack.displayName = 'Slack';
exports.SlackDimensions = { height: 24, width: 24 };
var templateObject_1;
