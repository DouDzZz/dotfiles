import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MinusSquare: StyledIcon<any>;
export declare const MinusSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
