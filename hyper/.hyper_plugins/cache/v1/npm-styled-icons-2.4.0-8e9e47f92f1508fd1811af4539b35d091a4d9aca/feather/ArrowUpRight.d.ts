import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowUpRight: StyledIcon<any>;
export declare const ArrowUpRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
