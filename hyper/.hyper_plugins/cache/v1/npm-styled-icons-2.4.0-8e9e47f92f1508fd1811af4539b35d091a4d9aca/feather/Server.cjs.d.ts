import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Server: StyledIcon<any>;
export declare const ServerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
