import { StyledIcon, StyledIconProps } from '..';
export declare const Delete: StyledIcon<any>;
export declare const DeleteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
