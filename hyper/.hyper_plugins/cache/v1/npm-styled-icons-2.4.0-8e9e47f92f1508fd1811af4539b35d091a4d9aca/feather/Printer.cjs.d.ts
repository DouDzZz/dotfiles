import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Printer: StyledIcon<any>;
export declare const PrinterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
