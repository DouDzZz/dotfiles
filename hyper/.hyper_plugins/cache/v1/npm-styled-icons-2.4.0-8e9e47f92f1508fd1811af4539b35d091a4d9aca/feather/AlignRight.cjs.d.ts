import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignRight: StyledIcon<any>;
export declare const AlignRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
