import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeX: StyledIcon<any>;
export declare const VolumeXDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
