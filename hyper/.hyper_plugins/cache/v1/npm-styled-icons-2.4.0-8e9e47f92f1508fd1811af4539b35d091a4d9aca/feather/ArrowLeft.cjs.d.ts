import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowLeft: StyledIcon<any>;
export declare const ArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
