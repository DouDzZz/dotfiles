import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MapPin: StyledIcon<any>;
export declare const MapPinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
