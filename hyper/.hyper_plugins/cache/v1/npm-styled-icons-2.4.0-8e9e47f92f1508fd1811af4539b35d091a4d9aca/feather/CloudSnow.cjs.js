"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.CloudSnow = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "CloudSnow-title" }, props.title), react_1.default.createElement("path", { d: "M20 17.58A5 5 0 0 0 18 8h-1.26A8 8 0 1 0 4 16.25", key: "k0" }),
            react_1.default.createElement("line", { x1: 8, x2: 8, y1: 16, y2: 16, key: "k1" }),
            react_1.default.createElement("line", { x1: 8, x2: 8, y1: 20, y2: 20, key: "k2" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 18, key: "k3" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 22, key: "k4" }),
            react_1.default.createElement("line", { x1: 16, x2: 16, y1: 16, y2: 16, key: "k5" }),
            react_1.default.createElement("line", { x1: 16, x2: 16, y1: 20, y2: 20, key: "k6" })
        ]
        : [react_1.default.createElement("path", { d: "M20 17.58A5 5 0 0 0 18 8h-1.26A8 8 0 1 0 4 16.25", key: "k0" }),
            react_1.default.createElement("line", { x1: 8, x2: 8, y1: 16, y2: 16, key: "k1" }),
            react_1.default.createElement("line", { x1: 8, x2: 8, y1: 20, y2: 20, key: "k2" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 18, key: "k3" }),
            react_1.default.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 22, key: "k4" }),
            react_1.default.createElement("line", { x1: 16, x2: 16, y1: 16, y2: 16, key: "k5" }),
            react_1.default.createElement("line", { x1: 16, x2: 16, y1: 20, y2: 20, key: "k6" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.CloudSnow.displayName = 'CloudSnow';
exports.CloudSnowDimensions = { height: 24, width: 24 };
var templateObject_1;
