import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BookOpen: StyledIcon<any>;
export declare const BookOpenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
