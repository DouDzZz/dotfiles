import { StyledIcon, StyledIconProps } from '..';
export declare const AlertTriangle: StyledIcon<any>;
export declare const AlertTriangleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
