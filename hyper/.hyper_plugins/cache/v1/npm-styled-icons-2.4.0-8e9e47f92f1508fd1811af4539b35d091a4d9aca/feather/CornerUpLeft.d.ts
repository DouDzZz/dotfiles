import { StyledIcon, StyledIconProps } from '..';
export declare const CornerUpLeft: StyledIcon<any>;
export declare const CornerUpLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
