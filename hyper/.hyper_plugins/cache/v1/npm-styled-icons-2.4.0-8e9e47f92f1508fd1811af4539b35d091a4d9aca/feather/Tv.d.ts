import { StyledIcon, StyledIconProps } from '..';
export declare const Tv: StyledIcon<any>;
export declare const TvDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
