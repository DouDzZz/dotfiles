import { StyledIcon, StyledIconProps } from '..';
export declare const FileText: StyledIcon<any>;
export declare const FileTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
