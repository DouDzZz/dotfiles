import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Scissors: StyledIcon<any>;
export declare const ScissorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
