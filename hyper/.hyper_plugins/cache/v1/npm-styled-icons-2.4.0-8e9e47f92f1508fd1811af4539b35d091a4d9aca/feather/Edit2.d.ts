import { StyledIcon, StyledIconProps } from '..';
export declare const Edit2: StyledIcon<any>;
export declare const Edit2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
