import { StyledIcon, StyledIconProps } from '..';
export declare const Rewind: StyledIcon<any>;
export declare const RewindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
