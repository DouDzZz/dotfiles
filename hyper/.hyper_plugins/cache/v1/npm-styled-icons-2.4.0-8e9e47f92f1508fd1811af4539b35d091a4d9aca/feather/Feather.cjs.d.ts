import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Feather: StyledIcon<any>;
export declare const FeatherDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
