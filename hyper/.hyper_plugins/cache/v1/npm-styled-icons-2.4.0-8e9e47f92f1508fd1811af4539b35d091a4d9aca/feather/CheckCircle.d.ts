import { StyledIcon, StyledIconProps } from '..';
export declare const CheckCircle: StyledIcon<any>;
export declare const CheckCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
