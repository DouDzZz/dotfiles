var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Shuffle = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Shuffle-title" }, props.title), React.createElement("polyline", { points: "16 3 21 3 21 8", key: "k0" }),
            React.createElement("line", { x1: 4, x2: 21, y1: 20, y2: 3, key: "k1" }),
            React.createElement("polyline", { points: "21 16 21 21 16 21", key: "k2" }),
            React.createElement("line", { x1: 15, x2: 21, y1: 15, y2: 21, key: "k3" }),
            React.createElement("line", { x1: 4, x2: 9, y1: 4, y2: 9, key: "k4" })
        ]
        : [React.createElement("polyline", { points: "16 3 21 3 21 8", key: "k0" }),
            React.createElement("line", { x1: 4, x2: 21, y1: 20, y2: 3, key: "k1" }),
            React.createElement("polyline", { points: "21 16 21 21 16 21", key: "k2" }),
            React.createElement("line", { x1: 15, x2: 21, y1: 15, y2: 21, key: "k3" }),
            React.createElement("line", { x1: 4, x2: 9, y1: 4, y2: 9, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Shuffle.displayName = 'Shuffle';
export var ShuffleDimensions = { height: 24, width: 24 };
var templateObject_1;
