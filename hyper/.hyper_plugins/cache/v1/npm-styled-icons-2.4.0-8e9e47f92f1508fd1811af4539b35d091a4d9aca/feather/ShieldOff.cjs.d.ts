import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShieldOff: StyledIcon<any>;
export declare const ShieldOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
