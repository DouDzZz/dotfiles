import { StyledIcon, StyledIconProps } from '..';
export declare const Triangle: StyledIcon<any>;
export declare const TriangleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
