import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Type: StyledIcon<any>;
export declare const TypeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
