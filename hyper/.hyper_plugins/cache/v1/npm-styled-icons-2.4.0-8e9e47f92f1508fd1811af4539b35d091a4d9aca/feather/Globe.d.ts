import { StyledIcon, StyledIconProps } from '..';
export declare const Globe: StyledIcon<any>;
export declare const GlobeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
