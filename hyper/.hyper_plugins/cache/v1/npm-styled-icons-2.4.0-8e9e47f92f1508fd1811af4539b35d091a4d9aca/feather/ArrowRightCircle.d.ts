import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowRightCircle: StyledIcon<any>;
export declare const ArrowRightCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
