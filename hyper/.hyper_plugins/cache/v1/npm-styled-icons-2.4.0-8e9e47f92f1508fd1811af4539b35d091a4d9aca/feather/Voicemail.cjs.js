"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Voicemail = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Voicemail-title" }, props.title), react_1.default.createElement("circle", { cx: 5.5, cy: 11.5, r: 4.5, key: "k0" }),
            react_1.default.createElement("circle", { cx: 18.5, cy: 11.5, r: 4.5, key: "k1" }),
            react_1.default.createElement("line", { x1: 5.5, x2: 18.5, y1: 16, y2: 16, key: "k2" })
        ]
        : [react_1.default.createElement("circle", { cx: 5.5, cy: 11.5, r: 4.5, key: "k0" }),
            react_1.default.createElement("circle", { cx: 18.5, cy: 11.5, r: 4.5, key: "k1" }),
            react_1.default.createElement("line", { x1: 5.5, x2: 18.5, y1: 16, y2: 16, key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Voicemail.displayName = 'Voicemail';
exports.VoicemailDimensions = { height: 24, width: 24 };
var templateObject_1;
