import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pause: StyledIcon<any>;
export declare const PauseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
