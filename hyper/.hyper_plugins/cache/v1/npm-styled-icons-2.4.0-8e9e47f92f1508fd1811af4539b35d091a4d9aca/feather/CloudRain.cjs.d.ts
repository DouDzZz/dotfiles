import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudRain: StyledIcon<any>;
export declare const CloudRainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
