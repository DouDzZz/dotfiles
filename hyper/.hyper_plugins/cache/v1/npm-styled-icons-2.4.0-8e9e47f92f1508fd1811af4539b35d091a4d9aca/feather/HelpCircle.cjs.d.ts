import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HelpCircle: StyledIcon<any>;
export declare const HelpCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
