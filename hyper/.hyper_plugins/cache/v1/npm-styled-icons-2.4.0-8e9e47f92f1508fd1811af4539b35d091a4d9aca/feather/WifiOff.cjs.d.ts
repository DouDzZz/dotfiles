import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WifiOff: StyledIcon<any>;
export declare const WifiOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
