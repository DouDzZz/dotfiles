import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowUpCircle: StyledIcon<any>;
export declare const ArrowUpCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
