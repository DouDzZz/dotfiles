import { StyledIcon, StyledIconProps } from '..';
export declare const DollarSign: StyledIcon<any>;
export declare const DollarSignDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
