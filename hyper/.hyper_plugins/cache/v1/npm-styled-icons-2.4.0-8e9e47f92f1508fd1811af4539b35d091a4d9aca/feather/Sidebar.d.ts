import { StyledIcon, StyledIconProps } from '..';
export declare const Sidebar: StyledIcon<any>;
export declare const SidebarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
