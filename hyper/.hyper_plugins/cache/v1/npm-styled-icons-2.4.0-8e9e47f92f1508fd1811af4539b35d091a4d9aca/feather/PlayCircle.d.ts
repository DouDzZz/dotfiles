import { StyledIcon, StyledIconProps } from '..';
export declare const PlayCircle: StyledIcon<any>;
export declare const PlayCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
