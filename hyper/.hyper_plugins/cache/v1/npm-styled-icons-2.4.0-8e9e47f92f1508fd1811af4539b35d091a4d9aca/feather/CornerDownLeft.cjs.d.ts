import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerDownLeft: StyledIcon<any>;
export declare const CornerDownLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
