var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var RefreshCw = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "RefreshCw-title" }, props.title), React.createElement("polyline", { points: "23 4 23 10 17 10", key: "k0" }),
            React.createElement("polyline", { points: "1 20 1 14 7 14", key: "k1" }),
            React.createElement("path", { d: "M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15", key: "k2" })
        ]
        : [React.createElement("polyline", { points: "23 4 23 10 17 10", key: "k0" }),
            React.createElement("polyline", { points: "1 20 1 14 7 14", key: "k1" }),
            React.createElement("path", { d: "M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15", key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
RefreshCw.displayName = 'RefreshCw';
export var RefreshCwDimensions = { height: 24, width: 24 };
var templateObject_1;
