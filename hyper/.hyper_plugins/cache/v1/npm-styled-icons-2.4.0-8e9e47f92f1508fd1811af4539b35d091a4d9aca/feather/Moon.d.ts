import { StyledIcon, StyledIconProps } from '..';
export declare const Moon: StyledIcon<any>;
export declare const MoonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
