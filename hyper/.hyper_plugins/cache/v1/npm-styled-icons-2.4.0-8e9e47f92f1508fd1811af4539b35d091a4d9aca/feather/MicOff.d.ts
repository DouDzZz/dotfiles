import { StyledIcon, StyledIconProps } from '..';
export declare const MicOff: StyledIcon<any>;
export declare const MicOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
