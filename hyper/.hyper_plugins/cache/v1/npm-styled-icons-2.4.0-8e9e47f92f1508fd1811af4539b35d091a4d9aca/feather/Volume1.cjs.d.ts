import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Volume1: StyledIcon<any>;
export declare const Volume1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
