import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowUpLeft: StyledIcon<any>;
export declare const ArrowUpLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
