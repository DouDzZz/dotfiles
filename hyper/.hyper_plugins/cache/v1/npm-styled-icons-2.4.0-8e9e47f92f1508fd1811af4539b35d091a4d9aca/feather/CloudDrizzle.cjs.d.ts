import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudDrizzle: StyledIcon<any>;
export declare const CloudDrizzleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
