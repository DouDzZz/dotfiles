import { StyledIcon, StyledIconProps } from '..';
export declare const GitBranch: StyledIcon<any>;
export declare const GitBranchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
