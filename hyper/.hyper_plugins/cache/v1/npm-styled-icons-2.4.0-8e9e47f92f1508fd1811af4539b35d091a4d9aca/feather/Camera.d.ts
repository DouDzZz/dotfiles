import { StyledIcon, StyledIconProps } from '..';
export declare const Camera: StyledIcon<any>;
export declare const CameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
