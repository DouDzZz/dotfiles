import { StyledIcon, StyledIconProps } from '..';
export declare const Paperclip: StyledIcon<any>;
export declare const PaperclipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
