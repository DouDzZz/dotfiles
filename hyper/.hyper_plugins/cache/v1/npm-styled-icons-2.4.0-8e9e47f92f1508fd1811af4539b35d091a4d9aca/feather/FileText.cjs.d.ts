import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileText: StyledIcon<any>;
export declare const FileTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
