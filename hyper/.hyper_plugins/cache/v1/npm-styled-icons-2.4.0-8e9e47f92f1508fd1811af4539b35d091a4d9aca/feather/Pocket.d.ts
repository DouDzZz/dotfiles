import { StyledIcon, StyledIconProps } from '..';
export declare const Pocket: StyledIcon<any>;
export declare const PocketDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
