import { StyledIcon, StyledIconProps } from '..';
export declare const Anchor: StyledIcon<any>;
export declare const AnchorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
