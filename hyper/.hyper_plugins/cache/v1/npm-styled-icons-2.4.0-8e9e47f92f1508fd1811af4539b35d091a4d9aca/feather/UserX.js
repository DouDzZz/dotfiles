var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var UserX = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "UserX-title" }, props.title), React.createElement("path", { d: "M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2", key: "k0" }),
            React.createElement("circle", { cx: 8.5, cy: 7, r: 4, key: "k1" }),
            React.createElement("line", { x1: 18, x2: 23, y1: 8, y2: 13, key: "k2" }),
            React.createElement("line", { x1: 23, x2: 18, y1: 8, y2: 13, key: "k3" })
        ]
        : [React.createElement("path", { d: "M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2", key: "k0" }),
            React.createElement("circle", { cx: 8.5, cy: 7, r: 4, key: "k1" }),
            React.createElement("line", { x1: 18, x2: 23, y1: 8, y2: 13, key: "k2" }),
            React.createElement("line", { x1: 23, x2: 18, y1: 8, y2: 13, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
UserX.displayName = 'UserX';
export var UserXDimensions = { height: 24, width: 24 };
var templateObject_1;
