import { StyledIcon, StyledIconProps } from '..';
export declare const Circle: StyledIcon<any>;
export declare const CircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
