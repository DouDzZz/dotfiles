import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerUpRight: StyledIcon<any>;
export declare const CornerUpRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
