import { StyledIcon, StyledIconProps } from '..';
export declare const Repeat: StyledIcon<any>;
export declare const RepeatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
