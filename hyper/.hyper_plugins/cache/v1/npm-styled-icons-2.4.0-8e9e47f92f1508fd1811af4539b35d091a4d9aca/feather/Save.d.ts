import { StyledIcon, StyledIconProps } from '..';
export declare const Save: StyledIcon<any>;
export declare const SaveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
