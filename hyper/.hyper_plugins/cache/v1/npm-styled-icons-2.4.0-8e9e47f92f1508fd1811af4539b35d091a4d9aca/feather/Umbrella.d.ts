import { StyledIcon, StyledIconProps } from '..';
export declare const Umbrella: StyledIcon<any>;
export declare const UmbrellaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
