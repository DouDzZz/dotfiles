import { StyledIcon, StyledIconProps } from '..';
export declare const Droplet: StyledIcon<any>;
export declare const DropletDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
