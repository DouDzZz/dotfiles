import { StyledIcon, StyledIconProps } from '..';
export declare const CheckSquare: StyledIcon<any>;
export declare const CheckSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
