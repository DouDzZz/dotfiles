import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Edit3: StyledIcon<any>;
export declare const Edit3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
