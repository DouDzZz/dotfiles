import { StyledIcon, StyledIconProps } from '..';
export declare const Archive: StyledIcon<any>;
export declare const ArchiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
