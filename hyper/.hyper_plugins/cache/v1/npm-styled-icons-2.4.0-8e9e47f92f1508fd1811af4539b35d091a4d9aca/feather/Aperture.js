var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Aperture = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Aperture-title" }, props.title), React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("line", { x1: 14.31, x2: 20.05, y1: 8, y2: 17.94, key: "k1" }),
            React.createElement("line", { x1: 9.69, x2: 21.17, y1: 8, y2: 8, key: "k2" }),
            React.createElement("line", { x1: 7.38, x2: 13.12, y1: 12, y2: 2.06, key: "k3" }),
            React.createElement("line", { x1: 9.69, x2: 3.95, y1: 16, y2: 6.06, key: "k4" }),
            React.createElement("line", { x1: 14.31, x2: 2.83, y1: 16, y2: 16, key: "k5" }),
            React.createElement("line", { x1: 16.62, x2: 10.88, y1: 12, y2: 21.94, key: "k6" })
        ]
        : [React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("line", { x1: 14.31, x2: 20.05, y1: 8, y2: 17.94, key: "k1" }),
            React.createElement("line", { x1: 9.69, x2: 21.17, y1: 8, y2: 8, key: "k2" }),
            React.createElement("line", { x1: 7.38, x2: 13.12, y1: 12, y2: 2.06, key: "k3" }),
            React.createElement("line", { x1: 9.69, x2: 3.95, y1: 16, y2: 6.06, key: "k4" }),
            React.createElement("line", { x1: 14.31, x2: 2.83, y1: 16, y2: 16, key: "k5" }),
            React.createElement("line", { x1: 16.62, x2: 10.88, y1: 12, y2: 21.94, key: "k6" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Aperture.displayName = 'Aperture';
export var ApertureDimensions = { height: 24, width: 24 };
var templateObject_1;
