import { StyledIcon, StyledIconProps } from '..';
export declare const UserCheck: StyledIcon<any>;
export declare const UserCheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
