import { StyledIcon, StyledIconProps } from '..';
export declare const ZoomOut: StyledIcon<any>;
export declare const ZoomOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
