import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paperclip: StyledIcon<any>;
export declare const PaperclipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
