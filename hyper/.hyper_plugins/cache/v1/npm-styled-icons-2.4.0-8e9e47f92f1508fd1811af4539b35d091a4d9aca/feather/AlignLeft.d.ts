import { StyledIcon, StyledIconProps } from '..';
export declare const AlignLeft: StyledIcon<any>;
export declare const AlignLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
