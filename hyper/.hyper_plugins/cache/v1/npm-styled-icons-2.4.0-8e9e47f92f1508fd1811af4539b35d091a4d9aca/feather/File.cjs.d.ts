import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const File: StyledIcon<any>;
export declare const FileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
