import { StyledIcon, StyledIconProps } from '..';
export declare const Loader: StyledIcon<any>;
export declare const LoaderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
