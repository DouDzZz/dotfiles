import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShoppingBag: StyledIcon<any>;
export declare const ShoppingBagDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
