import { StyledIcon, StyledIconProps } from '..';
export declare const Volume1: StyledIcon<any>;
export declare const Volume1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
