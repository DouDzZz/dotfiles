var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Type = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Type-title" }, props.title), React.createElement("polyline", { points: "4 7 4 4 20 4 20 7", key: "k0" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 20, y2: 20, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 4, y2: 20, key: "k2" })
        ]
        : [React.createElement("polyline", { points: "4 7 4 4 20 4 20 7", key: "k0" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 20, y2: 20, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 4, y2: 20, key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Type.displayName = 'Type';
export var TypeDimensions = { height: 24, width: 24 };
var templateObject_1;
