import { StyledIcon, StyledIconProps } from '..';
export declare const RefreshCw: StyledIcon<any>;
export declare const RefreshCwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
