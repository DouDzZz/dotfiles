import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Camera: StyledIcon<any>;
export declare const CameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
