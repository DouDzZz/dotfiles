import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wind: StyledIcon<any>;
export declare const WindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
