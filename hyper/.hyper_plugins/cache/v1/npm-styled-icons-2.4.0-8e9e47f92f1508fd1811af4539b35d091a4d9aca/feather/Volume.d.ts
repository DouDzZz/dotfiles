import { StyledIcon, StyledIconProps } from '..';
export declare const Volume: StyledIcon<any>;
export declare const VolumeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
