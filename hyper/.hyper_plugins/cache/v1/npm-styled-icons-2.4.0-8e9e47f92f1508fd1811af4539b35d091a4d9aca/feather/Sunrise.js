var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Sunrise = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Sunrise-title" }, props.title), React.createElement("path", { d: "M17 18a5 5 0 0 0-10 0", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 9, key: "k1" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 10.22, y2: 11.64, key: "k2" }),
            React.createElement("line", { x1: 1, x2: 3, y1: 18, y2: 18, key: "k3" }),
            React.createElement("line", { x1: 21, x2: 23, y1: 18, y2: 18, key: "k4" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 11.64, y2: 10.22, key: "k5" }),
            React.createElement("line", { x1: 23, x2: 1, y1: 22, y2: 22, key: "k6" }),
            React.createElement("polyline", { points: "8 6 12 2 16 6", key: "k7" })
        ]
        : [React.createElement("path", { d: "M17 18a5 5 0 0 0-10 0", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 9, key: "k1" }),
            React.createElement("line", { x1: 4.22, x2: 5.64, y1: 10.22, y2: 11.64, key: "k2" }),
            React.createElement("line", { x1: 1, x2: 3, y1: 18, y2: 18, key: "k3" }),
            React.createElement("line", { x1: 21, x2: 23, y1: 18, y2: 18, key: "k4" }),
            React.createElement("line", { x1: 18.36, x2: 19.78, y1: 11.64, y2: 10.22, key: "k5" }),
            React.createElement("line", { x1: 23, x2: 1, y1: 22, y2: 22, key: "k6" }),
            React.createElement("polyline", { points: "8 6 12 2 16 6", key: "k7" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Sunrise.displayName = 'Sunrise';
export var SunriseDimensions = { height: 24, width: 24 };
var templateObject_1;
