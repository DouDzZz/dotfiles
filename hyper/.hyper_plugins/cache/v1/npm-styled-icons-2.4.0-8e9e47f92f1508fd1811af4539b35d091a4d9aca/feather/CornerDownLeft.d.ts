import { StyledIcon, StyledIconProps } from '..';
export declare const CornerDownLeft: StyledIcon<any>;
export declare const CornerDownLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
