import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronsLeft: StyledIcon<any>;
export declare const ChevronsLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
