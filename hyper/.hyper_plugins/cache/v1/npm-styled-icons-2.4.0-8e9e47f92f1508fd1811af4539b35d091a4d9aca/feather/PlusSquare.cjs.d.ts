import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlusSquare: StyledIcon<any>;
export declare const PlusSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
