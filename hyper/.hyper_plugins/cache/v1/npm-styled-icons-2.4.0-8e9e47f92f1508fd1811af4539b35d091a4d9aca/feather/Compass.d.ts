import { StyledIcon, StyledIconProps } from '..';
export declare const Compass: StyledIcon<any>;
export declare const CompassDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
