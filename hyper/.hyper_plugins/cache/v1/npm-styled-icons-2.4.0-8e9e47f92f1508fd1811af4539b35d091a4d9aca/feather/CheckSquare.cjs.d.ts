import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckSquare: StyledIcon<any>;
export declare const CheckSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
