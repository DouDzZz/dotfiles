import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneIncoming: StyledIcon<any>;
export declare const PhoneIncomingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
