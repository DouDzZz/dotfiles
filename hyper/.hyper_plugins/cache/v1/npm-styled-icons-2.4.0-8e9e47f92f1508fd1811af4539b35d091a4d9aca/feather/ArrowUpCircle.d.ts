import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowUpCircle: StyledIcon<any>;
export declare const ArrowUpCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
