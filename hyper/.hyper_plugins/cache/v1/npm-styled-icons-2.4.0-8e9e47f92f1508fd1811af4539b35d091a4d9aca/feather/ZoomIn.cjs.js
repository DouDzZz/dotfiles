"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.ZoomIn = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "ZoomIn-title" }, props.title), react_1.default.createElement("circle", { cx: 11, cy: 11, r: 8, key: "k0" }),
            react_1.default.createElement("line", { x1: 21, x2: 16.65, y1: 21, y2: 16.65, key: "k1" }),
            react_1.default.createElement("line", { x1: 11, x2: 11, y1: 8, y2: 14, key: "k2" }),
            react_1.default.createElement("line", { x1: 8, x2: 14, y1: 11, y2: 11, key: "k3" })
        ]
        : [react_1.default.createElement("circle", { cx: 11, cy: 11, r: 8, key: "k0" }),
            react_1.default.createElement("line", { x1: 21, x2: 16.65, y1: 21, y2: 16.65, key: "k1" }),
            react_1.default.createElement("line", { x1: 11, x2: 11, y1: 8, y2: 14, key: "k2" }),
            react_1.default.createElement("line", { x1: 8, x2: 14, y1: 11, y2: 11, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.ZoomIn.displayName = 'ZoomIn';
exports.ZoomInDimensions = { height: 24, width: 24 };
var templateObject_1;
