import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Youtube: StyledIcon<any>;
export declare const YoutubeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
