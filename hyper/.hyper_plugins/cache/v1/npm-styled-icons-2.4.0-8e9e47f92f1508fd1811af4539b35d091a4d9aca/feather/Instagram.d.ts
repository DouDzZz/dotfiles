import { StyledIcon, StyledIconProps } from '..';
export declare const Instagram: StyledIcon<any>;
export declare const InstagramDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
