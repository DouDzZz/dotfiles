import { StyledIcon, StyledIconProps } from '..';
export declare const FileMinus: StyledIcon<any>;
export declare const FileMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
