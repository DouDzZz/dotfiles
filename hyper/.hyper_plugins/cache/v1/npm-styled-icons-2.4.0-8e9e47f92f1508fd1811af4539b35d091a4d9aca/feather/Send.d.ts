import { StyledIcon, StyledIconProps } from '..';
export declare const Send: StyledIcon<any>;
export declare const SendDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
