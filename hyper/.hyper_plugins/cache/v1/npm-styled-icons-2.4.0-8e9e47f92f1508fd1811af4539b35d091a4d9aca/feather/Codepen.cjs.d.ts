import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Codepen: StyledIcon<any>;
export declare const CodepenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
