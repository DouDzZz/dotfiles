import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lock: StyledIcon<any>;
export declare const LockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
