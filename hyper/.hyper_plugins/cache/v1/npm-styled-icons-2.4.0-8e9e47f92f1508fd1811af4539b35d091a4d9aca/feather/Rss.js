var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Rss = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Rss-title" }, props.title), React.createElement("path", { d: "M4 11a9 9 0 0 1 9 9M4 4a16 16 0 0 1 16 16", key: "k0" }),
            React.createElement("circle", { cx: 5, cy: 19, r: 1, key: "k1" })
        ]
        : [React.createElement("path", { d: "M4 11a9 9 0 0 1 9 9M4 4a16 16 0 0 1 16 16", key: "k0" }),
            React.createElement("circle", { cx: 5, cy: 19, r: 1, key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Rss.displayName = 'Rss';
export var RssDimensions = { height: 24, width: 24 };
var templateObject_1;
