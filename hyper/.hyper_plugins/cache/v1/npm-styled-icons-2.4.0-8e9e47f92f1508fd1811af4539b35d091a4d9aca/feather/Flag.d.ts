import { StyledIcon, StyledIconProps } from '..';
export declare const Flag: StyledIcon<any>;
export declare const FlagDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
