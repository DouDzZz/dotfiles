import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BarChart: StyledIcon<any>;
export declare const BarChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
