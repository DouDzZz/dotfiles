import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pocket: StyledIcon<any>;
export declare const PocketDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
