var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Loader = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Loader-title" }, props.title), React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 6, key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 22, key: "k1" }),
            React.createElement("line", { x1: 4.93, x2: 7.76, y1: 4.93, y2: 7.76, key: "k2" }),
            React.createElement("line", { x1: 16.24, x2: 19.07, y1: 16.24, y2: 19.07, key: "k3" }),
            React.createElement("line", { x1: 2, x2: 6, y1: 12, y2: 12, key: "k4" }),
            React.createElement("line", { x1: 18, x2: 22, y1: 12, y2: 12, key: "k5" }),
            React.createElement("line", { x1: 4.93, x2: 7.76, y1: 19.07, y2: 16.24, key: "k6" }),
            React.createElement("line", { x1: 16.24, x2: 19.07, y1: 7.76, y2: 4.93, key: "k7" })
        ]
        : [React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 6, key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 22, key: "k1" }),
            React.createElement("line", { x1: 4.93, x2: 7.76, y1: 4.93, y2: 7.76, key: "k2" }),
            React.createElement("line", { x1: 16.24, x2: 19.07, y1: 16.24, y2: 19.07, key: "k3" }),
            React.createElement("line", { x1: 2, x2: 6, y1: 12, y2: 12, key: "k4" }),
            React.createElement("line", { x1: 18, x2: 22, y1: 12, y2: 12, key: "k5" }),
            React.createElement("line", { x1: 4.93, x2: 7.76, y1: 19.07, y2: 16.24, key: "k6" }),
            React.createElement("line", { x1: 16.24, x2: 19.07, y1: 7.76, y2: 4.93, key: "k7" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Loader.displayName = 'Loader';
export var LoaderDimensions = { height: 24, width: 24 };
var templateObject_1;
