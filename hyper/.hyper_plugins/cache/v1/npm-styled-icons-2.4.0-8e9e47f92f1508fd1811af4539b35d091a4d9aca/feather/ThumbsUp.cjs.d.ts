import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbsUp: StyledIcon<any>;
export declare const ThumbsUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
