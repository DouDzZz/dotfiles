var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var FilePlus = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "FilePlus-title" }, props.title), React.createElement("path", { d: "M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z", key: "k0" }),
            React.createElement("polyline", { points: "14 2 14 8 20 8", key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 15, y2: 15, key: "k3" })
        ]
        : [React.createElement("path", { d: "M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z", key: "k0" }),
            React.createElement("polyline", { points: "14 2 14 8 20 8", key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 18, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 15, y2: 15, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
FilePlus.displayName = 'FilePlus';
export var FilePlusDimensions = { height: 24, width: 24 };
var templateObject_1;
