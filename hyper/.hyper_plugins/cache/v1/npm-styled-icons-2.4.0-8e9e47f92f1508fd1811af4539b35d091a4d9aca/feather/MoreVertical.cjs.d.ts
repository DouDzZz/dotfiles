import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoreVertical: StyledIcon<any>;
export declare const MoreVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
