import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronsUp: StyledIcon<any>;
export declare const ChevronsUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
