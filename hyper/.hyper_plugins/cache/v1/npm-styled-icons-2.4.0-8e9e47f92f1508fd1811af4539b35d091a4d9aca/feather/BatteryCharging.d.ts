import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryCharging: StyledIcon<any>;
export declare const BatteryChargingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
