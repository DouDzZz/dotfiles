import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneMissed: StyledIcon<any>;
export declare const PhoneMissedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
