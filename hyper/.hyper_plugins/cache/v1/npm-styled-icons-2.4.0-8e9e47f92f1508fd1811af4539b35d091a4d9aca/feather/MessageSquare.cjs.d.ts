import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MessageSquare: StyledIcon<any>;
export declare const MessageSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
