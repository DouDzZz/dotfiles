import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronsDown: StyledIcon<any>;
export declare const ChevronsDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
