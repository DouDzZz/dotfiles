import { StyledIcon, StyledIconProps } from '..';
export declare const Scissors: StyledIcon<any>;
export declare const ScissorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
