import { StyledIcon, StyledIconProps } from '..';
export declare const FolderMinus: StyledIcon<any>;
export declare const FolderMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
