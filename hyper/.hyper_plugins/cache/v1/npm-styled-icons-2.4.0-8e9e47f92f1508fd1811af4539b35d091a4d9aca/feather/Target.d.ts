import { StyledIcon, StyledIconProps } from '..';
export declare const Target: StyledIcon<any>;
export declare const TargetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
