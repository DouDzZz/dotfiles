import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Award: StyledIcon<any>;
export declare const AwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
