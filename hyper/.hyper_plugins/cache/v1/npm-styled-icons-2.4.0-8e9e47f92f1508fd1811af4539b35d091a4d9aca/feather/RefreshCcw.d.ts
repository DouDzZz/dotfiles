import { StyledIcon, StyledIconProps } from '..';
export declare const RefreshCcw: StyledIcon<any>;
export declare const RefreshCcwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
