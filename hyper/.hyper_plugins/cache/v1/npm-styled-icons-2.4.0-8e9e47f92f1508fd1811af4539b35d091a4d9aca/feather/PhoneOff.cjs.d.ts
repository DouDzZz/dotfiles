import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneOff: StyledIcon<any>;
export declare const PhoneOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
