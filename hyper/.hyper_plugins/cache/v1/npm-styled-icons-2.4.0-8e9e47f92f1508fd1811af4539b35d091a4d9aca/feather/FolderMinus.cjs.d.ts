import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FolderMinus: StyledIcon<any>;
export declare const FolderMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
