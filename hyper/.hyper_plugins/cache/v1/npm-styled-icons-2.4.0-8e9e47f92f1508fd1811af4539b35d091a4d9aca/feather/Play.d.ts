import { StyledIcon, StyledIconProps } from '..';
export declare const Play: StyledIcon<any>;
export declare const PlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
