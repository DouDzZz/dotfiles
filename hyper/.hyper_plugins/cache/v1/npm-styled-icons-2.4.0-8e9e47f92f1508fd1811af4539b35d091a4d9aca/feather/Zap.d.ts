import { StyledIcon, StyledIconProps } from '..';
export declare const Zap: StyledIcon<any>;
export declare const ZapDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
