var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Sliders = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Sliders-title" }, props.title), React.createElement("line", { x1: 4, x2: 4, y1: 21, y2: 14, key: "k0" }),
            React.createElement("line", { x1: 4, x2: 4, y1: 10, y2: 3, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 8, y2: 3, key: "k3" }),
            React.createElement("line", { x1: 20, x2: 20, y1: 21, y2: 16, key: "k4" }),
            React.createElement("line", { x1: 20, x2: 20, y1: 12, y2: 3, key: "k5" }),
            React.createElement("line", { x1: 1, x2: 7, y1: 14, y2: 14, key: "k6" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 8, y2: 8, key: "k7" }),
            React.createElement("line", { x1: 17, x2: 23, y1: 16, y2: 16, key: "k8" })
        ]
        : [React.createElement("line", { x1: 4, x2: 4, y1: 21, y2: 14, key: "k0" }),
            React.createElement("line", { x1: 4, x2: 4, y1: 10, y2: 3, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 8, y2: 3, key: "k3" }),
            React.createElement("line", { x1: 20, x2: 20, y1: 21, y2: 16, key: "k4" }),
            React.createElement("line", { x1: 20, x2: 20, y1: 12, y2: 3, key: "k5" }),
            React.createElement("line", { x1: 1, x2: 7, y1: 14, y2: 14, key: "k6" }),
            React.createElement("line", { x1: 9, x2: 15, y1: 8, y2: 8, key: "k7" }),
            React.createElement("line", { x1: 17, x2: 23, y1: 16, y2: 16, key: "k8" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Sliders.displayName = 'Sliders';
export var SlidersDimensions = { height: 24, width: 24 };
var templateObject_1;
