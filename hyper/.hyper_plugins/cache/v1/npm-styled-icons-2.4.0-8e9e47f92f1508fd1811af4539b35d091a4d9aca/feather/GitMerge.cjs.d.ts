import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitMerge: StyledIcon<any>;
export declare const GitMergeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
