import { StyledIcon, StyledIconProps } from '..';
export declare const Aperture: StyledIcon<any>;
export declare const ApertureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
