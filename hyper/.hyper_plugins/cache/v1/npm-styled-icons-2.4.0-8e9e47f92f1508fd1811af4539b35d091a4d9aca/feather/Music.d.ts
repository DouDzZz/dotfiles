import { StyledIcon, StyledIconProps } from '..';
export declare const Music: StyledIcon<any>;
export declare const MusicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
