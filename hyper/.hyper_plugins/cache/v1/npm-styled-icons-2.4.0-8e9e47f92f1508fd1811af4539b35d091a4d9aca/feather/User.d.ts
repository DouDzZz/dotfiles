import { StyledIcon, StyledIconProps } from '..';
export declare const User: StyledIcon<any>;
export declare const UserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
