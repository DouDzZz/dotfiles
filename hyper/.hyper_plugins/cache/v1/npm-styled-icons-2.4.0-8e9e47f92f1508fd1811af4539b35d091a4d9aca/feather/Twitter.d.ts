import { StyledIcon, StyledIconProps } from '..';
export declare const Twitter: StyledIcon<any>;
export declare const TwitterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
