import { StyledIcon, StyledIconProps } from '..';
export declare const MessageSquare: StyledIcon<any>;
export declare const MessageSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
