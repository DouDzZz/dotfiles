import { StyledIcon, StyledIconProps } from '..';
export declare const Navigation2: StyledIcon<any>;
export declare const Navigation2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
