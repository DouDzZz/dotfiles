import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Book: StyledIcon<any>;
export declare const BookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
