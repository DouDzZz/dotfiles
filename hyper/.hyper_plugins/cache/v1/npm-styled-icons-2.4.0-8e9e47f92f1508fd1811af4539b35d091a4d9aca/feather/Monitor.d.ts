import { StyledIcon, StyledIconProps } from '..';
export declare const Monitor: StyledIcon<any>;
export declare const MonitorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
