import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneMissed: StyledIcon<any>;
export declare const PhoneMissedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
