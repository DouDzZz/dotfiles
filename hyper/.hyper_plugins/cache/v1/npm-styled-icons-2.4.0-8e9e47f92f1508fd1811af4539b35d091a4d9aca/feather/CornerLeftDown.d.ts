import { StyledIcon, StyledIconProps } from '..';
export declare const CornerLeftDown: StyledIcon<any>;
export declare const CornerLeftDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
