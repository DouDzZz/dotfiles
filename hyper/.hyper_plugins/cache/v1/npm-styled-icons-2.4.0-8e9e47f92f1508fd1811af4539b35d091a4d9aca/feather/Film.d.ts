import { StyledIcon, StyledIconProps } from '..';
export declare const Film: StyledIcon<any>;
export declare const FilmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
