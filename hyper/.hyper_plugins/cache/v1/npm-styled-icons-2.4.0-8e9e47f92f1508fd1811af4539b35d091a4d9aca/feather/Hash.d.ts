import { StyledIcon, StyledIconProps } from '..';
export declare const Hash: StyledIcon<any>;
export declare const HashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
