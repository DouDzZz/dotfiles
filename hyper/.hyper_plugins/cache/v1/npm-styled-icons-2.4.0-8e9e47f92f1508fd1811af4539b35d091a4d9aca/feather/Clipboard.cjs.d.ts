import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Clipboard: StyledIcon<any>;
export declare const ClipboardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
