import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDown: StyledIcon<any>;
export declare const ArrowDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
