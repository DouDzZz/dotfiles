import { StyledIcon, StyledIconProps } from '..';
export declare const Crosshair: StyledIcon<any>;
export declare const CrosshairDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
