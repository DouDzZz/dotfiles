import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LogIn: StyledIcon<any>;
export declare const LogInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
