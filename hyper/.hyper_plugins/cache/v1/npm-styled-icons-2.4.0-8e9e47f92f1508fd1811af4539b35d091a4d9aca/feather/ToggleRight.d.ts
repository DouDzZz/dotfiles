import { StyledIcon, StyledIconProps } from '..';
export declare const ToggleRight: StyledIcon<any>;
export declare const ToggleRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
