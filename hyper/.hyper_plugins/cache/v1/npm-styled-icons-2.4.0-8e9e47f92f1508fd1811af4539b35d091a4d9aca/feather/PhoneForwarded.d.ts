import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneForwarded: StyledIcon<any>;
export declare const PhoneForwardedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
