import { StyledIcon, StyledIconProps } from '..';
export declare const Minus: StyledIcon<any>;
export declare const MinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
