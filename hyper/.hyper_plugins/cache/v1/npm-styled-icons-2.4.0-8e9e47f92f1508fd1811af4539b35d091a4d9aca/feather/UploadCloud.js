var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var UploadCloud = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "UploadCloud-title" }, props.title), React.createElement("polyline", { points: "16 16 12 12 8 16", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 12, y2: 21, key: "k1" }),
            React.createElement("path", { d: "M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3", key: "k2" }),
            React.createElement("polyline", { points: "16 16 12 12 8 16", key: "k3" })
        ]
        : [React.createElement("polyline", { points: "16 16 12 12 8 16", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 12, y2: 21, key: "k1" }),
            React.createElement("path", { d: "M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3", key: "k2" }),
            React.createElement("polyline", { points: "16 16 12 12 8 16", key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
UploadCloud.displayName = 'UploadCloud';
export var UploadCloudDimensions = { height: 24, width: 24 };
var templateObject_1;
