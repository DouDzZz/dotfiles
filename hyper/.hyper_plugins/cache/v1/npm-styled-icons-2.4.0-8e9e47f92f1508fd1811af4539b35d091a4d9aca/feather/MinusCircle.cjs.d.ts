import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MinusCircle: StyledIcon<any>;
export declare const MinusCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
