import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Terminal: StyledIcon<any>;
export declare const TerminalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
