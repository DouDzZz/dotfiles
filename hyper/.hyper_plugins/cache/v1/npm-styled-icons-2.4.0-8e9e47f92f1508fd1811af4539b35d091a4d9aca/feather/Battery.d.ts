import { StyledIcon, StyledIconProps } from '..';
export declare const Battery: StyledIcon<any>;
export declare const BatteryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
