var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Cpu = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Cpu-title" }, props.title), React.createElement("rect", { width: 16, height: 16, x: 4, y: 4, rx: 2, ry: 2, key: "k0" }),
            React.createElement("rect", { width: 6, height: 6, x: 9, y: 9, key: "k1" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 1, y2: 4, key: "k2" }),
            React.createElement("line", { x1: 15, x2: 15, y1: 1, y2: 4, key: "k3" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 20, y2: 23, key: "k4" }),
            React.createElement("line", { x1: 15, x2: 15, y1: 20, y2: 23, key: "k5" }),
            React.createElement("line", { x1: 20, x2: 23, y1: 9, y2: 9, key: "k6" }),
            React.createElement("line", { x1: 20, x2: 23, y1: 14, y2: 14, key: "k7" }),
            React.createElement("line", { x1: 1, x2: 4, y1: 9, y2: 9, key: "k8" }),
            React.createElement("line", { x1: 1, x2: 4, y1: 14, y2: 14, key: "k9" })
        ]
        : [React.createElement("rect", { width: 16, height: 16, x: 4, y: 4, rx: 2, ry: 2, key: "k0" }),
            React.createElement("rect", { width: 6, height: 6, x: 9, y: 9, key: "k1" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 1, y2: 4, key: "k2" }),
            React.createElement("line", { x1: 15, x2: 15, y1: 1, y2: 4, key: "k3" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 20, y2: 23, key: "k4" }),
            React.createElement("line", { x1: 15, x2: 15, y1: 20, y2: 23, key: "k5" }),
            React.createElement("line", { x1: 20, x2: 23, y1: 9, y2: 9, key: "k6" }),
            React.createElement("line", { x1: 20, x2: 23, y1: 14, y2: 14, key: "k7" }),
            React.createElement("line", { x1: 1, x2: 4, y1: 9, y2: 9, key: "k8" }),
            React.createElement("line", { x1: 1, x2: 4, y1: 14, y2: 14, key: "k9" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Cpu.displayName = 'Cpu';
export var CpuDimensions = { height: 24, width: 24 };
var templateObject_1;
