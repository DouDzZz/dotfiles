import { StyledIcon, StyledIconProps } from '..';
export declare const XSquare: StyledIcon<any>;
export declare const XSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
