import { StyledIcon, StyledIconProps } from '..';
export declare const ShoppingCart: StyledIcon<any>;
export declare const ShoppingCartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
