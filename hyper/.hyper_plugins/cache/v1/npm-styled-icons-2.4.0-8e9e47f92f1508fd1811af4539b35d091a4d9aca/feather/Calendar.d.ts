import { StyledIcon, StyledIconProps } from '..';
export declare const Calendar: StyledIcon<any>;
export declare const CalendarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
