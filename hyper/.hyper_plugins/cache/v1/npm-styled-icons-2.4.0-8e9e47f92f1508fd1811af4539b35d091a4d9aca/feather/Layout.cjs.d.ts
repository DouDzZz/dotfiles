import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Layout: StyledIcon<any>;
export declare const LayoutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
