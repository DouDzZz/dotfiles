import { StyledIcon, StyledIconProps } from '..';
export declare const CloudSnow: StyledIcon<any>;
export declare const CloudSnowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
