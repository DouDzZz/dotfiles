import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Instagram: StyledIcon<any>;
export declare const InstagramDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
