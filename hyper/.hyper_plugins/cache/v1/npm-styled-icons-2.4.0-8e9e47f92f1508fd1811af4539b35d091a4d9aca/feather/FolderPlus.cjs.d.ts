import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FolderPlus: StyledIcon<any>;
export declare const FolderPlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
