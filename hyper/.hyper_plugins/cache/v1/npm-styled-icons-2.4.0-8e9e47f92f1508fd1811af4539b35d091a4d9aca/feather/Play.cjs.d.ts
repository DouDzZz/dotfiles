import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Play: StyledIcon<any>;
export declare const PlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
