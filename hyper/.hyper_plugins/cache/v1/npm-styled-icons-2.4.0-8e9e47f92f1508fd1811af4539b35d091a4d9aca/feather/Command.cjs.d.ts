import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Command: StyledIcon<any>;
export declare const CommandDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
