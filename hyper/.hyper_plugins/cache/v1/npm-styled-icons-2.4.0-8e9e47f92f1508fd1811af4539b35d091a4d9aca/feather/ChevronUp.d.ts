import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronUp: StyledIcon<any>;
export declare const ChevronUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
