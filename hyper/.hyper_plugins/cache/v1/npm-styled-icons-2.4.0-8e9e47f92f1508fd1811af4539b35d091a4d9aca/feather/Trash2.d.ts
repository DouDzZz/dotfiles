import { StyledIcon, StyledIconProps } from '..';
export declare const Trash2: StyledIcon<any>;
export declare const Trash2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
