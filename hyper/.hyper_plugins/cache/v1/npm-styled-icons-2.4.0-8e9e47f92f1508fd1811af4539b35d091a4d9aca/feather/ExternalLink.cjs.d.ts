import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExternalLink: StyledIcon<any>;
export declare const ExternalLinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
