import { StyledIcon, StyledIconProps } from '..';
export declare const Box: StyledIcon<any>;
export declare const BoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
