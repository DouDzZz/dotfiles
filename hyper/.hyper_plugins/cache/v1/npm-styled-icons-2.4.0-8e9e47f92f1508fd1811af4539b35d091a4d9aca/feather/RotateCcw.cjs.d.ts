import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RotateCcw: StyledIcon<any>;
export declare const RotateCcwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
