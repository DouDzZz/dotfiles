var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var RotateCcw = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "RotateCcw-title" }, props.title), React.createElement("polyline", { points: "1 4 1 10 7 10", key: "k0" }),
            React.createElement("path", { d: "M3.51 15a9 9 0 1 0 2.13-9.36L1 10", key: "k1" })
        ]
        : [React.createElement("polyline", { points: "1 4 1 10 7 10", key: "k0" }),
            React.createElement("path", { d: "M3.51 15a9 9 0 1 0 2.13-9.36L1 10", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
RotateCcw.displayName = 'RotateCcw';
export var RotateCcwDimensions = { height: 24, width: 24 };
var templateObject_1;
