import { StyledIcon, StyledIconProps } from '..';
export declare const Sunrise: StyledIcon<any>;
export declare const SunriseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
