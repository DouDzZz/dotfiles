import { StyledIcon, StyledIconProps } from '..';
export declare const EyeOff: StyledIcon<any>;
export declare const EyeOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
