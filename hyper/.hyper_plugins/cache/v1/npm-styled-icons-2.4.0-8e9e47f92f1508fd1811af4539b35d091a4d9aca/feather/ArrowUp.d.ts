import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowUp: StyledIcon<any>;
export declare const ArrowUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
