import { StyledIcon, StyledIconProps } from '..';
export declare const CameraOff: StyledIcon<any>;
export declare const CameraOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
