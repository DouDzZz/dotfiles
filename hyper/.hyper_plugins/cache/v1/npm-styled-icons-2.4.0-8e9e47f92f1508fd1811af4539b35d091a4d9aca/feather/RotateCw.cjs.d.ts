import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RotateCw: StyledIcon<any>;
export declare const RotateCwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
