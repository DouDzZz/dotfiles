import { StyledIcon, StyledIconProps } from '..';
export declare const SkipForward: StyledIcon<any>;
export declare const SkipForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
