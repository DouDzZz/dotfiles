import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerLeftDown: StyledIcon<any>;
export declare const CornerLeftDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
