import { StyledIcon, StyledIconProps } from '..';
export declare const Folder: StyledIcon<any>;
export declare const FolderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
