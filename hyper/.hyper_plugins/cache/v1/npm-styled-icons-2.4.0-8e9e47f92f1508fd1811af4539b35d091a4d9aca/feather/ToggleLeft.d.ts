import { StyledIcon, StyledIconProps } from '..';
export declare const ToggleLeft: StyledIcon<any>;
export declare const ToggleLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
