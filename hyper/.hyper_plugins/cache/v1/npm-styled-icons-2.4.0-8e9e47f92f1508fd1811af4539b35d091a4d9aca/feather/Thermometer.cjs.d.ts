import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Thermometer: StyledIcon<any>;
export declare const ThermometerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
