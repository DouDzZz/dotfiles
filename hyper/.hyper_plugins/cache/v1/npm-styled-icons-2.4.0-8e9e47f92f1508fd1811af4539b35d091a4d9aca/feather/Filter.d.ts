import { StyledIcon, StyledIconProps } from '..';
export declare const Filter: StyledIcon<any>;
export declare const FilterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
