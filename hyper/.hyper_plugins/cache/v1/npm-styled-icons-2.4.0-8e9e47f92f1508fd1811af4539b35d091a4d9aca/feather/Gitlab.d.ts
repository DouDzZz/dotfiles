import { StyledIcon, StyledIconProps } from '..';
export declare const Gitlab: StyledIcon<any>;
export declare const GitlabDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
