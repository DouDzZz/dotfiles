"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Italic = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Italic-title" }, props.title), react_1.default.createElement("line", { x1: 19, x2: 10, y1: 4, y2: 4, key: "k0" }),
            react_1.default.createElement("line", { x1: 14, x2: 5, y1: 20, y2: 20, key: "k1" }),
            react_1.default.createElement("line", { x1: 15, x2: 9, y1: 4, y2: 20, key: "k2" })
        ]
        : [react_1.default.createElement("line", { x1: 19, x2: 10, y1: 4, y2: 4, key: "k0" }),
            react_1.default.createElement("line", { x1: 14, x2: 5, y1: 20, y2: 20, key: "k1" }),
            react_1.default.createElement("line", { x1: 15, x2: 9, y1: 4, y2: 20, key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Italic.displayName = 'Italic';
exports.ItalicDimensions = { height: 24, width: 24 };
var templateObject_1;
