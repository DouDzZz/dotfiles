import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronLeft: StyledIcon<any>;
export declare const ChevronLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
