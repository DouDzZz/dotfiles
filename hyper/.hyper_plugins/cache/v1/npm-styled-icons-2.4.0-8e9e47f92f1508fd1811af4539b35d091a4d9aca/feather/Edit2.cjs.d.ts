import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Edit2: StyledIcon<any>;
export declare const Edit2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
