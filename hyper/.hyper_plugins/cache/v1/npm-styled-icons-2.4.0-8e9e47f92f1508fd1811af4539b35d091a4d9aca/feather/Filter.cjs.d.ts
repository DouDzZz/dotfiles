import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter: StyledIcon<any>;
export declare const FilterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
