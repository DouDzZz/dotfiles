import { StyledIcon, StyledIconProps } from '..';
export declare const Menu: StyledIcon<any>;
export declare const MenuDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
