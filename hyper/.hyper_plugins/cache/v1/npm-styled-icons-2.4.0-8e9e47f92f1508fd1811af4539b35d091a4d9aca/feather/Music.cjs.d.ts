import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Music: StyledIcon<any>;
export declare const MusicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
