import { StyledIcon, StyledIconProps } from '..';
export declare const AlertCircle: StyledIcon<any>;
export declare const AlertCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
