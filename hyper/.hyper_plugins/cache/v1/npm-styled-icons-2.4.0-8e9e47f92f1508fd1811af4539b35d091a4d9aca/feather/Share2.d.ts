import { StyledIcon, StyledIconProps } from '..';
export declare const Share2: StyledIcon<any>;
export declare const Share2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
