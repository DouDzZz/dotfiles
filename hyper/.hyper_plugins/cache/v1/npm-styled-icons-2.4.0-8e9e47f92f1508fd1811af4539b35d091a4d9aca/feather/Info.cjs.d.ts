import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Info: StyledIcon<any>;
export declare const InfoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
