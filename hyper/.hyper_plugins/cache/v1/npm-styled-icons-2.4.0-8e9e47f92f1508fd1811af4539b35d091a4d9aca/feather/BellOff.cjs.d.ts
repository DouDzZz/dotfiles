import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BellOff: StyledIcon<any>;
export declare const BellOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
