import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Triangle: StyledIcon<any>;
export declare const TriangleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
