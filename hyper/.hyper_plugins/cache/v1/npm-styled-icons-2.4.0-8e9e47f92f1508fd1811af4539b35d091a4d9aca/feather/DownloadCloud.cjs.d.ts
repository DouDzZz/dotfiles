import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DownloadCloud: StyledIcon<any>;
export declare const DownloadCloudDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
