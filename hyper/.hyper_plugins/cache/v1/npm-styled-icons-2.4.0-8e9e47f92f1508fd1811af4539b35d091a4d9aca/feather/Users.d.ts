import { StyledIcon, StyledIconProps } from '..';
export declare const Users: StyledIcon<any>;
export declare const UsersDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
