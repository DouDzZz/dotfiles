import { StyledIcon, StyledIconProps } from '..';
export declare const ShieldOff: StyledIcon<any>;
export declare const ShieldOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
