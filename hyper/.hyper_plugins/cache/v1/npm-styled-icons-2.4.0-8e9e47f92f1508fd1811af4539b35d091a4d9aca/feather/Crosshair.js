var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Crosshair = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Crosshair-title" }, props.title), React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("line", { x1: 22, x2: 18, y1: 12, y2: 12, key: "k1" }),
            React.createElement("line", { x1: 6, x2: 2, y1: 12, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 6, y2: 2, key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 18, key: "k4" })
        ]
        : [React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("line", { x1: 22, x2: 18, y1: 12, y2: 12, key: "k1" }),
            React.createElement("line", { x1: 6, x2: 2, y1: 12, y2: 12, key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 6, y2: 2, key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 18, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Crosshair.displayName = 'Crosshair';
export var CrosshairDimensions = { height: 24, width: 24 };
var templateObject_1;
