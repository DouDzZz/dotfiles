import { StyledIcon, StyledIconProps } from '..';
export declare const Award: StyledIcon<any>;
export declare const AwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
