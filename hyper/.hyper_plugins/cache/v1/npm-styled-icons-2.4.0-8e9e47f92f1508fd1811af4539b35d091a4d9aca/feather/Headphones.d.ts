import { StyledIcon, StyledIconProps } from '..';
export declare const Headphones: StyledIcon<any>;
export declare const HeadphonesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
