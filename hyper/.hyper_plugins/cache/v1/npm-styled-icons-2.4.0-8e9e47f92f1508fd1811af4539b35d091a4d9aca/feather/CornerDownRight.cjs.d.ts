import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerDownRight: StyledIcon<any>;
export declare const CornerDownRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
