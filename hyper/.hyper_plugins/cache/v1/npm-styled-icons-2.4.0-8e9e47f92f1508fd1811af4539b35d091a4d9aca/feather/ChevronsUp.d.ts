import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronsUp: StyledIcon<any>;
export declare const ChevronsUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
