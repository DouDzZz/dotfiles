import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlertOctagon: StyledIcon<any>;
export declare const AlertOctagonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
