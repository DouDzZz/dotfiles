import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckCircle: StyledIcon<any>;
export declare const CheckCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
