import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Link: StyledIcon<any>;
export declare const LinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
