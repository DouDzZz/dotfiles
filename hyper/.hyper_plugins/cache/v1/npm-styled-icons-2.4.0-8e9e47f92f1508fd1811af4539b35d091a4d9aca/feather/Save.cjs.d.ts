import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Save: StyledIcon<any>;
export declare const SaveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
