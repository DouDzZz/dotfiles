import { StyledIcon, StyledIconProps } from '..';
export declare const Clipboard: StyledIcon<any>;
export declare const ClipboardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
