var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var CloudDrizzle = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "CloudDrizzle-title" }, props.title), React.createElement("line", { x1: 8, x2: 8, y1: 19, y2: 21, key: "k0" }),
            React.createElement("line", { x1: 8, x2: 8, y1: 13, y2: 15, key: "k1" }),
            React.createElement("line", { x1: 16, x2: 16, y1: 19, y2: 21, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 16, y1: 13, y2: 15, key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 23, key: "k4" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 15, y2: 17, key: "k5" }),
            React.createElement("path", { d: "M20 16.58A5 5 0 0 0 18 7h-1.26A8 8 0 1 0 4 15.25", key: "k6" })
        ]
        : [React.createElement("line", { x1: 8, x2: 8, y1: 19, y2: 21, key: "k0" }),
            React.createElement("line", { x1: 8, x2: 8, y1: 13, y2: 15, key: "k1" }),
            React.createElement("line", { x1: 16, x2: 16, y1: 19, y2: 21, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 16, y1: 13, y2: 15, key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 21, y2: 23, key: "k4" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 15, y2: 17, key: "k5" }),
            React.createElement("path", { d: "M20 16.58A5 5 0 0 0 18 7h-1.26A8 8 0 1 0 4 15.25", key: "k6" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
CloudDrizzle.displayName = 'CloudDrizzle';
export var CloudDrizzleDimensions = { height: 24, width: 24 };
var templateObject_1;
