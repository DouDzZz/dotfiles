import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Octagon: StyledIcon<any>;
export declare const OctagonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
