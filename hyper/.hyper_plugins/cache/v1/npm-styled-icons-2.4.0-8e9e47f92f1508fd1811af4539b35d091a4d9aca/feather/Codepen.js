var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Codepen = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Codepen-title" }, props.title), React.createElement("polygon", { points: "12 2 22 8.5 22 15.5 12 22 2 15.5 2 8.5 12 2", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 15.5, key: "k1" }),
            React.createElement("polyline", { points: "22 8.5 12 15.5 2 8.5", key: "k2" }),
            React.createElement("polyline", { points: "2 15.5 12 8.5 22 15.5", key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 8.5, key: "k4" })
        ]
        : [React.createElement("polygon", { points: "12 2 22 8.5 22 15.5 12 22 2 15.5 2 8.5 12 2", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 22, y2: 15.5, key: "k1" }),
            React.createElement("polyline", { points: "22 8.5 12 15.5 2 8.5", key: "k2" }),
            React.createElement("polyline", { points: "2 15.5 12 8.5 22 15.5", key: "k3" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 2, y2: 8.5, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Codepen.displayName = 'Codepen';
export var CodepenDimensions = { height: 24, width: 24 };
var templateObject_1;
