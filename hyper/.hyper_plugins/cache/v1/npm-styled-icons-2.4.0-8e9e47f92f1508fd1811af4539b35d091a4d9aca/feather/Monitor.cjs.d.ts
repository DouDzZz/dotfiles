import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Monitor: StyledIcon<any>;
export declare const MonitorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
