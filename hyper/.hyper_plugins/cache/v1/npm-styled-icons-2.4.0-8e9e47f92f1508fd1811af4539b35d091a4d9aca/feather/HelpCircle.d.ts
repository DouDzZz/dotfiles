import { StyledIcon, StyledIconProps } from '..';
export declare const HelpCircle: StyledIcon<any>;
export declare const HelpCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
