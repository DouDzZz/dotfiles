import { StyledIcon, StyledIconProps } from '..';
export declare const CloudOff: StyledIcon<any>;
export declare const CloudOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
