import { StyledIcon, StyledIconProps } from '..';
export declare const UserX: StyledIcon<any>;
export declare const UserXDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
