import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ToggleLeft: StyledIcon<any>;
export declare const ToggleLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
