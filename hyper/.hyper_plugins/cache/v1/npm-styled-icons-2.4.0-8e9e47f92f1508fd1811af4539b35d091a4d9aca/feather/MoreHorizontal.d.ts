import { StyledIcon, StyledIconProps } from '..';
export declare const MoreHorizontal: StyledIcon<any>;
export declare const MoreHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
