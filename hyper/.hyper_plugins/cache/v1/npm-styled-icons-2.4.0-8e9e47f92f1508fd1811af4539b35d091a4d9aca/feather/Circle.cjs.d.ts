import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Circle: StyledIcon<any>;
export declare const CircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
