var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Trash2 = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Trash2-title" }, props.title), React.createElement("polyline", { points: "3 6 5 6 21 6", key: "k0" }),
            React.createElement("path", { d: "M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2", key: "k1" }),
            React.createElement("line", { x1: 10, x2: 10, y1: 11, y2: 17, key: "k2" }),
            React.createElement("line", { x1: 14, x2: 14, y1: 11, y2: 17, key: "k3" })
        ]
        : [React.createElement("polyline", { points: "3 6 5 6 21 6", key: "k0" }),
            React.createElement("path", { d: "M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2", key: "k1" }),
            React.createElement("line", { x1: 10, x2: 10, y1: 11, y2: 17, key: "k2" }),
            React.createElement("line", { x1: 14, x2: 14, y1: 11, y2: 17, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Trash2.displayName = 'Trash2';
export var Trash2Dimensions = { height: 24, width: 24 };
var templateObject_1;
