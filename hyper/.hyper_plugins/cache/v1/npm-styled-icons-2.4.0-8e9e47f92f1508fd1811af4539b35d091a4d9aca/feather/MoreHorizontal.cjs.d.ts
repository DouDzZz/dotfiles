import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoreHorizontal: StyledIcon<any>;
export declare const MoreHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
