import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Home: StyledIcon<any>;
export declare const HomeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
