import { StyledIcon, StyledIconProps } from '..';
export declare const Disc: StyledIcon<any>;
export declare const DiscDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
