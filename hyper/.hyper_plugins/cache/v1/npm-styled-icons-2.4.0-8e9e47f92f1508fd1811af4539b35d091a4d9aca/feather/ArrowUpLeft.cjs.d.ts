import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowUpLeft: StyledIcon<any>;
export declare const ArrowUpLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
