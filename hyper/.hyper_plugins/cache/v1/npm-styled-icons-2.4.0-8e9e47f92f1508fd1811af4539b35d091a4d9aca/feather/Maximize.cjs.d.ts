import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Maximize: StyledIcon<any>;
export declare const MaximizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
