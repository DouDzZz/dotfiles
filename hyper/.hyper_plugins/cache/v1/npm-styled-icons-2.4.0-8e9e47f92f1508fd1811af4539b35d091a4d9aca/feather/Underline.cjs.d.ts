import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Underline: StyledIcon<any>;
export declare const UnderlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
