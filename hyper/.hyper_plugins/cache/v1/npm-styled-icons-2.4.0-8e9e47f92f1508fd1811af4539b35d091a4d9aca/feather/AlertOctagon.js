var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var AlertOctagon = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "AlertOctagon-title" }, props.title), React.createElement("polygon", { points: "7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 8, y2: 12, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 16, y2: 16, key: "k2" })
        ]
        : [React.createElement("polygon", { points: "7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2", key: "k0" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 8, y2: 12, key: "k1" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 16, y2: 16, key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
AlertOctagon.displayName = 'AlertOctagon';
export var AlertOctagonDimensions = { height: 24, width: 24 };
var templateObject_1;
