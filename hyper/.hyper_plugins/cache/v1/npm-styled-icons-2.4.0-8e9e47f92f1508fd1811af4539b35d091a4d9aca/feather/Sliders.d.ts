import { StyledIcon, StyledIconProps } from '..';
export declare const Sliders: StyledIcon<any>;
export declare const SlidersDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
