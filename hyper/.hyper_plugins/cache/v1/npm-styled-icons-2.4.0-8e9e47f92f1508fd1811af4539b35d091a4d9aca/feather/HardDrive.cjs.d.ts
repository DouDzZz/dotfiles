import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HardDrive: StyledIcon<any>;
export declare const HardDriveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
