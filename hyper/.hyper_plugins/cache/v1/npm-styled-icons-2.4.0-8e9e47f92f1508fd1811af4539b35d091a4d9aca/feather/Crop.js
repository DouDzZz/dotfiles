var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Crop = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Crop-title" }, props.title), React.createElement("path", { d: "M6.13 1L6 16a2 2 0 0 0 2 2h15", key: "k0" }),
            React.createElement("path", { d: "M1 6.13L16 6a2 2 0 0 1 2 2v15", key: "k1" })
        ]
        : [React.createElement("path", { d: "M6.13 1L6 16a2 2 0 0 0 2 2h15", key: "k0" }),
            React.createElement("path", { d: "M1 6.13L16 6a2 2 0 0 1 2 2v15", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Crop.displayName = 'Crop';
export var CropDimensions = { height: 24, width: 24 };
var templateObject_1;
