"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.ZapOff = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "ZapOff-title" }, props.title), react_1.default.createElement("polyline", { points: "12.41 6.75 13 2 10.57 4.92", key: "k0" }),
            react_1.default.createElement("polyline", { points: "18.57 12.91 21 10 15.66 10", key: "k1" }),
            react_1.default.createElement("polyline", { points: "8 8 3 14 12 14 11 22 16 16", key: "k2" }),
            react_1.default.createElement("line", { x1: 1, x2: 23, y1: 1, y2: 23, key: "k3" })
        ]
        : [react_1.default.createElement("polyline", { points: "12.41 6.75 13 2 10.57 4.92", key: "k0" }),
            react_1.default.createElement("polyline", { points: "18.57 12.91 21 10 15.66 10", key: "k1" }),
            react_1.default.createElement("polyline", { points: "8 8 3 14 12 14 11 22 16 16", key: "k2" }),
            react_1.default.createElement("line", { x1: 1, x2: 23, y1: 1, y2: 23, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.ZapOff.displayName = 'ZapOff';
exports.ZapOffDimensions = { height: 24, width: 24 };
var templateObject_1;
