var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var LifeBuoy = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "LifeBuoy-title" }, props.title), React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("circle", { cx: 12, cy: 12, r: 4, key: "k1" }),
            React.createElement("line", { x1: 4.93, x2: 9.17, y1: 4.93, y2: 9.17, key: "k2" }),
            React.createElement("line", { x1: 14.83, x2: 19.07, y1: 14.83, y2: 19.07, key: "k3" }),
            React.createElement("line", { x1: 14.83, x2: 19.07, y1: 9.17, y2: 4.93, key: "k4" }),
            React.createElement("line", { x1: 14.83, x2: 18.36, y1: 9.17, y2: 5.64, key: "k5" }),
            React.createElement("line", { x1: 4.93, x2: 9.17, y1: 19.07, y2: 14.83, key: "k6" })
        ]
        : [React.createElement("circle", { cx: 12, cy: 12, r: 10, key: "k0" }),
            React.createElement("circle", { cx: 12, cy: 12, r: 4, key: "k1" }),
            React.createElement("line", { x1: 4.93, x2: 9.17, y1: 4.93, y2: 9.17, key: "k2" }),
            React.createElement("line", { x1: 14.83, x2: 19.07, y1: 14.83, y2: 19.07, key: "k3" }),
            React.createElement("line", { x1: 14.83, x2: 19.07, y1: 9.17, y2: 4.93, key: "k4" }),
            React.createElement("line", { x1: 14.83, x2: 18.36, y1: 9.17, y2: 5.64, key: "k5" }),
            React.createElement("line", { x1: 4.93, x2: 9.17, y1: 19.07, y2: 14.83, key: "k6" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
LifeBuoy.displayName = 'LifeBuoy';
export var LifeBuoyDimensions = { height: 24, width: 24 };
var templateObject_1;
