import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Move: StyledIcon<any>;
export declare const MoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
