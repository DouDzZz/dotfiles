import { StyledIcon, StyledIconProps } from '..';
export declare const Eye: StyledIcon<any>;
export declare const EyeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
