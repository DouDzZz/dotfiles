import { StyledIcon, StyledIconProps } from '..';
export declare const CloudDrizzle: StyledIcon<any>;
export declare const CloudDrizzleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
