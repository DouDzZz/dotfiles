import { StyledIcon, StyledIconProps } from '..';
export declare const CloudLightning: StyledIcon<any>;
export declare const CloudLightningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
