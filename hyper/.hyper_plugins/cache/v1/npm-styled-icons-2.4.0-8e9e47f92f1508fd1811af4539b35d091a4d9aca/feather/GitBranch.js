var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var GitBranch = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "GitBranch-title" }, props.title), React.createElement("line", { x1: 6, x2: 6, y1: 3, y2: 15, key: "k0" }),
            React.createElement("circle", { cx: 18, cy: 6, r: 3, key: "k1" }),
            React.createElement("circle", { cx: 6, cy: 18, r: 3, key: "k2" }),
            React.createElement("path", { d: "M18 9a9 9 0 0 1-9 9", key: "k3" })
        ]
        : [React.createElement("line", { x1: 6, x2: 6, y1: 3, y2: 15, key: "k0" }),
            React.createElement("circle", { cx: 18, cy: 6, r: 3, key: "k1" }),
            React.createElement("circle", { cx: 6, cy: 18, r: 3, key: "k2" }),
            React.createElement("path", { d: "M18 9a9 9 0 0 1-9 9", key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
GitBranch.displayName = 'GitBranch';
export var GitBranchDimensions = { height: 24, width: 24 };
var templateObject_1;
