var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Hash = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Hash-title" }, props.title), React.createElement("line", { x1: 4, x2: 20, y1: 9, y2: 9, key: "k0" }),
            React.createElement("line", { x1: 4, x2: 20, y1: 15, y2: 15, key: "k1" }),
            React.createElement("line", { x1: 10, x2: 8, y1: 3, y2: 21, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 14, y1: 3, y2: 21, key: "k3" })
        ]
        : [React.createElement("line", { x1: 4, x2: 20, y1: 9, y2: 9, key: "k0" }),
            React.createElement("line", { x1: 4, x2: 20, y1: 15, y2: 15, key: "k1" }),
            React.createElement("line", { x1: 10, x2: 8, y1: 3, y2: 21, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 14, y1: 3, y2: 21, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Hash.displayName = 'Hash';
export var HashDimensions = { height: 24, width: 24 };
var templateObject_1;
