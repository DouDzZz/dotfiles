import { StyledIcon, StyledIconProps } from '..';
export declare const BarChart: StyledIcon<any>;
export declare const BarChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
