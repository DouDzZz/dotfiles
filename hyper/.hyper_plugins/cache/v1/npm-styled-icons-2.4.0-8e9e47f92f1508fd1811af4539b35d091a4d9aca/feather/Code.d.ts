import { StyledIcon, StyledIconProps } from '..';
export declare const Code: StyledIcon<any>;
export declare const CodeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
