import { StyledIcon, StyledIconProps } from '..';
export declare const ExternalLink: StyledIcon<any>;
export declare const ExternalLinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
