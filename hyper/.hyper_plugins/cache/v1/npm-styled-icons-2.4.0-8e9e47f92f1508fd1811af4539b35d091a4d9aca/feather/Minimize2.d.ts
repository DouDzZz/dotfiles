import { StyledIcon, StyledIconProps } from '..';
export declare const Minimize2: StyledIcon<any>;
export declare const Minimize2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
