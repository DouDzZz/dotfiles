import { StyledIcon, StyledIconProps } from '..';
export declare const FolderPlus: StyledIcon<any>;
export declare const FolderPlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
