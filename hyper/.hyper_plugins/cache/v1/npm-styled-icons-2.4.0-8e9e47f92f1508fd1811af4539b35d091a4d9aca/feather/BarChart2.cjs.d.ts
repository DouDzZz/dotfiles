import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BarChart2: StyledIcon<any>;
export declare const BarChart2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
