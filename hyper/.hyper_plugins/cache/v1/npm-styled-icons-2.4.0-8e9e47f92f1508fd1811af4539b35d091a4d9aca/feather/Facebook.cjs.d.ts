import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Facebook: StyledIcon<any>;
export declare const FacebookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
