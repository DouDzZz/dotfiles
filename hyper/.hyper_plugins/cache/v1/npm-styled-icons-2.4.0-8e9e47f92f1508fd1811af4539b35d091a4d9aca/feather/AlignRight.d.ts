import { StyledIcon, StyledIconProps } from '..';
export declare const AlignRight: StyledIcon<any>;
export declare const AlignRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
