import { StyledIcon, StyledIconProps } from '..';
export declare const Unlock: StyledIcon<any>;
export declare const UnlockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
