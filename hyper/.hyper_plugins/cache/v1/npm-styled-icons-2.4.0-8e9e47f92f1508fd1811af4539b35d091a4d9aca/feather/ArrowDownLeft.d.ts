import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDownLeft: StyledIcon<any>;
export declare const ArrowDownLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
