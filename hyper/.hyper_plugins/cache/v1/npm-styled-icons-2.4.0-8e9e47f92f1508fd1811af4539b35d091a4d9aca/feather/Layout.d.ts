import { StyledIcon, StyledIconProps } from '..';
export declare const Layout: StyledIcon<any>;
export declare const LayoutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
