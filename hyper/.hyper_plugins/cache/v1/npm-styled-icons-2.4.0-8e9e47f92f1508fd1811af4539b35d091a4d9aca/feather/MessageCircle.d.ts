import { StyledIcon, StyledIconProps } from '..';
export declare const MessageCircle: StyledIcon<any>;
export declare const MessageCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
