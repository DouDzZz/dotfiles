import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Linkedin: StyledIcon<any>;
export declare const LinkedinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
