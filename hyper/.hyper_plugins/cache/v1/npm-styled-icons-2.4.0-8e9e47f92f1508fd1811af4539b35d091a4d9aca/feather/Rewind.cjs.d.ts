import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rewind: StyledIcon<any>;
export declare const RewindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
