import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LifeBuoy: StyledIcon<any>;
export declare const LifeBuoyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
