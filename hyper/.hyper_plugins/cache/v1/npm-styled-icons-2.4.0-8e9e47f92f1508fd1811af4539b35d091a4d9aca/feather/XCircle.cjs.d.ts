import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const XCircle: StyledIcon<any>;
export declare const XCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
