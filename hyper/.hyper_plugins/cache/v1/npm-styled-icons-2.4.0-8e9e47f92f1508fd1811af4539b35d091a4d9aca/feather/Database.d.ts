import { StyledIcon, StyledIconProps } from '..';
export declare const Database: StyledIcon<any>;
export declare const DatabaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
