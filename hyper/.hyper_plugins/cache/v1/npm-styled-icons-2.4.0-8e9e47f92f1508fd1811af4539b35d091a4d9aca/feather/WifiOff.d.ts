import { StyledIcon, StyledIconProps } from '..';
export declare const WifiOff: StyledIcon<any>;
export declare const WifiOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
