import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sunrise: StyledIcon<any>;
export declare const SunriseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
