import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileMinus: StyledIcon<any>;
export declare const FileMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
