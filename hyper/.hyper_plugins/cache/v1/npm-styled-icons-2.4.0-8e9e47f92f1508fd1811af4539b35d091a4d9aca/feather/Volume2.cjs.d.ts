import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Volume2: StyledIcon<any>;
export declare const Volume2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
