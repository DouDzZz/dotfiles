import { StyledIcon, StyledIconProps } from '..';
export declare const PlusSquare: StyledIcon<any>;
export declare const PlusSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
