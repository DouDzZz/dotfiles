import { StyledIcon, StyledIconProps } from '..';
export declare const FilePlus: StyledIcon<any>;
export declare const FilePlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
