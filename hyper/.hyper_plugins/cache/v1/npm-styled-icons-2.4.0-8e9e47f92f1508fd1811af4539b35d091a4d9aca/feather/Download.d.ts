import { StyledIcon, StyledIconProps } from '..';
export declare const Download: StyledIcon<any>;
export declare const DownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
