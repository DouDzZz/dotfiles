import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeX: StyledIcon<any>;
export declare const VolumeXDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
