import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Send: StyledIcon<any>;
export declare const SendDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
