import { StyledIcon, StyledIconProps } from '..';
export declare const Clock: StyledIcon<any>;
export declare const ClockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
