import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EyeOff: StyledIcon<any>;
export declare const EyeOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
