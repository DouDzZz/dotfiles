import { StyledIcon, StyledIconProps } from '..';
export declare const List: StyledIcon<any>;
export declare const ListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
