import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneOutgoing: StyledIcon<any>;
export declare const PhoneOutgoingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
