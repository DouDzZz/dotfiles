import { StyledIcon, StyledIconProps } from '..';
export declare const CornerRightUp: StyledIcon<any>;
export declare const CornerRightUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
