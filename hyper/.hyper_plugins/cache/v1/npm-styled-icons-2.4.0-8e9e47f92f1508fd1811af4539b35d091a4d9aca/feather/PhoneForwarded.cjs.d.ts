import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneForwarded: StyledIcon<any>;
export declare const PhoneForwardedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
