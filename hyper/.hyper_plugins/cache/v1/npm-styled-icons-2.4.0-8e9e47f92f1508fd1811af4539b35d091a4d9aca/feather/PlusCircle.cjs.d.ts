import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlusCircle: StyledIcon<any>;
export declare const PlusCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
