import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Star: StyledIcon<any>;
export declare const StarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
