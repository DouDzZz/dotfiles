import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CornerRightDown: StyledIcon<any>;
export declare const CornerRightDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
