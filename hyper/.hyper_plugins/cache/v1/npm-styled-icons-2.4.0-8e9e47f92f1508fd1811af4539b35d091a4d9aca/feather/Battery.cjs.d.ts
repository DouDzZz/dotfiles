import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery: StyledIcon<any>;
export declare const BatteryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
