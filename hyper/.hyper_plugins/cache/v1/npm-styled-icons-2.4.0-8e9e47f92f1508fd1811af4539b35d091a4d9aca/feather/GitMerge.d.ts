import { StyledIcon, StyledIconProps } from '..';
export declare const GitMerge: StyledIcon<any>;
export declare const GitMergeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
