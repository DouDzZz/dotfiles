import { StyledIcon, StyledIconProps } from '..';
export declare const ThumbsDown: StyledIcon<any>;
export declare const ThumbsDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
