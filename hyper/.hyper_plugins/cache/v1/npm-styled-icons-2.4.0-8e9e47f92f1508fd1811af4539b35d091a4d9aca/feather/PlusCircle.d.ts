import { StyledIcon, StyledIconProps } from '..';
export declare const PlusCircle: StyledIcon<any>;
export declare const PlusCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
