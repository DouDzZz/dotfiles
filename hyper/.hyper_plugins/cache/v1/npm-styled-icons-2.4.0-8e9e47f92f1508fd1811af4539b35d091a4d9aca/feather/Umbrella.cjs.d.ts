import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Umbrella: StyledIcon<any>;
export declare const UmbrellaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
