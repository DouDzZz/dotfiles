import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Download: StyledIcon<any>;
export declare const DownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
