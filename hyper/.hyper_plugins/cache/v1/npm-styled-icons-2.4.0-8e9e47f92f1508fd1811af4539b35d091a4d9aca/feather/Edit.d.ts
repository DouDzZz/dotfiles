import { StyledIcon, StyledIconProps } from '..';
export declare const Edit: StyledIcon<any>;
export declare const EditDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
