import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cpu: StyledIcon<any>;
export declare const CpuDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
