import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneCall: StyledIcon<any>;
export declare const PhoneCallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
