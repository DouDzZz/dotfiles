import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneOff: StyledIcon<any>;
export declare const PhoneOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
