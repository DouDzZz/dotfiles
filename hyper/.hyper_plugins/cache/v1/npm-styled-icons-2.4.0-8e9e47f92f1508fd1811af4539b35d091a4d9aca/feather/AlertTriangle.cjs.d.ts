import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlertTriangle: StyledIcon<any>;
export declare const AlertTriangleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
