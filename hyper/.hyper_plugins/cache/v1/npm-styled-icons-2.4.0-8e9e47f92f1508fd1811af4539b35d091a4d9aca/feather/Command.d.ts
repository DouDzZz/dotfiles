import { StyledIcon, StyledIconProps } from '..';
export declare const Command: StyledIcon<any>;
export declare const CommandDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
