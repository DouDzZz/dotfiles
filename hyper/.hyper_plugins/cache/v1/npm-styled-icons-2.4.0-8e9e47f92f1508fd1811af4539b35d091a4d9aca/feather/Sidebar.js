var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Sidebar = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Sidebar-title" }, props.title), React.createElement("rect", { width: 18, height: 18, x: 3, y: 3, rx: 2, ry: 2, key: "k0" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 3, y2: 21, key: "k1" })
        ]
        : [React.createElement("rect", { width: 18, height: 18, x: 3, y: 3, rx: 2, ry: 2, key: "k0" }),
            React.createElement("line", { x1: 9, x2: 9, y1: 3, y2: 21, key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Sidebar.displayName = 'Sidebar';
export var SidebarDimensions = { height: 24, width: 24 };
var templateObject_1;
