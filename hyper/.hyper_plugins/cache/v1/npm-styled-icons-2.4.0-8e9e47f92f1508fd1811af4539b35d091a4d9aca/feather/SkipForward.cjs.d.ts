import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SkipForward: StyledIcon<any>;
export declare const SkipForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
