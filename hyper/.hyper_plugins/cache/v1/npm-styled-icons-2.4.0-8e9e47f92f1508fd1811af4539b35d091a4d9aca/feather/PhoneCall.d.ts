import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneCall: StyledIcon<any>;
export declare const PhoneCallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
