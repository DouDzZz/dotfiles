var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var FileText = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "FileText-title" }, props.title), React.createElement("path", { d: "M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z", key: "k0" }),
            React.createElement("polyline", { points: "14 2 14 8 20 8", key: "k1" }),
            React.createElement("line", { x1: 16, x2: 8, y1: 13, y2: 13, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 8, y1: 17, y2: 17, key: "k3" }),
            React.createElement("polyline", { points: "10 9 9 9 8 9", key: "k4" })
        ]
        : [React.createElement("path", { d: "M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z", key: "k0" }),
            React.createElement("polyline", { points: "14 2 14 8 20 8", key: "k1" }),
            React.createElement("line", { x1: 16, x2: 8, y1: 13, y2: 13, key: "k2" }),
            React.createElement("line", { x1: 16, x2: 8, y1: 17, y2: 17, key: "k3" }),
            React.createElement("polyline", { points: "10 9 9 9 8 9", key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
FileText.displayName = 'FileText';
export var FileTextDimensions = { height: 24, width: 24 };
var templateObject_1;
