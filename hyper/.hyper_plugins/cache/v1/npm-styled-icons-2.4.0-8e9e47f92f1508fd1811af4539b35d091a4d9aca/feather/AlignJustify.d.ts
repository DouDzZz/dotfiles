import { StyledIcon, StyledIconProps } from '..';
export declare const AlignJustify: StyledIcon<any>;
export declare const AlignJustifyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
