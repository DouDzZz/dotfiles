import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TrendingUp: StyledIcon<any>;
export declare const TrendingUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
