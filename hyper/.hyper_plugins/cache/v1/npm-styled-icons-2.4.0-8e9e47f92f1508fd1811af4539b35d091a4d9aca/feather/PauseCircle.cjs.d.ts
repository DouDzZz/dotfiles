import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PauseCircle: StyledIcon<any>;
export declare const PauseCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
