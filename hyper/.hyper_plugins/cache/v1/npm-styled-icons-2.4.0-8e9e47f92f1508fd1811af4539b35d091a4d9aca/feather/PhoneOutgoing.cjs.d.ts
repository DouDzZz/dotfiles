import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneOutgoing: StyledIcon<any>;
export declare const PhoneOutgoingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
