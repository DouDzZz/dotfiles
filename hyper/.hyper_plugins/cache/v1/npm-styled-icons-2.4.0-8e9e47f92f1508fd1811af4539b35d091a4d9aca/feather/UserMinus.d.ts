import { StyledIcon, StyledIconProps } from '..';
export declare const UserMinus: StyledIcon<any>;
export declare const UserMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
