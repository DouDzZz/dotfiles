import { StyledIcon, StyledIconProps } from '..';
export declare const Sun: StyledIcon<any>;
export declare const SunDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
