import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Compass: StyledIcon<any>;
export declare const CompassDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
