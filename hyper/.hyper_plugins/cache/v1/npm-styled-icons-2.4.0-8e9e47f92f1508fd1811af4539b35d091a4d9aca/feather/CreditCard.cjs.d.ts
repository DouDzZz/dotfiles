import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreditCard: StyledIcon<any>;
export declare const CreditCardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
