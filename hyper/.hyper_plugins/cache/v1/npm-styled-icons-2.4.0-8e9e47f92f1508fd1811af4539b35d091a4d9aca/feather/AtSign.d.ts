import { StyledIcon, StyledIconProps } from '..';
export declare const AtSign: StyledIcon<any>;
export declare const AtSignDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
