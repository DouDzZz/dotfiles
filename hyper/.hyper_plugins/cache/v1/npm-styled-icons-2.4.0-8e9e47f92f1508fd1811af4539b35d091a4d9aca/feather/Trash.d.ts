import { StyledIcon, StyledIconProps } from '..';
export declare const Trash: StyledIcon<any>;
export declare const TrashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
