import { StyledIcon, StyledIconProps } from '..';
export declare const CornerDownRight: StyledIcon<any>;
export declare const CornerDownRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
