import { StyledIcon, StyledIconProps } from '..';
export declare const X: StyledIcon<any>;
export declare const XDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
