import { StyledIcon, StyledIconProps } from '..';
export declare const Octagon: StyledIcon<any>;
export declare const OctagonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
