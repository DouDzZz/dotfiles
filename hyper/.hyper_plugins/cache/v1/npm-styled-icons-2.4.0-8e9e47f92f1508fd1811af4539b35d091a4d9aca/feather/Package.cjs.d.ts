import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Package: StyledIcon<any>;
export declare const PackageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
