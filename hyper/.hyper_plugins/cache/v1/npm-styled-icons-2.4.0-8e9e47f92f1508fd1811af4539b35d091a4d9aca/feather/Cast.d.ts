import { StyledIcon, StyledIconProps } from '..';
export declare const Cast: StyledIcon<any>;
export declare const CastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
