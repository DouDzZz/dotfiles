import { StyledIcon, StyledIconProps } from '..';
export declare const Link2: StyledIcon<any>;
export declare const Link2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
