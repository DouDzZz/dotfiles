import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Target: StyledIcon<any>;
export declare const TargetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
