import { StyledIcon, StyledIconProps } from '..';
export declare const UploadCloud: StyledIcon<any>;
export declare const UploadCloudDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
