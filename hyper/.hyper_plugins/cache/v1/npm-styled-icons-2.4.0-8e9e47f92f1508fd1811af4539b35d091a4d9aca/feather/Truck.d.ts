import { StyledIcon, StyledIconProps } from '..';
export declare const Truck: StyledIcon<any>;
export declare const TruckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
