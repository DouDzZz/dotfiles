var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var MicOff = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "MicOff-title" }, props.title), React.createElement("line", { x1: 1, x2: 23, y1: 1, y2: 23, key: "k0" }),
            React.createElement("path", { d: "M9 9v3a3 3 0 0 0 5.12 2.12M15 9.34V4a3 3 0 0 0-5.94-.6", key: "k1" }),
            React.createElement("path", { d: "M17 16.95A7 7 0 0 1 5 12v-2m14 0v2a7 7 0 0 1-.11 1.23", key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 19, y2: 23, key: "k3" }),
            React.createElement("line", { x1: 8, x2: 16, y1: 23, y2: 23, key: "k4" })
        ]
        : [React.createElement("line", { x1: 1, x2: 23, y1: 1, y2: 23, key: "k0" }),
            React.createElement("path", { d: "M9 9v3a3 3 0 0 0 5.12 2.12M15 9.34V4a3 3 0 0 0-5.94-.6", key: "k1" }),
            React.createElement("path", { d: "M17 16.95A7 7 0 0 1 5 12v-2m14 0v2a7 7 0 0 1-.11 1.23", key: "k2" }),
            React.createElement("line", { x1: 12, x2: 12, y1: 19, y2: 23, key: "k3" }),
            React.createElement("line", { x1: 8, x2: 16, y1: 23, y2: 23, key: "k4" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
MicOff.displayName = 'MicOff';
export var MicOffDimensions = { height: 24, width: 24 };
var templateObject_1;
