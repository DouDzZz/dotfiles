import { StyledIcon, StyledIconProps } from '..';
export declare const CornerUpRight: StyledIcon<any>;
export declare const CornerUpRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
