import { StyledIcon, StyledIconProps } from '..';
export declare const LogOut: StyledIcon<any>;
export declare const LogOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
