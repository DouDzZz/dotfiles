import { StyledIcon, StyledIconProps } from '..';
export declare const Move: StyledIcon<any>;
export declare const MoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
