import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignCenter: StyledIcon<any>;
export declare const AlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
