import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Moon: StyledIcon<any>;
export declare const MoonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
