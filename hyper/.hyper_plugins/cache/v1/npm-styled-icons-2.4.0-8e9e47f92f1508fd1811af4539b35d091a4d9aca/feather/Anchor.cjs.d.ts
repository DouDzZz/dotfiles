import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Anchor: StyledIcon<any>;
export declare const AnchorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
