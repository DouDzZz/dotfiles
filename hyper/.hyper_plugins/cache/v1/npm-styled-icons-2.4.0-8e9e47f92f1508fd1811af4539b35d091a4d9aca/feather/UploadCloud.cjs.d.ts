import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UploadCloud: StyledIcon<any>;
export declare const UploadCloudDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
