import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowRight: StyledIcon<any>;
export declare const ArrowRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
