import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Box: StyledIcon<any>;
export declare const BoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
