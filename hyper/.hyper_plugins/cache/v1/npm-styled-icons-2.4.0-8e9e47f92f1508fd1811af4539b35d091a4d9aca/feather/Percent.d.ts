import { StyledIcon, StyledIconProps } from '..';
export declare const Percent: StyledIcon<any>;
export declare const PercentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
