import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserMinus: StyledIcon<any>;
export declare const UserMinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
