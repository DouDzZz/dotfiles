import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Minus: StyledIcon<any>;
export declare const MinusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
