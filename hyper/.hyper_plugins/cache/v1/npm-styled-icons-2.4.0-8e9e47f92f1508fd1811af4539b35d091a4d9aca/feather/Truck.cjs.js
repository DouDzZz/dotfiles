"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Truck = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Truck-title" }, props.title), react_1.default.createElement("rect", { width: 15, height: 13, x: 1, y: 3, key: "k0" }),
            react_1.default.createElement("polygon", { points: "16 8 20 8 23 11 23 16 16 16 16 8", key: "k1" }),
            react_1.default.createElement("circle", { cx: 5.5, cy: 18.5, r: 2.5, key: "k2" }),
            react_1.default.createElement("circle", { cx: 18.5, cy: 18.5, r: 2.5, key: "k3" })
        ]
        : [react_1.default.createElement("rect", { width: 15, height: 13, x: 1, y: 3, key: "k0" }),
            react_1.default.createElement("polygon", { points: "16 8 20 8 23 11 23 16 16 16 16 8", key: "k1" }),
            react_1.default.createElement("circle", { cx: 5.5, cy: 18.5, r: 2.5, key: "k2" }),
            react_1.default.createElement("circle", { cx: 18.5, cy: 18.5, r: 2.5, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Truck.displayName = 'Truck';
exports.TruckDimensions = { height: 24, width: 24 };
var templateObject_1;
