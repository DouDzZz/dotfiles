import { StyledIcon, StyledIconProps } from '..';
export declare const Chrome: StyledIcon<any>;
export declare const ChromeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
