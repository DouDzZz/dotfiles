import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tag: StyledIcon<any>;
export declare const TagDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
