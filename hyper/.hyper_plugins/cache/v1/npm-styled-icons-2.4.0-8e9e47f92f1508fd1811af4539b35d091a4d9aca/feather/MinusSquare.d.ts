import { StyledIcon, StyledIconProps } from '..';
export declare const MinusSquare: StyledIcon<any>;
export declare const MinusSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
