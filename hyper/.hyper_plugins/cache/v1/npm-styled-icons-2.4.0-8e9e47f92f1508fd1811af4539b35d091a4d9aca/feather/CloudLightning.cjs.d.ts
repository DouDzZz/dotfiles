import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudLightning: StyledIcon<any>;
export declare const CloudLightningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
