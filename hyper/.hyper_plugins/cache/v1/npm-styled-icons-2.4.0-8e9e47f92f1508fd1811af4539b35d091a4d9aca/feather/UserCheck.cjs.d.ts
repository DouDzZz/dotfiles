import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserCheck: StyledIcon<any>;
export declare const UserCheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
