import { StyledIcon, StyledIconProps } from '..';
export declare const Copy: StyledIcon<any>;
export declare const CopyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
