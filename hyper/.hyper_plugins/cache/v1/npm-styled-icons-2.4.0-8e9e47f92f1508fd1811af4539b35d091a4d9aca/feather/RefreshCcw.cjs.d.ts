import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RefreshCcw: StyledIcon<any>;
export declare const RefreshCcwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
