import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Loader: StyledIcon<any>;
export declare const LoaderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
