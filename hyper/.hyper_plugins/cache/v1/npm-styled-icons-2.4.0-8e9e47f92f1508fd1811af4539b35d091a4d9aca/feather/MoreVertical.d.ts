import { StyledIcon, StyledIconProps } from '..';
export declare const MoreVertical: StyledIcon<any>;
export declare const MoreVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
