var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var HardDrive = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "HardDrive-title" }, props.title), React.createElement("line", { x1: 22, x2: 2, y1: 12, y2: 12, key: "k0" }),
            React.createElement("path", { d: "M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z", key: "k1" }),
            React.createElement("line", { x1: 6, x2: 6, y1: 16, y2: 16, key: "k2" }),
            React.createElement("line", { x1: 10, x2: 10, y1: 16, y2: 16, key: "k3" })
        ]
        : [React.createElement("line", { x1: 22, x2: 2, y1: 12, y2: 12, key: "k0" }),
            React.createElement("path", { d: "M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z", key: "k1" }),
            React.createElement("line", { x1: 6, x2: 6, y1: 16, y2: 16, key: "k2" }),
            React.createElement("line", { x1: 10, x2: 10, y1: 16, y2: 16, key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "none",
    "stroke": "currentColor",
    "strokeLinecap": "round",
    "strokeLinejoin": "round",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
HardDrive.displayName = 'HardDrive';
export var HardDriveDimensions = { height: 24, width: 24 };
var templateObject_1;
