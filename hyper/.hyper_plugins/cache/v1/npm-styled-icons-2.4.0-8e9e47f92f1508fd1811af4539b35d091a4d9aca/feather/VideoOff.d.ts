import { StyledIcon, StyledIconProps } from '..';
export declare const VideoOff: StyledIcon<any>;
export declare const VideoOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
