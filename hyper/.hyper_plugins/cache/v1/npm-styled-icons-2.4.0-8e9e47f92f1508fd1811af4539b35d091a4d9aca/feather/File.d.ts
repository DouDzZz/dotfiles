import { StyledIcon, StyledIconProps } from '..';
export declare const File: StyledIcon<any>;
export declare const FileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
