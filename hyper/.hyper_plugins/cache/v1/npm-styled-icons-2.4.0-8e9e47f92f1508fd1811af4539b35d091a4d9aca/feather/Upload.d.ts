import { StyledIcon, StyledIconProps } from '..';
export declare const Upload: StyledIcon<any>;
export declare const UploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
