import { StyledIcon, StyledIconProps } from '..';
export declare const ZapOff: StyledIcon<any>;
export declare const ZapOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
