{
  "manifest": {
    "name": "styled-icons",
    "version": "2.4.0",
    "description": "Font Awesome, Material Icons, and Octicons available as Styled Components",
    "author": {
      "name": "Jacob Gillespie",
      "email": "jacobwgillespie@gmail.com"
    },
    "homepage": "https://github.com/jacobwgillespie/styled-icons",
    "repository": {
      "type": "git",
      "url": "git://github.com/jacobwgillespie/styled-icons.git"
    },
    "license": "MIT",
    "main": "index.js",
    "module": "index.js",
    "jsnext:main": "index.js",
    "typings": "./index.d.ts",
    "sideEffects": false,
    "files": [
      "/fa-brands",
      "/fa-regular",
      "/fa-solid",
      "/feather",
      "/material",
      "/octicons",
      "/CHANGELOG.md",
      "/index.d.ts",
      "/index.cjs.js",
      "/index.js",
      "/LICENSE",
      "/package.json",
      "/README.md"
    ],
    "keywords": [
      "styled-components",
      "icons",
      "font-awesome",
      "feather-icons",
      "material-ui",
      "material",
      "octicons",
      "svg"
    ],
    "prettier": {
      "bracketSpacing": false,
      "printWidth": 100,
      "semi": false,
      "singleQuote": true,
      "trailingComma": "all"
    },
    "scripts": {
      "build": "node generate/generate.js",
      "build-website": "gatsby build",
      "clean": "rm -rf build fa-brands fa-regular fa-solid feather material octicons public manifest.json index.d.ts index.cjs.js index.js",
      "dev": "gatsby develop",
      "serve": "serve public"
    },
    "peerDependencies": {
      "react": "*",
      "styled-components": ">=3 <4"
    },
    "devDependencies": {
      "@fortawesome/fontawesome": "^1.1.5",
      "@fortawesome/fontawesome-free-brands": "^5.0.10",
      "@fortawesome/fontawesome-free-regular": "^5.0.10",
      "@fortawesome/fontawesome-free-solid": "^5.0.10",
      "@types/node": "^10.5.0",
      "@types/react": "^16.3.11",
      "babel-plugin-styled-components": "^1.5.1",
      "caniuse-db": "^1.0.30000652",
      "copy-to-clipboard": "^3.0.8",
      "execa": "^1.0.0",
      "fast-case": "^1.1.0",
      "fast-glob": "^2.2.0",
      "feather-icons": "^4.7.0",
      "fs-extra": "^7.0.0",
      "gatsby": "^1.9.247",
      "gatsby-link": "^1.6.40",
      "gatsby-plugin-favicon": "^2.1.1",
      "gatsby-plugin-manifest": "^1.0.20",
      "gatsby-plugin-netlify": "^1.0.19",
      "gatsby-plugin-offline": "^1.0.15",
      "gatsby-plugin-react-helmet": "^2.0.10",
      "gatsby-plugin-react-next": "^1.0.11",
      "gatsby-plugin-styled-components": "^2.0.11",
      "h2x-core": "^0.1.9",
      "h2x-plugin-jsx": "^0.1.9",
      "js-search": "^1.4.2",
      "material-design-icons": "^3.0.1",
      "number-to-words": "^1.2.3",
      "octicons": "^8.0.0",
      "ora": "^3.0.0",
      "prettier": "^1.13.0",
      "react": "^16.3.2",
      "react-dom": "^16.3.2",
      "react-helmet": "^5.2.0",
      "react-virtualized": "^9.18.5",
      "serve": "^10.0.0",
      "styled-components": "^3.2.5",
      "svgo": "^1.0.5",
      "typescript": "^3.0.1"
    },
    "_registry": "npm",
    "_loc": "/Users/edjubert/.hyper_plugins/cache/v1/npm-styled-icons-2.4.0-8e9e47f92f1508fd1811af4539b35d091a4d9aca/package.json",
    "readmeFilename": "README.md",
    "readme": "# 💅 styled-icons\n\n[![Build Status](https://img.shields.io/travis/jacobwgillespie/styled-icons/master.svg)](https://travis-ci.org/jacobwgillespie/styled-icons)\n[![npm](https://img.shields.io/npm/dm/styled-icons.svg)](https://www.npmjs.com/package/styled-icons)\n[![npm](https://img.shields.io/npm/v/styled-icons.svg)](https://www.npmjs.com/package/styled-icons)\n[![Built with Styled Components](https://img.shields.io/badge/built%20with-styled%20components-db7093.svg)](https://www.styled-components.com/)\n![Powered by TypeScript](https://img.shields.io/badge/powered%20by-typescript-blue.svg)\n\n[![View Icons](https://gui.apex.sh/component?name=ShadowButton&config=%7B%22text%22%3A%22ICON%20EXPLORER%22%2C%22color%22%3A%22db7093%22%7D)](https://styled-icons.js.org)\n\n`styled-icons` provides the [Font Awesome](https://fontawesome.com/), [Feather](https://feathericons.com/), [Material Design](https://material.io/icons/), and [Octicons](https://octicons.github.com/) icon packs as [Styled Components](https://www.styled-components.com/), with full support for TypeScript types and tree-shaking / ES modules.\n\n---\n\n### Table of Contents\n\n* [Installation](#installation)\n* [Usage](#usage)\n  * [Props](#props)\n  * [Icon Dimensions](#icon-dimensions)\n  * [Styled Components](#styled-components)\n  * [Accessibility](#accessibility)\n  * [Tree Shaking](#tree-shaking)\n  * [TypeScript](#typescript)\n* [Contributing](#contributing)\n* [License](#license)\n\n## Installation\n\n```\nyarn add styled-icons\n```\n\nor\n\n```\nnpm install styled-icons --save\n```\n\nAdditionally, you will need to have installed a version of `styled-components`, as `styled-icons` depends on `styled-components` as a peer dependency.\n\n## Usage\n\nAll Font Awesome (free), Feather, Material, and Octicon icons are available for preview at the [Icon Explorer](https://styled-icons.js.org).\n\nThe entire icon packs are available via the main import and sub-imports:\n\n```javascript\nimport {material, octicons} from 'styled-icons'\n\nimport * as faBrands from 'styled-icons/fa-brands'\nimport * as faRegular from 'styled-icons/fa-regular'\nimport * as faSolid from 'styled-icons/fa-solid'\nimport * as feather from 'styled-icons/feather'\nimport * as material from 'styled-icons/material'\nimport * as octicons from 'styled-icons/octicons'\n```\n\nThe icons are also available as individual imports - it is recommended that you import icons individually using ES modules along with a module bundler like Webpack or Rollup in order to enable tree-shaking. This means that the module bundler will remove unused icons from the final JavaScript bundle, saving bandwidth and speeding up loading:\n\n```javascript\nimport React, {Fragment} from 'react'\nimport {Account, Lock} from 'styled-icons/material'\n\nconst App = () => (\n  <Fragment>\n    <Account />\n    <Lock />\n  </Fragment>\n)\n```\n\n### Props\n\nStyled Icons accept all the valid props of an `<svg />` element, in addition to the following custom props:\n\n| Prop    | Required | Type                                                                                            | Description                                                                                                                                   |\n| ------- | -------- | ----------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |\n| `size`  | optional | string, number                                                                                  | This is a convenience for setting both `width` and `height` to the same value                                                                 |\n| `title` | optional | string                                                                                          | This sets the icon title and enables the standalone icon accessability mode. See [accessibility](#accessibility) below for additional details |\n| `css`   | optional | string, function, [css tagged template literal](https://www.styled-components.com/docs/api#css) | This prop accepts additional CSS to attach to the icon. It accepts the CSS as a string or as any valid Style Components interpolation         |\n\n**Example**\n\n```javascript\nimport React from 'react'\nimport {Lock} from 'styled-icons/material'\n\nconst App = () => <Lock size=\"48\" title=\"Unlock account\" css=\"color: red;\" />\n```\n\n### Icon Dimensions\n\nSome icons natively have non-square dimensions - original dimensions are exported from the individual icon exports:\n\n```javascript\nimport {LogoGithub, LogoGithubDimensions} from 'styled-icons/octicons/LogoGithub'\n\nconst App = () => <LogoGithub />\n\nconsole.log(LogoGithubDimension) // {height: 16, width: 45}\n```\n\nDimension exports are not available on the icon pack or index exports:\n\n```javascript\nimport * as octicons from 'styled-icons/octicons'\nimport {octicons} from 'styled-icons'\n\n// octicons contains only icon exports\n```\n\n### Styled Components\n\nAll icons are exported as [Styled Components](https://www.styled-components.com/), which means it is possible to utilize the Styled Components API:\n\n```javascript\nimport styled from 'styled-components'\nimport {Lock} from 'styled-icons/material'\n\nexport const RedLock = styled(Lock)`\n  color: red;\n\n  font-weight: ${props => (props.important ? 'bold' : 'normal')};\n`\n```\n\n### Accessibility\n\nStyled Icons have two different built-in \"modes\" for accessibility purposes. By default, icons are considered decorative, meaning the icon is just visual sugar and the surrounding content is sufficient for conveying meaning. This will set the `aria-hidden` attribute on the resulting HTML to hide the icon from screen readers.\n\n```javascript\n// this icon\n<Lock />\n\n// will result in\n<svg aria-hidden=\"true\">...</svg>\n```\n\nAlternatively the icon could be used in standalone mode, meaning the icon itself should convey meaning. This mode is enabled by setting a `title` prop - this is the text that will be read by a screen reader. This results in `role` being set to `img` and the addition of a `<title>` element.\n\n```javascript\n// this icon\n<Lock title=\"Lock account\" />\n\n// will result in\n<svg role=\"img\"><title>Lock account</title> ...</svg>\n```\n\nSince Style Icons accept all `<svg>` element attributes as props, you are free to override these `aria-*` attributes if you have a more complex use-case.\n\nAs this library provides direct access to the `<svg>` element, you may wish to further wrap the icon for additional semantic meaning. For example, for a loading spinner:\n\n```javascript\nimport styled from 'styled-components'\nimport {Spinner} from 'styled-icons/fa-solid/Spinner'\n\nconst VisuallyHidden = styled.span`\n  border: 0 !important;\n  clip: rect(1px, 1px, 1px, 1px) !important;\n  height: 1px !important;\n  overflow: hidden !important;\n  padding-top: 0 !important;\n  padding-right: 0 !important;\n  padding-bottom: 0 !important;\n  padding-left: 0 !important;\n  position: absolute !important;\n  white-space: nowrap !important;\n  width: 1px !important;\n`\n\n<span title=\"Loading posts...\" role=\"alert\" aria-live=\"assertive\">\n  <Spinner />\n  <VisuallyHidden>Loading posts...</VisuallyHidden>\n</span>\n```\n\n### Tree Shaking\n\n**NOTE:** tree shaking should work without modification using [Create React App](https://github.com/facebook/create-react-app).\n\nTree shaking has been tested with Create React App, Rollup, and Webpack. If your bundler is unable to import the icons, additional CommonJS bundles are available as `.cjs`:\n\n```javascript\nimport React, {Fragment} from 'react'\n\n// This will result in all Material icons being bundled\nimport {Account} from 'styled-icons/material.cjs'\n\n// This will only include the Lock icon\nimport {Lock} from 'styled-icons/material/Lock.cjs'\n\nconst App = () => (\n  <Fragment>\n    <Account />\n    <Lock />\n  </Fragment>\n)\n```\n\nBe aware though that importing from the CommonJS icon pack bundles will likely result in significantly larger bundle sizes, because unused icons will be included in the final bundle. If you are unable to configure your bundler to process the ES module bundles, you should import icons individually to avoid large bundles.\n\n### TypeScript\n\nThe icons of `styled-icons` are built using TypeScript and export type definitions. By default, the `theme` prop is typed as `any`, but if you would like to override the theme interface, this is possible via the `StyledIcon` type:\n\n```typescript\nimport styled from 'styled-components'\nimport {StyledIcon} from 'styled-icons/material'\nimport {Lock} from 'styled-icons/material'\n\ninterface ThemeInterface {\n  lockColor: string\n}\n\nexport const ThemedLock: StyledIcon<ThemeInterface> = styled(Lock)`\n  color: ${props => props.theme.lockColor};\n`\n```\n\nIf you have any ideas for improvements to the TypeScript typing, please open an issue or PR!\n\n## Contributing\n\nContributions are welcome! Feel free to open an issue or a pull request and participate at whatever level you would like.\n\n## License\n\nThe MIT License - see `LICENSE`.\n\nThe Font Awesome icons are licensed under the [CC BY 4.0 License](https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt).\n\nThe Feather icons are licensed under the [MIT License](https://github.com/feathericons/feather/blob/master/LICENSE).\n\nThe Material Design icons are licensed under the [Apache License Version 2.0](https://github.com/google/material-design-icons/blob/master/LICENSE).\n\nThe Octicons are licensed under the [MIT License](https://github.com/primer/octicons/blob/master/LICENSE).\n",
    "licenseText": "MIT License\n\nCopyright (c) 2018 Jacob Gillespie <jacobwgillespie@gmail.com>\n\nPermission is hereby granted, free of charge, to any person obtaining a copy\nof this software and associated documentation files (the \"Software\"), to deal\nin the Software without restriction, including without limitation the rights\nto use, copy, modify, merge, publish, distribute, sublicense, and/or sell\ncopies of the Software, and to permit persons to whom the Software is\nfurnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all\ncopies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\nSOFTWARE.\n"
  },
  "artifacts": [],
  "remote": {
    "resolved": "https://registry.yarnpkg.com/styled-icons/-/styled-icons-2.4.0.tgz#8e9e47f92f1508fd1811af4539b35d091a4d9aca",
    "type": "tarball",
    "reference": "https://registry.yarnpkg.com/styled-icons/-/styled-icons-2.4.0.tgz",
    "hash": "8e9e47f92f1508fd1811af4539b35d091a4d9aca",
    "registry": "npm",
    "packageName": "styled-icons"
  },
  "registry": "npm",
  "hash": "8e9e47f92f1508fd1811af4539b35d091a4d9aca"
}