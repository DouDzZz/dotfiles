import { StyledIcon, StyledIconProps } from '..';
export declare const Ellipsis: StyledIcon<any>;
export declare const EllipsisDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
