import { StyledIcon, StyledIconProps } from '..';
export declare const Dash: StyledIcon<any>;
export declare const DashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
