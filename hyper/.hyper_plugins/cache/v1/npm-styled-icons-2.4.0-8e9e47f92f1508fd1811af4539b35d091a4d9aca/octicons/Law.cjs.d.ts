import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Law: StyledIcon<any>;
export declare const LawDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
