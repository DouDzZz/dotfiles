import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KebabHorizontal: StyledIcon<any>;
export declare const KebabHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
