import { StyledIcon, StyledIconProps } from '..';
export declare const DiffIgnored: StyledIcon<any>;
export declare const DiffIgnoredDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
