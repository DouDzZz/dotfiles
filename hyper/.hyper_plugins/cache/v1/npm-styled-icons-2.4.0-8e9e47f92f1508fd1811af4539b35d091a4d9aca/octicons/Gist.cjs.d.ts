import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gist: StyledIcon<any>;
export declare const GistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
