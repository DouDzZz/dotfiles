import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Report: StyledIcon<any>;
export declare const ReportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
