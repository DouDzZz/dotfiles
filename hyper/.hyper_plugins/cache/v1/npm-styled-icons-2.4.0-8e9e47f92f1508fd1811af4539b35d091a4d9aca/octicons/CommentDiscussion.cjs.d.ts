import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CommentDiscussion: StyledIcon<any>;
export declare const CommentDiscussionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
