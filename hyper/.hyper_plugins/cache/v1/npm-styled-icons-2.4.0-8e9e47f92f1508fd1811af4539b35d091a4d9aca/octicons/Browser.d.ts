import { StyledIcon, StyledIconProps } from '..';
export declare const Browser: StyledIcon<any>;
export declare const BrowserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
