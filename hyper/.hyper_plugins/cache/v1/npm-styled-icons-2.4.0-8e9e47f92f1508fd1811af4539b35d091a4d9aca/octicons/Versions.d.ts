import { StyledIcon, StyledIconProps } from '..';
export declare const Versions: StyledIcon<any>;
export declare const VersionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
