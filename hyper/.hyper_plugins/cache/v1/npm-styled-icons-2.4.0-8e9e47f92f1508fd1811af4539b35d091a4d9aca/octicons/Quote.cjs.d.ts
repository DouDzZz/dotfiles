import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Quote: StyledIcon<any>;
export declare const QuoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
