import { StyledIcon, StyledIconProps } from '..';
export declare const DiffAdded: StyledIcon<any>;
export declare const DiffAddedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
