import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IssueReopened: StyledIcon<any>;
export declare const IssueReopenedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
