import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Jersey: StyledIcon<any>;
export declare const JerseyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
