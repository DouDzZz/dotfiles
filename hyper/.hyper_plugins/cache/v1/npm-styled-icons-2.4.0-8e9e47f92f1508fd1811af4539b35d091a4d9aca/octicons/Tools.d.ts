import { StyledIcon, StyledIconProps } from '..';
export declare const Tools: StyledIcon<any>;
export declare const ToolsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
