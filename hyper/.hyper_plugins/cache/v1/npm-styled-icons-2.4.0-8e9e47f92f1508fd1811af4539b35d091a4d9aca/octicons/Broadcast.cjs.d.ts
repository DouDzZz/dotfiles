import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Broadcast: StyledIcon<any>;
export declare const BroadcastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
