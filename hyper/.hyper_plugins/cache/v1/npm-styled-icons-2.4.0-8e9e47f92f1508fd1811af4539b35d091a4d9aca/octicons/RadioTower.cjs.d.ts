import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RadioTower: StyledIcon<any>;
export declare const RadioTowerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
