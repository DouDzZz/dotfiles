import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowBoth: StyledIcon<any>;
export declare const ArrowBothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
