import { StyledIcon, StyledIconProps } from '..';
export declare const TriangleUp: StyledIcon<any>;
export declare const TriangleUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
