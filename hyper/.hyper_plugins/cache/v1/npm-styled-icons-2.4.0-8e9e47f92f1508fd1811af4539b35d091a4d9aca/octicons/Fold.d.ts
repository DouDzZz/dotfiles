import { StyledIcon, StyledIconProps } from '..';
export declare const Fold: StyledIcon<any>;
export declare const FoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
