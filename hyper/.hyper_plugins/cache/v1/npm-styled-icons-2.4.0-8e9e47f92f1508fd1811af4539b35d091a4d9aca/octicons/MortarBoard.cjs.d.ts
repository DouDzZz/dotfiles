import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MortarBoard: StyledIcon<any>;
export declare const MortarBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
