import { StyledIcon, StyledIconProps } from '..';
export declare const Info: StyledIcon<any>;
export declare const InfoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
