import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IssueClosed: StyledIcon<any>;
export declare const IssueClosedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
