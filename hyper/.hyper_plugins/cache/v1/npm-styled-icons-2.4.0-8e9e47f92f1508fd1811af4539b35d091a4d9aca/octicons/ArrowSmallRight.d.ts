import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowSmallRight: StyledIcon<any>;
export declare const ArrowSmallRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
