import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilePdf: StyledIcon<any>;
export declare const FilePdfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
