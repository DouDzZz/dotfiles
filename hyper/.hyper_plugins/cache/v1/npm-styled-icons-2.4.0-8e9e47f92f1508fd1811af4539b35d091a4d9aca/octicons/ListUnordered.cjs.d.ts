import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ListUnordered: StyledIcon<any>;
export declare const ListUnorderedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
