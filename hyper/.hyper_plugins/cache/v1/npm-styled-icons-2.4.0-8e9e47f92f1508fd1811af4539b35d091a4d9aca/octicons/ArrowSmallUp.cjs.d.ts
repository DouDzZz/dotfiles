import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowSmallUp: StyledIcon<any>;
export declare const ArrowSmallUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
