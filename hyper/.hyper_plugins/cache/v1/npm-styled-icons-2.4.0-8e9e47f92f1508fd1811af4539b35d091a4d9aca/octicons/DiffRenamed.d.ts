import { StyledIcon, StyledIconProps } from '..';
export declare const DiffRenamed: StyledIcon<any>;
export declare const DiffRenamedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
