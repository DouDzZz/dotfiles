import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenFull: StyledIcon<any>;
export declare const ScreenFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
