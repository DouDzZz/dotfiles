import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Checklist: StyledIcon<any>;
export declare const ChecklistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
