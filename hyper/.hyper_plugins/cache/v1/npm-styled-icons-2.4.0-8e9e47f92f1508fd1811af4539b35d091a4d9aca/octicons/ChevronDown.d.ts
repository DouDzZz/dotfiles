import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronDown: StyledIcon<any>;
export declare const ChevronDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
