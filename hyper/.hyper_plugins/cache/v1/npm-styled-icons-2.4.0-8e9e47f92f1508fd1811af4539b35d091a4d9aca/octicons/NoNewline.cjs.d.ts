import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NoNewline: StyledIcon<any>;
export declare const NoNewlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
