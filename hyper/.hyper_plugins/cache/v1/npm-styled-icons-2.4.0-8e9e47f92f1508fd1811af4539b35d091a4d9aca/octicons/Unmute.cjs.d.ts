import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unmute: StyledIcon<any>;
export declare const UnmuteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
