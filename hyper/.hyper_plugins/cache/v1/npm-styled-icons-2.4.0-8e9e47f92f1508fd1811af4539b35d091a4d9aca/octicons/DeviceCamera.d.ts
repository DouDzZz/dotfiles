import { StyledIcon, StyledIconProps } from '..';
export declare const DeviceCamera: StyledIcon<any>;
export declare const DeviceCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
