import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignIn: StyledIcon<any>;
export declare const SignInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
