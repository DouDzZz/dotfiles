import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitPullRequest: StyledIcon<any>;
export declare const GitPullRequestDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
