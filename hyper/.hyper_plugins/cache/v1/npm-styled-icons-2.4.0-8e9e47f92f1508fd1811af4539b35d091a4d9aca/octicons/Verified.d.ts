import { StyledIcon, StyledIconProps } from '..';
export declare const Verified: StyledIcon<any>;
export declare const VerifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
