import { StyledIcon, StyledIconProps } from '..';
export declare const Smiley: StyledIcon<any>;
export declare const SmileyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
