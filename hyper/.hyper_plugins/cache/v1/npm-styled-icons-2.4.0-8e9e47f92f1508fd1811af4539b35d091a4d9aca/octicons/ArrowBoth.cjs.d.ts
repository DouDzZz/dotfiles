import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowBoth: StyledIcon<any>;
export declare const ArrowBothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
