import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowSmallUp: StyledIcon<any>;
export declare const ArrowSmallUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
