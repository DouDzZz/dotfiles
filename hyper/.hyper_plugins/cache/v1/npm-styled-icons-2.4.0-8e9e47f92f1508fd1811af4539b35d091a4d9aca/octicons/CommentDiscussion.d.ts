import { StyledIcon, StyledIconProps } from '..';
export declare const CommentDiscussion: StyledIcon<any>;
export declare const CommentDiscussionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
