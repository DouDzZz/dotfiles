import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Markdown: StyledIcon<any>;
export declare const MarkdownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
