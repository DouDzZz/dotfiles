import { StyledIcon, StyledIconProps } from '..';
export declare const LinkExternal: StyledIcon<any>;
export declare const LinkExternalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
