import { StyledIcon, StyledIconProps } from '..';
export declare const Milestone: StyledIcon<any>;
export declare const MilestoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
