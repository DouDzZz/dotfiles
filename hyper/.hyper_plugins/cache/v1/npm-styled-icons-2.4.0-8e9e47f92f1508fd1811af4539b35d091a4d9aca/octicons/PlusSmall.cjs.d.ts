import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlusSmall: StyledIcon<any>;
export declare const PlusSmallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
