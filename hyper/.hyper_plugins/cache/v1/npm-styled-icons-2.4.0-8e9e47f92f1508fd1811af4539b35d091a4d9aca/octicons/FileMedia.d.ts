import { StyledIcon, StyledIconProps } from '..';
export declare const FileMedia: StyledIcon<any>;
export declare const FileMediaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
