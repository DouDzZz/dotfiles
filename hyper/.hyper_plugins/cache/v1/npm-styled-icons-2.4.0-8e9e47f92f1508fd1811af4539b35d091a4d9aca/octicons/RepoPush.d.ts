import { StyledIcon, StyledIconProps } from '..';
export declare const RepoPush: StyledIcon<any>;
export declare const RepoPushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
