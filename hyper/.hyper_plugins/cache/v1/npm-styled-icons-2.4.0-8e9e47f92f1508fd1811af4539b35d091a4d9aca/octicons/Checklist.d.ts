import { StyledIcon, StyledIconProps } from '..';
export declare const Checklist: StyledIcon<any>;
export declare const ChecklistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
