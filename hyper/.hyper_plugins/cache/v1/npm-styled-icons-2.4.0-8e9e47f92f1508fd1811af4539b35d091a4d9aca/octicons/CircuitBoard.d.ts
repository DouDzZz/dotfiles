import { StyledIcon, StyledIconProps } from '..';
export declare const CircuitBoard: StyledIcon<any>;
export declare const CircuitBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
