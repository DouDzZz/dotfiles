import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flame: StyledIcon<any>;
export declare const FlameDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
