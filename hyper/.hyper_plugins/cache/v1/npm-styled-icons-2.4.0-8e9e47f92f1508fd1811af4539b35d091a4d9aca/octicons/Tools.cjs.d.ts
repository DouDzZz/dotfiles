import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tools: StyledIcon<any>;
export declare const ToolsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
