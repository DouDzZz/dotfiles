import { StyledIcon, StyledIconProps } from '..';
export declare const GitCommit: StyledIcon<any>;
export declare const GitCommitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
