import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Alert: StyledIcon<any>;
export declare const AlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
