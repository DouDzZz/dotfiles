import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Squirrel: StyledIcon<any>;
export declare const SquirrelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
