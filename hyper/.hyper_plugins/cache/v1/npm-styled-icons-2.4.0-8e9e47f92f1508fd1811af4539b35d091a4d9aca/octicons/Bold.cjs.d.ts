import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bold: StyledIcon<any>;
export declare const BoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
