import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowSmallDown: StyledIcon<any>;
export declare const ArrowSmallDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
