import { StyledIcon, StyledIconProps } from '..';
export declare const Grabber: StyledIcon<any>;
export declare const GrabberDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
