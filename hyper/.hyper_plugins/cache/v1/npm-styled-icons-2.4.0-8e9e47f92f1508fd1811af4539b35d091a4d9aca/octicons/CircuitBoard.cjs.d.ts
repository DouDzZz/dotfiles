import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CircuitBoard: StyledIcon<any>;
export declare const CircuitBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
