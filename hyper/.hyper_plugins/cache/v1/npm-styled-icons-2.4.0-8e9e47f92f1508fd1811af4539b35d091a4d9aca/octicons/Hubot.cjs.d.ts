import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hubot: StyledIcon<any>;
export declare const HubotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
