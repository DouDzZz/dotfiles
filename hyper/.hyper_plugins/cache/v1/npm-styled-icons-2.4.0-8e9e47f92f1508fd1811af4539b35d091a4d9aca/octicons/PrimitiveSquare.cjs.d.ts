import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PrimitiveSquare: StyledIcon<any>;
export declare const PrimitiveSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
