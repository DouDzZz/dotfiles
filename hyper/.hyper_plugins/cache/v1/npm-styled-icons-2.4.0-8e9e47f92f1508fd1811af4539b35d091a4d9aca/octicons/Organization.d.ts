import { StyledIcon, StyledIconProps } from '..';
export declare const Organization: StyledIcon<any>;
export declare const OrganizationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
