import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dash: StyledIcon<any>;
export declare const DashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
