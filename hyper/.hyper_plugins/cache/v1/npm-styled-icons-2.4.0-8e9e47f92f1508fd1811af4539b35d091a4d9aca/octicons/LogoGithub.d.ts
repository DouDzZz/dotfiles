import { StyledIcon, StyledIconProps } from '..';
export declare const LogoGithub: StyledIcon<any>;
export declare const LogoGithubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
