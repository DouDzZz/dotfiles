import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Versions: StyledIcon<any>;
export declare const VersionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
