import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unverified: StyledIcon<any>;
export declare const UnverifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
