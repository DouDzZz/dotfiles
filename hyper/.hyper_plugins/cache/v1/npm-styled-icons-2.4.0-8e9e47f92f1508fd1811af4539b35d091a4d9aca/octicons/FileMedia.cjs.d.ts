import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileMedia: StyledIcon<any>;
export declare const FileMediaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
