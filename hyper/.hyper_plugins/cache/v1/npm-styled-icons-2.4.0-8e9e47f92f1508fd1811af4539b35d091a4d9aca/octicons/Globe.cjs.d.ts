import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Globe: StyledIcon<any>;
export declare const GlobeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
