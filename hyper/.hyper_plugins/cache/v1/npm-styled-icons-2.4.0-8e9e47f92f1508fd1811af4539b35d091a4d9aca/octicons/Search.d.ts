import { StyledIcon, StyledIconProps } from '..';
export declare const Search: StyledIcon<any>;
export declare const SearchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
