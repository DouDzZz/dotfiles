import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Shield: StyledIcon<any>;
export declare const ShieldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
