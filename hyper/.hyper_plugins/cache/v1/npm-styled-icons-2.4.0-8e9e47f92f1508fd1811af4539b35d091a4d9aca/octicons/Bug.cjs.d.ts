import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bug: StyledIcon<any>;
export declare const BugDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
