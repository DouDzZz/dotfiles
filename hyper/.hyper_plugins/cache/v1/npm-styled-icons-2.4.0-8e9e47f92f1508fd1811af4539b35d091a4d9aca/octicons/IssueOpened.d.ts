import { StyledIcon, StyledIconProps } from '..';
export declare const IssueOpened: StyledIcon<any>;
export declare const IssueOpenedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
