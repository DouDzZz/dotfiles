import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Verified: StyledIcon<any>;
export declare const VerifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
