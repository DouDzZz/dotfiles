var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var ArrowSmallLeft = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "ArrowSmallLeft-title" }, props.title), React.createElement("path", { fillRule: "evenodd", d: "M4 7V5L0 8l4 3V9h2V7H4z", key: "k0" })
        ]
        : [React.createElement("path", { fillRule: "evenodd", d: "M4 7V5L0 8l4 3V9h2V7H4z", key: "k0" })
        ]); },
    viewBox: '0 0 6 16',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
ArrowSmallLeft.displayName = 'ArrowSmallLeft';
export var ArrowSmallLeftDimensions = { height: 16, width: 6 };
var templateObject_1;
