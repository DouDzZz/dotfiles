import { StyledIcon, StyledIconProps } from '..';
export declare const PrimitiveSquare: StyledIcon<any>;
export declare const PrimitiveSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
