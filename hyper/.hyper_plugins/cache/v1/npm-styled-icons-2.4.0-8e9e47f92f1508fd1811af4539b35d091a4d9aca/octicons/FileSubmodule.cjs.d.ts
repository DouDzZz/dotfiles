import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileSubmodule: StyledIcon<any>;
export declare const FileSubmoduleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
