import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiffAdded: StyledIcon<any>;
export declare const DiffAddedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
