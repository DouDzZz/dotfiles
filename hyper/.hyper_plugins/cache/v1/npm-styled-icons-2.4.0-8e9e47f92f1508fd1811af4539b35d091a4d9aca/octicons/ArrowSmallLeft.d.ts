import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowSmallLeft: StyledIcon<any>;
export declare const ArrowSmallLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
