import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Reply: StyledIcon<any>;
export declare const ReplyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
