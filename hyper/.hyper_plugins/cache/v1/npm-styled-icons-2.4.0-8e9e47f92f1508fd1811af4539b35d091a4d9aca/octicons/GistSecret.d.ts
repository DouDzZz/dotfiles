import { StyledIcon, StyledIconProps } from '..';
export declare const GistSecret: StyledIcon<any>;
export declare const GistSecretDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
