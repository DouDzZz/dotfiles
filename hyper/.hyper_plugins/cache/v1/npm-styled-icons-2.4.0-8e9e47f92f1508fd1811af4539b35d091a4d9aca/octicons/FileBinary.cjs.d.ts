import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileBinary: StyledIcon<any>;
export declare const FileBinaryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
