import { StyledIcon, StyledIconProps } from '..';
export declare const SignIn: StyledIcon<any>;
export declare const SignInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
