import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tasklist: StyledIcon<any>;
export declare const TasklistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
