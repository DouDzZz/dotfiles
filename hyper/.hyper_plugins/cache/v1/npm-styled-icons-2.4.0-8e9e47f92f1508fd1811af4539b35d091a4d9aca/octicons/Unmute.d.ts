import { StyledIcon, StyledIconProps } from '..';
export declare const Unmute: StyledIcon<any>;
export declare const UnmuteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
