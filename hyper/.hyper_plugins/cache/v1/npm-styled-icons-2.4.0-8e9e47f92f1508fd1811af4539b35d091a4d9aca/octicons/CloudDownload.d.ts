import { StyledIcon, StyledIconProps } from '..';
export declare const CloudDownload: StyledIcon<any>;
export declare const CloudDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
