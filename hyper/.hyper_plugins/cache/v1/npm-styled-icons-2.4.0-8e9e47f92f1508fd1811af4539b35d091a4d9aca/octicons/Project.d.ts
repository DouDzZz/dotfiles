import { StyledIcon, StyledIconProps } from '..';
export declare const Project: StyledIcon<any>;
export declare const ProjectDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
