import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Organization: StyledIcon<any>;
export declare const OrganizationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
