import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gift: StyledIcon<any>;
export declare const GiftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
