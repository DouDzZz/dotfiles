import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiffModified: StyledIcon<any>;
export declare const DiffModifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
