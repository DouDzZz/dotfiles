import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Italic: StyledIcon<any>;
export declare const ItalicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
