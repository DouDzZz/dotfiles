import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarkGithub: StyledIcon<any>;
export declare const MarkGithubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
