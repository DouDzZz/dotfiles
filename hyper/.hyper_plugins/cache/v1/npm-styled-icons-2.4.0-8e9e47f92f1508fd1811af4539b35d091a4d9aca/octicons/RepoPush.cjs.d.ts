import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RepoPush: StyledIcon<any>;
export declare const RepoPushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
