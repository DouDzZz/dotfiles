import { StyledIcon, StyledIconProps } from '..';
export declare const Location: StyledIcon<any>;
export declare const LocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
