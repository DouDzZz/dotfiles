import { StyledIcon, StyledIconProps } from '..';
export declare const Tasklist: StyledIcon<any>;
export declare const TasklistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
