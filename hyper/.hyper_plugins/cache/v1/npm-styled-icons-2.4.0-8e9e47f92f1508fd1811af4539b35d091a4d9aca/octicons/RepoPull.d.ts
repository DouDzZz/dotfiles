import { StyledIcon, StyledIconProps } from '..';
export declare const RepoPull: StyledIcon<any>;
export declare const RepoPullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
