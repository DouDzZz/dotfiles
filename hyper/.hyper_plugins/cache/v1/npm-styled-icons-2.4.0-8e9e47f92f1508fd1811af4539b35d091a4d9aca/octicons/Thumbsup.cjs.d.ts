import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Thumbsup: StyledIcon<any>;
export declare const ThumbsupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
