import { StyledIcon, StyledIconProps } from '..';
export declare const Alert: StyledIcon<any>;
export declare const AlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
