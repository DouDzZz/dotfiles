import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Telescope: StyledIcon<any>;
export declare const TelescopeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
