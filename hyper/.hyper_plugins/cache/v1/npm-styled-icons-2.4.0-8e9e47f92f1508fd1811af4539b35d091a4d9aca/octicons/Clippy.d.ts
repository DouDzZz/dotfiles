import { StyledIcon, StyledIconProps } from '..';
export declare const Clippy: StyledIcon<any>;
export declare const ClippyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
