import { StyledIcon, StyledIconProps } from '..';
export declare const Ruby: StyledIcon<any>;
export declare const RubyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
