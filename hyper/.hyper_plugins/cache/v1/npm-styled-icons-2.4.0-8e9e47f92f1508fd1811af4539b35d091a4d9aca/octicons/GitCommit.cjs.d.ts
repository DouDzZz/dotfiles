import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitCommit: StyledIcon<any>;
export declare const GitCommitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
