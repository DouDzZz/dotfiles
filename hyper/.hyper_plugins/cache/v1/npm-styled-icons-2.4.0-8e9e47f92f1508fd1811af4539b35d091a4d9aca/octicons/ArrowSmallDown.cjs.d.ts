import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowSmallDown: StyledIcon<any>;
export declare const ArrowSmallDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
