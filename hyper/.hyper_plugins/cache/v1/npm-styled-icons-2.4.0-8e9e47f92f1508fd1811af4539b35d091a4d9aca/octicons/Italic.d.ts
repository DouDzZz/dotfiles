import { StyledIcon, StyledIconProps } from '..';
export declare const Italic: StyledIcon<any>;
export declare const ItalicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
