import { StyledIcon, StyledIconProps } from '..';
export declare const Jersey: StyledIcon<any>;
export declare const JerseyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
