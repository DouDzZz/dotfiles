import { StyledIcon, StyledIconProps } from '..';
export declare const Question: StyledIcon<any>;
export declare const QuestionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
