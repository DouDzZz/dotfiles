import { StyledIcon, StyledIconProps } from '..';
export declare const DeviceMobile: StyledIcon<any>;
export declare const DeviceMobileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
