import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Project: StyledIcon<any>;
export declare const ProjectDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
