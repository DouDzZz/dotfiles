import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Watch: StyledIcon<any>;
export declare const WatchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
