import { StyledIcon, StyledIconProps } from '..';
export declare const Pencil: StyledIcon<any>;
export declare const PencilDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
