import { StyledIcon, StyledIconProps } from '..';
export declare const Law: StyledIcon<any>;
export declare const LawDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
