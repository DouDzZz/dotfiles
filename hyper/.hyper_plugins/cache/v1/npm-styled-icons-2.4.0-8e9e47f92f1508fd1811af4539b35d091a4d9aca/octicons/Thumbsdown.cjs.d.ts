import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Thumbsdown: StyledIcon<any>;
export declare const ThumbsdownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
