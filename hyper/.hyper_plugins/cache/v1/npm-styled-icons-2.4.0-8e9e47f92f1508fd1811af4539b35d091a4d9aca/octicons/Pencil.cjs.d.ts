import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pencil: StyledIcon<any>;
export declare const PencilDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
