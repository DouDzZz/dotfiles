import { StyledIcon, StyledIconProps } from '..';
export declare const Bold: StyledIcon<any>;
export declare const BoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
