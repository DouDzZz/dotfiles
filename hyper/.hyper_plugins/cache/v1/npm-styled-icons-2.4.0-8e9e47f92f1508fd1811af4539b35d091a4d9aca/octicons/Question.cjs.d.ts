import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Question: StyledIcon<any>;
export declare const QuestionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
