import { StyledIcon, StyledIconProps } from '..';
export declare const DiffModified: StyledIcon<any>;
export declare const DiffModifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
