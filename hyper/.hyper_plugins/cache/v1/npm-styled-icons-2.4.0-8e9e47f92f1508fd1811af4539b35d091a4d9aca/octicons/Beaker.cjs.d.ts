import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Beaker: StyledIcon<any>;
export declare const BeakerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
