import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LightBulb: StyledIcon<any>;
export declare const LightBulbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
