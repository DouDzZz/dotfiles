import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignOut: StyledIcon<any>;
export declare const SignOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
