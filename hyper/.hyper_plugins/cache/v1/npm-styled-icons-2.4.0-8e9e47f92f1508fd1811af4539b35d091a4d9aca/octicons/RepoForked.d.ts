import { StyledIcon, StyledIconProps } from '..';
export declare const RepoForked: StyledIcon<any>;
export declare const RepoForkedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
