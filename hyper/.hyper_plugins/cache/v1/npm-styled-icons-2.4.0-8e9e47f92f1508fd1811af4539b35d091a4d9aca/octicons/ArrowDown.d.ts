import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDown: StyledIcon<any>;
export declare const ArrowDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
