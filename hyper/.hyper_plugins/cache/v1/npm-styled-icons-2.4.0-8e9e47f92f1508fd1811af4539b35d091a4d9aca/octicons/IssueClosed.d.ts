import { StyledIcon, StyledIconProps } from '..';
export declare const IssueClosed: StyledIcon<any>;
export declare const IssueClosedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
