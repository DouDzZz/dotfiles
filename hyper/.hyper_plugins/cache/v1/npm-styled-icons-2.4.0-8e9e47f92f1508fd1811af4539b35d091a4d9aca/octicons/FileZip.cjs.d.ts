import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileZip: StyledIcon<any>;
export declare const FileZipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
