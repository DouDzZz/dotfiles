import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TriangleDown: StyledIcon<any>;
export declare const TriangleDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
