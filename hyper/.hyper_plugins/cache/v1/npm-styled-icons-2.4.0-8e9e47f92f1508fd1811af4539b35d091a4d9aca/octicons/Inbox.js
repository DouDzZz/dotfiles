var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Inbox = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Inbox-title" }, props.title), React.createElement("path", { fillRule: "evenodd", d: "M14 9l-1.13-7.14c-.08-.48-.5-.86-1-.86H2.13c-.5 0-.92.38-1 .86L0 9v5c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V9zm-3.28.55l-.44.89c-.17.34-.52.56-.91.56H4.61c-.38 0-.72-.22-.89-.55l-.44-.91c-.17-.33-.52-.55-.89-.55H1l1-7h10l1 7h-1.38c-.39 0-.73.22-.91.55l.01.01z", key: "k0" })
        ]
        : [React.createElement("path", { fillRule: "evenodd", d: "M14 9l-1.13-7.14c-.08-.48-.5-.86-1-.86H2.13c-.5 0-.92.38-1 .86L0 9v5c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V9zm-3.28.55l-.44.89c-.17.34-.52.56-.91.56H4.61c-.38 0-.72-.22-.89-.55l-.44-.91c-.17-.33-.52-.55-.89-.55H1l1-7h10l1 7h-1.38c-.39 0-.73.22-.91.55l.01.01z", key: "k0" })
        ]); },
    viewBox: '0 0 14 16',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Inbox.displayName = 'Inbox';
export var InboxDimensions = { height: 16, width: 14 };
var templateObject_1;
