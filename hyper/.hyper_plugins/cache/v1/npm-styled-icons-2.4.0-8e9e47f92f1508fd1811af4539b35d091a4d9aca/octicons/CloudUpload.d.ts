import { StyledIcon, StyledIconProps } from '..';
export declare const CloudUpload: StyledIcon<any>;
export declare const CloudUploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
