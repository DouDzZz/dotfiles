import { StyledIcon, StyledIconProps } from '..';
export declare const Plus: StyledIcon<any>;
export declare const PlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
