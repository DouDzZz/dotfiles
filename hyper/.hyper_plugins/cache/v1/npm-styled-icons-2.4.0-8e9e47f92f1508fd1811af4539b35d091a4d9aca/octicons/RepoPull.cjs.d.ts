import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RepoPull: StyledIcon<any>;
export declare const RepoPullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
