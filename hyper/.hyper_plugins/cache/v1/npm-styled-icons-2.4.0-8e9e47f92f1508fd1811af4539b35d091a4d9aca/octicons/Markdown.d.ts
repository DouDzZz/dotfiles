import { StyledIcon, StyledIconProps } from '..';
export declare const Markdown: StyledIcon<any>;
export declare const MarkdownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
