import { StyledIcon, StyledIconProps } from '..';
export declare const Book: StyledIcon<any>;
export declare const BookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
