import { StyledIcon, StyledIconProps } from '..';
export declare const Plug: StyledIcon<any>;
export declare const PlugDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
