import { StyledIcon, StyledIconProps } from '..';
export declare const DeviceDesktop: StyledIcon<any>;
export declare const DeviceDesktopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
