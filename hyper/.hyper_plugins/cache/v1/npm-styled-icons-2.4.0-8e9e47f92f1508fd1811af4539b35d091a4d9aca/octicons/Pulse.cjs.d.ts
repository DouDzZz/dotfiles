import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pulse: StyledIcon<any>;
export declare const PulseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
