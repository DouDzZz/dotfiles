import { StyledIcon, StyledIconProps } from '..';
export declare const Bookmark: StyledIcon<any>;
export declare const BookmarkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
