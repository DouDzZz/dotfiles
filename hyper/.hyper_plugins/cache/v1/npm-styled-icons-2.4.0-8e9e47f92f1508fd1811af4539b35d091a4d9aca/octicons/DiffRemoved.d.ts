import { StyledIcon, StyledIconProps } from '..';
export declare const DiffRemoved: StyledIcon<any>;
export declare const DiffRemovedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
