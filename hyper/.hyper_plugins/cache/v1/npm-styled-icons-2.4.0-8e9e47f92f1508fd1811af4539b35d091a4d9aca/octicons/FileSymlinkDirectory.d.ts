import { StyledIcon, StyledIconProps } from '..';
export declare const FileSymlinkDirectory: StyledIcon<any>;
export declare const FileSymlinkDirectoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
