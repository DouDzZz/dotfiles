import { StyledIcon, StyledIconProps } from '..';
export declare const Telescope: StyledIcon<any>;
export declare const TelescopeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
