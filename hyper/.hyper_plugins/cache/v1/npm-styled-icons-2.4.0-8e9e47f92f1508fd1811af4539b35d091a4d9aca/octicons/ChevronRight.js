var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var ChevronRight = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "ChevronRight-title" }, props.title), React.createElement("path", { fillRule: "evenodd", d: "M7.5 8l-5 5L1 11.5 4.75 8 1 4.5 2.5 3l5 5z", key: "k0" })
        ]
        : [React.createElement("path", { fillRule: "evenodd", d: "M7.5 8l-5 5L1 11.5 4.75 8 1 4.5 2.5 3l5 5z", key: "k0" })
        ]); },
    viewBox: '0 0 8 16',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
ChevronRight.displayName = 'ChevronRight';
export var ChevronRightDimensions = { height: 16, width: 8 };
var templateObject_1;
