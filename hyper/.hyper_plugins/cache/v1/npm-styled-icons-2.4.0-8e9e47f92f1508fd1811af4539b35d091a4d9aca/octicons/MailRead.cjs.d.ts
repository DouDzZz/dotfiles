import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MailRead: StyledIcon<any>;
export declare const MailReadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
