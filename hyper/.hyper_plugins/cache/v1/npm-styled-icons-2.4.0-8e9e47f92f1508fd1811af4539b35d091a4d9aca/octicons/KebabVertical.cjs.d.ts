import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KebabVertical: StyledIcon<any>;
export declare const KebabVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
