import { StyledIcon, StyledIconProps } from '..';
export declare const Megaphone: StyledIcon<any>;
export declare const MegaphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
