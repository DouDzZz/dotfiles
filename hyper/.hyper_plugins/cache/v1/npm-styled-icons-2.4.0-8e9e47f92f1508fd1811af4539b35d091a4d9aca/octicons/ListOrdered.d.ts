import { StyledIcon, StyledIconProps } from '..';
export declare const ListOrdered: StyledIcon<any>;
export declare const ListOrderedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
