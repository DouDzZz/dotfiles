import { StyledIcon, StyledIconProps } from '..';
export declare const MortarBoard: StyledIcon<any>;
export declare const MortarBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
