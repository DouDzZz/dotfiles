import { StyledIcon, StyledIconProps } from '..';
export declare const Pulse: StyledIcon<any>;
export declare const PulseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
