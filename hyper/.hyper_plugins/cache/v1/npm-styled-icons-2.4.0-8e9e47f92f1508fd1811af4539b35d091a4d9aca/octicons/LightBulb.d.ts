import { StyledIcon, StyledIconProps } from '..';
export declare const LightBulb: StyledIcon<any>;
export declare const LightBulbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
