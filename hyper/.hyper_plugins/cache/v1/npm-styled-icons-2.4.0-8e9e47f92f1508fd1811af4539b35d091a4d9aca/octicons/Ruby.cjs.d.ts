import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ruby: StyledIcon<any>;
export declare const RubyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
