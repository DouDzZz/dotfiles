import { StyledIcon, StyledIconProps } from '..';
export declare const SignOut: StyledIcon<any>;
export declare const SignOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
