import { StyledIcon, StyledIconProps } from '..';
export declare const Settings: StyledIcon<any>;
export declare const SettingsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
