import { StyledIcon, StyledIconProps } from '..';
export declare const Broadcast: StyledIcon<any>;
export declare const BroadcastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
