import { StyledIcon, StyledIconProps } from '..';
export declare const Paintcan: StyledIcon<any>;
export declare const PaintcanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
