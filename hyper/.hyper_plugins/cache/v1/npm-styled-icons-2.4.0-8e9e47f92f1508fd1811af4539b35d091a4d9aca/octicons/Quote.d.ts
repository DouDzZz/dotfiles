import { StyledIcon, StyledIconProps } from '..';
export declare const Quote: StyledIcon<any>;
export declare const QuoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
