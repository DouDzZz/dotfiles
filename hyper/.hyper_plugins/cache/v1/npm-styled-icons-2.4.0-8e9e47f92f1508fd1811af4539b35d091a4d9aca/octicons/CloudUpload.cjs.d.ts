import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudUpload: StyledIcon<any>;
export declare const CloudUploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
