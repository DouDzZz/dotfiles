import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CircleSlash: StyledIcon<any>;
export declare const CircleSlashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
