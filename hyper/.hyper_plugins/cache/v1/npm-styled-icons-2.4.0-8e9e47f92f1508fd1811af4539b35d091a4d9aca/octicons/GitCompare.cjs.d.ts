import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitCompare: StyledIcon<any>;
export declare const GitCompareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
