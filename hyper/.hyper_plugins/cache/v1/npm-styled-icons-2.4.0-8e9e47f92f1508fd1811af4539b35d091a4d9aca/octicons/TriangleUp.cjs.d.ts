import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TriangleUp: StyledIcon<any>;
export declare const TriangleUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
