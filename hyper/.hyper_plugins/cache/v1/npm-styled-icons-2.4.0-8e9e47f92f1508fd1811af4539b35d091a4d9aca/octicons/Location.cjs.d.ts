import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Location: StyledIcon<any>;
export declare const LocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
