import { StyledIcon, StyledIconProps } from '..';
export declare const Trashcan: StyledIcon<any>;
export declare const TrashcanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
