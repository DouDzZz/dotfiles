import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Milestone: StyledIcon<any>;
export declare const MilestoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
