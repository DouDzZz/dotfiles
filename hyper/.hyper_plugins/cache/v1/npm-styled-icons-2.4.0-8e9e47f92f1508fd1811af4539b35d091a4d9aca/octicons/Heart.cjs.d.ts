import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Heart: StyledIcon<any>;
export declare const HeartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
