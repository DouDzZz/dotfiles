import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenNormal: StyledIcon<any>;
export declare const ScreenNormalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
