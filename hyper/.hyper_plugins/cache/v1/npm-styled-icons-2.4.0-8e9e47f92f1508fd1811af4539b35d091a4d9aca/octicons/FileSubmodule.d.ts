import { StyledIcon, StyledIconProps } from '..';
export declare const FileSubmodule: StyledIcon<any>;
export declare const FileSubmoduleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
