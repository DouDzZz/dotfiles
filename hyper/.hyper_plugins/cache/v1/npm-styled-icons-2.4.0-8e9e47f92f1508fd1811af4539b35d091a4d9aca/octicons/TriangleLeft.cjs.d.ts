import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TriangleLeft: StyledIcon<any>;
export declare const TriangleLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
