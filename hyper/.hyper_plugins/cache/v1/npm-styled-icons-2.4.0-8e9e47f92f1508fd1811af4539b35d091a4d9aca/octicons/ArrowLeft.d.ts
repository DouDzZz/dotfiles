import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowLeft: StyledIcon<any>;
export declare const ArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
