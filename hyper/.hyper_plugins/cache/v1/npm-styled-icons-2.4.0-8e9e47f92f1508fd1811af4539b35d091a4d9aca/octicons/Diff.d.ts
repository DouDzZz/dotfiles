import { StyledIcon, StyledIconProps } from '..';
export declare const Diff: StyledIcon<any>;
export declare const DiffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
