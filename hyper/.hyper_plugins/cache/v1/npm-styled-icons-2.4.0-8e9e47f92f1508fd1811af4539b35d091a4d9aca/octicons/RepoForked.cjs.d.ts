import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RepoForked: StyledIcon<any>;
export declare const RepoForkedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
