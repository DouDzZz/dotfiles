import { StyledIcon, StyledIconProps } from '..';
export declare const DesktopDownload: StyledIcon<any>;
export declare const DesktopDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
