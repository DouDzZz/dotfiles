import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bell: StyledIcon<any>;
export declare const BellDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
