import { StyledIcon, StyledIconProps } from '..';
export declare const Key: StyledIcon<any>;
export declare const KeyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
