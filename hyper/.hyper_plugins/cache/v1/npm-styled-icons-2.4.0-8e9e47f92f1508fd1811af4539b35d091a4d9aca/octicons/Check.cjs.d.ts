import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Check: StyledIcon<any>;
export declare const CheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
