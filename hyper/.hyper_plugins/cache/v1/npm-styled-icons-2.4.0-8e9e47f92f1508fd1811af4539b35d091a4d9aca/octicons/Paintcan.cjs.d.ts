import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paintcan: StyledIcon<any>;
export declare const PaintcanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
