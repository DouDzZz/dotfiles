import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Megaphone: StyledIcon<any>;
export declare const MegaphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
