import { StyledIcon, StyledIconProps } from '..';
export declare const GitCompare: StyledIcon<any>;
export declare const GitCompareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
