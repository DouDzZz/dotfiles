import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Octoface: StyledIcon<any>;
export declare const OctofaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
