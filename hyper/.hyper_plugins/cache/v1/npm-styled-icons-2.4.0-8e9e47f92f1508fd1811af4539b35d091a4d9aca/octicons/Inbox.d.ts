import { StyledIcon, StyledIconProps } from '..';
export declare const Inbox: StyledIcon<any>;
export declare const InboxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
