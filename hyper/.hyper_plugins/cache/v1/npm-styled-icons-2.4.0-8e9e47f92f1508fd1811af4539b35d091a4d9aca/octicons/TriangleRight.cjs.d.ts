import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TriangleRight: StyledIcon<any>;
export declare const TriangleRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
