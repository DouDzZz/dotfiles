import { StyledIcon, StyledIconProps } from '..';
export declare const RadioTower: StyledIcon<any>;
export declare const RadioTowerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
