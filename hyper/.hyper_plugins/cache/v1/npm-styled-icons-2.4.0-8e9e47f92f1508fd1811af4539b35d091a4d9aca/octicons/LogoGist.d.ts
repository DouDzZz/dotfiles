import { StyledIcon, StyledIconProps } from '..';
export declare const LogoGist: StyledIcon<any>;
export declare const LogoGistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
