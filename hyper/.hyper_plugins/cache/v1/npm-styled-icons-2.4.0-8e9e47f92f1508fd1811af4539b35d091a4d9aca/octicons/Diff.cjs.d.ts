import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Diff: StyledIcon<any>;
export declare const DiffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
