import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const X: StyledIcon<any>;
export declare const XDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
