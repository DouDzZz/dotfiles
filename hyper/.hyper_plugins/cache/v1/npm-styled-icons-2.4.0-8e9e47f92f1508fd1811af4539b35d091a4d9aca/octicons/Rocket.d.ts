import { StyledIcon, StyledIconProps } from '..';
export declare const Rocket: StyledIcon<any>;
export declare const RocketDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
