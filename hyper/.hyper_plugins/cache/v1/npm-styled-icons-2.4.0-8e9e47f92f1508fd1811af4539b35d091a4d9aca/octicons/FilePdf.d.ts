import { StyledIcon, StyledIconProps } from '..';
export declare const FilePdf: StyledIcon<any>;
export declare const FilePdfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
