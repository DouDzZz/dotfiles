import { StyledIcon, StyledIconProps } from '..';
export declare const Mirror: StyledIcon<any>;
export declare const MirrorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
