import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GistSecret: StyledIcon<any>;
export declare const GistSecretDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
