import { StyledIcon, StyledIconProps } from '..';
export declare const Rss: StyledIcon<any>;
export declare const RssDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
