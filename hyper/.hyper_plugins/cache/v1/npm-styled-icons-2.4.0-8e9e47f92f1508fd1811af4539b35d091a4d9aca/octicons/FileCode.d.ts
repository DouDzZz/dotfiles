import { StyledIcon, StyledIconProps } from '..';
export declare const FileCode: StyledIcon<any>;
export declare const FileCodeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
