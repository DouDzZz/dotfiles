import { StyledIcon, StyledIconProps } from '..';
export declare const Mention: StyledIcon<any>;
export declare const MentionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
