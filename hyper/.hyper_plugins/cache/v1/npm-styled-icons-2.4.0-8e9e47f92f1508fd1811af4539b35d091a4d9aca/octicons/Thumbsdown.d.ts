import { StyledIcon, StyledIconProps } from '..';
export declare const Thumbsdown: StyledIcon<any>;
export declare const ThumbsdownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
