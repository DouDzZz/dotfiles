import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fold: StyledIcon<any>;
export declare const FoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
