import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RepoForcePush: StyledIcon<any>;
export declare const RepoForcePushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
