import { StyledIcon, StyledIconProps } from '..';
export declare const Gist: StyledIcon<any>;
export declare const GistDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
