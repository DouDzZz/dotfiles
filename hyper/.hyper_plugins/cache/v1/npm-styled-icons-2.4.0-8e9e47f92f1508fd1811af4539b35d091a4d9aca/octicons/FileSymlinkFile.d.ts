import { StyledIcon, StyledIconProps } from '..';
export declare const FileSymlinkFile: StyledIcon<any>;
export declare const FileSymlinkFileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
