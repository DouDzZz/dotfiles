import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenNormal: StyledIcon<any>;
export declare const ScreenNormalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
