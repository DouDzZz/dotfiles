import { StyledIcon, StyledIconProps } from '..';
export declare const MarkGithub: StyledIcon<any>;
export declare const MarkGithubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
