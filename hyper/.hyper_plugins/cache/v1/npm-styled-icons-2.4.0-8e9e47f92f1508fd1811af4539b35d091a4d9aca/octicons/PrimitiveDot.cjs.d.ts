import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PrimitiveDot: StyledIcon<any>;
export declare const PrimitiveDotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
