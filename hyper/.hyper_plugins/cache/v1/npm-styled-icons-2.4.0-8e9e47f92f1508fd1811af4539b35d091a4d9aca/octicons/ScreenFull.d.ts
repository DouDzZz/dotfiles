import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenFull: StyledIcon<any>;
export declare const ScreenFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
