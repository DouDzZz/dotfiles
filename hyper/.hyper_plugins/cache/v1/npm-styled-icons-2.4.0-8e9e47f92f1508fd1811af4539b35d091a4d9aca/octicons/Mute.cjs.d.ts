import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mute: StyledIcon<any>;
export declare const MuteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
