import { StyledIcon, StyledIconProps } from '..';
export declare const Repo: StyledIcon<any>;
export declare const RepoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
