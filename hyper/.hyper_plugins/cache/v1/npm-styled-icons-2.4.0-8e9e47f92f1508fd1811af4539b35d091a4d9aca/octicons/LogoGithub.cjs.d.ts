import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LogoGithub: StyledIcon<any>;
export declare const LogoGithubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
