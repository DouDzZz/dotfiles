import { StyledIcon, StyledIconProps } from '..';
export declare const Unverified: StyledIcon<any>;
export declare const UnverifiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
