import { StyledIcon, StyledIconProps } from '..';
export declare const Tag: StyledIcon<any>;
export declare const TagDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
