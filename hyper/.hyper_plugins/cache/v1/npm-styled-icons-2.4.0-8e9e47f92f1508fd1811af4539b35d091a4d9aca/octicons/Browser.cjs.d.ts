import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Browser: StyledIcon<any>;
export declare const BrowserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
