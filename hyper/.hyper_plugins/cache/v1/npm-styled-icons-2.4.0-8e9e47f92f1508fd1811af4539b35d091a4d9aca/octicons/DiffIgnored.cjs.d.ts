import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiffIgnored: StyledIcon<any>;
export declare const DiffIgnoredDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
