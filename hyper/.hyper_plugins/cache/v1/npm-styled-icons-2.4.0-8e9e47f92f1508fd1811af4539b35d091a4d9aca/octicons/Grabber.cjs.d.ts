import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Grabber: StyledIcon<any>;
export declare const GrabberDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
