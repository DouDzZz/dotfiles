import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mention: StyledIcon<any>;
export declare const MentionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
