import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HorizontalRule: StyledIcon<any>;
export declare const HorizontalRuleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
