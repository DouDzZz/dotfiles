import { StyledIcon, StyledIconProps } from '..';
export declare const ThreeBars: StyledIcon<any>;
export declare const ThreeBarsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
