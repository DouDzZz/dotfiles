import { StyledIcon, StyledIconProps } from '..';
export declare const Shield: StyledIcon<any>;
export declare const ShieldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
