var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var PlusSmall = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "PlusSmall-title" }, props.title), React.createElement("path", { fillRule: "evenodd", d: "M4 4H3v3H0v1h3v3h1V8h3V7H4V4z", key: "k0" })
        ]
        : [React.createElement("path", { fillRule: "evenodd", d: "M4 4H3v3H0v1h3v3h1V8h3V7H4V4z", key: "k0" })
        ]); },
    viewBox: '0 0 7 16',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
PlusSmall.displayName = 'PlusSmall';
export var PlusSmallDimensions = { height: 16, width: 7 };
var templateObject_1;
