import { StyledIcon, StyledIconProps } from '..';
export declare const Gear: StyledIcon<any>;
export declare const GearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
