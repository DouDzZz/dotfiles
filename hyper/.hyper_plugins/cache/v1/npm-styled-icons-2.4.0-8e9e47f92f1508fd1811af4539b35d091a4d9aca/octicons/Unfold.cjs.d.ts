import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unfold: StyledIcon<any>;
export declare const UnfoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
