import { StyledIcon, StyledIconProps } from '..';
export declare const RepoForcePush: StyledIcon<any>;
export declare const RepoForcePushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
