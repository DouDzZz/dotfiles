import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rocket: StyledIcon<any>;
export declare const RocketDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
