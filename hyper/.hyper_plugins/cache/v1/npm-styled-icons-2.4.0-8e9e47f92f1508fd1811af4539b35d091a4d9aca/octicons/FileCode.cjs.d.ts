import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileCode: StyledIcon<any>;
export declare const FileCodeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
