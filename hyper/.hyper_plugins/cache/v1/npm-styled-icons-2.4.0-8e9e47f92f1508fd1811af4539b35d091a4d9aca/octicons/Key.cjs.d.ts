import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Key: StyledIcon<any>;
export declare const KeyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
