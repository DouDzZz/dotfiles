import { StyledIcon, StyledIconProps } from '..';
export declare const PlusSmall: StyledIcon<any>;
export declare const PlusSmallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
