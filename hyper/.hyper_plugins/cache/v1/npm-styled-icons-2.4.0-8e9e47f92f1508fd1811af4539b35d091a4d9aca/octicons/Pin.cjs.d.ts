import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pin: StyledIcon<any>;
export declare const PinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
