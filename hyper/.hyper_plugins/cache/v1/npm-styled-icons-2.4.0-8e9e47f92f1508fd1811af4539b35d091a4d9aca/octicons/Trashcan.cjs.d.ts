import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Trashcan: StyledIcon<any>;
export declare const TrashcanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
