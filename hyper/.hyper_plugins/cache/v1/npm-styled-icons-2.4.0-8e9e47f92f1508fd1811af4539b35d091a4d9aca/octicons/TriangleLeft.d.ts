import { StyledIcon, StyledIconProps } from '..';
export declare const TriangleLeft: StyledIcon<any>;
export declare const TriangleLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
