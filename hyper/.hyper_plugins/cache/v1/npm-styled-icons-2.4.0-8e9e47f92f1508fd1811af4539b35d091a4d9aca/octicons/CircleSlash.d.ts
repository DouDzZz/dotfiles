import { StyledIcon, StyledIconProps } from '..';
export declare const CircleSlash: StyledIcon<any>;
export declare const CircleSlashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
