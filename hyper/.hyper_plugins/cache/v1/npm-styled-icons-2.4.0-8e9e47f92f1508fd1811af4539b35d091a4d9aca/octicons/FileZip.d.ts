import { StyledIcon, StyledIconProps } from '..';
export declare const FileZip: StyledIcon<any>;
export declare const FileZipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
