import { StyledIcon, StyledIconProps } from '..';
export declare const DeviceCameraVideo: StyledIcon<any>;
export declare const DeviceCameraVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
