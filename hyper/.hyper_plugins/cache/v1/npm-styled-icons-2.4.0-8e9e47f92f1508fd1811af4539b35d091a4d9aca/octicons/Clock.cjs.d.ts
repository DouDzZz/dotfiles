import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Clock: StyledIcon<any>;
export declare const ClockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
