import { StyledIcon, StyledIconProps } from '..';
export declare const RepoClone: StyledIcon<any>;
export declare const RepoCloneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
