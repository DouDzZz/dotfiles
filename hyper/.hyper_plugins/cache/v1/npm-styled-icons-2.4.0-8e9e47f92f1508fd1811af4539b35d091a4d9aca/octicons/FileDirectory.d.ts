import { StyledIcon, StyledIconProps } from '..';
export declare const FileDirectory: StyledIcon<any>;
export declare const FileDirectoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
