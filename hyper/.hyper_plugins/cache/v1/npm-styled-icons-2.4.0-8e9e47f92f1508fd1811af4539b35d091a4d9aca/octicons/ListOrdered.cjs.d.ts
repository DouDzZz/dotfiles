import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ListOrdered: StyledIcon<any>;
export declare const ListOrderedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
