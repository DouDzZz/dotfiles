import { StyledIcon, StyledIconProps } from '..';
export declare const PrimitiveDot: StyledIcon<any>;
export declare const PrimitiveDotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
