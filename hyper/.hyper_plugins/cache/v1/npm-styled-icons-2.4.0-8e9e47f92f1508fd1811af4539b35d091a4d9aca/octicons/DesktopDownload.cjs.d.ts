import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DesktopDownload: StyledIcon<any>;
export declare const DesktopDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
