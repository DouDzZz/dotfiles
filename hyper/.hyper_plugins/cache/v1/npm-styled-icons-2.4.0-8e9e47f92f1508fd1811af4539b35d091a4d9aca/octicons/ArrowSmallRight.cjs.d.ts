import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowSmallRight: StyledIcon<any>;
export declare const ArrowSmallRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
