import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TextSize: StyledIcon<any>;
export declare const TextSizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
