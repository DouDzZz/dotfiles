import { StyledIcon, StyledIconProps } from '..';
export declare const Bug: StyledIcon<any>;
export declare const BugDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
