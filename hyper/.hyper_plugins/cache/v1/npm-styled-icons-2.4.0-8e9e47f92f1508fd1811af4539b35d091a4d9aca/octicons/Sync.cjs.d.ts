import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sync: StyledIcon<any>;
export declare const SyncDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
