import { StyledIcon, StyledIconProps } from '..';
export declare const Mute: StyledIcon<any>;
export declare const MuteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
