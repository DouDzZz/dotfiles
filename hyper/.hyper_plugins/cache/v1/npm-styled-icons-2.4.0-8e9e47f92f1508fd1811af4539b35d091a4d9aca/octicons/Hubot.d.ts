import { StyledIcon, StyledIconProps } from '..';
export declare const Hubot: StyledIcon<any>;
export declare const HubotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
