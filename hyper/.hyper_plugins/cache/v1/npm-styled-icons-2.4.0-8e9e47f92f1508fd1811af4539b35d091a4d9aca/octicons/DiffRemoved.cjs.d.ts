import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiffRemoved: StyledIcon<any>;
export declare const DiffRemovedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
