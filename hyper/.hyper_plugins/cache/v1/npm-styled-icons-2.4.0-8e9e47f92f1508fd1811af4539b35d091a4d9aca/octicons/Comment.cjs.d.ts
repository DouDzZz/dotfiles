import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Comment: StyledIcon<any>;
export declare const CommentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
