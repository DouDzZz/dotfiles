import { StyledIcon, StyledIconProps } from '..';
export declare const Graph: StyledIcon<any>;
export declare const GraphDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
