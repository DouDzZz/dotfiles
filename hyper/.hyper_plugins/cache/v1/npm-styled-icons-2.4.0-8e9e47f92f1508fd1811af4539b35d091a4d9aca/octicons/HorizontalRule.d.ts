import { StyledIcon, StyledIconProps } from '..';
export declare const HorizontalRule: StyledIcon<any>;
export declare const HorizontalRuleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
