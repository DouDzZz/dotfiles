import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DeviceMobile: StyledIcon<any>;
export declare const DeviceMobileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
