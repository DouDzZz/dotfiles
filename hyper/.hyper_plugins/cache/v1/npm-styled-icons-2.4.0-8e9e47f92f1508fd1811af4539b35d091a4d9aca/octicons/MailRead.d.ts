import { StyledIcon, StyledIconProps } from '..';
export declare const MailRead: StyledIcon<any>;
export declare const MailReadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
