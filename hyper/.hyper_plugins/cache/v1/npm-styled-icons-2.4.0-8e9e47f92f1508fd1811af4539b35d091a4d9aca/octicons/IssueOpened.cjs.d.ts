import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IssueOpened: StyledIcon<any>;
export declare const IssueOpenedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
