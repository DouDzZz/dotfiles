import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Java: StyledIcon<any>;
export declare const JavaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
