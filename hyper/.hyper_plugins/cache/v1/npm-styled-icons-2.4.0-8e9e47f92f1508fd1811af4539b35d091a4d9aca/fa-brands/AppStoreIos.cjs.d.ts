import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AppStoreIos: StyledIcon<any>;
export declare const AppStoreIosDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
