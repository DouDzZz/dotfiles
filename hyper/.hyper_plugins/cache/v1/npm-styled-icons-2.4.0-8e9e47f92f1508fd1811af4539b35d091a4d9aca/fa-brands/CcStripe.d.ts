import { StyledIcon, StyledIconProps } from '..';
export declare const CcStripe: StyledIcon<any>;
export declare const CcStripeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
