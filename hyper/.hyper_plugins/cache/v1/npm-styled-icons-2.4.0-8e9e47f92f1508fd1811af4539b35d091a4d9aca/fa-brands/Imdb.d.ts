import { StyledIcon, StyledIconProps } from '..';
export declare const Imdb: StyledIcon<any>;
export declare const ImdbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
