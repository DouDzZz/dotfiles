import { StyledIcon, StyledIconProps } from '..';
export declare const CcDiscover: StyledIcon<any>;
export declare const CcDiscoverDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
