import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcJcb: StyledIcon<any>;
export declare const CcJcbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
