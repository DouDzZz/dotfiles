import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Avianex: StyledIcon<any>;
export declare const AvianexDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
