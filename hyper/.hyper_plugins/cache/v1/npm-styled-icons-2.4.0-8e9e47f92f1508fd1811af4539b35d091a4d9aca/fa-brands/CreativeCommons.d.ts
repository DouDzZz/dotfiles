import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommons: StyledIcon<any>;
export declare const CreativeCommonsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
