import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mix: StyledIcon<any>;
export declare const MixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
