import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcAmex: StyledIcon<any>;
export declare const CcAmexDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
