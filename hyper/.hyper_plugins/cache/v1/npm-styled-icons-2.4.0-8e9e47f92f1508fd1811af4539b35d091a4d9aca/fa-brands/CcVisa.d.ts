import { StyledIcon, StyledIconProps } from '..';
export declare const CcVisa: StyledIcon<any>;
export declare const CcVisaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
