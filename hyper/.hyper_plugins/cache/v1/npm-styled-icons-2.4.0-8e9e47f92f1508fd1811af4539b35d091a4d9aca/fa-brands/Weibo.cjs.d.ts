import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Weibo: StyledIcon<any>;
export declare const WeiboDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
