import { StyledIcon, StyledIconProps } from '..';
export declare const Bandcamp: StyledIcon<any>;
export declare const BandcampDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
