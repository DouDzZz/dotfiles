import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cpanel: StyledIcon<any>;
export declare const CpanelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
