import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GoodreadsG: StyledIcon<any>;
export declare const GoodreadsGDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
