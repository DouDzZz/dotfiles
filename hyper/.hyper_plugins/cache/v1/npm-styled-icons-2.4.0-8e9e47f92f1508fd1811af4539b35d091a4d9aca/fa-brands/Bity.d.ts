import { StyledIcon, StyledIconProps } from '..';
export declare const Bity: StyledIcon<any>;
export declare const BityDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
