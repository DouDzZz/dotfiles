import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FontAwesomeAlt: StyledIcon<any>;
export declare const FontAwesomeAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
