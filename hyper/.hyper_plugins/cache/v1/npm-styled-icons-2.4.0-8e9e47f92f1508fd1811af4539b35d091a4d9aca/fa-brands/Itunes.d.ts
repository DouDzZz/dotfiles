import { StyledIcon, StyledIconProps } from '..';
export declare const Itunes: StyledIcon<any>;
export declare const ItunesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
