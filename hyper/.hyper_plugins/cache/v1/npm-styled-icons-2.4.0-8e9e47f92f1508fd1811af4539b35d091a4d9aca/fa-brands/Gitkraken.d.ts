import { StyledIcon, StyledIconProps } from '..';
export declare const Gitkraken: StyledIcon<any>;
export declare const GitkrakenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
