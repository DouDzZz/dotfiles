import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoenixFramework: StyledIcon<any>;
export declare const PhoenixFrameworkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
