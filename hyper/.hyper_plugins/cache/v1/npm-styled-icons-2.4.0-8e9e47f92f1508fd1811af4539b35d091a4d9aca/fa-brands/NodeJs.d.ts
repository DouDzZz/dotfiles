import { StyledIcon, StyledIconProps } from '..';
export declare const NodeJs: StyledIcon<any>;
export declare const NodeJsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
