import { StyledIcon, StyledIconProps } from '..';
export declare const Osi: StyledIcon<any>;
export declare const OsiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
