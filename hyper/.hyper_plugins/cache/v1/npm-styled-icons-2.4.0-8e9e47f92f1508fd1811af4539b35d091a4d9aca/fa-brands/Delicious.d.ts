import { StyledIcon, StyledIconProps } from '..';
export declare const Delicious: StyledIcon<any>;
export declare const DeliciousDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
