import { StyledIcon, StyledIconProps } from '..';
export declare const Python: StyledIcon<any>;
export declare const PythonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
