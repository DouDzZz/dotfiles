import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ReactLogo: StyledIcon<any>;
export declare const ReactLogoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
