import { StyledIcon, StyledIconProps } from '..';
export declare const Slideshare: StyledIcon<any>;
export declare const SlideshareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
