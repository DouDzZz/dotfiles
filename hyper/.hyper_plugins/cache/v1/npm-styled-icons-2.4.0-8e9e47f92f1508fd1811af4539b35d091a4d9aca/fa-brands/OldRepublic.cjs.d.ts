import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OldRepublic: StyledIcon<any>;
export declare const OldRepublicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
