import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bluetooth: StyledIcon<any>;
export declare const BluetoothDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
