import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FontAwesomeFlag: StyledIcon<any>;
export declare const FontAwesomeFlagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
