import { StyledIcon, StyledIconProps } from '..';
export declare const Cloudscale: StyledIcon<any>;
export declare const CloudscaleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
