import { StyledIcon, StyledIconProps } from '..';
export declare const Sith: StyledIcon<any>;
export declare const SithDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
