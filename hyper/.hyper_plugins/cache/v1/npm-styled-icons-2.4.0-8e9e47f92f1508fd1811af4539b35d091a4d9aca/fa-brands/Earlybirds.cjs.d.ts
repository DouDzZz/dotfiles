import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Earlybirds: StyledIcon<any>;
export declare const EarlybirdsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
