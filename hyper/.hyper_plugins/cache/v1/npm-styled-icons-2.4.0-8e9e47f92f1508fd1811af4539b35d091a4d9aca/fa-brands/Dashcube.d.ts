import { StyledIcon, StyledIconProps } from '..';
export declare const Dashcube: StyledIcon<any>;
export declare const DashcubeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
