import { StyledIcon, StyledIconProps } from '..';
export declare const Vine: StyledIcon<any>;
export declare const VineDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
