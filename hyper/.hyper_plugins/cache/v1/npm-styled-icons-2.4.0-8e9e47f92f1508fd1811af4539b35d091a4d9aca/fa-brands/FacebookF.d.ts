import { StyledIcon, StyledIconProps } from '..';
export declare const FacebookF: StyledIcon<any>;
export declare const FacebookFDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
