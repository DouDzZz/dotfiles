import { StyledIcon, StyledIconProps } from '..';
export declare const Adn: StyledIcon<any>;
export declare const AdnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
