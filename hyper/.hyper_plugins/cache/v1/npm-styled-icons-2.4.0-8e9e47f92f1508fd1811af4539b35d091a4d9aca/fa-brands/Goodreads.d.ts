import { StyledIcon, StyledIconProps } from '..';
export declare const Goodreads: StyledIcon<any>;
export declare const GoodreadsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
