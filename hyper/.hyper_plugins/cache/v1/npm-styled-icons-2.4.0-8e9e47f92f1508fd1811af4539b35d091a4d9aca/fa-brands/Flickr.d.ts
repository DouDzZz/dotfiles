import { StyledIcon, StyledIconProps } from '..';
export declare const Flickr: StyledIcon<any>;
export declare const FlickrDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
