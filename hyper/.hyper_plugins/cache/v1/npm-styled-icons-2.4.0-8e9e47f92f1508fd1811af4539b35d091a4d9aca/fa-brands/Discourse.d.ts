import { StyledIcon, StyledIconProps } from '..';
export declare const Discourse: StyledIcon<any>;
export declare const DiscourseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
