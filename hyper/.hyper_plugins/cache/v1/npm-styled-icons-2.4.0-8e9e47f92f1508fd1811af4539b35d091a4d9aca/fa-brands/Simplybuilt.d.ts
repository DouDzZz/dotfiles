import { StyledIcon, StyledIconProps } from '..';
export declare const Simplybuilt: StyledIcon<any>;
export declare const SimplybuiltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
