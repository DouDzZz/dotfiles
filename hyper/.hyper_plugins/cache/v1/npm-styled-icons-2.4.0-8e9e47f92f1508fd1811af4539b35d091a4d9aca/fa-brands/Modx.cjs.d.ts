import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Modx: StyledIcon<any>;
export declare const ModxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
