import { StyledIcon, StyledIconProps } from '..';
export declare const PhoenixFramework: StyledIcon<any>;
export declare const PhoenixFrameworkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
