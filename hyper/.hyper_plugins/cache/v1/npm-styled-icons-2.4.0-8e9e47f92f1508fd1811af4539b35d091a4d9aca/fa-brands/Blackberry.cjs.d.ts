import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Blackberry: StyledIcon<any>;
export declare const BlackberryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
