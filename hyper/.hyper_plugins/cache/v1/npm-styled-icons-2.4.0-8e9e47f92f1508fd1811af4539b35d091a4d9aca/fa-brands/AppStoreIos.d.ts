import { StyledIcon, StyledIconProps } from '..';
export declare const AppStoreIos: StyledIcon<any>;
export declare const AppStoreIosDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
