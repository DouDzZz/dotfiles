import { StyledIcon, StyledIconProps } from '..';
export declare const SlackHash: StyledIcon<any>;
export declare const SlackHashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
