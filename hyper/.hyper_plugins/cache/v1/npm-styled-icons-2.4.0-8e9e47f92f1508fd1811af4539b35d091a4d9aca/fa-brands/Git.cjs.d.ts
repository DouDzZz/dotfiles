import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Git: StyledIcon<any>;
export declare const GitDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
