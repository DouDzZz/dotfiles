import { StyledIcon, StyledIconProps } from '..';
export declare const Napster: StyledIcon<any>;
export declare const NapsterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
