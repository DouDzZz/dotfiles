import { StyledIcon, StyledIconProps } from '..';
export declare const Ebay: StyledIcon<any>;
export declare const EbayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
