import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rockrms: StyledIcon<any>;
export declare const RockrmsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
