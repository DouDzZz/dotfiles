import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BloggerB: StyledIcon<any>;
export declare const BloggerBDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
