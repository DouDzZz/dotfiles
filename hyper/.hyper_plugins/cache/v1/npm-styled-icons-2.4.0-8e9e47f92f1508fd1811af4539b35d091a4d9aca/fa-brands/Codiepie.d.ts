import { StyledIcon, StyledIconProps } from '..';
export declare const Codiepie: StyledIcon<any>;
export declare const CodiepieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
