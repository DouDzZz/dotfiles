import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paypal: StyledIcon<any>;
export declare const PaypalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
