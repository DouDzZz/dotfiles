import { StyledIcon, StyledIconProps } from '..';
export declare const GooglePlusG: StyledIcon<any>;
export declare const GooglePlusGDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
