import { StyledIcon, StyledIconProps } from '..';
export declare const YoutubeSquare: StyledIcon<any>;
export declare const YoutubeSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
