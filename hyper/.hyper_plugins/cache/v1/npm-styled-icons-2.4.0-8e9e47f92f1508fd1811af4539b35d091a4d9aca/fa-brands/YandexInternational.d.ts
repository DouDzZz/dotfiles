import { StyledIcon, StyledIconProps } from '..';
export declare const YandexInternational: StyledIcon<any>;
export declare const YandexInternationalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
