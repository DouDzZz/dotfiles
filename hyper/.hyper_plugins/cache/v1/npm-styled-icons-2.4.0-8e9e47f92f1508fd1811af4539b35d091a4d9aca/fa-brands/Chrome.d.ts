import { StyledIcon, StyledIconProps } from '..';
export declare const Chrome: StyledIcon<any>;
export declare const ChromeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
