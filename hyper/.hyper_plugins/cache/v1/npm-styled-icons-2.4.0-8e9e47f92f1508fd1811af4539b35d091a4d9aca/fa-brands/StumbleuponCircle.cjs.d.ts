import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StumbleuponCircle: StyledIcon<any>;
export declare const StumbleuponCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
