import { StyledIcon, StyledIconProps } from '..';
export declare const LinkedinIn: StyledIcon<any>;
export declare const LinkedinInDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
