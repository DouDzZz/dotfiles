import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Css3Alt: StyledIcon<any>;
export declare const Css3AltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
