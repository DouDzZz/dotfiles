import { StyledIcon, StyledIconProps } from '..';
export declare const Aws: StyledIcon<any>;
export declare const AwsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
