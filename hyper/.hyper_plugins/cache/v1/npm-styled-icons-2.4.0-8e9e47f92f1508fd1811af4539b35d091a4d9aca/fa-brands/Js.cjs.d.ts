import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Js: StyledIcon<any>;
export declare const JsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
