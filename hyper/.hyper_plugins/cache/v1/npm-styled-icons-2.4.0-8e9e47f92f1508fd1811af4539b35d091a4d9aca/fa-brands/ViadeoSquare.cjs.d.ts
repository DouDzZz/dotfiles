import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViadeoSquare: StyledIcon<any>;
export declare const ViadeoSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
