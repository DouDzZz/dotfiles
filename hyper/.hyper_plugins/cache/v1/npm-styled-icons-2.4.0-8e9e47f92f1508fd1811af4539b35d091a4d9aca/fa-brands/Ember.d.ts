import { StyledIcon, StyledIconProps } from '..';
export declare const Ember: StyledIcon<any>;
export declare const EmberDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
