import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Joomla: StyledIcon<any>;
export declare const JoomlaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
