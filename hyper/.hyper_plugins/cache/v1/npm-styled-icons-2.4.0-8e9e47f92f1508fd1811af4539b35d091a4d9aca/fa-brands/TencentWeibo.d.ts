import { StyledIcon, StyledIconProps } from '..';
export declare const TencentWeibo: StyledIcon<any>;
export declare const TencentWeiboDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
