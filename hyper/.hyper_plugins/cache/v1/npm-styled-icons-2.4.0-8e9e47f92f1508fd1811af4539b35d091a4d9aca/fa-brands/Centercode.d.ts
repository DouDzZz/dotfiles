import { StyledIcon, StyledIconProps } from '..';
export declare const Centercode: StyledIcon<any>;
export declare const CentercodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
