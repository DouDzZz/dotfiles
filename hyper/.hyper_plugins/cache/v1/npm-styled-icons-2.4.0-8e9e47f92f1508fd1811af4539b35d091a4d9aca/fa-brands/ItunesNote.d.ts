import { StyledIcon, StyledIconProps } from '..';
export declare const ItunesNote: StyledIcon<any>;
export declare const ItunesNoteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
