import { StyledIcon, StyledIconProps } from '..';
export declare const CcDinersClub: StyledIcon<any>;
export declare const CcDinersClubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
