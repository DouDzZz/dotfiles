import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcStripe: StyledIcon<any>;
export declare const CcStripeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
