import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fly: StyledIcon<any>;
export declare const FlyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
