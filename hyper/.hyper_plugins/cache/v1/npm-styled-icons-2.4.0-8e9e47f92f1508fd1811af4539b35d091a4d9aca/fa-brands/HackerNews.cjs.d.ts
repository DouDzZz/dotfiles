import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HackerNews: StyledIcon<any>;
export declare const HackerNewsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
