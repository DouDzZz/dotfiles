import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GooglePlus: StyledIcon<any>;
export declare const GooglePlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
