import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Meetup: StyledIcon<any>;
export declare const MeetupDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
