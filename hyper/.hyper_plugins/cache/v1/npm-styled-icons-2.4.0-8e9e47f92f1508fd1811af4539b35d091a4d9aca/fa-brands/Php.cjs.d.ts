import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Php: StyledIcon<any>;
export declare const PhpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
