import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Stripe: StyledIcon<any>;
export declare const StripeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
