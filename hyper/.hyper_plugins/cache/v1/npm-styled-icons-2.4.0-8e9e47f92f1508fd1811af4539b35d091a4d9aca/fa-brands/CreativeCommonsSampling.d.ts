import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsSampling: StyledIcon<any>;
export declare const CreativeCommonsSamplingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
