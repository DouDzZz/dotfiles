import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Vaadin: StyledIcon<any>;
export declare const VaadinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
