import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Foursquare: StyledIcon<any>;
export declare const FoursquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
