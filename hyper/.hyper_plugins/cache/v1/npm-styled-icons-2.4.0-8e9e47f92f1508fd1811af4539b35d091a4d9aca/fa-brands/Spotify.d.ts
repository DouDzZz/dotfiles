import { StyledIcon, StyledIconProps } from '..';
export declare const Spotify: StyledIcon<any>;
export declare const SpotifyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
