import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TwitterSquare: StyledIcon<any>;
export declare const TwitterSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
