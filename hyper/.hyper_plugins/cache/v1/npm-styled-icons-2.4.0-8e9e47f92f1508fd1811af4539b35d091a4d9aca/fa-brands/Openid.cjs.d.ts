import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Openid: StyledIcon<any>;
export declare const OpenidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
