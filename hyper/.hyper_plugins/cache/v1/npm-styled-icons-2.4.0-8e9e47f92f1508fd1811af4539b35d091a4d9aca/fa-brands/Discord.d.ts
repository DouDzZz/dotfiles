import { StyledIcon, StyledIconProps } from '..';
export declare const Discord: StyledIcon<any>;
export declare const DiscordDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
