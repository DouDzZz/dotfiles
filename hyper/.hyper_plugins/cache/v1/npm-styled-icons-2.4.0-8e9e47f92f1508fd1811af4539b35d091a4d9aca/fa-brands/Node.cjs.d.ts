import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Node: StyledIcon<any>;
export declare const NodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
