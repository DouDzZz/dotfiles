import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lastfm: StyledIcon<any>;
export declare const LastfmDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
