import { StyledIcon, StyledIconProps } from '..';
export declare const StackOverflow: StyledIcon<any>;
export declare const StackOverflowDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
