import { StyledIcon, StyledIconProps } from '..';
export declare const PinterestSquare: StyledIcon<any>;
export declare const PinterestSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
