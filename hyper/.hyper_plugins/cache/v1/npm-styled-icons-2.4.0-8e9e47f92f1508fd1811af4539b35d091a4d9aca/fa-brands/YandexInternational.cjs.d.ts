import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const YandexInternational: StyledIcon<any>;
export declare const YandexInternationalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
