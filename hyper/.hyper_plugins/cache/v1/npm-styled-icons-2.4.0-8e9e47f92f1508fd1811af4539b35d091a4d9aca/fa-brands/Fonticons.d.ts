import { StyledIcon, StyledIconProps } from '..';
export declare const Fonticons: StyledIcon<any>;
export declare const FonticonsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
