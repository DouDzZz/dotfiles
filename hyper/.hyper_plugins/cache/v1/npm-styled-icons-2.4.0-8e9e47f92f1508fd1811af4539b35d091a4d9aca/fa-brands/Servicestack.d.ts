import { StyledIcon, StyledIconProps } from '..';
export declare const Servicestack: StyledIcon<any>;
export declare const ServicestackDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
