import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ebay: StyledIcon<any>;
export declare const EbayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
