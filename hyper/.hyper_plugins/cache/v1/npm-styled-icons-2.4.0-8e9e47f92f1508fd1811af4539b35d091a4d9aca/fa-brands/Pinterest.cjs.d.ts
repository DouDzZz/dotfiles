import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pinterest: StyledIcon<any>;
export declare const PinterestDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
