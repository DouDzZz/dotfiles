import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsNcEu: StyledIcon<any>;
export declare const CreativeCommonsNcEuDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
