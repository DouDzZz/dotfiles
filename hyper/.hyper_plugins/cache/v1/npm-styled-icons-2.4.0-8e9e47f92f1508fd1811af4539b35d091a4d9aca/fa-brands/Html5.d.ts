import { StyledIcon, StyledIconProps } from '..';
export declare const Html5: StyledIcon<any>;
export declare const Html5Dimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
