import { StyledIcon, StyledIconProps } from '..';
export declare const Ethereum: StyledIcon<any>;
export declare const EthereumDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
