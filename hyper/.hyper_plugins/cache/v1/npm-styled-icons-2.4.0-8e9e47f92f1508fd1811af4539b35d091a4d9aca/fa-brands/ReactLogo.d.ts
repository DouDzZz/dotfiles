import { StyledIcon, StyledIconProps } from '..';
export declare const ReactLogo: StyledIcon<any>;
export declare const ReactLogoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
