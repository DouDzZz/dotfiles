import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Linux: StyledIcon<any>;
export declare const LinuxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
