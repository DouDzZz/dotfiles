import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Drupal: StyledIcon<any>;
export declare const DrupalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
