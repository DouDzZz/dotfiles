import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Elementor: StyledIcon<any>;
export declare const ElementorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
