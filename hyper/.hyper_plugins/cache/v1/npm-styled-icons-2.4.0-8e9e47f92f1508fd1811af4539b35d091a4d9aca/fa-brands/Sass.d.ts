import { StyledIcon, StyledIconProps } from '..';
export declare const Sass: StyledIcon<any>;
export declare const SassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
