import { StyledIcon, StyledIconProps } from '..';
export declare const Angellist: StyledIcon<any>;
export declare const AngellistDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
