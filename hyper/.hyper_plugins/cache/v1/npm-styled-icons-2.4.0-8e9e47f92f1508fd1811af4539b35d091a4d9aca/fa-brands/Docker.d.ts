import { StyledIcon, StyledIconProps } from '..';
export declare const Docker: StyledIcon<any>;
export declare const DockerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
