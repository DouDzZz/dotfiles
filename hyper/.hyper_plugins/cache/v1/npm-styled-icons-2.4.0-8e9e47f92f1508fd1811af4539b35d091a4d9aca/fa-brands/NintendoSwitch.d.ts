import { StyledIcon, StyledIconProps } from '..';
export declare const NintendoSwitch: StyledIcon<any>;
export declare const NintendoSwitchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
