import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TencentWeibo: StyledIcon<any>;
export declare const TencentWeiboDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
