import { StyledIcon, StyledIconProps } from '..';
export declare const Xbox: StyledIcon<any>;
export declare const XboxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
