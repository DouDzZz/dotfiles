import { StyledIcon, StyledIconProps } from '..';
export declare const FontAwesomeAlt: StyledIcon<any>;
export declare const FontAwesomeAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
