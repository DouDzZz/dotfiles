import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Instagram: StyledIcon<any>;
export declare const InstagramDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
