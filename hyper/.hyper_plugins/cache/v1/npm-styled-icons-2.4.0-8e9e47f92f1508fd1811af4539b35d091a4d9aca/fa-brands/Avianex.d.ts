import { StyledIcon, StyledIconProps } from '..';
export declare const Avianex: StyledIcon<any>;
export declare const AvianexDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
