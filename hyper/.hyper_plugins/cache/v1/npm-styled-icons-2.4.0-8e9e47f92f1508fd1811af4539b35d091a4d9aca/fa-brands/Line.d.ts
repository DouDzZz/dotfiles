import { StyledIcon, StyledIconProps } from '..';
export declare const Line: StyledIcon<any>;
export declare const LineDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
