import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Etsy: StyledIcon<any>;
export declare const EtsyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
