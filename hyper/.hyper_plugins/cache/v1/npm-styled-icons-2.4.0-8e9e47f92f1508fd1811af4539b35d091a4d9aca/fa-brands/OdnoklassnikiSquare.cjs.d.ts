import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OdnoklassnikiSquare: StyledIcon<any>;
export declare const OdnoklassnikiSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
