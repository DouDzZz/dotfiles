import { StyledIcon, StyledIconProps } from '..';
export declare const Soundcloud: StyledIcon<any>;
export declare const SoundcloudDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
