import { StyledIcon, StyledIconProps } from '..';
export declare const Audible: StyledIcon<any>;
export declare const AudibleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
