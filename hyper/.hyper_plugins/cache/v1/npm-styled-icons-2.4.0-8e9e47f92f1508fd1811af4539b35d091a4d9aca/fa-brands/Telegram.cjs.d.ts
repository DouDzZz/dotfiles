import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Telegram: StyledIcon<any>;
export declare const TelegramDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
