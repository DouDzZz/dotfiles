import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Uniregistry: StyledIcon<any>;
export declare const UniregistryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
