import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Microsoft: StyledIcon<any>;
export declare const MicrosoftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
