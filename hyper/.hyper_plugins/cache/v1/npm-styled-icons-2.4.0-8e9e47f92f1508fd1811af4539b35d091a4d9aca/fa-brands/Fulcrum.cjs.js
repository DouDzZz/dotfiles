"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Fulcrum = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Fulcrum-title" }, props.title), react_1.default.createElement("path", { fill: "currentColor", d: "M70.75 164.14l-35.38 43.55L0 164.14l35.38-43.55 35.37 43.55zM119.23 0L98.69 198.18 47.72 256l50.98 57.82L119.23 512V300.89L78.15 256l41.08-44.89V0zm79.67 164.14l35.38 43.55 35.38-43.55-35.38-43.55-35.38 43.55zm-48.48 46.97L191.5 256l-41.08 44.89V512l20.54-198.18L221.94 256l-50.98-57.82L150.42 0v211.11z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { fill: "currentColor", d: "M70.75 164.14l-35.38 43.55L0 164.14l35.38-43.55 35.37 43.55zM119.23 0L98.69 198.18 47.72 256l50.98 57.82L119.23 512V300.89L78.15 256l41.08-44.89V0zm79.67 164.14l35.38 43.55 35.38-43.55-35.38-43.55-35.38 43.55zm-48.48 46.97L191.5 256l-41.08 44.89V512l20.54-198.18L221.94 256l-50.98-57.82L150.42 0v211.11z", key: "k0" })
        ]); },
    viewBox: '0 0 269 512',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Fulcrum.displayName = 'Fulcrum';
exports.FulcrumDimensions = { height: undefined, width: undefined };
var templateObject_1;
