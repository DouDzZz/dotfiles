import { StyledIcon, StyledIconProps } from '..';
export declare const Kickstarter: StyledIcon<any>;
export declare const KickstarterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
