import { StyledIcon, StyledIconProps } from '..';
export declare const TradeFederation: StyledIcon<any>;
export declare const TradeFederationDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
