import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Algolia: StyledIcon<any>;
export declare const AlgoliaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
