import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Houzz: StyledIcon<any>;
export declare const HouzzDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
