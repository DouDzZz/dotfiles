import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Untappd: StyledIcon<any>;
export declare const UntappdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
