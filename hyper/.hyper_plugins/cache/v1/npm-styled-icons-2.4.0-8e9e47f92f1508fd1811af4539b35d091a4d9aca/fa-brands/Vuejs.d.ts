import { StyledIcon, StyledIconProps } from '..';
export declare const Vuejs: StyledIcon<any>;
export declare const VuejsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
