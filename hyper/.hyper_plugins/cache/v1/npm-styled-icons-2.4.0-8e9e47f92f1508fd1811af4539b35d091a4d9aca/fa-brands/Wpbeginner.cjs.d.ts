import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wpbeginner: StyledIcon<any>;
export declare const WpbeginnerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
