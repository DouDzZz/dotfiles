import { StyledIcon, StyledIconProps } from '..';
export declare const KickstarterK: StyledIcon<any>;
export declare const KickstarterKDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
