import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dochub: StyledIcon<any>;
export declare const DochubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
