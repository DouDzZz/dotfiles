import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const XingSquare: StyledIcon<any>;
export declare const XingSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
