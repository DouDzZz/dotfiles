import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Discord: StyledIcon<any>;
export declare const DiscordDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
