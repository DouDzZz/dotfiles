import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HireAHelper: StyledIcon<any>;
export declare const HireAHelperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
