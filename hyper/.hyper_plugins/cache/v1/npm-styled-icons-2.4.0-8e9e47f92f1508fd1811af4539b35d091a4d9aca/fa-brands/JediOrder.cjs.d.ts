import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const JediOrder: StyledIcon<any>;
export declare const JediOrderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
