import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hotjar: StyledIcon<any>;
export declare const HotjarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
