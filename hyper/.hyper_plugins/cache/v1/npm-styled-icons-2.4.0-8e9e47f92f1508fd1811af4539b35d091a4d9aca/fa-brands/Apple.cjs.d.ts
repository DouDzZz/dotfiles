import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Apple: StyledIcon<any>;
export declare const AppleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
