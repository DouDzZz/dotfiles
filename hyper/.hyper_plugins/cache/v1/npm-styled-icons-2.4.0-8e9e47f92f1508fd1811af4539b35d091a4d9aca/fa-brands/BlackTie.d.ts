import { StyledIcon, StyledIconProps } from '..';
export declare const BlackTie: StyledIcon<any>;
export declare const BlackTieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
