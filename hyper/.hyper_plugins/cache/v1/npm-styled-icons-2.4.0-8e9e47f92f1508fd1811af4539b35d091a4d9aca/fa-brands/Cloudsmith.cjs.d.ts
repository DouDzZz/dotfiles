import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cloudsmith: StyledIcon<any>;
export declare const CloudsmithDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
