import { StyledIcon, StyledIconProps } from '..';
export declare const Cuttlefish: StyledIcon<any>;
export declare const CuttlefishDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
