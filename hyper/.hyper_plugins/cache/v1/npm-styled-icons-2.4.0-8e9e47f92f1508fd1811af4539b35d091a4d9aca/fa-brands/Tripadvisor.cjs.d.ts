import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tripadvisor: StyledIcon<any>;
export declare const TripadvisorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
