import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FortAwesome: StyledIcon<any>;
export declare const FortAwesomeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
