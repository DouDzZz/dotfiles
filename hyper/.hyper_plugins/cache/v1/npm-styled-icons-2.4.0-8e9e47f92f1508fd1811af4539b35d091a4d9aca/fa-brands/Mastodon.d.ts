import { StyledIcon, StyledIconProps } from '..';
export declare const Mastodon: StyledIcon<any>;
export declare const MastodonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
