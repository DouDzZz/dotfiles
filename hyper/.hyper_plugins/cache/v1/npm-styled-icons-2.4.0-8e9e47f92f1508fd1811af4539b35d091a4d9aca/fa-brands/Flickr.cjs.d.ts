import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flickr: StyledIcon<any>;
export declare const FlickrDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
