import { StyledIcon, StyledIconProps } from '..';
export declare const Foursquare: StyledIcon<any>;
export declare const FoursquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
