import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Jsfiddle: StyledIcon<any>;
export declare const JsfiddleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
