import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lyft: StyledIcon<any>;
export declare const LyftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
