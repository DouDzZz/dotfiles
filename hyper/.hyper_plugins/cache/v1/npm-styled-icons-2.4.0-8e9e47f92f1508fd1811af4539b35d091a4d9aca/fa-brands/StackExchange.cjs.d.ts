import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StackExchange: StyledIcon<any>;
export declare const StackExchangeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
