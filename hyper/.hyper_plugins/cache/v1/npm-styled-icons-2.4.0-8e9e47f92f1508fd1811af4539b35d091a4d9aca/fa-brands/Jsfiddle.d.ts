import { StyledIcon, StyledIconProps } from '..';
export declare const Jsfiddle: StyledIcon<any>;
export declare const JsfiddleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
