"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.CreativeCommonsPdAlt = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "CreativeCommonsPdAlt-title" }, props.title), react_1.default.createElement("path", { fill: "currentColor", d: "M247.6 8C389.4 8 496 118.1 496 256c0 147.1-118.5 248-248.4 248C113.6 504 0 394.5 0 256 0 123.1 104.7 8 247.6 8zm.8 44.7C130.2 52.7 44.7 150.6 44.7 256c0 109.8 91.2 202.8 203.7 202.8 103.2 0 202.8-81.1 202.8-202.8.1-113.8-90.2-203.3-202.8-203.3zM135.5 323.3V186h68.2c33.7 0 50.5 15.5 50.5 46.5 0 9-3 46.5-57.1 46.5h-27v44.3h-34.6zm34.1-111.6v41.6h29.2c27.9 0 30-41.6-.9-41.6h-28.3zm93.9 111.6V186h53.2c21.4 0 70 5.2 70 68.6 0 63.5-48.6 68.6-70 68.6h-53.2zm34.1-108.5v79.7h19.9c24 0 34.5-15.3 34.5-39.9 0-42-31.2-39.9-35-39.9l-19.4.1z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { fill: "currentColor", d: "M247.6 8C389.4 8 496 118.1 496 256c0 147.1-118.5 248-248.4 248C113.6 504 0 394.5 0 256 0 123.1 104.7 8 247.6 8zm.8 44.7C130.2 52.7 44.7 150.6 44.7 256c0 109.8 91.2 202.8 203.7 202.8 103.2 0 202.8-81.1 202.8-202.8.1-113.8-90.2-203.3-202.8-203.3zM135.5 323.3V186h68.2c33.7 0 50.5 15.5 50.5 46.5 0 9-3 46.5-57.1 46.5h-27v44.3h-34.6zm34.1-111.6v41.6h29.2c27.9 0 30-41.6-.9-41.6h-28.3zm93.9 111.6V186h53.2c21.4 0 70 5.2 70 68.6 0 63.5-48.6 68.6-70 68.6h-53.2zm34.1-108.5v79.7h19.9c24 0 34.5-15.3 34.5-39.9 0-42-31.2-39.9-35-39.9l-19.4.1z", key: "k0" })
        ]); },
    viewBox: '0 0 496 512',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.CreativeCommonsPdAlt.displayName = 'CreativeCommonsPdAlt';
exports.CreativeCommonsPdAltDimensions = { height: undefined, width: undefined };
var templateObject_1;
