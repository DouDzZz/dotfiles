import { StyledIcon, StyledIconProps } from '..';
export declare const GgCircle: StyledIcon<any>;
export declare const GgCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
