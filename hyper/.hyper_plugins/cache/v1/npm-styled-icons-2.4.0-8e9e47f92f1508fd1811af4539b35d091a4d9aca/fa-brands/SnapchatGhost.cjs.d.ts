import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SnapchatGhost: StyledIcon<any>;
export declare const SnapchatGhostDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
