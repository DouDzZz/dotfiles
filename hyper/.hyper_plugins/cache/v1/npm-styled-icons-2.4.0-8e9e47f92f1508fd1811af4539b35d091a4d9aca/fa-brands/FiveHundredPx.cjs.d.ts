import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiveHundredPx: StyledIcon<any>;
export declare const FiveHundredPxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
