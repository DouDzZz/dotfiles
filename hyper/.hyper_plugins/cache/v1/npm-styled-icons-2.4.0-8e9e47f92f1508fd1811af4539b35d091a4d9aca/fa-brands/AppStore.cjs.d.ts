import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AppStore: StyledIcon<any>;
export declare const AppStoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
