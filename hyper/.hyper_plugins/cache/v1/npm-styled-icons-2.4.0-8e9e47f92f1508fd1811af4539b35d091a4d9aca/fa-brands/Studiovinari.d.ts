import { StyledIcon, StyledIconProps } from '..';
export declare const Studiovinari: StyledIcon<any>;
export declare const StudiovinariDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
