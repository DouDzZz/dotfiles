import { StyledIcon, StyledIconProps } from '..';
export declare const GalacticSenate: StyledIcon<any>;
export declare const GalacticSenateDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
