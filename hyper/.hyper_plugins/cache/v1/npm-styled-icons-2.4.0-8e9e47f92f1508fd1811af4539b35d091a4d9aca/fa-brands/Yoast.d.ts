import { StyledIcon, StyledIconProps } from '..';
export declare const Yoast: StyledIcon<any>;
export declare const YoastDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
