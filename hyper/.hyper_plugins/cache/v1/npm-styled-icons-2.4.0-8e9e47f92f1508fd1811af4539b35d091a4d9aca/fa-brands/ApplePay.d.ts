import { StyledIcon, StyledIconProps } from '..';
export declare const ApplePay: StyledIcon<any>;
export declare const ApplePayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
