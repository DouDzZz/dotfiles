import { StyledIcon, StyledIconProps } from '..';
export declare const JediOrder: StyledIcon<any>;
export declare const JediOrderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
