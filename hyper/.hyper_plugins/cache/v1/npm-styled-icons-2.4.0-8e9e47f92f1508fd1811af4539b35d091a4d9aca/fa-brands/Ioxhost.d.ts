import { StyledIcon, StyledIconProps } from '..';
export declare const Ioxhost: StyledIcon<any>;
export declare const IoxhostDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
