import { StyledIcon, StyledIconProps } from '..';
export declare const HackerNews: StyledIcon<any>;
export declare const HackerNewsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
