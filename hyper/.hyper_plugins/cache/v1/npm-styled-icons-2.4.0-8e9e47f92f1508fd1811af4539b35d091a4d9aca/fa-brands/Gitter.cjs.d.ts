import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gitter: StyledIcon<any>;
export declare const GitterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
