import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Whmcs: StyledIcon<any>;
export declare const WhmcsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
