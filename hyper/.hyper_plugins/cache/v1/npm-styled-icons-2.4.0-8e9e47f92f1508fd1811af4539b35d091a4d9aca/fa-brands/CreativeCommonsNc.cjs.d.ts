import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsNc: StyledIcon<any>;
export declare const CreativeCommonsNcDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
