import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Digg: StyledIcon<any>;
export declare const DiggDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
