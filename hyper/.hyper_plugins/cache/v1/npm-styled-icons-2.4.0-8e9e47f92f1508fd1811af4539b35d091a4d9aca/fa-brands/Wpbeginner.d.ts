import { StyledIcon, StyledIconProps } from '..';
export declare const Wpbeginner: StyledIcon<any>;
export declare const WpbeginnerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
