import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LinkedinIn: StyledIcon<any>;
export declare const LinkedinInDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
