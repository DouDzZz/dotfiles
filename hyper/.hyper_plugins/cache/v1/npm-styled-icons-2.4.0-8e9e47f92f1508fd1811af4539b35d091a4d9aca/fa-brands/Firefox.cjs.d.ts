import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Firefox: StyledIcon<any>;
export declare const FirefoxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
