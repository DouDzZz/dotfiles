import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DribbbleSquare: StyledIcon<any>;
export declare const DribbbleSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
