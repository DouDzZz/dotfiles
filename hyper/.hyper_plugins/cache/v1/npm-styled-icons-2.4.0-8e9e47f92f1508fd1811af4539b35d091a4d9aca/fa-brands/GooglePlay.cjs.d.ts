import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GooglePlay: StyledIcon<any>;
export declare const GooglePlayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
