import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mandalorian: StyledIcon<any>;
export declare const MandalorianDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
