import { StyledIcon, StyledIconProps } from '..';
export declare const Autoprefixer: StyledIcon<any>;
export declare const AutoprefixerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
