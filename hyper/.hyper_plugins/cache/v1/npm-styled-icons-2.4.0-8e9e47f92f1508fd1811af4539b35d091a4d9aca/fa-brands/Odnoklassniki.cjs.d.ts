import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Odnoklassniki: StyledIcon<any>;
export declare const OdnoklassnikiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
