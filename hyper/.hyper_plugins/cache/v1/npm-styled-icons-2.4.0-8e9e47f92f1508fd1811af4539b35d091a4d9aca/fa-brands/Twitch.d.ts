import { StyledIcon, StyledIconProps } from '..';
export declare const Twitch: StyledIcon<any>;
export declare const TwitchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
