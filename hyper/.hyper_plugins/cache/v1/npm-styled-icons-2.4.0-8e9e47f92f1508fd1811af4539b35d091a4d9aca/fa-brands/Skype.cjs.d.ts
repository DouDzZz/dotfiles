import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Skype: StyledIcon<any>;
export declare const SkypeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
