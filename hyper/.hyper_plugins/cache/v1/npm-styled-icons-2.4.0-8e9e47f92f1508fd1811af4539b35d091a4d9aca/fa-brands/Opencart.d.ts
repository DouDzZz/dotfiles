import { StyledIcon, StyledIconProps } from '..';
export declare const Opencart: StyledIcon<any>;
export declare const OpencartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
