import { StyledIcon, StyledIconProps } from '..';
export declare const Patreon: StyledIcon<any>;
export declare const PatreonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
