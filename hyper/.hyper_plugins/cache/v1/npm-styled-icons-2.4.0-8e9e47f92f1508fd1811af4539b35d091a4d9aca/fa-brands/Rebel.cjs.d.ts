import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rebel: StyledIcon<any>;
export declare const RebelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
