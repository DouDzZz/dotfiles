import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsNc: StyledIcon<any>;
export declare const CreativeCommonsNcDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
