import { StyledIcon, StyledIconProps } from '..';
export declare const Openid: StyledIcon<any>;
export declare const OpenidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
