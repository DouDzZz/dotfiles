import { StyledIcon, StyledIconProps } from '..';
export declare const Keybase: StyledIcon<any>;
export declare const KeybaseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
