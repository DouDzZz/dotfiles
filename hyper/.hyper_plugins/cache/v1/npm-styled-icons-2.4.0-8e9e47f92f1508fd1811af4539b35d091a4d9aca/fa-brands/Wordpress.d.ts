import { StyledIcon, StyledIconProps } from '..';
export declare const Wordpress: StyledIcon<any>;
export declare const WordpressDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
