import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Expeditedssl: StyledIcon<any>;
export declare const ExpeditedsslDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
