import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsNcJp: StyledIcon<any>;
export declare const CreativeCommonsNcJpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
