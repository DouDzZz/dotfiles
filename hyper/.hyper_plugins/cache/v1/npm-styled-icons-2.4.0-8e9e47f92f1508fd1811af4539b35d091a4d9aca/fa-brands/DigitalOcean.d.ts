import { StyledIcon, StyledIconProps } from '..';
export declare const DigitalOcean: StyledIcon<any>;
export declare const DigitalOceanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
