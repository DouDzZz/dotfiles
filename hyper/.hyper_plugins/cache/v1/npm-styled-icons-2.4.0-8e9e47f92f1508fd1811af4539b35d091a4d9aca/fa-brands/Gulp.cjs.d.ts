import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gulp: StyledIcon<any>;
export declare const GulpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
