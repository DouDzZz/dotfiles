import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsNd: StyledIcon<any>;
export declare const CreativeCommonsNdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
