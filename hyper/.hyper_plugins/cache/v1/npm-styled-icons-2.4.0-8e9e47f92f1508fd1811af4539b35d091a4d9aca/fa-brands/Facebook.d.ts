import { StyledIcon, StyledIconProps } from '..';
export declare const Facebook: StyledIcon<any>;
export declare const FacebookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
