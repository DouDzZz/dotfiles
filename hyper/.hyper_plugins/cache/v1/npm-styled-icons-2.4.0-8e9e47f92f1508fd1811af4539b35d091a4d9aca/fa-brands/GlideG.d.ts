import { StyledIcon, StyledIconProps } from '..';
export declare const GlideG: StyledIcon<any>;
export declare const GlideGDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
