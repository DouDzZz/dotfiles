import { StyledIcon, StyledIconProps } from '..';
export declare const Apple: StyledIcon<any>;
export declare const AppleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
