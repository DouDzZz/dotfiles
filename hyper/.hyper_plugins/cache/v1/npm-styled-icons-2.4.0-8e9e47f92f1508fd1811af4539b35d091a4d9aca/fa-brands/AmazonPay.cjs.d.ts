import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AmazonPay: StyledIcon<any>;
export declare const AmazonPayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
