import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Napster: StyledIcon<any>;
export declare const NapsterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
