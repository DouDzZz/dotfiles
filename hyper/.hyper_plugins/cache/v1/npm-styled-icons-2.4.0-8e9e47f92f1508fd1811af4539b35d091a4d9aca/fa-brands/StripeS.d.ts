import { StyledIcon, StyledIconProps } from '..';
export declare const StripeS: StyledIcon<any>;
export declare const StripeSDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
