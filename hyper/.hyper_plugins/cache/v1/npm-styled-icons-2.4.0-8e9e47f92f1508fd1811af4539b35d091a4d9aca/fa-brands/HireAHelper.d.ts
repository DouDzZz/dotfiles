import { StyledIcon, StyledIconProps } from '..';
export declare const HireAHelper: StyledIcon<any>;
export declare const HireAHelperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
