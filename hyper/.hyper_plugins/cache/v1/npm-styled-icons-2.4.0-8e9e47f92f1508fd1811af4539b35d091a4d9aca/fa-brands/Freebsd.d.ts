import { StyledIcon, StyledIconProps } from '..';
export declare const Freebsd: StyledIcon<any>;
export declare const FreebsdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
