import { StyledIcon, StyledIconProps } from '..';
export declare const RedditAlien: StyledIcon<any>;
export declare const RedditAlienDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
