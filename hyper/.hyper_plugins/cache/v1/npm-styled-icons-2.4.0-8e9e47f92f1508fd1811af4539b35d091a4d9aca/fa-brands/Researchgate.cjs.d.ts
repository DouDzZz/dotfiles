import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Researchgate: StyledIcon<any>;
export declare const ResearchgateDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
