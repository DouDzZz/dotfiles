import { StyledIcon, StyledIconProps } from '..';
export declare const Weixin: StyledIcon<any>;
export declare const WeixinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
