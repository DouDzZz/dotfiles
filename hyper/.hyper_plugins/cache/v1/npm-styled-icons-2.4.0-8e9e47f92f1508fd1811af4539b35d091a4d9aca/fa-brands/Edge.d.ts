import { StyledIcon, StyledIconProps } from '..';
export declare const Edge: StyledIcon<any>;
export declare const EdgeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
