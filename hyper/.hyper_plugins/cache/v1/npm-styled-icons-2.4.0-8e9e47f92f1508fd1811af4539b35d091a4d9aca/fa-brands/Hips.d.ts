import { StyledIcon, StyledIconProps } from '..';
export declare const Hips: StyledIcon<any>;
export declare const HipsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
