import { StyledIcon, StyledIconProps } from '..';
export declare const Themeisle: StyledIcon<any>;
export declare const ThemeisleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
