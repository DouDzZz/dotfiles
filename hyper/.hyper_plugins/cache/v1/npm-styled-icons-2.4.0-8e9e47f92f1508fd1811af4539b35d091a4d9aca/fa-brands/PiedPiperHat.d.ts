import { StyledIcon, StyledIconProps } from '..';
export declare const PiedPiperHat: StyledIcon<any>;
export declare const PiedPiperHatDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
