import { StyledIcon, StyledIconProps } from '..';
export declare const Digg: StyledIcon<any>;
export declare const DiggDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
