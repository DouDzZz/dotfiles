import { StyledIcon, StyledIconProps } from '..';
export declare const Cloudversify: StyledIcon<any>;
export declare const CloudversifyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
