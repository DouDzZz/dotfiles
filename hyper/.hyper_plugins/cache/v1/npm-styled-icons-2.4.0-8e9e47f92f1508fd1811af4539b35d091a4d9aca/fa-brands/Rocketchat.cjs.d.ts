import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rocketchat: StyledIcon<any>;
export declare const RocketchatDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
