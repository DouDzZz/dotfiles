import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fonticons: StyledIcon<any>;
export declare const FonticonsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
