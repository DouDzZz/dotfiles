import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SteamSquare: StyledIcon<any>;
export declare const SteamSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
