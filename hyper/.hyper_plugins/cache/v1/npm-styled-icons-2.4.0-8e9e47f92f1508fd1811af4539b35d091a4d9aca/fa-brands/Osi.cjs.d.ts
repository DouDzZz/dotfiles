import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Osi: StyledIcon<any>;
export declare const OsiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
