import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcDiscover: StyledIcon<any>;
export declare const CcDiscoverDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
