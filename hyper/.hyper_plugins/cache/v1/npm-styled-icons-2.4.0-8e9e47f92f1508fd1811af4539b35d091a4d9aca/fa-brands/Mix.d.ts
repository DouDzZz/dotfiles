import { StyledIcon, StyledIconProps } from '..';
export declare const Mix: StyledIcon<any>;
export declare const MixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
