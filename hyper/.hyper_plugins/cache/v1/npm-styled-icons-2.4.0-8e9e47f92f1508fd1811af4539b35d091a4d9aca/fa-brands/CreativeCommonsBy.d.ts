import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsBy: StyledIcon<any>;
export declare const CreativeCommonsByDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
