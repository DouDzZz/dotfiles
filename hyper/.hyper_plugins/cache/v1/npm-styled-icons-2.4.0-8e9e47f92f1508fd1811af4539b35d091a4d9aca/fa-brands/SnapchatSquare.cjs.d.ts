import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SnapchatSquare: StyledIcon<any>;
export declare const SnapchatSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
