import { StyledIcon, StyledIconProps } from '..';
export declare const Gitlab: StyledIcon<any>;
export declare const GitlabDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
