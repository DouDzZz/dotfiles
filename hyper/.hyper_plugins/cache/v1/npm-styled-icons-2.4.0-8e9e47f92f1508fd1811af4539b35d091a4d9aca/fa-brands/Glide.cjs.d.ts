import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Glide: StyledIcon<any>;
export declare const GlideDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
