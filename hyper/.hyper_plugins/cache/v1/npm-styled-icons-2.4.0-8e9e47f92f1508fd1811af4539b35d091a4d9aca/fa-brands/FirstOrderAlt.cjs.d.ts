import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FirstOrderAlt: StyledIcon<any>;
export declare const FirstOrderAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
