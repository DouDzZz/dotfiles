import { StyledIcon, StyledIconProps } from '..';
export declare const PiedPiperPp: StyledIcon<any>;
export declare const PiedPiperPpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
