import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bandcamp: StyledIcon<any>;
export declare const BandcampDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
