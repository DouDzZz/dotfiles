import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoenixSquadron: StyledIcon<any>;
export declare const PhoenixSquadronDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
