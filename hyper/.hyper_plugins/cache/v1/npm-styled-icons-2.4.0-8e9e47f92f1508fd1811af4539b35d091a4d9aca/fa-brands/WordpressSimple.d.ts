import { StyledIcon, StyledIconProps } from '..';
export declare const WordpressSimple: StyledIcon<any>;
export declare const WordpressSimpleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
