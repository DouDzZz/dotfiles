import { StyledIcon, StyledIconProps } from '..';
export declare const Modx: StyledIcon<any>;
export declare const ModxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
