import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Weixin: StyledIcon<any>;
export declare const WeixinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
