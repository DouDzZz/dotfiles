import { StyledIcon, StyledIconProps } from '..';
export declare const FacebookSquare: StyledIcon<any>;
export declare const FacebookSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
