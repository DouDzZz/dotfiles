import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mizuni: StyledIcon<any>;
export declare const MizuniDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
