import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StackOverflow: StyledIcon<any>;
export declare const StackOverflowDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
