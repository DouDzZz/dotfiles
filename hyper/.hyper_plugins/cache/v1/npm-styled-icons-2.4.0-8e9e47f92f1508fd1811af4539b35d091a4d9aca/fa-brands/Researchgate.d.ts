import { StyledIcon, StyledIconProps } from '..';
export declare const Researchgate: StyledIcon<any>;
export declare const ResearchgateDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
