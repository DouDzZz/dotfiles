import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PiedPiperHat: StyledIcon<any>;
export declare const PiedPiperHatDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
