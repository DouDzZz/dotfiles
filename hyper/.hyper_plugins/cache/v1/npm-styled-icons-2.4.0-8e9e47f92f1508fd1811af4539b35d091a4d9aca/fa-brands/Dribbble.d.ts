import { StyledIcon, StyledIconProps } from '..';
export declare const Dribbble: StyledIcon<any>;
export declare const DribbbleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
