import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dyalog: StyledIcon<any>;
export declare const DyalogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
