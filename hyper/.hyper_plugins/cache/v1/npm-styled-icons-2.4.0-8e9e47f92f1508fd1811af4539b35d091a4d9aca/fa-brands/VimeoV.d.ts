import { StyledIcon, StyledIconProps } from '..';
export declare const VimeoV: StyledIcon<any>;
export declare const VimeoVDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
