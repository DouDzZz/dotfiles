import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FacebookSquare: StyledIcon<any>;
export declare const FacebookSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
