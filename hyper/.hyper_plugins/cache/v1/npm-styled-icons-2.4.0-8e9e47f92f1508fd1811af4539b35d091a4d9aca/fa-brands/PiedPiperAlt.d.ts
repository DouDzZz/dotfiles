import { StyledIcon, StyledIconProps } from '..';
export declare const PiedPiperAlt: StyledIcon<any>;
export declare const PiedPiperAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
