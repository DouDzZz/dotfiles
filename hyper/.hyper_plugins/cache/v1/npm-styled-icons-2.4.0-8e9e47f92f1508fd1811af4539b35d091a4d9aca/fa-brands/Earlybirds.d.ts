import { StyledIcon, StyledIconProps } from '..';
export declare const Earlybirds: StyledIcon<any>;
export declare const EarlybirdsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
