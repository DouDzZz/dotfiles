import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dropbox: StyledIcon<any>;
export declare const DropboxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
