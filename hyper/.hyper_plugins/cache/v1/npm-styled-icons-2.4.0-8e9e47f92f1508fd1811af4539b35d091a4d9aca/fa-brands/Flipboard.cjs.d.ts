import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flipboard: StyledIcon<any>;
export declare const FlipboardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
