import { StyledIcon, StyledIconProps } from '..';
export declare const OdnoklassnikiSquare: StyledIcon<any>;
export declare const OdnoklassnikiSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
