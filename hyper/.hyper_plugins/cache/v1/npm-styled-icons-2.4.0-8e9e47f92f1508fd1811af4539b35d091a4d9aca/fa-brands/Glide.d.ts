import { StyledIcon, StyledIconProps } from '..';
export declare const Glide: StyledIcon<any>;
export declare const GlideDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
