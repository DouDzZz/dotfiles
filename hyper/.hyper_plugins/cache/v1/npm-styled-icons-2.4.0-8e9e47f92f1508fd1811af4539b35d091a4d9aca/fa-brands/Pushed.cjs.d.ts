import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pushed: StyledIcon<any>;
export declare const PushedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
