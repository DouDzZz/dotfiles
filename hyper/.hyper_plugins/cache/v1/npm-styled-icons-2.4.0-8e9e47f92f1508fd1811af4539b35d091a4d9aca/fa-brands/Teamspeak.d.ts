import { StyledIcon, StyledIconProps } from '..';
export declare const Teamspeak: StyledIcon<any>;
export declare const TeamspeakDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
