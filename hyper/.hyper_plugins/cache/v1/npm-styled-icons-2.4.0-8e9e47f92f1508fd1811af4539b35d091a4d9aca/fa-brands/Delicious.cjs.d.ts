import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Delicious: StyledIcon<any>;
export declare const DeliciousDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
