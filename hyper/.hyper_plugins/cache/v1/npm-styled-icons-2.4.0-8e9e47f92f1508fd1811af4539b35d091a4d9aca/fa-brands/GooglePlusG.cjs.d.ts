import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GooglePlusG: StyledIcon<any>;
export declare const GooglePlusGDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
