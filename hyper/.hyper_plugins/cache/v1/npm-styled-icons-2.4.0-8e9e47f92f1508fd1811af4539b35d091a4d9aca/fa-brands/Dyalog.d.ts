import { StyledIcon, StyledIconProps } from '..';
export declare const Dyalog: StyledIcon<any>;
export declare const DyalogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
