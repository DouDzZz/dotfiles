import { StyledIcon, StyledIconProps } from '..';
export declare const Scribd: StyledIcon<any>;
export declare const ScribdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
