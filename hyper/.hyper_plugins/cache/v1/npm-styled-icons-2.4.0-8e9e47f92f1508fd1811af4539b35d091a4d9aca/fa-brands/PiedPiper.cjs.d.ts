import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PiedPiper: StyledIcon<any>;
export declare const PiedPiperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
