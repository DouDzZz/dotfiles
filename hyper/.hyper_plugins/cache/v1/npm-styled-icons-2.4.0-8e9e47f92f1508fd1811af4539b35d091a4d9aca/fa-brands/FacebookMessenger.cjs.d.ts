import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FacebookMessenger: StyledIcon<any>;
export declare const FacebookMessengerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
