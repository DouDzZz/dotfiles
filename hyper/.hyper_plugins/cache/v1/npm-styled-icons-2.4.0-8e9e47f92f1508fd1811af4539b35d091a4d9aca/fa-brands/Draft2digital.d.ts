import { StyledIcon, StyledIconProps } from '..';
export declare const Draft2digital: StyledIcon<any>;
export declare const Draft2digitalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
