import { StyledIcon, StyledIconProps } from '..';
export declare const Uikit: StyledIcon<any>;
export declare const UikitDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
