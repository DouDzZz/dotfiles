import { StyledIcon, StyledIconProps } from '..';
export declare const Typo3: StyledIcon<any>;
export declare const Typo3Dimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
