import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Autoprefixer: StyledIcon<any>;
export declare const AutoprefixerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
