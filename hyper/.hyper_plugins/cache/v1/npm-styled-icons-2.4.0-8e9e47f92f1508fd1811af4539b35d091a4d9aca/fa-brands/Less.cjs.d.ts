import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Less: StyledIcon<any>;
export declare const LessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
