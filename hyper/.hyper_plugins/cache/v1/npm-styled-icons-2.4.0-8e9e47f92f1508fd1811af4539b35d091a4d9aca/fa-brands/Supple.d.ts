import { StyledIcon, StyledIconProps } from '..';
export declare const Supple: StyledIcon<any>;
export declare const SuppleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
