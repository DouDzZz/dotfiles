import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GalacticRepublic: StyledIcon<any>;
export declare const GalacticRepublicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
