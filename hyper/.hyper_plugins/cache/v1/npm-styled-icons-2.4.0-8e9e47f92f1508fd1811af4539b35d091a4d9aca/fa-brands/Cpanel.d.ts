import { StyledIcon, StyledIconProps } from '..';
export declare const Cpanel: StyledIcon<any>;
export declare const CpanelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
