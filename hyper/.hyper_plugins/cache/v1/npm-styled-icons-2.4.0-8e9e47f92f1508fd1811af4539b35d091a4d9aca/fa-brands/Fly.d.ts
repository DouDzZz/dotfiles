import { StyledIcon, StyledIconProps } from '..';
export declare const Fly: StyledIcon<any>;
export declare const FlyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
