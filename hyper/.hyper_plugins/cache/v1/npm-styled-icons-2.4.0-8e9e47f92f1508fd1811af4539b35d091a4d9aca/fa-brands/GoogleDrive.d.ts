import { StyledIcon, StyledIconProps } from '..';
export declare const GoogleDrive: StyledIcon<any>;
export declare const GoogleDriveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
