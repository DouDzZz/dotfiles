import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Schlix: StyledIcon<any>;
export declare const SchlixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
