import { StyledIcon, StyledIconProps } from '..';
export declare const Joomla: StyledIcon<any>;
export declare const JoomlaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
