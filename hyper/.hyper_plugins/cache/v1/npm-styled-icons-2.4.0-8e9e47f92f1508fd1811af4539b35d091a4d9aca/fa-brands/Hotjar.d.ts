import { StyledIcon, StyledIconProps } from '..';
export declare const Hotjar: StyledIcon<any>;
export declare const HotjarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
