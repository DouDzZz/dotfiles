import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sistrix: StyledIcon<any>;
export declare const SistrixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
