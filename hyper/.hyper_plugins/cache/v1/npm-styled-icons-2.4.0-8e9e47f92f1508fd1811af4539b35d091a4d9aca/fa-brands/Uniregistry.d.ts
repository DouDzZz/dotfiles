import { StyledIcon, StyledIconProps } from '..';
export declare const Uniregistry: StyledIcon<any>;
export declare const UniregistryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
