import { StyledIcon, StyledIconProps } from '..';
export declare const Dochub: StyledIcon<any>;
export declare const DochubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
