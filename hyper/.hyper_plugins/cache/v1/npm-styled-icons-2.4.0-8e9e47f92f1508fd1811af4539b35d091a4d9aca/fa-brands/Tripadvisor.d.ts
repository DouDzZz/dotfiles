import { StyledIcon, StyledIconProps } from '..';
export declare const Tripadvisor: StyledIcon<any>;
export declare const TripadvisorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
