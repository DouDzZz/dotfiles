import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GitSquare: StyledIcon<any>;
export declare const GitSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
