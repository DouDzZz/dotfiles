import { StyledIcon, StyledIconProps } from '..';
export declare const Flipboard: StyledIcon<any>;
export declare const FlipboardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
