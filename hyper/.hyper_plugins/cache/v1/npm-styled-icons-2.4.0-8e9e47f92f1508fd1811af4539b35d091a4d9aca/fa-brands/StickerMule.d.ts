import { StyledIcon, StyledIconProps } from '..';
export declare const StickerMule: StyledIcon<any>;
export declare const StickerMuleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
