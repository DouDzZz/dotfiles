import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Angular: StyledIcon<any>;
export declare const AngularDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
