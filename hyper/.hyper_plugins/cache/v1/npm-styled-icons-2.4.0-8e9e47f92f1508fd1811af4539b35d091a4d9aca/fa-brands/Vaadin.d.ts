import { StyledIcon, StyledIconProps } from '..';
export declare const Vaadin: StyledIcon<any>;
export declare const VaadinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
