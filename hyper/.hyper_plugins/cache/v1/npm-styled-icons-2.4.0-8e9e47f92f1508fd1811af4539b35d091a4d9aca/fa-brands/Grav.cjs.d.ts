import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Grav: StyledIcon<any>;
export declare const GravDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
