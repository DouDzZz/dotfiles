import { StyledIcon, StyledIconProps } from '..';
export declare const SteamSquare: StyledIcon<any>;
export declare const SteamSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
