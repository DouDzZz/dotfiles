import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcAmazonPay: StyledIcon<any>;
export declare const CcAmazonPayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
