import { StyledIcon, StyledIconProps } from '..';
export declare const Sistrix: StyledIcon<any>;
export declare const SistrixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
