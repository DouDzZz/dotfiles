import { StyledIcon, StyledIconProps } from '..';
export declare const DribbbleSquare: StyledIcon<any>;
export declare const DribbbleSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
