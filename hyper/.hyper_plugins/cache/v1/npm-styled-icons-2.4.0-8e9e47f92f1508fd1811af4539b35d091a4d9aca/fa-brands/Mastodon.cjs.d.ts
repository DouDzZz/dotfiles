import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mastodon: StyledIcon<any>;
export declare const MastodonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
