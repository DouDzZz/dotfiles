import { StyledIcon, StyledIconProps } from '..';
export declare const SteamSymbol: StyledIcon<any>;
export declare const SteamSymbolDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
