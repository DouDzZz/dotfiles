import { StyledIcon, StyledIconProps } from '..';
export declare const Schlix: StyledIcon<any>;
export declare const SchlixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
