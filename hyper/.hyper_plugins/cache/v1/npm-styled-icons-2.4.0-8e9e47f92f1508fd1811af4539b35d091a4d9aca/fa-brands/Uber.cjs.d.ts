import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Uber: StyledIcon<any>;
export declare const UberDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
