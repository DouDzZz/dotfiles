import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Github: StyledIcon<any>;
export declare const GithubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
