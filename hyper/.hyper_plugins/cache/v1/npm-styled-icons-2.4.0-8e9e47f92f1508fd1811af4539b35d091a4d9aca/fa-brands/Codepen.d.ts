import { StyledIcon, StyledIconProps } from '..';
export declare const Codepen: StyledIcon<any>;
export declare const CodepenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
