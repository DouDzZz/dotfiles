import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Staylinked: StyledIcon<any>;
export declare const StaylinkedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
