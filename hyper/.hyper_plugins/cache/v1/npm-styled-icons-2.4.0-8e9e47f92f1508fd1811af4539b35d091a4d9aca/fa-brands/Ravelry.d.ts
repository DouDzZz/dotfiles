import { StyledIcon, StyledIconProps } from '..';
export declare const Ravelry: StyledIcon<any>;
export declare const RavelryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
