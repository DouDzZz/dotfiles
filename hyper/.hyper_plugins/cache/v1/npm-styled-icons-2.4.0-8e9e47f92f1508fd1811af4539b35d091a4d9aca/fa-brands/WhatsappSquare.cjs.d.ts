import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WhatsappSquare: StyledIcon<any>;
export declare const WhatsappSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
