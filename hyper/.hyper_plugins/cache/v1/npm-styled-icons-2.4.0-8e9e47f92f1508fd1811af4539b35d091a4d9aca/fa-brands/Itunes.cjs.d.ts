import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Itunes: StyledIcon<any>;
export declare const ItunesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
