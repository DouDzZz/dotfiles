import { StyledIcon, StyledIconProps } from '..';
export declare const Wpexplorer: StyledIcon<any>;
export declare const WpexplorerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
