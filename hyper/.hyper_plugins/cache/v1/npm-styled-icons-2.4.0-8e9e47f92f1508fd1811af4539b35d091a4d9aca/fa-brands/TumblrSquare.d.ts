import { StyledIcon, StyledIconProps } from '..';
export declare const TumblrSquare: StyledIcon<any>;
export declare const TumblrSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
