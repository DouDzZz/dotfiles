import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Accusoft: StyledIcon<any>;
export declare const AccusoftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
