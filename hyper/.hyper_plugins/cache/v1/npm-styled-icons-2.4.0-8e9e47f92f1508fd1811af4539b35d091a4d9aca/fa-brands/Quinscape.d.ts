import { StyledIcon, StyledIconProps } from '..';
export declare const Quinscape: StyledIcon<any>;
export declare const QuinscapeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
