import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gripfire: StyledIcon<any>;
export declare const GripfireDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
