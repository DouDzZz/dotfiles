import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hooli: StyledIcon<any>;
export declare const HooliDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
