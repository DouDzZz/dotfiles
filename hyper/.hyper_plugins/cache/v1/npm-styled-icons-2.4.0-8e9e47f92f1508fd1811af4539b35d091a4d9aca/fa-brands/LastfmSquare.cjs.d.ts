import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LastfmSquare: StyledIcon<any>;
export declare const LastfmSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
