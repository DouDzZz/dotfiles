import { StyledIcon, StyledIconProps } from '..';
export declare const ViadeoSquare: StyledIcon<any>;
export declare const ViadeoSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
