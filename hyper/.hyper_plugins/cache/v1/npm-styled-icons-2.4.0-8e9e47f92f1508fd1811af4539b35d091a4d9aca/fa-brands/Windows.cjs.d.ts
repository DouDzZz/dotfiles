import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Windows: StyledIcon<any>;
export declare const WindowsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
