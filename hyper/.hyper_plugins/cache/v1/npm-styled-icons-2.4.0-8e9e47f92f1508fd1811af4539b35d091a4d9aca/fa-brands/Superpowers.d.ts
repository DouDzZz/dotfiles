import { StyledIcon, StyledIconProps } from '..';
export declare const Superpowers: StyledIcon<any>;
export declare const SuperpowersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
