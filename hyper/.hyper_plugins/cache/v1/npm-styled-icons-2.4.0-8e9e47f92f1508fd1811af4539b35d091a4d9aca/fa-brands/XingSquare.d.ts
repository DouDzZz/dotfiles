import { StyledIcon, StyledIconProps } from '..';
export declare const XingSquare: StyledIcon<any>;
export declare const XingSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
