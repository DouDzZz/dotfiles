import { StyledIcon, StyledIconProps } from '..';
export declare const Yelp: StyledIcon<any>;
export declare const YelpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
