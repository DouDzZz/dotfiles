import { StyledIcon, StyledIconProps } from '..';
export declare const Readme: StyledIcon<any>;
export declare const ReadmeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
