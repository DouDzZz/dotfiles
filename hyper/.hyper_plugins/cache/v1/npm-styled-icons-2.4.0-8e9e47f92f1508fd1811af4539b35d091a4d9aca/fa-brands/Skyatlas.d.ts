import { StyledIcon, StyledIconProps } from '..';
export declare const Skyatlas: StyledIcon<any>;
export declare const SkyatlasDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
