import { StyledIcon, StyledIconProps } from '..';
export declare const Quora: StyledIcon<any>;
export declare const QuoraDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
