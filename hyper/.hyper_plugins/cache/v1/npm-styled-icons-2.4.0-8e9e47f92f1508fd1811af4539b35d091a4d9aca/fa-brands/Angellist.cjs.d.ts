import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Angellist: StyledIcon<any>;
export declare const AngellistDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
