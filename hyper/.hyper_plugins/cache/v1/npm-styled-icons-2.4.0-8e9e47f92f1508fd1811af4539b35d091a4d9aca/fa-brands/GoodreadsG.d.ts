import { StyledIcon, StyledIconProps } from '..';
export declare const GoodreadsG: StyledIcon<any>;
export declare const GoodreadsGDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
