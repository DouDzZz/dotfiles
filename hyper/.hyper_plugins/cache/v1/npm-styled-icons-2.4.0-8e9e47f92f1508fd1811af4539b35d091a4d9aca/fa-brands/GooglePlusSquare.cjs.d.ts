import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GooglePlusSquare: StyledIcon<any>;
export declare const GooglePlusSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
