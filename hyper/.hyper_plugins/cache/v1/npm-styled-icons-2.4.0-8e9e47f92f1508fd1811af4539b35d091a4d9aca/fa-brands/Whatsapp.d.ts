import { StyledIcon, StyledIconProps } from '..';
export declare const Whatsapp: StyledIcon<any>;
export declare const WhatsappDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
