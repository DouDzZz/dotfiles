import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Laravel: StyledIcon<any>;
export declare const LaravelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
