import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ApplePay: StyledIcon<any>;
export declare const ApplePayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
