import { StyledIcon, StyledIconProps } from '..';
export declare const Paypal: StyledIcon<any>;
export declare const PaypalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
