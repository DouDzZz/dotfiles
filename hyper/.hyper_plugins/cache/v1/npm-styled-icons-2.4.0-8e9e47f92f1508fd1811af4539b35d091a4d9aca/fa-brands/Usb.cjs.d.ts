import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Usb: StyledIcon<any>;
export declare const UsbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
