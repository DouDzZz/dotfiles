import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Vine: StyledIcon<any>;
export declare const VineDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
