import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsNcJp: StyledIcon<any>;
export declare const CreativeCommonsNcJpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
