import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VimeoSquare: StyledIcon<any>;
export declare const VimeoSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
