import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WikipediaW: StyledIcon<any>;
export declare const WikipediaWDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
