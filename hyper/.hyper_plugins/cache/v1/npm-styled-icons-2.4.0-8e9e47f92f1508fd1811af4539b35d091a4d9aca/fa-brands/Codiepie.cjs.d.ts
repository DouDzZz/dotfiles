import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Codiepie: StyledIcon<any>;
export declare const CodiepieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
