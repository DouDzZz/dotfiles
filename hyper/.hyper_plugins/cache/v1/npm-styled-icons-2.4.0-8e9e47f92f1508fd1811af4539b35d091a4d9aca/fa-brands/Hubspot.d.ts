import { StyledIcon, StyledIconProps } from '..';
export declare const Hubspot: StyledIcon<any>;
export declare const HubspotDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
