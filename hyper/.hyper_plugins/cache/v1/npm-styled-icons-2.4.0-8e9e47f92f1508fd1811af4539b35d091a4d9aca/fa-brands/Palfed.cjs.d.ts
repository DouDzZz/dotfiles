import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Palfed: StyledIcon<any>;
export declare const PalfedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
