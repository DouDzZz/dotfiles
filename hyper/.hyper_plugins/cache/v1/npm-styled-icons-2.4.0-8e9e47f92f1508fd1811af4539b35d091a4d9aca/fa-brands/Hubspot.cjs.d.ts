import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hubspot: StyledIcon<any>;
export declare const HubspotDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
