import { StyledIcon, StyledIconProps } from '..';
export declare const BluetoothB: StyledIcon<any>;
export declare const BluetoothBDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
