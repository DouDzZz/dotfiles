import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Replyd: StyledIcon<any>;
export declare const ReplydDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
