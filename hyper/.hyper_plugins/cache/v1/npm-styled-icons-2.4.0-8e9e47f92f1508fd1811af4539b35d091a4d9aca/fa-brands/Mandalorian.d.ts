import { StyledIcon, StyledIconProps } from '..';
export declare const Mandalorian: StyledIcon<any>;
export declare const MandalorianDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
