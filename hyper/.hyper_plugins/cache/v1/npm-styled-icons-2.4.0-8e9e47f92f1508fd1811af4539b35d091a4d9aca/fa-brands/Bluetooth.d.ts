import { StyledIcon, StyledIconProps } from '..';
export declare const Bluetooth: StyledIcon<any>;
export declare const BluetoothDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
