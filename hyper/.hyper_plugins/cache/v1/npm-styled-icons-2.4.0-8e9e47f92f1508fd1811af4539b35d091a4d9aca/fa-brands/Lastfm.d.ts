import { StyledIcon, StyledIconProps } from '..';
export declare const Lastfm: StyledIcon<any>;
export declare const LastfmDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
