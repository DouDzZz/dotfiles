import { StyledIcon, StyledIconProps } from '..';
export declare const MediumM: StyledIcon<any>;
export declare const MediumMDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
