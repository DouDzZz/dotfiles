import { StyledIcon, StyledIconProps } from '..';
export declare const Amazon: StyledIcon<any>;
export declare const AmazonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
