import { StyledIcon, StyledIconProps } from '..';
export declare const Resolving: StyledIcon<any>;
export declare const ResolvingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
