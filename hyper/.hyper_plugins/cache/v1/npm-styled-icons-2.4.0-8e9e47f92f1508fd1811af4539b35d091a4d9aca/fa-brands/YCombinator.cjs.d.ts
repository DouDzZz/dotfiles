import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const YCombinator: StyledIcon<any>;
export declare const YCombinatorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
