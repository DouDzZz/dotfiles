import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const YoutubeSquare: StyledIcon<any>;
export declare const YoutubeSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
