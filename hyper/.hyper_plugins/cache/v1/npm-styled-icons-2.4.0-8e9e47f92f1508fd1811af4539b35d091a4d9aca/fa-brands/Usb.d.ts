import { StyledIcon, StyledIconProps } from '..';
export declare const Usb: StyledIcon<any>;
export declare const UsbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
