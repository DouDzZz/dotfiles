import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Stumbleupon: StyledIcon<any>;
export declare const StumbleuponDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
