import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bity: StyledIcon<any>;
export declare const BityDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
