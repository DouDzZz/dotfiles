import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsPdAlt: StyledIcon<any>;
export declare const CreativeCommonsPdAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
