import { StyledIcon, StyledIconProps } from '..';
export declare const Replyd: StyledIcon<any>;
export declare const ReplydDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
