import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mixcloud: StyledIcon<any>;
export declare const MixcloudDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
