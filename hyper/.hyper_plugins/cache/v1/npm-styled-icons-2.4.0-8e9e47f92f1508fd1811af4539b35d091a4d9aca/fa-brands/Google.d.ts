import { StyledIcon, StyledIconProps } from '..';
export declare const Google: StyledIcon<any>;
export declare const GoogleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
