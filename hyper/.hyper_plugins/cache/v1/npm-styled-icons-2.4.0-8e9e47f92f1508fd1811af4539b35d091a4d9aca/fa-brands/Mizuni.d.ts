import { StyledIcon, StyledIconProps } from '..';
export declare const Mizuni: StyledIcon<any>;
export declare const MizuniDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
