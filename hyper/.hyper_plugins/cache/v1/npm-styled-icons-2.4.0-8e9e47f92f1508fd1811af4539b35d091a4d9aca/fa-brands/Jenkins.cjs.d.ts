import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Jenkins: StyledIcon<any>;
export declare const JenkinsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
