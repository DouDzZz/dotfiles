import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Phabricator: StyledIcon<any>;
export declare const PhabricatorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
