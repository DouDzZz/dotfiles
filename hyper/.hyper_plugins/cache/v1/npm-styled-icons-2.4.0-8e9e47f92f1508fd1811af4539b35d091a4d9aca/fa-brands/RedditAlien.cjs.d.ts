import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RedditAlien: StyledIcon<any>;
export declare const RedditAlienDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
