import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NintendoSwitch: StyledIcon<any>;
export declare const NintendoSwitchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
