import { StyledIcon, StyledIconProps } from '..';
export declare const LastfmSquare: StyledIcon<any>;
export declare const LastfmSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
