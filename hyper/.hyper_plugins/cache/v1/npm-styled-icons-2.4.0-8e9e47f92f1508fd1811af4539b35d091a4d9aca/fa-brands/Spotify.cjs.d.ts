import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Spotify: StyledIcon<any>;
export declare const SpotifyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
