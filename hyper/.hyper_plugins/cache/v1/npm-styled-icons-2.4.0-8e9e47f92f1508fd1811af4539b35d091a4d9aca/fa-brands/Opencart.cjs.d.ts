import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Opencart: StyledIcon<any>;
export declare const OpencartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
