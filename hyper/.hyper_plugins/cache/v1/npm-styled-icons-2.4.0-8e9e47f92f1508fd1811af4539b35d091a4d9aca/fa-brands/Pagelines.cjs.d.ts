import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pagelines: StyledIcon<any>;
export declare const PagelinesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
