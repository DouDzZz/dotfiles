import { StyledIcon, StyledIconProps } from '..';
export declare const Opera: StyledIcon<any>;
export declare const OperaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
