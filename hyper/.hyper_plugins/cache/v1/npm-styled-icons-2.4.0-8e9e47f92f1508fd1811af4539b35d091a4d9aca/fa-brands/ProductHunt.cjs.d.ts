import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ProductHunt: StyledIcon<any>;
export declare const ProductHuntDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
