import { StyledIcon, StyledIconProps } from '..';
export declare const Algolia: StyledIcon<any>;
export declare const AlgoliaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
