import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DAndD: StyledIcon<any>;
export declare const DAndDDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
