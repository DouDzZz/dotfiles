import { StyledIcon, StyledIconProps } from '..';
export declare const Meetup: StyledIcon<any>;
export declare const MeetupDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
