import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Supple: StyledIcon<any>;
export declare const SuppleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
