import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FreeCodeCamp: StyledIcon<any>;
export declare const FreeCodeCampDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
