import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Medapps: StyledIcon<any>;
export declare const MedappsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
