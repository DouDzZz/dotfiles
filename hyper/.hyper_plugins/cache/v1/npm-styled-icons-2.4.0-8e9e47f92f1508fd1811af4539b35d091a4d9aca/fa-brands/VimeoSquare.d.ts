import { StyledIcon, StyledIconProps } from '..';
export declare const VimeoSquare: StyledIcon<any>;
export declare const VimeoSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
