import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Strava: StyledIcon<any>;
export declare const StravaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
