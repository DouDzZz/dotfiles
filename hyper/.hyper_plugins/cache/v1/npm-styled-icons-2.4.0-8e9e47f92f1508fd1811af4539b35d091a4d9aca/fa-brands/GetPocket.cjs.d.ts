import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GetPocket: StyledIcon<any>;
export declare const GetPocketDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
