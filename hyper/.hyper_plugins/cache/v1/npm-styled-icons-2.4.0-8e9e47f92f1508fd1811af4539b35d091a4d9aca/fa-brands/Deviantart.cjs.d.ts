import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Deviantart: StyledIcon<any>;
export declare const DeviantartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
