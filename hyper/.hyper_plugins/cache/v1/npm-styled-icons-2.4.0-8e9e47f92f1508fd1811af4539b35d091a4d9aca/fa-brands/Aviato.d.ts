import { StyledIcon, StyledIconProps } from '..';
export declare const Aviato: StyledIcon<any>;
export declare const AviatoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
