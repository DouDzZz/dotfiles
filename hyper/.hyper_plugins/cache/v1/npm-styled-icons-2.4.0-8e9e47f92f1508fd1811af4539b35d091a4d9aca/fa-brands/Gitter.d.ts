import { StyledIcon, StyledIconProps } from '..';
export declare const Gitter: StyledIcon<any>;
export declare const GitterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
