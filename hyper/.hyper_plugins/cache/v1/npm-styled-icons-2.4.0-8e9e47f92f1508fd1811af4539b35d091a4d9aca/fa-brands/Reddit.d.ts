import { StyledIcon, StyledIconProps } from '..';
export declare const Reddit: StyledIcon<any>;
export declare const RedditDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
