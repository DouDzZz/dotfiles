import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BluetoothB: StyledIcon<any>;
export declare const BluetoothBDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
