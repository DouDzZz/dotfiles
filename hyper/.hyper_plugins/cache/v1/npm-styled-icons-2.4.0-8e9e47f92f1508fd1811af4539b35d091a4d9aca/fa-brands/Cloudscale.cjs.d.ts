import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cloudscale: StyledIcon<any>;
export declare const CloudscaleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
