import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsBy: StyledIcon<any>;
export declare const CreativeCommonsByDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
