import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Typo3: StyledIcon<any>;
export declare const Typo3Dimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
