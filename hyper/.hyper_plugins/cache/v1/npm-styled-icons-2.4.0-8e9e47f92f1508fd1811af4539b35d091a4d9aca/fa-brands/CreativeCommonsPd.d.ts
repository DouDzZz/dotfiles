import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsPd: StyledIcon<any>;
export declare const CreativeCommonsPdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
