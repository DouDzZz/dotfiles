import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OptinMonster: StyledIcon<any>;
export declare const OptinMonsterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
