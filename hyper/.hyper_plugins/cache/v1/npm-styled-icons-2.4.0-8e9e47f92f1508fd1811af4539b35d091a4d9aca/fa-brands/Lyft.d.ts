import { StyledIcon, StyledIconProps } from '..';
export declare const Lyft: StyledIcon<any>;
export declare const LyftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
