import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FirstOrder: StyledIcon<any>;
export declare const FirstOrderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
