import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gofore: StyledIcon<any>;
export declare const GoforeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
