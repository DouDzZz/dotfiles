import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcVisa: StyledIcon<any>;
export declare const CcVisaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
