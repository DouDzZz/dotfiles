import { StyledIcon, StyledIconProps } from '..';
export declare const Drupal: StyledIcon<any>;
export declare const DrupalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
