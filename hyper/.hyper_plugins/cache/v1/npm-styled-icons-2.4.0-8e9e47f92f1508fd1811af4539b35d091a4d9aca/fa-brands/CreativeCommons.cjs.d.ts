import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommons: StyledIcon<any>;
export declare const CreativeCommonsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
