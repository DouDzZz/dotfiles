import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PiedPiperAlt: StyledIcon<any>;
export declare const PiedPiperAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
