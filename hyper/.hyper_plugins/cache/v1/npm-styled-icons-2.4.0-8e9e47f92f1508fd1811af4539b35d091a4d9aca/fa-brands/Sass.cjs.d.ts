import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sass: StyledIcon<any>;
export declare const SassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
