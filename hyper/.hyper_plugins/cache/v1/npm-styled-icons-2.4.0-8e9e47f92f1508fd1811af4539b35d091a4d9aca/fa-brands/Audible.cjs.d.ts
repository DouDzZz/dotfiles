import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Audible: StyledIcon<any>;
export declare const AudibleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
