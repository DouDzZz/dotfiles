import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GalacticSenate: StyledIcon<any>;
export declare const GalacticSenateDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
