import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Slideshare: StyledIcon<any>;
export declare const SlideshareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
