import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsRemix: StyledIcon<any>;
export declare const CreativeCommonsRemixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
