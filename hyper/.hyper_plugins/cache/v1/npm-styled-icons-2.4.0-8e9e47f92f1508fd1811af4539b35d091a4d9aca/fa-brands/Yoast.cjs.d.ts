import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Yoast: StyledIcon<any>;
export declare const YoastDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
