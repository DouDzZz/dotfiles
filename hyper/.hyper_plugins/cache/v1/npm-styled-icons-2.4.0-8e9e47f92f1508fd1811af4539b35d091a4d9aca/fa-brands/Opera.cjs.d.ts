import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Opera: StyledIcon<any>;
export declare const OperaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
