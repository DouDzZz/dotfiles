import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Medium: StyledIcon<any>;
export declare const MediumDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
