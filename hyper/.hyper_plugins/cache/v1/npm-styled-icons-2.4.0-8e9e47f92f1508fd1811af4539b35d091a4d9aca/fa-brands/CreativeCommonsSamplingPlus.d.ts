import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsSamplingPlus: StyledIcon<any>;
export declare const CreativeCommonsSamplingPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
