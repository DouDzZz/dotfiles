import { StyledIcon, StyledIconProps } from '..';
export declare const CcPaypal: StyledIcon<any>;
export declare const CcPaypalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
