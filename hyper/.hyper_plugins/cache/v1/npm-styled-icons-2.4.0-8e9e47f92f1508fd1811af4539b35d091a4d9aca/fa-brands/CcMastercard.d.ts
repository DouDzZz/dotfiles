import { StyledIcon, StyledIconProps } from '..';
export declare const CcMastercard: StyledIcon<any>;
export declare const CcMastercardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
