import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsSa: StyledIcon<any>;
export declare const CreativeCommonsSaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
