import { StyledIcon, StyledIconProps } from '..';
export declare const StumbleuponCircle: StyledIcon<any>;
export declare const StumbleuponCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
