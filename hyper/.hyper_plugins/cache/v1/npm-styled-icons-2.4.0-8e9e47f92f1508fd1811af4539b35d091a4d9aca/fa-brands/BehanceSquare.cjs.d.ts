import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BehanceSquare: StyledIcon<any>;
export declare const BehanceSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
