import { StyledIcon, StyledIconProps } from '..';
export declare const Monero: StyledIcon<any>;
export declare const MoneroDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
