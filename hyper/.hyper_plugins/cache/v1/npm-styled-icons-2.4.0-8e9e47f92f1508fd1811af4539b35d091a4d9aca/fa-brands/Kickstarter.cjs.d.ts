import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Kickstarter: StyledIcon<any>;
export declare const KickstarterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
