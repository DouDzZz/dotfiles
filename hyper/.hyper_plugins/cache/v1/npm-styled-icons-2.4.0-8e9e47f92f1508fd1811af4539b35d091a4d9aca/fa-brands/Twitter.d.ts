import { StyledIcon, StyledIconProps } from '..';
export declare const Twitter: StyledIcon<any>;
export declare const TwitterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
