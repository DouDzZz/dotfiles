import { StyledIcon, StyledIconProps } from '..';
export declare const CcJcb: StyledIcon<any>;
export declare const CcJcbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
