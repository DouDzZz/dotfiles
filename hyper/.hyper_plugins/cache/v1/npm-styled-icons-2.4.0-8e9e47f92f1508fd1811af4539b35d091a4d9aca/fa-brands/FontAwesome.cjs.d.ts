import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FontAwesome: StyledIcon<any>;
export declare const FontAwesomeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
