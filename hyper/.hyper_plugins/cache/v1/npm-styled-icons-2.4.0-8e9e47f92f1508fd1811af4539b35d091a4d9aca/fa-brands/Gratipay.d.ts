import { StyledIcon, StyledIconProps } from '..';
export declare const Gratipay: StyledIcon<any>;
export declare const GratipayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
