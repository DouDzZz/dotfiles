import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Leanpub: StyledIcon<any>;
export declare const LeanpubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
