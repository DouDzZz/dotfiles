import { StyledIcon, StyledIconProps } from '..';
export declare const Github: StyledIcon<any>;
export declare const GithubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
