import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Deskpro: StyledIcon<any>;
export declare const DeskproDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
