import { StyledIcon, StyledIconProps } from '..';
export declare const Behance: StyledIcon<any>;
export declare const BehanceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
