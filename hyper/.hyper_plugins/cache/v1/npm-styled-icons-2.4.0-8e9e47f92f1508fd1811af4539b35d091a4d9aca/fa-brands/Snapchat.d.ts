import { StyledIcon, StyledIconProps } from '..';
export declare const Snapchat: StyledIcon<any>;
export declare const SnapchatDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
