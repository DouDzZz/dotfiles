import { StyledIcon, StyledIconProps } from '..';
export declare const Periscope: StyledIcon<any>;
export declare const PeriscopeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
