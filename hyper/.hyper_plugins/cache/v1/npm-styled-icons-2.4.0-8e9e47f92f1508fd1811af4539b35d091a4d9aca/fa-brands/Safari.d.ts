import { StyledIcon, StyledIconProps } from '..';
export declare const Safari: StyledIcon<any>;
export declare const SafariDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
