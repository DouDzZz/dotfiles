import { StyledIcon, StyledIconProps } from '..';
export declare const Leanpub: StyledIcon<any>;
export declare const LeanpubDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
