import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsNd: StyledIcon<any>;
export declare const CreativeCommonsNdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
