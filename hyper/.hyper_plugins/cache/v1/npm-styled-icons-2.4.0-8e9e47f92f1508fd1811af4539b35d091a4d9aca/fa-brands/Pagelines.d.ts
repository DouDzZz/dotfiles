import { StyledIcon, StyledIconProps } from '..';
export declare const Pagelines: StyledIcon<any>;
export declare const PagelinesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
