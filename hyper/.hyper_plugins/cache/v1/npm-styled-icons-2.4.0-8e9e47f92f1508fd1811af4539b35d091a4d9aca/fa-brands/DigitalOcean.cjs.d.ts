import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DigitalOcean: StyledIcon<any>;
export declare const DigitalOceanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
