import { StyledIcon, StyledIconProps } from '..';
export declare const Git: StyledIcon<any>;
export declare const GitDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
