import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Buromobelexperte: StyledIcon<any>;
export declare const BuromobelexperteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
