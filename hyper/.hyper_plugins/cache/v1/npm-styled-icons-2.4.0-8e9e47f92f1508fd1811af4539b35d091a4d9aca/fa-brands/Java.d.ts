import { StyledIcon, StyledIconProps } from '..';
export declare const Java: StyledIcon<any>;
export declare const JavaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
