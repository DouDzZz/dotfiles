import { StyledIcon, StyledIconProps } from '..';
export declare const FirstOrder: StyledIcon<any>;
export declare const FirstOrderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
