import { StyledIcon, StyledIconProps } from '..';
export declare const HackerNewsSquare: StyledIcon<any>;
export declare const HackerNewsSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
