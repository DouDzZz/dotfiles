import { StyledIcon, StyledIconProps } from '..';
export declare const Skype: StyledIcon<any>;
export declare const SkypeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
