import { StyledIcon, StyledIconProps } from '..';
export declare const Node: StyledIcon<any>;
export declare const NodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
