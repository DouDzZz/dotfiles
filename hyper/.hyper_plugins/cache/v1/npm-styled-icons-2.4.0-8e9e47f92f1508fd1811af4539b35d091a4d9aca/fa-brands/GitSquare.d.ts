import { StyledIcon, StyledIconProps } from '..';
export declare const GitSquare: StyledIcon<any>;
export declare const GitSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
