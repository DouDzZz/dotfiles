import { StyledIcon, StyledIconProps } from '..';
export declare const CcApplePay: StyledIcon<any>;
export declare const CcApplePayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
