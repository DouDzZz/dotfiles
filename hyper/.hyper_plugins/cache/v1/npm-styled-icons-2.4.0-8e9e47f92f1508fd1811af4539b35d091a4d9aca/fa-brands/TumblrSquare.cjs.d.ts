import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TumblrSquare: StyledIcon<any>;
export declare const TumblrSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
