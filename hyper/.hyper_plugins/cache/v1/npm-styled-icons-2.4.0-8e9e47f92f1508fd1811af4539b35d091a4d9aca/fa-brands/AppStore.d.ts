import { StyledIcon, StyledIconProps } from '..';
export declare const AppStore: StyledIcon<any>;
export declare const AppStoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
