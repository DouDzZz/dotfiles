import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Whatsapp: StyledIcon<any>;
export declare const WhatsappDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
