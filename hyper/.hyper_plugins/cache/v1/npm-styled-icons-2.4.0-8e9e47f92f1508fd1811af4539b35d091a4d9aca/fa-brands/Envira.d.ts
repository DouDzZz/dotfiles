import { StyledIcon, StyledIconProps } from '..';
export declare const Envira: StyledIcon<any>;
export declare const EnviraDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
