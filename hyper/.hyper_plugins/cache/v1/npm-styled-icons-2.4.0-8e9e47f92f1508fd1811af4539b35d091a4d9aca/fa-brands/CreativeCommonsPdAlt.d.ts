import { StyledIcon, StyledIconProps } from '..';
export declare const CreativeCommonsPdAlt: StyledIcon<any>;
export declare const CreativeCommonsPdAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
