import { StyledIcon, StyledIconProps } from '..';
export declare const Empire: StyledIcon<any>;
export declare const EmpireDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
