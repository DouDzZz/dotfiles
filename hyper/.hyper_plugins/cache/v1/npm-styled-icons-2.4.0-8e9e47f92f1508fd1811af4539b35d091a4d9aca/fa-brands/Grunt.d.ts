import { StyledIcon, StyledIconProps } from '..';
export declare const Grunt: StyledIcon<any>;
export declare const GruntDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
