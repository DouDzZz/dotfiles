import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GithubAlt: StyledIcon<any>;
export declare const GithubAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
