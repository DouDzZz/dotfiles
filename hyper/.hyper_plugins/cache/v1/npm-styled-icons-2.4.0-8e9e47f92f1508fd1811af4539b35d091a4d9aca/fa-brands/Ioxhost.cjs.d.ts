import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ioxhost: StyledIcon<any>;
export declare const IoxhostDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
