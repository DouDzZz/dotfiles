import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Amazon: StyledIcon<any>;
export declare const AmazonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
