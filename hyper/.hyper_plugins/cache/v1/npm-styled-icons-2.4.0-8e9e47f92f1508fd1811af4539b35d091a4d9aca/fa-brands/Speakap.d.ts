import { StyledIcon, StyledIconProps } from '..';
export declare const Speakap: StyledIcon<any>;
export declare const SpeakapDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
