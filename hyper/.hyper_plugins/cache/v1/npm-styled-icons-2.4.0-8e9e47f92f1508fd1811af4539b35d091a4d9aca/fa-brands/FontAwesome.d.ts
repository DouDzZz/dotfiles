import { StyledIcon, StyledIconProps } from '..';
export declare const FontAwesome: StyledIcon<any>;
export declare const FontAwesomeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
