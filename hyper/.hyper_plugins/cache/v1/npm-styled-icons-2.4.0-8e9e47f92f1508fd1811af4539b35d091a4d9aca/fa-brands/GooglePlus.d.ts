import { StyledIcon, StyledIconProps } from '..';
export declare const GooglePlus: StyledIcon<any>;
export declare const GooglePlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
