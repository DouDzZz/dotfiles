import { StyledIcon, StyledIconProps } from '..';
export declare const Mixcloud: StyledIcon<any>;
export declare const MixcloudDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
