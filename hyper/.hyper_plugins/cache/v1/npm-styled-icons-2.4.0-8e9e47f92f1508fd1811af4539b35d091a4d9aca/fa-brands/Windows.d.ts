import { StyledIcon, StyledIconProps } from '..';
export declare const Windows: StyledIcon<any>;
export declare const WindowsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
