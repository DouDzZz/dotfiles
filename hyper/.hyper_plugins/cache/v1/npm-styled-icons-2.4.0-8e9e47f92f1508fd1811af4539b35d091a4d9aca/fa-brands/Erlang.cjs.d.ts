import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Erlang: StyledIcon<any>;
export declare const ErlangDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
