import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wpforms: StyledIcon<any>;
export declare const WpformsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
