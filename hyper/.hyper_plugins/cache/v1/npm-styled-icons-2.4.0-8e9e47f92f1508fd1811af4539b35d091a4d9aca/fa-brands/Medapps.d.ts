import { StyledIcon, StyledIconProps } from '..';
export declare const Medapps: StyledIcon<any>;
export declare const MedappsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
