import { StyledIcon, StyledIconProps } from '..';
export declare const TelegramPlane: StyledIcon<any>;
export declare const TelegramPlaneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
