import { StyledIcon, StyledIconProps } from '..';
export declare const Less: StyledIcon<any>;
export declare const LessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
