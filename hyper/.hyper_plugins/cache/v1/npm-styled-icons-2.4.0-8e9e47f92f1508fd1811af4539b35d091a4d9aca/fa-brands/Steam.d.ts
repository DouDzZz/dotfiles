import { StyledIcon, StyledIconProps } from '..';
export declare const Steam: StyledIcon<any>;
export declare const SteamDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
