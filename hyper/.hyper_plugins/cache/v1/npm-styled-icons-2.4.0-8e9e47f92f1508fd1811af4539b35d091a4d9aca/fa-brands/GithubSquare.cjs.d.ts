import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GithubSquare: StyledIcon<any>;
export declare const GithubSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
