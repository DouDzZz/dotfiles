import { StyledIcon, StyledIconProps } from '..';
export declare const CcAmazonPay: StyledIcon<any>;
export declare const CcAmazonPayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
