import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Linode: StyledIcon<any>;
export declare const LinodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
