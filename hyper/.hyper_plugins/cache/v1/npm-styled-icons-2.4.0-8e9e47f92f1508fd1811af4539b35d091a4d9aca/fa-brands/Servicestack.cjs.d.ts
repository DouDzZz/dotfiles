import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Servicestack: StyledIcon<any>;
export declare const ServicestackDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
