import { StyledIcon, StyledIconProps } from '..';
export declare const PinterestP: StyledIcon<any>;
export declare const PinterestPDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
