import { StyledIcon, StyledIconProps } from '..';
export declare const RProject: StyledIcon<any>;
export declare const RProjectDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
