import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsRemix: StyledIcon<any>;
export declare const CreativeCommonsRemixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
