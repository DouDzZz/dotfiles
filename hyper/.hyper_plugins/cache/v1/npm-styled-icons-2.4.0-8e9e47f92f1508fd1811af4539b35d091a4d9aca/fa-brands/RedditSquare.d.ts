import { StyledIcon, StyledIconProps } from '..';
export declare const RedditSquare: StyledIcon<any>;
export declare const RedditSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
