import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Viadeo: StyledIcon<any>;
export declare const ViadeoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
