import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Aws: StyledIcon<any>;
export declare const AwsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
