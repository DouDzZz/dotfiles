import { StyledIcon, StyledIconProps } from '..';
export declare const Fulcrum: StyledIcon<any>;
export declare const FulcrumDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
