import { StyledIcon, StyledIconProps } from '..';
export declare const Bitcoin: StyledIcon<any>;
export declare const BitcoinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
