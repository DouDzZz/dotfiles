import { StyledIcon, StyledIconProps } from '..';
export declare const GithubAlt: StyledIcon<any>;
export declare const GithubAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
