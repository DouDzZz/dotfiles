import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Facebook: StyledIcon<any>;
export declare const FacebookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
