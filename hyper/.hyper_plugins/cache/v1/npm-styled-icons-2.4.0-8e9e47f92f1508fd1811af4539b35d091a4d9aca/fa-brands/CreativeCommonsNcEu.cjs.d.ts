import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsNcEu: StyledIcon<any>;
export declare const CreativeCommonsNcEuDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
