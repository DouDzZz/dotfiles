import { StyledIcon, StyledIconProps } from '..';
export declare const Weibo: StyledIcon<any>;
export declare const WeiboDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
