import { StyledIcon, StyledIconProps } from '..';
export declare const SnapchatSquare: StyledIcon<any>;
export declare const SnapchatSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
