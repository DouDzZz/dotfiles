import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Imdb: StyledIcon<any>;
export declare const ImdbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
