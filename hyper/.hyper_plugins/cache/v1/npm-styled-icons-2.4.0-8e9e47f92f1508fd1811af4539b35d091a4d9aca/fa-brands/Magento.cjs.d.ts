import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Magento: StyledIcon<any>;
export declare const MagentoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
