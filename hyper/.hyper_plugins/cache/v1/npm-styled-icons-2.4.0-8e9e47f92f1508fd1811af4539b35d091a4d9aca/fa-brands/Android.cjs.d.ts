import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Android: StyledIcon<any>;
export declare const AndroidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
