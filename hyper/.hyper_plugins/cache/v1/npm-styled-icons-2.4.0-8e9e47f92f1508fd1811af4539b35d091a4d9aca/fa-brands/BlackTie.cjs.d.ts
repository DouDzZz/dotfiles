import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BlackTie: StyledIcon<any>;
export declare const BlackTieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
