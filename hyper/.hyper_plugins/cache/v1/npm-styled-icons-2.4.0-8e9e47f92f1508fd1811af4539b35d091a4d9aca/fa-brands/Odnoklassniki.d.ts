import { StyledIcon, StyledIconProps } from '..';
export declare const Odnoklassniki: StyledIcon<any>;
export declare const OdnoklassnikiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
