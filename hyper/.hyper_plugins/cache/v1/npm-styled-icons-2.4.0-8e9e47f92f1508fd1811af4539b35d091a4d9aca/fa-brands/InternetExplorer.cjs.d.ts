import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InternetExplorer: StyledIcon<any>;
export declare const InternetExplorerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
