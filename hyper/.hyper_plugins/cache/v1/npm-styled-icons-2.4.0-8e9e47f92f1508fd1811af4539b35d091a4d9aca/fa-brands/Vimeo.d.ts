import { StyledIcon, StyledIconProps } from '..';
export declare const Vimeo: StyledIcon<any>;
export declare const VimeoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
