import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Html5: StyledIcon<any>;
export declare const Html5Dimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
