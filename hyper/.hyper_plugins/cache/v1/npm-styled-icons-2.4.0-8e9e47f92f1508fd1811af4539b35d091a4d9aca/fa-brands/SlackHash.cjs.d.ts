import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SlackHash: StyledIcon<any>;
export declare const SlackHashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
