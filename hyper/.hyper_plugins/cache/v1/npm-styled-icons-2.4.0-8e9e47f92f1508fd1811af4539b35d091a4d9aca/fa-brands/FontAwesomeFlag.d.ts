import { StyledIcon, StyledIconProps } from '..';
export declare const FontAwesomeFlag: StyledIcon<any>;
export declare const FontAwesomeFlagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
