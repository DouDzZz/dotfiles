import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsShare: StyledIcon<any>;
export declare const CreativeCommonsShareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
