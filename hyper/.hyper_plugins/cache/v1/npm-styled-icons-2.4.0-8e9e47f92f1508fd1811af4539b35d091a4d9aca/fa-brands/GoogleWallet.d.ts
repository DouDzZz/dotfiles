import { StyledIcon, StyledIconProps } from '..';
export declare const GoogleWallet: StyledIcon<any>;
export declare const GoogleWalletDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
