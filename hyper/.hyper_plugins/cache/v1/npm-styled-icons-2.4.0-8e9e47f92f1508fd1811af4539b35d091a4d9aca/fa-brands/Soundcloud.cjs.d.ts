import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Soundcloud: StyledIcon<any>;
export declare const SoundcloudDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
