import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GgCircle: StyledIcon<any>;
export declare const GgCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
