import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Discourse: StyledIcon<any>;
export declare const DiscourseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
