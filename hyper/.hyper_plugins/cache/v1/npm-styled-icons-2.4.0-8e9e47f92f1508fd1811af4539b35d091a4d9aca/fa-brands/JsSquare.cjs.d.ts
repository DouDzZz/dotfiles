import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const JsSquare: StyledIcon<any>;
export declare const JsSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
