import { StyledIcon, StyledIconProps } from '..';
export declare const PhoenixSquadron: StyledIcon<any>;
export declare const PhoenixSquadronDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
