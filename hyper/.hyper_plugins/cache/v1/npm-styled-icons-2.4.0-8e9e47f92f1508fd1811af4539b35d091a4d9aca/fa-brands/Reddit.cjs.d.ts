import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Reddit: StyledIcon<any>;
export declare const RedditDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
