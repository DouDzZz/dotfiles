import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Quora: StyledIcon<any>;
export declare const QuoraDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
