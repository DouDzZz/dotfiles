import { StyledIcon, StyledIconProps } from '..';
export declare const Blogger: StyledIcon<any>;
export declare const BloggerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
