import { StyledIcon, StyledIconProps } from '..';
export declare const Viber: StyledIcon<any>;
export declare const ViberDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
