import { StyledIcon, StyledIconProps } from '..';
export declare const Bitbucket: StyledIcon<any>;
export declare const BitbucketDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
