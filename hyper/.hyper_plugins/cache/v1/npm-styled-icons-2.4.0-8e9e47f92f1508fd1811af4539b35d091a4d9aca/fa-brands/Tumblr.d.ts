import { StyledIcon, StyledIconProps } from '..';
export declare const Tumblr: StyledIcon<any>;
export declare const TumblrDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
