import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Steam: StyledIcon<any>;
export declare const SteamDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
