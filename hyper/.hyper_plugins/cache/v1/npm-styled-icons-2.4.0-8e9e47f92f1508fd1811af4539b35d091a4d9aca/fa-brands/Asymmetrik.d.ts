import { StyledIcon, StyledIconProps } from '..';
export declare const Asymmetrik: StyledIcon<any>;
export declare const AsymmetrikDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
