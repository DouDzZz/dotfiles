import { StyledIcon, StyledIconProps } from '..';
export declare const CcAmex: StyledIcon<any>;
export declare const CcAmexDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
