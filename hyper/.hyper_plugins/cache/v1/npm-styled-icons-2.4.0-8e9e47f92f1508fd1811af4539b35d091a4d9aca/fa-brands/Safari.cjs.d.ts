import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Safari: StyledIcon<any>;
export declare const SafariDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
