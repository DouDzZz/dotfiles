import { StyledIcon, StyledIconProps } from '..';
export declare const Telegram: StyledIcon<any>;
export declare const TelegramDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
