import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StripeS: StyledIcon<any>;
export declare const StripeSDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
