import { StyledIcon, StyledIconProps } from '..';
export declare const StackExchange: StyledIcon<any>;
export declare const StackExchangeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
