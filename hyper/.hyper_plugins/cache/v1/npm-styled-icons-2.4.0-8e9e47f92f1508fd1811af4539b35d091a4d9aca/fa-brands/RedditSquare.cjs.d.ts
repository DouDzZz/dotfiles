import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RedditSquare: StyledIcon<any>;
export declare const RedditSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
