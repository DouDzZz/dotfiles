import { StyledIcon, StyledIconProps } from '..';
export declare const Slack: StyledIcon<any>;
export declare const SlackDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
