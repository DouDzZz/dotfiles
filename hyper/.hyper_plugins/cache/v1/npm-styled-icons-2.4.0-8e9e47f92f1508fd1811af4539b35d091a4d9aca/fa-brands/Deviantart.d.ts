import { StyledIcon, StyledIconProps } from '..';
export declare const Deviantart: StyledIcon<any>;
export declare const DeviantartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
