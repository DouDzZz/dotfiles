import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Nutritionix: StyledIcon<any>;
export declare const NutritionixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
