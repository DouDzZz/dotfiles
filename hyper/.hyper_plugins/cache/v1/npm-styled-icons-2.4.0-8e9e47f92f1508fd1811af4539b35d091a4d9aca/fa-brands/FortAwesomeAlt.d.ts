import { StyledIcon, StyledIconProps } from '..';
export declare const FortAwesomeAlt: StyledIcon<any>;
export declare const FortAwesomeAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
