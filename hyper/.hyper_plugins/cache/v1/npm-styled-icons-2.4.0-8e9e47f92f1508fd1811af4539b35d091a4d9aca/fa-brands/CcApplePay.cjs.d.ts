import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CcApplePay: StyledIcon<any>;
export declare const CcApplePayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
