import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hips: StyledIcon<any>;
export declare const HipsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
