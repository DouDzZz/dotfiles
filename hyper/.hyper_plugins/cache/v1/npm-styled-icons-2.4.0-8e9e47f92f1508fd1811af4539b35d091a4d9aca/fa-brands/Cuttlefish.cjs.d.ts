import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cuttlefish: StyledIcon<any>;
export declare const CuttlefishDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
