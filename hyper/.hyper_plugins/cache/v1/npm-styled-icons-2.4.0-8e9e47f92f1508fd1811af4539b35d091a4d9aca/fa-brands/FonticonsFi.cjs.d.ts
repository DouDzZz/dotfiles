import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FonticonsFi: StyledIcon<any>;
export declare const FonticonsFiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
