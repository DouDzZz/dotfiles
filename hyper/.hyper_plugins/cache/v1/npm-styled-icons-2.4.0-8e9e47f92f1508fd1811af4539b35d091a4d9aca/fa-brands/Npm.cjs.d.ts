import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Npm: StyledIcon<any>;
export declare const NpmDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
