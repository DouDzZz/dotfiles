import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Python: StyledIcon<any>;
export declare const PythonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
