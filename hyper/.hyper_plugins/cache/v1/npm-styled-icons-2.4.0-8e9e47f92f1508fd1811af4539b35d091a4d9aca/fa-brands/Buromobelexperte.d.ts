import { StyledIcon, StyledIconProps } from '..';
export declare const Buromobelexperte: StyledIcon<any>;
export declare const BuromobelexperteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
