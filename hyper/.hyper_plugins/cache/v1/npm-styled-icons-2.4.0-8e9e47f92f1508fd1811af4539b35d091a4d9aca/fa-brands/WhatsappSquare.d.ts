import { StyledIcon, StyledIconProps } from '..';
export declare const WhatsappSquare: StyledIcon<any>;
export declare const WhatsappSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
