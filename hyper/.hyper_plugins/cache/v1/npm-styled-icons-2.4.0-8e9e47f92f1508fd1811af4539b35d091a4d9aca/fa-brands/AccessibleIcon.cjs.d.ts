import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AccessibleIcon: StyledIcon<any>;
export declare const AccessibleIconDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
