import { StyledIcon, StyledIconProps } from '..';
export declare const Xing: StyledIcon<any>;
export declare const XingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
