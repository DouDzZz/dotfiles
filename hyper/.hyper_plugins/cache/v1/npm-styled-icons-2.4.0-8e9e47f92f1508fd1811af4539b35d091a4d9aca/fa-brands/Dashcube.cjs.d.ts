import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dashcube: StyledIcon<any>;
export declare const DashcubeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
