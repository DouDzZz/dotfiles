import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreativeCommonsSampling: StyledIcon<any>;
export declare const CreativeCommonsSamplingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
