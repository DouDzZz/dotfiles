import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bitcoin: StyledIcon<any>;
export declare const BitcoinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
