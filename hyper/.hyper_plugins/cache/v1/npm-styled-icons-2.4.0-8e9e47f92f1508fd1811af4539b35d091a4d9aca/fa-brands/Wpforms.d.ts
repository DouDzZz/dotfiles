import { StyledIcon, StyledIconProps } from '..';
export declare const Wpforms: StyledIcon<any>;
export declare const WpformsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
