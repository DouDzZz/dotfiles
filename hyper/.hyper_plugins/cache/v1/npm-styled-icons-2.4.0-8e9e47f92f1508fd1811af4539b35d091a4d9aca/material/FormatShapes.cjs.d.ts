import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatShapes: StyledIcon<any>;
export declare const FormatShapesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
