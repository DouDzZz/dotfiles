import { StyledIcon, StyledIconProps } from '..';
export declare const FormatSize: StyledIcon<any>;
export declare const FormatSizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
