import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tram: StyledIcon<any>;
export declare const TramDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
