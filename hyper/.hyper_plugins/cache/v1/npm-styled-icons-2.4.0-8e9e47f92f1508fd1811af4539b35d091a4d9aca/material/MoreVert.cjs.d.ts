import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoreVert: StyledIcon<any>;
export declare const MoreVertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
