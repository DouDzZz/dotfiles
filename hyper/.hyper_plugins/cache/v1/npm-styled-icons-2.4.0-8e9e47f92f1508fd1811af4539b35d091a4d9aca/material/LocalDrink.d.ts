import { StyledIcon, StyledIconProps } from '..';
export declare const LocalDrink: StyledIcon<any>;
export declare const LocalDrinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
