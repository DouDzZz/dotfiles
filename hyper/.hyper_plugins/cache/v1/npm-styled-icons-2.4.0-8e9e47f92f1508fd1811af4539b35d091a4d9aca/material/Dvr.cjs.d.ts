import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dvr: StyledIcon<any>;
export declare const DvrDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
