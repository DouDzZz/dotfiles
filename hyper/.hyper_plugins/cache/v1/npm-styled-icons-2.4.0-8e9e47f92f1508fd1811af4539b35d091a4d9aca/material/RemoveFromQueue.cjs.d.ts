import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RemoveFromQueue: StyledIcon<any>;
export declare const RemoveFromQueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
