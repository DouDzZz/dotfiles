import { StyledIcon, StyledIconProps } from '..';
export declare const FormatListNumbered: StyledIcon<any>;
export declare const FormatListNumberedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
