import { StyledIcon, StyledIconProps } from '..';
export declare const More: StyledIcon<any>;
export declare const MoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
