import { StyledIcon, StyledIconProps } from '..';
export declare const Transform: StyledIcon<any>;
export declare const TransformDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
