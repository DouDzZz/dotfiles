import { StyledIcon, StyledIconProps } from '..';
export declare const ChildFriendly: StyledIcon<any>;
export declare const ChildFriendlyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
