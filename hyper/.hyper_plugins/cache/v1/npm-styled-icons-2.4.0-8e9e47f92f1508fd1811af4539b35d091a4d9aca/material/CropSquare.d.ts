import { StyledIcon, StyledIconProps } from '..';
export declare const CropSquare: StyledIcon<any>;
export declare const CropSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
