import { StyledIcon, StyledIconProps } from '..';
export declare const StayPrimaryPortrait: StyledIcon<any>;
export declare const StayPrimaryPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
