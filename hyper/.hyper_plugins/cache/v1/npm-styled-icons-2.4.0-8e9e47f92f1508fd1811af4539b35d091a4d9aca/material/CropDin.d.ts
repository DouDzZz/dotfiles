import { StyledIcon, StyledIconProps } from '..';
export declare const CropDin: StyledIcon<any>;
export declare const CropDinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
