import { StyledIcon, StyledIconProps } from '..';
export declare const QueryBuilder: StyledIcon<any>;
export declare const QueryBuilderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
