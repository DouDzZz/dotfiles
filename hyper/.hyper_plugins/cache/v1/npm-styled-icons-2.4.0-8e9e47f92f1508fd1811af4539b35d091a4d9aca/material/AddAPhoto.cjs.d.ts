import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddAPhoto: StyledIcon<any>;
export declare const AddAPhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
