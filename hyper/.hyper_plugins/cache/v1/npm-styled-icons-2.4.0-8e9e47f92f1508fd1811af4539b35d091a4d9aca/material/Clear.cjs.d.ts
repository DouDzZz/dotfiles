import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Clear: StyledIcon<any>;
export declare const ClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
