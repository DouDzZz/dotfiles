import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Web: StyledIcon<any>;
export declare const WebDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
