import { StyledIcon, StyledIconProps } from '..';
export declare const InvertColors: StyledIcon<any>;
export declare const InvertColorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
