import { StyledIcon, StyledIconProps } from '..';
export declare const FiberNew: StyledIcon<any>;
export declare const FiberNewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
