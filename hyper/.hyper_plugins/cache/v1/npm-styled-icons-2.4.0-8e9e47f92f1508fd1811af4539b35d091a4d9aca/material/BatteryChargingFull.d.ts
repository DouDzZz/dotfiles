import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryChargingFull: StyledIcon<any>;
export declare const BatteryChargingFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
