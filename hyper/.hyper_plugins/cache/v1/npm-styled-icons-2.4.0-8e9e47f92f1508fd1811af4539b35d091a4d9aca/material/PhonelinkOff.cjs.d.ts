import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonelinkOff: StyledIcon<any>;
export declare const PhonelinkOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
