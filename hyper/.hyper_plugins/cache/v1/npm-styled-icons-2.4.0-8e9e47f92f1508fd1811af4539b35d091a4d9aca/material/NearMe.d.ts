import { StyledIcon, StyledIconProps } from '..';
export declare const NearMe: StyledIcon<any>;
export declare const NearMeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
