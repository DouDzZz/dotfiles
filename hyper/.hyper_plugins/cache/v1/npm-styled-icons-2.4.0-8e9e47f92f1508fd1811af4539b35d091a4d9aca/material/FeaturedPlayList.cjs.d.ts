import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FeaturedPlayList: StyledIcon<any>;
export declare const FeaturedPlayListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
