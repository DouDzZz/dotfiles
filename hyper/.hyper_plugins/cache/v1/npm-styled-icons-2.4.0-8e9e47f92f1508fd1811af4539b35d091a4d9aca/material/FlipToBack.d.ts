import { StyledIcon, StyledIconProps } from '..';
export declare const FlipToBack: StyledIcon<any>;
export declare const FlipToBackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
