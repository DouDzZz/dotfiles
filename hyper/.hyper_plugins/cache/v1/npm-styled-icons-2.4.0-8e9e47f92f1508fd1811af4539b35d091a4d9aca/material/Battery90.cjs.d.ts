import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery90: StyledIcon<any>;
export declare const Battery90Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
