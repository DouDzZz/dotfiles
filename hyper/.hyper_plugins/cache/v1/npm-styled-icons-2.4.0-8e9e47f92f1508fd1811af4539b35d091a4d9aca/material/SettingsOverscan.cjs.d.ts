import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsOverscan: StyledIcon<any>;
export declare const SettingsOverscanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
