import { StyledIcon, StyledIconProps } from '..';
export declare const LocationSearching: StyledIcon<any>;
export declare const LocationSearchingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
