import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellular4Bar: StyledIcon<any>;
export declare const SignalCellular4BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
