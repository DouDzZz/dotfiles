import { StyledIcon, StyledIconProps } from '..';
export declare const ChildCare: StyledIcon<any>;
export declare const ChildCareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
