import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsTransit: StyledIcon<any>;
export declare const DirectionsTransitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
