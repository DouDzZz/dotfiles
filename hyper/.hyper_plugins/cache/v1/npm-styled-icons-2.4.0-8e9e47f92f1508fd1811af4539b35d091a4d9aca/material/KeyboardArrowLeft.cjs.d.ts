import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardArrowLeft: StyledIcon<any>;
export declare const KeyboardArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
