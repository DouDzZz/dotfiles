import { StyledIcon, StyledIconProps } from '..';
export declare const Sync: StyledIcon<any>;
export declare const SyncDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
