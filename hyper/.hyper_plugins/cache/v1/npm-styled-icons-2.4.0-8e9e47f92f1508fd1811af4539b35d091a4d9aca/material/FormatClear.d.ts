import { StyledIcon, StyledIconProps } from '..';
export declare const FormatClear: StyledIcon<any>;
export declare const FormatClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
