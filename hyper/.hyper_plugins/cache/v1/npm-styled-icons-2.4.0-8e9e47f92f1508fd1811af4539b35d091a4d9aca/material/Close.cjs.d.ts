import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Close: StyledIcon<any>;
export declare const CloseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
