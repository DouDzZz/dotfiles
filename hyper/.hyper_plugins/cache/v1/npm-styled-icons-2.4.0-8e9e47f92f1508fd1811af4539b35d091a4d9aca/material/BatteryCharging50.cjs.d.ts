import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryCharging50: StyledIcon<any>;
export declare const BatteryCharging50Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
