import { StyledIcon, StyledIconProps } from '..';
export declare const LightbulbOutline: StyledIcon<any>;
export declare const LightbulbOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
