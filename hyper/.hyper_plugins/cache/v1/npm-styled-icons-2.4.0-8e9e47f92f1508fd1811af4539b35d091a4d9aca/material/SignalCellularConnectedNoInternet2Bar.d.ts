import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularConnectedNoInternet2Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
