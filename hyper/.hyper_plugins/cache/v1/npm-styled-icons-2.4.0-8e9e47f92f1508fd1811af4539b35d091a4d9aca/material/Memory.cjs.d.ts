import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Memory: StyledIcon<any>;
export declare const MemoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
