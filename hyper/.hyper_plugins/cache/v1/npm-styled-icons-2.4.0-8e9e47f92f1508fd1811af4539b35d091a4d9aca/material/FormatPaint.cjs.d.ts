import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatPaint: StyledIcon<any>;
export declare const FormatPaintDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
