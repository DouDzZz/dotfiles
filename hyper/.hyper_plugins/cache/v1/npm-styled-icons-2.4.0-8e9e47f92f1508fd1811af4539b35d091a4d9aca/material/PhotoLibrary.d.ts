import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoLibrary: StyledIcon<any>;
export declare const PhotoLibraryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
