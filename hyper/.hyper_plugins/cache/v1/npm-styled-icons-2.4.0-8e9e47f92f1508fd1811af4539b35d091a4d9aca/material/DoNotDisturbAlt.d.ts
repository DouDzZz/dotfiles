import { StyledIcon, StyledIconProps } from '..';
export declare const DoNotDisturbAlt: StyledIcon<any>;
export declare const DoNotDisturbAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
