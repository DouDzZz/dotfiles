import { StyledIcon, StyledIconProps } from '..';
export declare const LowPriority: StyledIcon<any>;
export declare const LowPriorityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
