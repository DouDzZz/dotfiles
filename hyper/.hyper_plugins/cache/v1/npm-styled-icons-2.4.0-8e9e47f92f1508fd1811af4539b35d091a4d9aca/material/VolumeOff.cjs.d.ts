import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeOff: StyledIcon<any>;
export declare const VolumeOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
