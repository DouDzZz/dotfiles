import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDropUp: StyledIcon<any>;
export declare const ArrowDropUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
