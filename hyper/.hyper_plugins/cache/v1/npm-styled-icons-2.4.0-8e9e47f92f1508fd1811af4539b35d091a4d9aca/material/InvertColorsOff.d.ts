import { StyledIcon, StyledIconProps } from '..';
export declare const InvertColorsOff: StyledIcon<any>;
export declare const InvertColorsOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
