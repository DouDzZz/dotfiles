import { StyledIcon, StyledIconProps } from '..';
export declare const CastConnected: StyledIcon<any>;
export declare const CastConnectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
