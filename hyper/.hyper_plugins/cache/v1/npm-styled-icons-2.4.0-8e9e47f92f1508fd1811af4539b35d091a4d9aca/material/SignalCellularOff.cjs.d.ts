import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularOff: StyledIcon<any>;
export declare const SignalCellularOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
