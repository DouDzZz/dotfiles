import { StyledIcon, StyledIconProps } from '..';
export declare const HourglassEmpty: StyledIcon<any>;
export declare const HourglassEmptyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
