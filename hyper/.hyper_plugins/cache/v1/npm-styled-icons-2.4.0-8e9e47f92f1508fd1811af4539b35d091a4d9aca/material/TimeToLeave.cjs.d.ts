import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TimeToLeave: StyledIcon<any>;
export declare const TimeToLeaveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
