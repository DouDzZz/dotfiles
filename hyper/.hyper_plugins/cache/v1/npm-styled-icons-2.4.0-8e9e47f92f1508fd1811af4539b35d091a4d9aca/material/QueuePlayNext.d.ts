import { StyledIcon, StyledIconProps } from '..';
export declare const QueuePlayNext: StyledIcon<any>;
export declare const QueuePlayNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
