import { StyledIcon, StyledIconProps } from '..';
export declare const Tonality: StyledIcon<any>;
export declare const TonalityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
