import { StyledIcon, StyledIconProps } from '..';
export declare const NoteAdd: StyledIcon<any>;
export declare const NoteAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
