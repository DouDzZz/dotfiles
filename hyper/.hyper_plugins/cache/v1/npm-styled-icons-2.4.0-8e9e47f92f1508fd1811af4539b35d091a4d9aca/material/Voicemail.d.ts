import { StyledIcon, StyledIconProps } from '..';
export declare const Voicemail: StyledIcon<any>;
export declare const VoicemailDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
