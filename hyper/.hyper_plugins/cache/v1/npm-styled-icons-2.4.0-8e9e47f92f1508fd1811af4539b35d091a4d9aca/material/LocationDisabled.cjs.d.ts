import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocationDisabled: StyledIcon<any>;
export declare const LocationDisabledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
