import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsRailway: StyledIcon<any>;
export declare const DirectionsRailwayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
