import { StyledIcon, StyledIconProps } from '..';
export declare const LocalHospital: StyledIcon<any>;
export declare const LocalHospitalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
