import { StyledIcon, StyledIconProps } from '..';
export declare const Cloud: StyledIcon<any>;
export declare const CloudDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
