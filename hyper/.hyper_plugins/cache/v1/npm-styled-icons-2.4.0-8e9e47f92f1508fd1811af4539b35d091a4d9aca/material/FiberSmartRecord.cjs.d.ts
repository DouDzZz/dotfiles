import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiberSmartRecord: StyledIcon<any>;
export declare const FiberSmartRecordDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
