import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DeveloperBoard: StyledIcon<any>;
export declare const DeveloperBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
