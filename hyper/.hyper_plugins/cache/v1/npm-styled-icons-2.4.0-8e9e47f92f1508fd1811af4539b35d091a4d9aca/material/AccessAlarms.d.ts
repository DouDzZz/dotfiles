import { StyledIcon, StyledIconProps } from '..';
export declare const AccessAlarms: StyledIcon<any>;
export declare const AccessAlarmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
