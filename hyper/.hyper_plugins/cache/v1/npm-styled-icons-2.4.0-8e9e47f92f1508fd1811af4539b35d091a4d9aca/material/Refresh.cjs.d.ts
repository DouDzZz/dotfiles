import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Refresh: StyledIcon<any>;
export declare const RefreshDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
