import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NoSim: StyledIcon<any>;
export declare const NoSimDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
