import { StyledIcon, StyledIconProps } from '..';
export declare const NextWeek: StyledIcon<any>;
export declare const NextWeekDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
