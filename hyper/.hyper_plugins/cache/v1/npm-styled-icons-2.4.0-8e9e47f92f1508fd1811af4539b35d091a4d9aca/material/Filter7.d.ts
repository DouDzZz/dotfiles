import { StyledIcon, StyledIconProps } from '..';
export declare const Filter7: StyledIcon<any>;
export declare const Filter7Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
