import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NewReleases: StyledIcon<any>;
export declare const NewReleasesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
