import { StyledIcon, StyledIconProps } from '..';
export declare const BorderAll: StyledIcon<any>;
export declare const BorderAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
