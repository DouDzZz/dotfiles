import { StyledIcon, StyledIconProps } from '..';
export declare const AssignmentTurnedIn: StyledIcon<any>;
export declare const AssignmentTurnedInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
