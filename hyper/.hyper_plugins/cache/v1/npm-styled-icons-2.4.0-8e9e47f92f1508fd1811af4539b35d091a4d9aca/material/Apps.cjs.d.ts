import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Apps: StyledIcon<any>;
export declare const AppsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
