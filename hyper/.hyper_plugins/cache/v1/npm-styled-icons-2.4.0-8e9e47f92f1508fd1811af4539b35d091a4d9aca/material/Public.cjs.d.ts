import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Public: StyledIcon<any>;
export declare const PublicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
