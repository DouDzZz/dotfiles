import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery50: StyledIcon<any>;
export declare const Battery50Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
