import { StyledIcon, StyledIconProps } from '..';
export declare const CallMissedOutgoing: StyledIcon<any>;
export declare const CallMissedOutgoingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
