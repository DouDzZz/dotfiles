import { StyledIcon, StyledIconProps } from '..';
export declare const BookmarkBorder: StyledIcon<any>;
export declare const BookmarkBorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
