import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhotoCamera: StyledIcon<any>;
export declare const PhotoCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
