import { StyledIcon, StyledIconProps } from '..';
export declare const Feedback: StyledIcon<any>;
export declare const FeedbackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
