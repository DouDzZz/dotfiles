import { StyledIcon, StyledIconProps } from '..';
export declare const Computer: StyledIcon<any>;
export declare const ComputerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
