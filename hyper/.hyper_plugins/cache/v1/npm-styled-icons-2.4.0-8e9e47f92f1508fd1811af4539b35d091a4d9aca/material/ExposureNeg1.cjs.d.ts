import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExposureNeg1: StyledIcon<any>;
export declare const ExposureNeg1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
