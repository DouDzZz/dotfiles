import { StyledIcon, StyledIconProps } from '..';
export declare const HotTub: StyledIcon<any>;
export declare const HotTubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
