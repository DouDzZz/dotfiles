import { StyledIcon, StyledIconProps } from '..';
export declare const PlusOne: StyledIcon<any>;
export declare const PlusOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
