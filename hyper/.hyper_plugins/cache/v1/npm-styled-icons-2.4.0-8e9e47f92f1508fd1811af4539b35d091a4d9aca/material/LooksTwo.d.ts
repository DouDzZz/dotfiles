import { StyledIcon, StyledIconProps } from '..';
export declare const LooksTwo: StyledIcon<any>;
export declare const LooksTwoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
