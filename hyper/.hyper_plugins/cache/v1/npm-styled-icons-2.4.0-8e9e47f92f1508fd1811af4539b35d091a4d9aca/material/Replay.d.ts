import { StyledIcon, StyledIconProps } from '..';
export declare const Replay: StyledIcon<any>;
export declare const ReplayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
