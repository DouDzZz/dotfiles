import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotificationsNone: StyledIcon<any>;
export declare const NotificationsNoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
