import { StyledIcon, StyledIconProps } from '..';
export declare const Colorize: StyledIcon<any>;
export declare const ColorizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
