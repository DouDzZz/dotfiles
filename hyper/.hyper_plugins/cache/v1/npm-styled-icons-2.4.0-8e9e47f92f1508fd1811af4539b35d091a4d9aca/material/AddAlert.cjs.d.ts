import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddAlert: StyledIcon<any>;
export declare const AddAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
