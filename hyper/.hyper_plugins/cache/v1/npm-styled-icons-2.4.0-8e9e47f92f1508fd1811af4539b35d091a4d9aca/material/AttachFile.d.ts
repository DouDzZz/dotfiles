import { StyledIcon, StyledIconProps } from '..';
export declare const AttachFile: StyledIcon<any>;
export declare const AttachFileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
