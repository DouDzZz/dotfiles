import { StyledIcon, StyledIconProps } from '..';
export declare const PhonelinkErase: StyledIcon<any>;
export declare const PhonelinkEraseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
