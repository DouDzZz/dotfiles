import { StyledIcon, StyledIconProps } from '..';
export declare const CardGiftcard: StyledIcon<any>;
export declare const CardGiftcardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
