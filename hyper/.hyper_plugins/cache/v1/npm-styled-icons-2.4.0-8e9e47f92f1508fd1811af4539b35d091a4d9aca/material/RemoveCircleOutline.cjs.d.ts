import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RemoveCircleOutline: StyledIcon<any>;
export declare const RemoveCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
