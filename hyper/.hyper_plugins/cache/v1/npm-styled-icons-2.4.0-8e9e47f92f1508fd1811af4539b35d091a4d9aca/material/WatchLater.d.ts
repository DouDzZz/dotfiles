import { StyledIcon, StyledIconProps } from '..';
export declare const WatchLater: StyledIcon<any>;
export declare const WatchLaterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
