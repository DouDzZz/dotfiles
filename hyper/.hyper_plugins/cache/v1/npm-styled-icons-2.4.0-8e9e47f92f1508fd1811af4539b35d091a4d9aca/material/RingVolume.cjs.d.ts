import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RingVolume: StyledIcon<any>;
export declare const RingVolumeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
