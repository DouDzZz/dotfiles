import { StyledIcon, StyledIconProps } from '..';
export declare const CropPortrait: StyledIcon<any>;
export declare const CropPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
