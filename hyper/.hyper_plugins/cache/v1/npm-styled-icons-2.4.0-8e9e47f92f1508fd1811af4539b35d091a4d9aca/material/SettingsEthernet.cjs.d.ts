import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsEthernet: StyledIcon<any>;
export declare const SettingsEthernetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
