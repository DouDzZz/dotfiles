import { StyledIcon, StyledIconProps } from '..';
export declare const Functions: StyledIcon<any>;
export declare const FunctionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
