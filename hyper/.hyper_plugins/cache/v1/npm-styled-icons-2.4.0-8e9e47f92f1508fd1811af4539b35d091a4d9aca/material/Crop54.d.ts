import { StyledIcon, StyledIconProps } from '..';
export declare const Crop54: StyledIcon<any>;
export declare const Crop54Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
