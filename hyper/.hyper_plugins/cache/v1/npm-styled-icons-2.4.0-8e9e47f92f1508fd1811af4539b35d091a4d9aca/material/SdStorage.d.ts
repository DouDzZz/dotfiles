import { StyledIcon, StyledIconProps } from '..';
export declare const SdStorage: StyledIcon<any>;
export declare const SdStorageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
