import { StyledIcon, StyledIconProps } from '..';
export declare const Build: StyledIcon<any>;
export declare const BuildDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
