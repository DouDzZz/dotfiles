import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsInputComponent: StyledIcon<any>;
export declare const SettingsInputComponentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
