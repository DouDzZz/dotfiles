import { StyledIcon, StyledIconProps } from '..';
export declare const TrackChanges: StyledIcon<any>;
export declare const TrackChangesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
