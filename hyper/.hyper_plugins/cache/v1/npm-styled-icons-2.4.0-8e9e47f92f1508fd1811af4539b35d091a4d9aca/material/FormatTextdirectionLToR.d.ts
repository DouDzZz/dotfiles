import { StyledIcon, StyledIconProps } from '..';
export declare const FormatTextdirectionLToR: StyledIcon<any>;
export declare const FormatTextdirectionLToRDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
