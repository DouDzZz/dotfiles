import { StyledIcon, StyledIconProps } from '..';
export declare const AlarmOn: StyledIcon<any>;
export declare const AlarmOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
