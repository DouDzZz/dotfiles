import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cached: StyledIcon<any>;
export declare const CachedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
