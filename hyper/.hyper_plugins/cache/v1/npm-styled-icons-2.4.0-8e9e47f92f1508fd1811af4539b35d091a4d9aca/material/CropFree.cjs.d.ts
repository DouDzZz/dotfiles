import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropFree: StyledIcon<any>;
export declare const CropFreeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
