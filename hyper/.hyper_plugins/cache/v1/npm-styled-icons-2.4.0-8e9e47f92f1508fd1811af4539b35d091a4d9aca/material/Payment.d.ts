import { StyledIcon, StyledIconProps } from '..';
export declare const Payment: StyledIcon<any>;
export declare const PaymentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
