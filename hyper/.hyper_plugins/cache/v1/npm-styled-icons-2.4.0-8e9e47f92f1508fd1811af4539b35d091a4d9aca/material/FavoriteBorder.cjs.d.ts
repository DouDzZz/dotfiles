import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FavoriteBorder: StyledIcon<any>;
export declare const FavoriteBorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
