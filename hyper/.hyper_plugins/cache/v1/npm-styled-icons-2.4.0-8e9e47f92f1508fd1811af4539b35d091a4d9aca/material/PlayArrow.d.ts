import { StyledIcon, StyledIconProps } from '..';
export declare const PlayArrow: StyledIcon<any>;
export declare const PlayArrowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
