import { StyledIcon, StyledIconProps } from '..';
export declare const Mouse: StyledIcon<any>;
export declare const MouseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
