import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pages: StyledIcon<any>;
export declare const PagesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
