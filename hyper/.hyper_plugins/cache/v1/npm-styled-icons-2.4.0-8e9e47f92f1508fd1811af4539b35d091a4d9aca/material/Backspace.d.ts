import { StyledIcon, StyledIconProps } from '..';
export declare const Backspace: StyledIcon<any>;
export declare const BackspaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
