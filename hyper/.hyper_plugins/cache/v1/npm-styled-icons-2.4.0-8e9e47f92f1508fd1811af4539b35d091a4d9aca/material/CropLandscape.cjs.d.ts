import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropLandscape: StyledIcon<any>;
export declare const CropLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
