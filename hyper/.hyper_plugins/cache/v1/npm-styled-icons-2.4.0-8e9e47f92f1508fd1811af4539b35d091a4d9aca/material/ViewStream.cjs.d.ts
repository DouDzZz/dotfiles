import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewStream: StyledIcon<any>;
export declare const ViewStreamDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
