import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiberNew: StyledIcon<any>;
export declare const FiberNewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
