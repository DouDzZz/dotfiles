import { StyledIcon, StyledIconProps } from '..';
export declare const PanoramaWideAngle: StyledIcon<any>;
export declare const PanoramaWideAngleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
