import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenLockPortrait: StyledIcon<any>;
export declare const ScreenLockPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
