import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsCell: StyledIcon<any>;
export declare const SettingsCellDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
