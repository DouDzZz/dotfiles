import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckBoxOutlineBlank: StyledIcon<any>;
export declare const CheckBoxOutlineBlankDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
