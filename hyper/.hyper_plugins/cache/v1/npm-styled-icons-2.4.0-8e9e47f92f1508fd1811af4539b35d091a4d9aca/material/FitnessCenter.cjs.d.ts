import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FitnessCenter: StyledIcon<any>;
export declare const FitnessCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
