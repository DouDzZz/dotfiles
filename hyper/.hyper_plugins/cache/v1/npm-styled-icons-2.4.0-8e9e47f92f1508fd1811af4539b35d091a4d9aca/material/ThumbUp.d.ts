import { StyledIcon, StyledIconProps } from '..';
export declare const ThumbUp: StyledIcon<any>;
export declare const ThumbUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
