import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenRotation: StyledIcon<any>;
export declare const ScreenRotationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
