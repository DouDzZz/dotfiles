import { StyledIcon, StyledIconProps } from '..';
export declare const FormatShapes: StyledIcon<any>;
export declare const FormatShapesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
