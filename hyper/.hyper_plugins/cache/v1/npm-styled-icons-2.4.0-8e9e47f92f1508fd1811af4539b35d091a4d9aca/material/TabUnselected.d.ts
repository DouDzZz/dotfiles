import { StyledIcon, StyledIconProps } from '..';
export declare const TabUnselected: StyledIcon<any>;
export declare const TabUnselectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
