import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallMissedOutgoing: StyledIcon<any>;
export declare const CallMissedOutgoingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
