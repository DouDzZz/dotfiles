import { StyledIcon, StyledIconProps } from '..';
export declare const GpsOff: StyledIcon<any>;
export declare const GpsOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
