import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Transform: StyledIcon<any>;
export declare const TransformDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
