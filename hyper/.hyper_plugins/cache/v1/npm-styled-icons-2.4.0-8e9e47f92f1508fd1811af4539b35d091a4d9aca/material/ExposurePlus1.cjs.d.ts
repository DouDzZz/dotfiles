import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExposurePlus1: StyledIcon<any>;
export declare const ExposurePlus1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
