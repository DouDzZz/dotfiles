import { StyledIcon, StyledIconProps } from '..';
export declare const AssistantPhoto: StyledIcon<any>;
export declare const AssistantPhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
