import { StyledIcon, StyledIconProps } from '..';
export declare const GolfCourse: StyledIcon<any>;
export declare const GolfCourseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
