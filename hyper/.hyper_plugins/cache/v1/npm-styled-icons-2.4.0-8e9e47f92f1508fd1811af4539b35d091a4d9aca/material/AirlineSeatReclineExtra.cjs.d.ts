import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirlineSeatReclineExtra: StyledIcon<any>;
export declare const AirlineSeatReclineExtraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
