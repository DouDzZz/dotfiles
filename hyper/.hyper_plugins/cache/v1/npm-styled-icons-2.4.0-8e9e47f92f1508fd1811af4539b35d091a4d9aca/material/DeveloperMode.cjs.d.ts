import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DeveloperMode: StyledIcon<any>;
export declare const DeveloperModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
