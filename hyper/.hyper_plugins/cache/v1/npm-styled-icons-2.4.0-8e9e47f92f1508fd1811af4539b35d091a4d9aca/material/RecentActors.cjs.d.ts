import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RecentActors: StyledIcon<any>;
export declare const RecentActorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
