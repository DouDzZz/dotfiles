import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsWalk: StyledIcon<any>;
export declare const DirectionsWalkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
