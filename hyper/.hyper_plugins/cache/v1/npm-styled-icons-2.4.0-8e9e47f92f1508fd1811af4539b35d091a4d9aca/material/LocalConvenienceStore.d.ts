import { StyledIcon, StyledIconProps } from '..';
export declare const LocalConvenienceStore: StyledIcon<any>;
export declare const LocalConvenienceStoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
