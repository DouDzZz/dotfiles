import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryCharging20: StyledIcon<any>;
export declare const BatteryCharging20Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
