import { StyledIcon, StyledIconProps } from '..';
export declare const InsertPhoto: StyledIcon<any>;
export declare const InsertPhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
