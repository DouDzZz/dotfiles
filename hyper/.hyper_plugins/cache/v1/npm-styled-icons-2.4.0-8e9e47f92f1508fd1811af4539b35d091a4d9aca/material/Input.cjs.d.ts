import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Input: StyledIcon<any>;
export declare const InputDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
