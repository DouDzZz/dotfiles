import { StyledIcon, StyledIconProps } from '..';
export declare const ExposureZero: StyledIcon<any>;
export declare const ExposureZeroDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
