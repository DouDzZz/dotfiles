import { StyledIcon, StyledIconProps } from '..';
export declare const Assignment: StyledIcon<any>;
export declare const AssignmentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
