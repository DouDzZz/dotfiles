import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderTop: StyledIcon<any>;
export declare const BorderTopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
