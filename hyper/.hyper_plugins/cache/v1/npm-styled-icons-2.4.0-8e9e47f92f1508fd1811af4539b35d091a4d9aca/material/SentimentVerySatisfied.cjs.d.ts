import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SentimentVerySatisfied: StyledIcon<any>;
export declare const SentimentVerySatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
