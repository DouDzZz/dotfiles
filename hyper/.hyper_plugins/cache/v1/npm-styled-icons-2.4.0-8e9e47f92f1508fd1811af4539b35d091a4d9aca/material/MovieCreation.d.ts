import { StyledIcon, StyledIconProps } from '..';
export declare const MovieCreation: StyledIcon<any>;
export declare const MovieCreationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
