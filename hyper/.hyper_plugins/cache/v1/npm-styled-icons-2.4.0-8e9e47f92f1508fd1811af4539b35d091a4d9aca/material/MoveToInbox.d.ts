import { StyledIcon, StyledIconProps } from '..';
export declare const MoveToInbox: StyledIcon<any>;
export declare const MoveToInboxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
