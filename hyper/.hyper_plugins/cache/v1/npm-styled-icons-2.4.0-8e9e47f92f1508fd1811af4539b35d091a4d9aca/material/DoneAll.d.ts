import { StyledIcon, StyledIconProps } from '..';
export declare const DoneAll: StyledIcon<any>;
export declare const DoneAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
