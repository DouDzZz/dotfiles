import { StyledIcon, StyledIconProps } from '..';
export declare const Dns: StyledIcon<any>;
export declare const DnsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
