import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Message: StyledIcon<any>;
export declare const MessageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
