import { StyledIcon, StyledIconProps } from '..';
export declare const CallMissed: StyledIcon<any>;
export declare const CallMissedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
