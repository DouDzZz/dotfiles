import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CastConnected: StyledIcon<any>;
export declare const CastConnectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
