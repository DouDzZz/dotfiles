import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LaptopWindows: StyledIcon<any>;
export declare const LaptopWindowsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
