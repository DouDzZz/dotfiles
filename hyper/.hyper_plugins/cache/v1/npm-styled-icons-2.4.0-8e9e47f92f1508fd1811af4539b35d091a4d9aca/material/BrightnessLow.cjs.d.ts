import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BrightnessLow: StyledIcon<any>;
export declare const BrightnessLowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
