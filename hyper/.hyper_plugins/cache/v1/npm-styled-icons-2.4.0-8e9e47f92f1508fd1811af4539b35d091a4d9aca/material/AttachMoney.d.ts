import { StyledIcon, StyledIconProps } from '..';
export declare const AttachMoney: StyledIcon<any>;
export declare const AttachMoneyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
