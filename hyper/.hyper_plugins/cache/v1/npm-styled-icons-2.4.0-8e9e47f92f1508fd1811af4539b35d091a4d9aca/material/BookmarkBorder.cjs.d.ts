import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BookmarkBorder: StyledIcon<any>;
export declare const BookmarkBorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
