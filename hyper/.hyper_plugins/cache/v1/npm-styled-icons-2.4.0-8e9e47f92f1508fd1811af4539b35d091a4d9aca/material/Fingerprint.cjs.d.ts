import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fingerprint: StyledIcon<any>;
export declare const FingerprintDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
