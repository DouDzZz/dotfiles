import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyOff: StyledIcon<any>;
export declare const MoneyOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
