import { StyledIcon, StyledIconProps } from '..';
export declare const CameraRear: StyledIcon<any>;
export declare const CameraRearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
