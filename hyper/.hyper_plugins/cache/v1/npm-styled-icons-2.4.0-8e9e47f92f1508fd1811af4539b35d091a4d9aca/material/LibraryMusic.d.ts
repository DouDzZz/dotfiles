import { StyledIcon, StyledIconProps } from '..';
export declare const LibraryMusic: StyledIcon<any>;
export declare const LibraryMusicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
