import { StyledIcon, StyledIconProps } from '..';
export declare const Drafts: StyledIcon<any>;
export declare const DraftsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
