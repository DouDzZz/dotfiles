import { StyledIcon, StyledIconProps } from '..';
export declare const CallMerge: StyledIcon<any>;
export declare const CallMergeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
