import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mms: StyledIcon<any>;
export declare const MmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
