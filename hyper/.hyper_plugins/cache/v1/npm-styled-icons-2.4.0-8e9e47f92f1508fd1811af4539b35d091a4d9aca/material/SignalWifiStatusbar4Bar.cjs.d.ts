import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbar4Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar4BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
