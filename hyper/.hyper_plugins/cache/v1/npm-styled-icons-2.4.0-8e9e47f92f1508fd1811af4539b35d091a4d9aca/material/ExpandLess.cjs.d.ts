import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExpandLess: StyledIcon<any>;
export declare const ExpandLessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
