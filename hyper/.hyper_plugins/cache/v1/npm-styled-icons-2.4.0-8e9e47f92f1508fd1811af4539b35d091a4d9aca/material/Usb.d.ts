import { StyledIcon, StyledIconProps } from '..';
export declare const Usb: StyledIcon<any>;
export declare const UsbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
