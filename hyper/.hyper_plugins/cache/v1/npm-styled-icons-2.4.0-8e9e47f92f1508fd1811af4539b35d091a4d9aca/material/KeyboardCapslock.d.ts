import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardCapslock: StyledIcon<any>;
export declare const KeyboardCapslockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
