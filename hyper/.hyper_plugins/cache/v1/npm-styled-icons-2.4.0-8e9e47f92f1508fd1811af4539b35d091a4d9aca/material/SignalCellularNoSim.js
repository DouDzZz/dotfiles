var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var SignalCellularNoSim = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "SignalCellularNoSim-title" }, props.title), React.createElement("path", { d: "M18.99 5c0-1.1-.89-2-1.99-2h-7L7.66 5.34 19 16.68 18.99 5zM3.65 3.88L2.38 5.15 5 7.77V19c0 1.1.9 2 2 2h10.01c.35 0 .67-.1.96-.26l1.88 1.88 1.27-1.27L3.65 3.88z", key: "k0" })
        ]
        : [React.createElement("path", { d: "M18.99 5c0-1.1-.89-2-1.99-2h-7L7.66 5.34 19 16.68 18.99 5zM3.65 3.88L2.38 5.15 5 7.77V19c0 1.1.9 2 2 2h10.01c.35 0 .67-.1.96-.26l1.88 1.88 1.27-1.27L3.65 3.88z", key: "k0" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
SignalCellularNoSim.displayName = 'SignalCellularNoSim';
export var SignalCellularNoSimDimensions = { height: 24, width: 24 };
var templateObject_1;
