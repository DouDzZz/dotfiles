import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BlurLinear: StyledIcon<any>;
export declare const BlurLinearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
