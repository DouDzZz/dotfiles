import { StyledIcon, StyledIconProps } from '..';
export declare const Forum: StyledIcon<any>;
export declare const ForumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
