import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderBottom: StyledIcon<any>;
export declare const BorderBottomDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
