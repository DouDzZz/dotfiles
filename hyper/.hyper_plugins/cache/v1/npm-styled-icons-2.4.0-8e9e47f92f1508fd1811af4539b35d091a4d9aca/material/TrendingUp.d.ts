import { StyledIcon, StyledIconProps } from '..';
export declare const TrendingUp: StyledIcon<any>;
export declare const TrendingUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
