import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderVertical: StyledIcon<any>;
export declare const BorderVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
