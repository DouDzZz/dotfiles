import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardArrowDown: StyledIcon<any>;
export declare const KeyboardArrowDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
