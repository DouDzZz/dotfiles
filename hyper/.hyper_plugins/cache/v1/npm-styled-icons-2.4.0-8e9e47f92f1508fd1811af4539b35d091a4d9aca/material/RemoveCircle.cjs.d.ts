import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RemoveCircle: StyledIcon<any>;
export declare const RemoveCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
