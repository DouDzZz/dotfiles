import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddCircle: StyledIcon<any>;
export declare const AddCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
