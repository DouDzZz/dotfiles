import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeDown: StyledIcon<any>;
export declare const VolumeDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
