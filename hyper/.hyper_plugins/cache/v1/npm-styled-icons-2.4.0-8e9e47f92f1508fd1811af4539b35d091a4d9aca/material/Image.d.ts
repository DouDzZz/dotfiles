import { StyledIcon, StyledIconProps } from '..';
export declare const Image: StyledIcon<any>;
export declare const ImageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
