import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Adb: StyledIcon<any>;
export declare const AdbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
