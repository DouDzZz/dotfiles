import { StyledIcon, StyledIconProps } from '..';
export declare const ModeEdit: StyledIcon<any>;
export declare const ModeEditDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
