import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BluetoothAudio: StyledIcon<any>;
export declare const BluetoothAudioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
