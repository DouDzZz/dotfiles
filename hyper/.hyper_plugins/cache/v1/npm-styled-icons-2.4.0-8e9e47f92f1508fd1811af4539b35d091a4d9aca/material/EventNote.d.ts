import { StyledIcon, StyledIconProps } from '..';
export declare const EventNote: StyledIcon<any>;
export declare const EventNoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
