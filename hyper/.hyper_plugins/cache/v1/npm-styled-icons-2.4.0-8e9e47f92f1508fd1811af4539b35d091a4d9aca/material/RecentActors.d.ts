import { StyledIcon, StyledIconProps } from '..';
export declare const RecentActors: StyledIcon<any>;
export declare const RecentActorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
