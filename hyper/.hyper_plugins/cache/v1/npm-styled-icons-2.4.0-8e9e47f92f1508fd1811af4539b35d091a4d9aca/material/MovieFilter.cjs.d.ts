import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MovieFilter: StyledIcon<any>;
export declare const MovieFilterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
