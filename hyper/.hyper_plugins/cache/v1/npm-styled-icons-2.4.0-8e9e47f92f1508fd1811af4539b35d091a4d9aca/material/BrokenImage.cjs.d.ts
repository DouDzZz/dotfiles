import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BrokenImage: StyledIcon<any>;
export declare const BrokenImageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
