import { StyledIcon, StyledIconProps } from '..';
export declare const CloudDone: StyledIcon<any>;
export declare const CloudDoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
