import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LowPriority: StyledIcon<any>;
export declare const LowPriorityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
