import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonelinkErase: StyledIcon<any>;
export declare const PhonelinkEraseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
