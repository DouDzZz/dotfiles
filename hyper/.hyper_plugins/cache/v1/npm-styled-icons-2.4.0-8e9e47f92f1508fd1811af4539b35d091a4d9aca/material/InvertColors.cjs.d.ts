import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InvertColors: StyledIcon<any>;
export declare const InvertColorsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
