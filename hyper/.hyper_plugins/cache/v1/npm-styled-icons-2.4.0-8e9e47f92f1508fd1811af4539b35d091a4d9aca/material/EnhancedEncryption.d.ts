import { StyledIcon, StyledIconProps } from '..';
export declare const EnhancedEncryption: StyledIcon<any>;
export declare const EnhancedEncryptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
