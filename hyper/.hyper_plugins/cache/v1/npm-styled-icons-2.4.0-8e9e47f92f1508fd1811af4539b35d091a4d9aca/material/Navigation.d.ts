import { StyledIcon, StyledIconProps } from '..';
export declare const Navigation: StyledIcon<any>;
export declare const NavigationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
