import { StyledIcon, StyledIconProps } from '..';
export declare const PermPhoneMsg: StyledIcon<any>;
export declare const PermPhoneMsgDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
