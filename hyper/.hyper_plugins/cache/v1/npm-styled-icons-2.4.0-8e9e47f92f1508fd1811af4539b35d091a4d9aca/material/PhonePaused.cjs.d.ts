import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonePaused: StyledIcon<any>;
export declare const PhonePausedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
