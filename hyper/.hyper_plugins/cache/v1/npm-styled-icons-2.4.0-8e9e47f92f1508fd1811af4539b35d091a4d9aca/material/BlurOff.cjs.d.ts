import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BlurOff: StyledIcon<any>;
export declare const BlurOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
