import { StyledIcon, StyledIconProps } from '..';
export declare const FilterNone: StyledIcon<any>;
export declare const FilterNoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
