import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Work: StyledIcon<any>;
export declare const WorkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
