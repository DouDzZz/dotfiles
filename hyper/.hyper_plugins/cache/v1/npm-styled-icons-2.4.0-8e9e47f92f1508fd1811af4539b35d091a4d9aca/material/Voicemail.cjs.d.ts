import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Voicemail: StyledIcon<any>;
export declare const VoicemailDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
