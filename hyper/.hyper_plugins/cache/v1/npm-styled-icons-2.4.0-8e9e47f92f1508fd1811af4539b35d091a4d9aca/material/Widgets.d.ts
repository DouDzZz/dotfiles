import { StyledIcon, StyledIconProps } from '..';
export declare const Widgets: StyledIcon<any>;
export declare const WidgetsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
