import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiOff: StyledIcon<any>;
export declare const SignalWifiOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
