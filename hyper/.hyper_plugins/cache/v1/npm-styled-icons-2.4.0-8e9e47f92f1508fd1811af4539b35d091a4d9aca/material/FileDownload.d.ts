import { StyledIcon, StyledIconProps } from '..';
export declare const FileDownload: StyledIcon<any>;
export declare const FileDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
