import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddCircleOutline: StyledIcon<any>;
export declare const AddCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
