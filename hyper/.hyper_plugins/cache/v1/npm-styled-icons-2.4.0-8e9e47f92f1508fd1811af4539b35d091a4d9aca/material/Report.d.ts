import { StyledIcon, StyledIconProps } from '..';
export declare const Report: StyledIcon<any>;
export declare const ReportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
