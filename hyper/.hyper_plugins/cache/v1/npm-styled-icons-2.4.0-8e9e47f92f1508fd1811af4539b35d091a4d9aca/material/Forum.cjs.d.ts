import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Forum: StyledIcon<any>;
export declare const ForumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
