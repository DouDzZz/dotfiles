import { StyledIcon, StyledIconProps } from '..';
export declare const Loupe: StyledIcon<any>;
export declare const LoupeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
