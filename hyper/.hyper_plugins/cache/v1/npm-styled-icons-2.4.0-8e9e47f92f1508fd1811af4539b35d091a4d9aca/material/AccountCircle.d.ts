import { StyledIcon, StyledIconProps } from '..';
export declare const AccountCircle: StyledIcon<any>;
export declare const AccountCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
