import { StyledIcon, StyledIconProps } from '..';
export declare const Filter9Plus: StyledIcon<any>;
export declare const Filter9PlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
