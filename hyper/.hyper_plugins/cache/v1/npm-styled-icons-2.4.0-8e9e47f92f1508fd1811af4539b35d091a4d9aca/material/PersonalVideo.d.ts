import { StyledIcon, StyledIconProps } from '..';
export declare const PersonalVideo: StyledIcon<any>;
export declare const PersonalVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
