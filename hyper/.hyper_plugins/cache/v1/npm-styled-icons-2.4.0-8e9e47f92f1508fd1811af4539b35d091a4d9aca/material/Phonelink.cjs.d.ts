import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Phonelink: StyledIcon<any>;
export declare const PhonelinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
