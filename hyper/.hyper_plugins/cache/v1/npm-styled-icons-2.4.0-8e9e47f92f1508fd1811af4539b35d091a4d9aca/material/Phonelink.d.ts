import { StyledIcon, StyledIconProps } from '..';
export declare const Phonelink: StyledIcon<any>;
export declare const PhonelinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
