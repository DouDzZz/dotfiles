import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TurnedInNot: StyledIcon<any>;
export declare const TurnedInNotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
