import { StyledIcon, StyledIconProps } from '..';
export declare const Audiotrack: StyledIcon<any>;
export declare const AudiotrackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
