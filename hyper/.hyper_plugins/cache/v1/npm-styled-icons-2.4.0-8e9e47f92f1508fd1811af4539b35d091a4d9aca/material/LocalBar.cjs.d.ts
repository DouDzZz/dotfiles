import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalBar: StyledIcon<any>;
export declare const LocalBarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
