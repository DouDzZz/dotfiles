import { StyledIcon, StyledIconProps } from '..';
export declare const LineStyle: StyledIcon<any>;
export declare const LineStyleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
