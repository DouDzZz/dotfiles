import { StyledIcon, StyledIconProps } from '..';
export declare const PauseCircleOutline: StyledIcon<any>;
export declare const PauseCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
