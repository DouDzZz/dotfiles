import { StyledIcon, StyledIconProps } from '..';
export declare const Brush: StyledIcon<any>;
export declare const BrushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
