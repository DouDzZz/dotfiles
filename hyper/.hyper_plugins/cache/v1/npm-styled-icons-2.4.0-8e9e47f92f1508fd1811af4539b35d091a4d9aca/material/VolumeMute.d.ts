import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeMute: StyledIcon<any>;
export declare const VolumeMuteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
