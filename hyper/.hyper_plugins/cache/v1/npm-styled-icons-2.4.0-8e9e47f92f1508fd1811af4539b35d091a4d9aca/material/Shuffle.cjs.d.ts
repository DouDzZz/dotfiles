import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Shuffle: StyledIcon<any>;
export declare const ShuffleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
