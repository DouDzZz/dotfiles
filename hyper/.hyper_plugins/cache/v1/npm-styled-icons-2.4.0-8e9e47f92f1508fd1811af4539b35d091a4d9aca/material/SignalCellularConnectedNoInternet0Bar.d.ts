import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularConnectedNoInternet0Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet0BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
