import { StyledIcon, StyledIconProps } from '..';
export declare const DesktopWindows: StyledIcon<any>;
export declare const DesktopWindowsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
