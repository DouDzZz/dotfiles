import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StoreMallDirectory: StyledIcon<any>;
export declare const StoreMallDirectoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
