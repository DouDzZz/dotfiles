import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularNull: StyledIcon<any>;
export declare const SignalCellularNullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
