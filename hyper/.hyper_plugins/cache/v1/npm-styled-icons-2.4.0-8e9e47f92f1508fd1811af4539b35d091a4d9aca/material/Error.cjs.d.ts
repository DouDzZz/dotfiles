import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Error: StyledIcon<any>;
export declare const ErrorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
