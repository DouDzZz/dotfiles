import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarkunreadMailbox: StyledIcon<any>;
export declare const MarkunreadMailboxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
