import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Slideshow: StyledIcon<any>;
export declare const SlideshowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
