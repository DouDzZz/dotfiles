import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterVintage: StyledIcon<any>;
export declare const FilterVintageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
