import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocationSearching: StyledIcon<any>;
export declare const LocationSearchingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
