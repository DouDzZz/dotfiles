import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropRotate: StyledIcon<any>;
export declare const CropRotateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
