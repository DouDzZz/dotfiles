import { StyledIcon, StyledIconProps } from '..';
export declare const PanoramaVertical: StyledIcon<any>;
export declare const PanoramaVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
