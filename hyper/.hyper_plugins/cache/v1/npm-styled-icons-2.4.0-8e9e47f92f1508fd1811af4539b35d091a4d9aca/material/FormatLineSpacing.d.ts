import { StyledIcon, StyledIconProps } from '..';
export declare const FormatLineSpacing: StyledIcon<any>;
export declare const FormatLineSpacingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
