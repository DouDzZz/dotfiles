import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Image: StyledIcon<any>;
export declare const ImageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
