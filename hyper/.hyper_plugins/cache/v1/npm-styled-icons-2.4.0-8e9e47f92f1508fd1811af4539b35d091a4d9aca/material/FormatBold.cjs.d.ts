import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatBold: StyledIcon<any>;
export declare const FormatBoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
