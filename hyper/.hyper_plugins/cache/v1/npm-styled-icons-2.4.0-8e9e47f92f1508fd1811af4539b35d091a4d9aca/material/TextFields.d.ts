import { StyledIcon, StyledIconProps } from '..';
export declare const TextFields: StyledIcon<any>;
export declare const TextFieldsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
