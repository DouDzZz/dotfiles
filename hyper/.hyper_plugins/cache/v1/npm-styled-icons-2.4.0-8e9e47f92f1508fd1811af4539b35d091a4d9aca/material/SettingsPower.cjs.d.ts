import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsPower: StyledIcon<any>;
export declare const SettingsPowerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
