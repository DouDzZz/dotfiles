import { StyledIcon, StyledIconProps } from '..';
export declare const Vignette: StyledIcon<any>;
export declare const VignetteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
