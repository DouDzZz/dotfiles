import { StyledIcon, StyledIconProps } from '..';
export declare const Timeline: StyledIcon<any>;
export declare const TimelineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
