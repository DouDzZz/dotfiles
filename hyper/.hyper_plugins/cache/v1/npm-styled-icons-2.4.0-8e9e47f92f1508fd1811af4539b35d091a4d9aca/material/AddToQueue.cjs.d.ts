import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddToQueue: StyledIcon<any>;
export declare const AddToQueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
