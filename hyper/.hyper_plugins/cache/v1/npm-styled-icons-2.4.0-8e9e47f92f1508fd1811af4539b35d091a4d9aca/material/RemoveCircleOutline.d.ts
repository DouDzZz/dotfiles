import { StyledIcon, StyledIconProps } from '..';
export declare const RemoveCircleOutline: StyledIcon<any>;
export declare const RemoveCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
