import { StyledIcon, StyledIconProps } from '..';
export declare const TextFormat: StyledIcon<any>;
export declare const TextFormatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
