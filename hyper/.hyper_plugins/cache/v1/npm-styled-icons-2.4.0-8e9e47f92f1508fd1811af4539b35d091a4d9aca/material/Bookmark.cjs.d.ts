import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bookmark: StyledIcon<any>;
export declare const BookmarkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
