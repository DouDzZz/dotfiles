import { StyledIcon, StyledIconProps } from '..';
export declare const FlashAuto: StyledIcon<any>;
export declare const FlashAutoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
