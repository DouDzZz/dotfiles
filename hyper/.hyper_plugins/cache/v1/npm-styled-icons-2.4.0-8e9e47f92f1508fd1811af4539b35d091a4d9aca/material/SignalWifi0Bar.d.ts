import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifi0Bar: StyledIcon<any>;
export declare const SignalWifi0BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
