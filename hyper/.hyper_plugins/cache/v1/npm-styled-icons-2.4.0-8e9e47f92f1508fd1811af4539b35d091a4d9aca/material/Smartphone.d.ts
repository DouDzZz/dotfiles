import { StyledIcon, StyledIconProps } from '..';
export declare const Smartphone: StyledIcon<any>;
export declare const SmartphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
