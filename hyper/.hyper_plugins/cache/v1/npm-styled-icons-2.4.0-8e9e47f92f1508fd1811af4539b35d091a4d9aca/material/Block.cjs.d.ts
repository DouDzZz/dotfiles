import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Block: StyledIcon<any>;
export declare const BlockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
