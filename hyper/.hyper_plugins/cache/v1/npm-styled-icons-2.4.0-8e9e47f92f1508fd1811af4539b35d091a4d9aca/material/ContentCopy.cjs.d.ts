import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ContentCopy: StyledIcon<any>;
export declare const ContentCopyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
