import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalAtm: StyledIcon<any>;
export declare const LocalAtmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
