import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardHide: StyledIcon<any>;
export declare const KeyboardHideDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
