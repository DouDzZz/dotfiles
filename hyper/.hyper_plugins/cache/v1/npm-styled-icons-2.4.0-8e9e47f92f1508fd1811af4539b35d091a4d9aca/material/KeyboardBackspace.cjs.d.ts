import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardBackspace: StyledIcon<any>;
export declare const KeyboardBackspaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
