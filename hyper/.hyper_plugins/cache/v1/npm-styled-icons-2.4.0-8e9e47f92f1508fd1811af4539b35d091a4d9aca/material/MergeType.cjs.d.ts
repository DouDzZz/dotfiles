import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MergeType: StyledIcon<any>;
export declare const MergeTypeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
