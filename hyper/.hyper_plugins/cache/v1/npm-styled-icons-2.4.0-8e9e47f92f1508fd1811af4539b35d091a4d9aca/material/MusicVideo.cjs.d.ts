import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MusicVideo: StyledIcon<any>;
export declare const MusicVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
