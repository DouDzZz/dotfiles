import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SkipNext: StyledIcon<any>;
export declare const SkipNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
