import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsBike: StyledIcon<any>;
export declare const DirectionsBikeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
