import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HdrOff: StyledIcon<any>;
export declare const HdrOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
