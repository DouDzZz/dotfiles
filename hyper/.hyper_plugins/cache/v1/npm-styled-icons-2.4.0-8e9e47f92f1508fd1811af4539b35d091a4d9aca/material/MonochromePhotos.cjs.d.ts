import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MonochromePhotos: StyledIcon<any>;
export declare const MonochromePhotosDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
