import { StyledIcon, StyledIconProps } from '..';
export declare const DeleteSweep: StyledIcon<any>;
export declare const DeleteSweepDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
