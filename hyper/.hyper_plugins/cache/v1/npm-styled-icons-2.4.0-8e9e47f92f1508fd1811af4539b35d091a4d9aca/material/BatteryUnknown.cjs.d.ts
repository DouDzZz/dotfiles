import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryUnknown: StyledIcon<any>;
export declare const BatteryUnknownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
