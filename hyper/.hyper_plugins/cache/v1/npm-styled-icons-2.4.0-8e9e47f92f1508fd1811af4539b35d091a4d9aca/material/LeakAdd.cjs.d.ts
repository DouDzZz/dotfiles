import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LeakAdd: StyledIcon<any>;
export declare const LeakAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
