import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DoNotDisturbOff: StyledIcon<any>;
export declare const DoNotDisturbOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
