import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddToPhotos: StyledIcon<any>;
export declare const AddToPhotosDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
