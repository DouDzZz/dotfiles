import { StyledIcon, StyledIconProps } from '..';
export declare const SmsFailed: StyledIcon<any>;
export declare const SmsFailedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
