import { StyledIcon, StyledIconProps } from '..';
export declare const Wc: StyledIcon<any>;
export declare const WcDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
