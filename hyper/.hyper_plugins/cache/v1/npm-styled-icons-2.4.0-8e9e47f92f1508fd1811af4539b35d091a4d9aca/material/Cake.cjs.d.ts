import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cake: StyledIcon<any>;
export declare const CakeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
