import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Panorama: StyledIcon<any>;
export declare const PanoramaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
