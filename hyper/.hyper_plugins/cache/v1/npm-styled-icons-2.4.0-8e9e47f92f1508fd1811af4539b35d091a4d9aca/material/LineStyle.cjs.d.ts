import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LineStyle: StyledIcon<any>;
export declare const LineStyleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
