import { StyledIcon, StyledIconProps } from '..';
export declare const GraphicEq: StyledIcon<any>;
export declare const GraphicEqDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
