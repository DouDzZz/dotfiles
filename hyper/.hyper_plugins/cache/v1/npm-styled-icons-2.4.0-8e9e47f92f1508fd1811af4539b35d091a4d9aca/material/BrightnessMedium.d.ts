import { StyledIcon, StyledIconProps } from '..';
export declare const BrightnessMedium: StyledIcon<any>;
export declare const BrightnessMediumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
