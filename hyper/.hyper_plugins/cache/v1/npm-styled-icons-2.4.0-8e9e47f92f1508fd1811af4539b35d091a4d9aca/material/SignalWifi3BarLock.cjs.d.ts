import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi3BarLock: StyledIcon<any>;
export declare const SignalWifi3BarLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
