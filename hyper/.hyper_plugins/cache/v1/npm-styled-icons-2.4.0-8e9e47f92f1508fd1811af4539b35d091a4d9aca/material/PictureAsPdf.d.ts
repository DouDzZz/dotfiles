import { StyledIcon, StyledIconProps } from '..';
export declare const PictureAsPdf: StyledIcon<any>;
export declare const PictureAsPdfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
