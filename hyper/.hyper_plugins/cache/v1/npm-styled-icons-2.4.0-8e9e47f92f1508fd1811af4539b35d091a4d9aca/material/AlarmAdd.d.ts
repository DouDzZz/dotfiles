import { StyledIcon, StyledIconProps } from '..';
export declare const AlarmAdd: StyledIcon<any>;
export declare const AlarmAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
