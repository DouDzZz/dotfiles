import { StyledIcon, StyledIconProps } from '..';
export declare const BluetoothSearching: StyledIcon<any>;
export declare const BluetoothSearchingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
