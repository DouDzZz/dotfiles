import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RoomService: StyledIcon<any>;
export declare const RoomServiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
