import { StyledIcon, StyledIconProps } from '..';
export declare const DonutLarge: StyledIcon<any>;
export declare const DonutLargeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
