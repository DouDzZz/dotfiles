import { StyledIcon, StyledIconProps } from '..';
export declare const Battery60: StyledIcon<any>;
export declare const Battery60Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
