import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Assessment: StyledIcon<any>;
export declare const AssessmentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
