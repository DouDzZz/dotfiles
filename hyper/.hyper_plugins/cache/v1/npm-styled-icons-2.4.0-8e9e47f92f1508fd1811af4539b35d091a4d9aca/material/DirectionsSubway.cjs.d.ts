import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsSubway: StyledIcon<any>;
export declare const DirectionsSubwayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
