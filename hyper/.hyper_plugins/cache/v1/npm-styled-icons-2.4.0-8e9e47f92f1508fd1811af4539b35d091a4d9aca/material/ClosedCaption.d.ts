import { StyledIcon, StyledIconProps } from '..';
export declare const ClosedCaption: StyledIcon<any>;
export declare const ClosedCaptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
