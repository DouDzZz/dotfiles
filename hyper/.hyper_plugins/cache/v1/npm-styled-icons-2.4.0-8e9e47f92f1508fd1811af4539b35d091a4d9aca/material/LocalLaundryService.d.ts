import { StyledIcon, StyledIconProps } from '..';
export declare const LocalLaundryService: StyledIcon<any>;
export declare const LocalLaundryServiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
