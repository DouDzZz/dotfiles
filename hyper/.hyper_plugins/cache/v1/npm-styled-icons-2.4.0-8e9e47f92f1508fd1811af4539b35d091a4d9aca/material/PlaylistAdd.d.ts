import { StyledIcon, StyledIconProps } from '..';
export declare const PlaylistAdd: StyledIcon<any>;
export declare const PlaylistAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
