import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PanoramaHorizontal: StyledIcon<any>;
export declare const PanoramaHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
