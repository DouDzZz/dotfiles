import { StyledIcon, StyledIconProps } from '..';
export declare const LockOpen: StyledIcon<any>;
export declare const LockOpenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
