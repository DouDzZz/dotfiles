import { StyledIcon, StyledIconProps } from '..';
export declare const TabletAndroid: StyledIcon<any>;
export declare const TabletAndroidDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
