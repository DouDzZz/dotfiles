import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LaptopChromebook: StyledIcon<any>;
export declare const LaptopChromebookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
