import { StyledIcon, StyledIconProps } from '..';
export declare const Restore: StyledIcon<any>;
export declare const RestoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
