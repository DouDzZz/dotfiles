import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WatchLater: StyledIcon<any>;
export declare const WatchLaterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
