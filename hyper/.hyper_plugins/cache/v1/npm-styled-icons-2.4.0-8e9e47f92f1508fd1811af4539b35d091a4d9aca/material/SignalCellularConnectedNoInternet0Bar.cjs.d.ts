import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularConnectedNoInternet0Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet0BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
