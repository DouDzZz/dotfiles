import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Brightness1: StyledIcon<any>;
export declare const Brightness1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
