import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MultilineChart: StyledIcon<any>;
export declare const MultilineChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
