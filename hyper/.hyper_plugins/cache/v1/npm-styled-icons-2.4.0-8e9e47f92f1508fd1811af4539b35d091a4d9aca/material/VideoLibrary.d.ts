import { StyledIcon, StyledIconProps } from '..';
export declare const VideoLibrary: StyledIcon<any>;
export declare const VideoLibraryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
