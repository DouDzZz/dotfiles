import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatTextdirectionRToL: StyledIcon<any>;
export declare const FormatTextdirectionRToLDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
