import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NetworkLocked: StyledIcon<any>;
export declare const NetworkLockedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
