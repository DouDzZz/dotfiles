import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsInputComponent: StyledIcon<any>;
export declare const SettingsInputComponentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
