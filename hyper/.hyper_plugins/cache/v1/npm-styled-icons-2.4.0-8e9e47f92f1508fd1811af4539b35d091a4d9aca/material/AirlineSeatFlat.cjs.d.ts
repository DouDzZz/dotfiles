import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirlineSeatFlat: StyledIcon<any>;
export declare const AirlineSeatFlatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
