import { StyledIcon, StyledIconProps } from '..';
export declare const LocalHotel: StyledIcon<any>;
export declare const LocalHotelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
