import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellular0Bar: StyledIcon<any>;
export declare const SignalCellular0BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
