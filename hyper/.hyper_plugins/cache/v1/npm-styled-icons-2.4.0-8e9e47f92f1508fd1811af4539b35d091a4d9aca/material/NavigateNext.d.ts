import { StyledIcon, StyledIconProps } from '..';
export declare const NavigateNext: StyledIcon<any>;
export declare const NavigateNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
