import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Room: StyledIcon<any>;
export declare const RoomDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
