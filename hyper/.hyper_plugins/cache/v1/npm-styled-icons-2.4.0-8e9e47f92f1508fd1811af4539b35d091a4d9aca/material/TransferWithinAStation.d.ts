import { StyledIcon, StyledIconProps } from '..';
export declare const TransferWithinAStation: StyledIcon<any>;
export declare const TransferWithinAStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
