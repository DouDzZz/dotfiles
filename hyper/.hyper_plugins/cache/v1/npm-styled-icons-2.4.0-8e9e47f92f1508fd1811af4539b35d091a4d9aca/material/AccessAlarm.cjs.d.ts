import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AccessAlarm: StyledIcon<any>;
export declare const AccessAlarmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
