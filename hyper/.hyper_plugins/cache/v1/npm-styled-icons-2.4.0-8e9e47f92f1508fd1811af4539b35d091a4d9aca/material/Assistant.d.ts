import { StyledIcon, StyledIconProps } from '..';
export declare const Assistant: StyledIcon<any>;
export declare const AssistantDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
