import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Brightness4: StyledIcon<any>;
export declare const Brightness4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
