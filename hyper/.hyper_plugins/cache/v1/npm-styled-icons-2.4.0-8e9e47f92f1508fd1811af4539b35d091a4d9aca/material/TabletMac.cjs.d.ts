import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TabletMac: StyledIcon<any>;
export declare const TabletMacDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
