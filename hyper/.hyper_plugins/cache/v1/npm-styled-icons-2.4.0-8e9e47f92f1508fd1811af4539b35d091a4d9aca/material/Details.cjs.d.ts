import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Details: StyledIcon<any>;
export declare const DetailsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
