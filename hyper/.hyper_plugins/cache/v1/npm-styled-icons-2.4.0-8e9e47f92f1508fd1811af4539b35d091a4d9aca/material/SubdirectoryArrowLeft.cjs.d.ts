import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SubdirectoryArrowLeft: StyledIcon<any>;
export declare const SubdirectoryArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
