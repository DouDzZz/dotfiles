import { StyledIcon, StyledIconProps } from '..';
export declare const Forward10: StyledIcon<any>;
export declare const Forward10Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
