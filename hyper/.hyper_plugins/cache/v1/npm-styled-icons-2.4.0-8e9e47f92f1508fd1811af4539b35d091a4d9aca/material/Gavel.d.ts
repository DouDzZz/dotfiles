import { StyledIcon, StyledIconProps } from '..';
export declare const Gavel: StyledIcon<any>;
export declare const GavelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
