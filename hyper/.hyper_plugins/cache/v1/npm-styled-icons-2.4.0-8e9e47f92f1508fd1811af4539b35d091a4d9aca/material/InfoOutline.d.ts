import { StyledIcon, StyledIconProps } from '..';
export declare const InfoOutline: StyledIcon<any>;
export declare const InfoOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
