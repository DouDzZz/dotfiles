import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirlineSeatIndividualSuite: StyledIcon<any>;
export declare const AirlineSeatIndividualSuiteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
