import { StyledIcon, StyledIconProps } from '..';
export declare const CardMembership: StyledIcon<any>;
export declare const CardMembershipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
