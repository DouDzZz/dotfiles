import { StyledIcon, StyledIconProps } from '..';
export declare const Comment: StyledIcon<any>;
export declare const CommentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
