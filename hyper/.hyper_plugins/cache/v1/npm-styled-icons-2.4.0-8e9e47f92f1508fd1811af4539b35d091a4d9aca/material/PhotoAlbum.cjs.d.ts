import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhotoAlbum: StyledIcon<any>;
export declare const PhotoAlbumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
