import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SentimentVeryDissatisfied: StyledIcon<any>;
export declare const SentimentVeryDissatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
