import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LinkedCamera: StyledIcon<any>;
export declare const LinkedCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
