import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HdrStrong: StyledIcon<any>;
export declare const HdrStrongDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
