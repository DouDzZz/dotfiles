import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WbIncandescent: StyledIcon<any>;
export declare const WbIncandescentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
