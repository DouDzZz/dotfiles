import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Drafts: StyledIcon<any>;
export declare const DraftsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
