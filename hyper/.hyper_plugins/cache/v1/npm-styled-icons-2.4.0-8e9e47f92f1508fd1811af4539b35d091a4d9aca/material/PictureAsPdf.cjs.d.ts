import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PictureAsPdf: StyledIcon<any>;
export declare const PictureAsPdfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
