import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CenterFocusWeak: StyledIcon<any>;
export declare const CenterFocusWeakDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
