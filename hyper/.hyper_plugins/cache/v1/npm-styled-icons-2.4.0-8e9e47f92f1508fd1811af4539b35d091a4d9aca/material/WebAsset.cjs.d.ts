import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WebAsset: StyledIcon<any>;
export declare const WebAssetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
