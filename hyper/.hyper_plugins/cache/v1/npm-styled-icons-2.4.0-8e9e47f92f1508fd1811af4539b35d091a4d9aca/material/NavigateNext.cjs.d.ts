import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NavigateNext: StyledIcon<any>;
export declare const NavigateNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
