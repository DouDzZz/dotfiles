import { StyledIcon, StyledIconProps } from '..';
export declare const SurroundSound: StyledIcon<any>;
export declare const SurroundSoundDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
