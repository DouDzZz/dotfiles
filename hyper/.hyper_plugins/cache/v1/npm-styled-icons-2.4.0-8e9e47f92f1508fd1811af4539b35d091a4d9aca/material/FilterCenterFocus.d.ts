import { StyledIcon, StyledIconProps } from '..';
export declare const FilterCenterFocus: StyledIcon<any>;
export declare const FilterCenterFocusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
