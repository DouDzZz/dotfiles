import { StyledIcon, StyledIconProps } from '..';
export declare const OndemandVideo: StyledIcon<any>;
export declare const OndemandVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
