import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertChart: StyledIcon<any>;
export declare const InsertChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
