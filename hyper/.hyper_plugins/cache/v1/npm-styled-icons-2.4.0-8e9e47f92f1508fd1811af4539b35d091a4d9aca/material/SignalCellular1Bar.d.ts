import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellular1Bar: StyledIcon<any>;
export declare const SignalCellular1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
