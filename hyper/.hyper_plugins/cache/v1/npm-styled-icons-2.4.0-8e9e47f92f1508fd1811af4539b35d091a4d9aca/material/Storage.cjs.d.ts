import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Storage: StyledIcon<any>;
export declare const StorageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
