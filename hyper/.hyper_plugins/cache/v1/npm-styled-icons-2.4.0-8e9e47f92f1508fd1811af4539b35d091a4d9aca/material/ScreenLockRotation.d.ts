import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenLockRotation: StyledIcon<any>;
export declare const ScreenLockRotationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
