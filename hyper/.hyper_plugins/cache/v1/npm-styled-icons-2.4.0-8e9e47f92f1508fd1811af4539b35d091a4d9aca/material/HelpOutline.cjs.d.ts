import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HelpOutline: StyledIcon<any>;
export declare const HelpOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
