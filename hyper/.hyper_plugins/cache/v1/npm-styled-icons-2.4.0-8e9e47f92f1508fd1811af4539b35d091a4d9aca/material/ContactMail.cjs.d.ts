import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ContactMail: StyledIcon<any>;
export declare const ContactMailDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
