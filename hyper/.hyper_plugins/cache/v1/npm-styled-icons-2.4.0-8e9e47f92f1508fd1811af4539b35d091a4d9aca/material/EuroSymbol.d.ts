import { StyledIcon, StyledIconProps } from '..';
export declare const EuroSymbol: StyledIcon<any>;
export declare const EuroSymbolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
