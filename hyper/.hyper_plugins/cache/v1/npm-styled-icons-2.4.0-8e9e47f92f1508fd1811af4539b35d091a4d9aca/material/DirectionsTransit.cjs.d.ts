import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsTransit: StyledIcon<any>;
export declare const DirectionsTransitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
