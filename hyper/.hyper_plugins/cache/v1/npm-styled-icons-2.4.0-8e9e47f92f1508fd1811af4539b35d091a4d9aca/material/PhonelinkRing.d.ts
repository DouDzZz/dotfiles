import { StyledIcon, StyledIconProps } from '..';
export declare const PhonelinkRing: StyledIcon<any>;
export declare const PhonelinkRingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
