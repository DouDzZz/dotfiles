import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WbCloudy: StyledIcon<any>;
export declare const WbCloudyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
