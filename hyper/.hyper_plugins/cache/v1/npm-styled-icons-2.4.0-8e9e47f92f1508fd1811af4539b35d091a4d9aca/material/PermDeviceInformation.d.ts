import { StyledIcon, StyledIconProps } from '..';
export declare const PermDeviceInformation: StyledIcon<any>;
export declare const PermDeviceInformationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
