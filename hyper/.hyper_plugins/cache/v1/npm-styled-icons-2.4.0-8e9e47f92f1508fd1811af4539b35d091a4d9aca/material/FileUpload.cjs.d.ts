import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileUpload: StyledIcon<any>;
export declare const FileUploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
