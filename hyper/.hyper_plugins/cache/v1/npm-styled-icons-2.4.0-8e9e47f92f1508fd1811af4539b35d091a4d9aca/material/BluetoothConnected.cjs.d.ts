import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BluetoothConnected: StyledIcon<any>;
export declare const BluetoothConnectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
