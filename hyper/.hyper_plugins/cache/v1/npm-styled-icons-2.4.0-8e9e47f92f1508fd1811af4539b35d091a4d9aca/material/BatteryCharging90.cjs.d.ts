import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryCharging90: StyledIcon<any>;
export declare const BatteryCharging90Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
