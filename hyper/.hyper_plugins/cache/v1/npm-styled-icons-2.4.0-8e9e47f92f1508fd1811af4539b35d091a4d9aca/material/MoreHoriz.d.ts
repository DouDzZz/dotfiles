import { StyledIcon, StyledIconProps } from '..';
export declare const MoreHoriz: StyledIcon<any>;
export declare const MoreHorizDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
