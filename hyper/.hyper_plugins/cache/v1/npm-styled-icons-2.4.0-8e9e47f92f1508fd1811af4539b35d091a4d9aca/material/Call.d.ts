import { StyledIcon, StyledIconProps } from '..';
export declare const Call: StyledIcon<any>;
export declare const CallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
