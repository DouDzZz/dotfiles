import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Terrain: StyledIcon<any>;
export declare const TerrainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
