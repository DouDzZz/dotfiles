import { StyledIcon, StyledIconProps } from '..';
export declare const MyLocation: StyledIcon<any>;
export declare const MyLocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
