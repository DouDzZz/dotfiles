import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SystemUpdateAlt: StyledIcon<any>;
export declare const SystemUpdateAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
