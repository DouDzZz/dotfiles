import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbDown: StyledIcon<any>;
export declare const ThumbDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
