import { StyledIcon, StyledIconProps } from '..';
export declare const StarHalf: StyledIcon<any>;
export declare const StarHalfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
