import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardReturn: StyledIcon<any>;
export declare const KeyboardReturnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
