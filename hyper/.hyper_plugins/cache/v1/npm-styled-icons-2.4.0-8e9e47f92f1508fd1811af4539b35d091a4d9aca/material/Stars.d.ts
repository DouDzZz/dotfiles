import { StyledIcon, StyledIconProps } from '..';
export declare const Stars: StyledIcon<any>;
export declare const StarsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
