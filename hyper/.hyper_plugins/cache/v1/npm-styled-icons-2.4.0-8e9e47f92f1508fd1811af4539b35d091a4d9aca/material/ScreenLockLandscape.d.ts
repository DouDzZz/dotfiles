import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenLockLandscape: StyledIcon<any>;
export declare const ScreenLockLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
