import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArtTrack: StyledIcon<any>;
export declare const ArtTrackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
