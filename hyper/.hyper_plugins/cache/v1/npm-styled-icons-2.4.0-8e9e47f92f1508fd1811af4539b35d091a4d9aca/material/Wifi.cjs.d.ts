import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wifi: StyledIcon<any>;
export declare const WifiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
