import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermPhoneMsg: StyledIcon<any>;
export declare const PermPhoneMsgDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
