import { StyledIcon, StyledIconProps } from '..';
export declare const ViewComfy: StyledIcon<any>;
export declare const ViewComfyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
