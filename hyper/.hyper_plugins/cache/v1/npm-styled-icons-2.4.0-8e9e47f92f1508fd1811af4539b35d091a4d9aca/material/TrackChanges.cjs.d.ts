import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TrackChanges: StyledIcon<any>;
export declare const TrackChangesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
