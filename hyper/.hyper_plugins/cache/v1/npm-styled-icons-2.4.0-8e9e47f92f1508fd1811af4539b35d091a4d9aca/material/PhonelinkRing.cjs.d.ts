import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonelinkRing: StyledIcon<any>;
export declare const PhonelinkRingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
