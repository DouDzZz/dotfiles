import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideoCall: StyledIcon<any>;
export declare const VideoCallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
