import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BrightnessAuto: StyledIcon<any>;
export declare const BrightnessAutoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
