import { StyledIcon, StyledIconProps } from '..';
export declare const WbIncandescent: StyledIcon<any>;
export declare const WbIncandescentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
