import { StyledIcon, StyledIconProps } from '..';
export declare const EvStation: StyledIcon<any>;
export declare const EvStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
