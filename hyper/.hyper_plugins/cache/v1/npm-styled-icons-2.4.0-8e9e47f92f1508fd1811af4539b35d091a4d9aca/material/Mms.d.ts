import { StyledIcon, StyledIconProps } from '..';
export declare const Mms: StyledIcon<any>;
export declare const MmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
