import { StyledIcon, StyledIconProps } from '..';
export declare const LocationOn: StyledIcon<any>;
export declare const LocationOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
