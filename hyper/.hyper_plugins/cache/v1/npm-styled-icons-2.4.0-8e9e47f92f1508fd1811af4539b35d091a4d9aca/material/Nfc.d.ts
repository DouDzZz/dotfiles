import { StyledIcon, StyledIconProps } from '..';
export declare const Nfc: StyledIcon<any>;
export declare const NfcDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
