import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BlurCircular: StyledIcon<any>;
export declare const BlurCircularDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
