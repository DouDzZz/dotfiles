import { StyledIcon, StyledIconProps } from '..';
export declare const Class: StyledIcon<any>;
export declare const ClassDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
