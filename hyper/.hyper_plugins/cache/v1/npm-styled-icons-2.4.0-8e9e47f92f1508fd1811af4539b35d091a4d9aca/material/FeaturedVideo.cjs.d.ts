import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FeaturedVideo: StyledIcon<any>;
export declare const FeaturedVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
