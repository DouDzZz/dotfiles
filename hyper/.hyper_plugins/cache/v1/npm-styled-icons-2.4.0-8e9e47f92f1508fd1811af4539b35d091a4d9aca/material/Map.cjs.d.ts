import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Map: StyledIcon<any>;
export declare const MapDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
