import { StyledIcon, StyledIconProps } from '..';
export declare const NavigateBefore: StyledIcon<any>;
export declare const NavigateBeforeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
