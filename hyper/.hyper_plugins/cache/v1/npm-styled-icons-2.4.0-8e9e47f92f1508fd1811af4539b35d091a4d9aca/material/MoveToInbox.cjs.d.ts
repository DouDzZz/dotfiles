import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoveToInbox: StyledIcon<any>;
export declare const MoveToInboxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
