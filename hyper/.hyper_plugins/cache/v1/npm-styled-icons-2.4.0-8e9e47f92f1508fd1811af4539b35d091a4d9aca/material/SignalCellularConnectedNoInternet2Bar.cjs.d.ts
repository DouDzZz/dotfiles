import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularConnectedNoInternet2Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
