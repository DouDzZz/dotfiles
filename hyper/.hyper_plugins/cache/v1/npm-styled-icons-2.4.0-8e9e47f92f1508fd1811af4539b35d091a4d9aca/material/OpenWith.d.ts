import { StyledIcon, StyledIconProps } from '..';
export declare const OpenWith: StyledIcon<any>;
export declare const OpenWithDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
