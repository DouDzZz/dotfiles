import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterFrames: StyledIcon<any>;
export declare const FilterFramesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
