import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter5: StyledIcon<any>;
export declare const Filter5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
