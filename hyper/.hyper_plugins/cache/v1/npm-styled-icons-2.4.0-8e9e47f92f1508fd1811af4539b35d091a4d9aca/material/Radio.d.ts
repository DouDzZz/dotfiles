import { StyledIcon, StyledIconProps } from '..';
export declare const Radio: StyledIcon<any>;
export declare const RadioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
