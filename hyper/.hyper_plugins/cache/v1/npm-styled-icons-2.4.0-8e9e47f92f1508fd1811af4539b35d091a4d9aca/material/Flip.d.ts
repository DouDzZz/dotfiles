import { StyledIcon, StyledIconProps } from '..';
export declare const Flip: StyledIcon<any>;
export declare const FlipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
