import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifi2BarLock: StyledIcon<any>;
export declare const SignalWifi2BarLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
