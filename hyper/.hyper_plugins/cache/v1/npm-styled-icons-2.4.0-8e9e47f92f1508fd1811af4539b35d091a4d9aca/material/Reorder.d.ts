import { StyledIcon, StyledIconProps } from '..';
export declare const Reorder: StyledIcon<any>;
export declare const ReorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
