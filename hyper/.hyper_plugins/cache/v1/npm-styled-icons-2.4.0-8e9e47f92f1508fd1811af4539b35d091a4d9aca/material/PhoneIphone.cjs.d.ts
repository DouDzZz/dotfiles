import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneIphone: StyledIcon<any>;
export declare const PhoneIphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
