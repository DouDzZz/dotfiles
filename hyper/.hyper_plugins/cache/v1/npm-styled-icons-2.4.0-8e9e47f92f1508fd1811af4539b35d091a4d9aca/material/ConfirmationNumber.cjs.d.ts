import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ConfirmationNumber: StyledIcon<any>;
export declare const ConfirmationNumberDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
