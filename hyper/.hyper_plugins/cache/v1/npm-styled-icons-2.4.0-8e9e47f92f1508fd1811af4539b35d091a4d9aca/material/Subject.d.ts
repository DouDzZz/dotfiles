import { StyledIcon, StyledIconProps } from '..';
export declare const Subject: StyledIcon<any>;
export declare const SubjectDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
