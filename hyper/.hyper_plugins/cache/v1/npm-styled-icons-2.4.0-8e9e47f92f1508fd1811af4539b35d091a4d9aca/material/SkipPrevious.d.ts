import { StyledIcon, StyledIconProps } from '..';
export declare const SkipPrevious: StyledIcon<any>;
export declare const SkipPreviousDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
