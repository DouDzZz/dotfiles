import { StyledIcon, StyledIconProps } from '..';
export declare const SkipNext: StyledIcon<any>;
export declare const SkipNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
