import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EditLocation: StyledIcon<any>;
export declare const EditLocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
