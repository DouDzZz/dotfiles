import { StyledIcon, StyledIconProps } from '..';
export declare const Looks3: StyledIcon<any>;
export declare const Looks3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
