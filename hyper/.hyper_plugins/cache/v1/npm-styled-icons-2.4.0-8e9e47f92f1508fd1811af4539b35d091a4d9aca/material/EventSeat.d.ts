import { StyledIcon, StyledIconProps } from '..';
export declare const EventSeat: StyledIcon<any>;
export declare const EventSeatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
