import { StyledIcon, StyledIconProps } from '..';
export declare const DevicesOther: StyledIcon<any>;
export declare const DevicesOtherDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
