import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlusOne: StyledIcon<any>;
export declare const PlusOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
