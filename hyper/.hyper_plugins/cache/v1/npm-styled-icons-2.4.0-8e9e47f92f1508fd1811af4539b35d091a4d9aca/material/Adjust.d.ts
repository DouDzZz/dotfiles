import { StyledIcon, StyledIconProps } from '..';
export declare const Adjust: StyledIcon<any>;
export declare const AdjustDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
