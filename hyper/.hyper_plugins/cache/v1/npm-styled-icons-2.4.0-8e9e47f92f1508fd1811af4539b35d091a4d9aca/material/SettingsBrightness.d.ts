import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsBrightness: StyledIcon<any>;
export declare const SettingsBrightnessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
