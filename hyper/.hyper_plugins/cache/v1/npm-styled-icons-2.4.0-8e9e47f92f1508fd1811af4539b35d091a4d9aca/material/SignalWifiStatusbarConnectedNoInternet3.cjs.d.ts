import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbarConnectedNoInternet3: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternet3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
