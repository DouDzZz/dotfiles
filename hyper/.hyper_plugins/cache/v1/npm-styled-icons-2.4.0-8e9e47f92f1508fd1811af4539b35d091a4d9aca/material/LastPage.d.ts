import { StyledIcon, StyledIconProps } from '..';
export declare const LastPage: StyledIcon<any>;
export declare const LastPageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
