import { StyledIcon, StyledIconProps } from '..';
export declare const FilterVintage: StyledIcon<any>;
export declare const FilterVintageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
