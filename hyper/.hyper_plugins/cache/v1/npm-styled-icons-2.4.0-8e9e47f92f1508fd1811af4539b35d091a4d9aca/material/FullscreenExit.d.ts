import { StyledIcon, StyledIconProps } from '..';
export declare const FullscreenExit: StyledIcon<any>;
export declare const FullscreenExitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
