import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalParking: StyledIcon<any>;
export declare const LocalParkingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
