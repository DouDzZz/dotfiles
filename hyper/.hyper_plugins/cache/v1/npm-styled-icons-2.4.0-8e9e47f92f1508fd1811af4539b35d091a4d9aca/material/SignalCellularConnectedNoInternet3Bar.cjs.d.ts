import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularConnectedNoInternet3Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
