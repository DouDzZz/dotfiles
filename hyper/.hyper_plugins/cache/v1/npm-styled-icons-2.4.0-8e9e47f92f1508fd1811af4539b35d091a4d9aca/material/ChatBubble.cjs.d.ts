import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChatBubble: StyledIcon<any>;
export declare const ChatBubbleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
