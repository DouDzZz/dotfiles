import { StyledIcon, StyledIconProps } from '..';
export declare const Cancel: StyledIcon<any>;
export declare const CancelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
