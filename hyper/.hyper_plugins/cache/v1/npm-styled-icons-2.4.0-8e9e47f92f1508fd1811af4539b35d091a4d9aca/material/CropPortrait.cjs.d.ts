import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropPortrait: StyledIcon<any>;
export declare const CropPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
