import { StyledIcon, StyledIconProps } from '..';
export declare const Directions: StyledIcon<any>;
export declare const DirectionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
