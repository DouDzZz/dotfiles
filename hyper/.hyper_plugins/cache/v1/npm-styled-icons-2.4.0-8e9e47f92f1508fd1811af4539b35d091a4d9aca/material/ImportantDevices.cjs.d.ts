import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ImportantDevices: StyledIcon<any>;
export declare const ImportantDevicesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
