import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Create: StyledIcon<any>;
export declare const CreateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
