import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsBus: StyledIcon<any>;
export declare const DirectionsBusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
