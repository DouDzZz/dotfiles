import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness2: StyledIcon<any>;
export declare const Brightness2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
