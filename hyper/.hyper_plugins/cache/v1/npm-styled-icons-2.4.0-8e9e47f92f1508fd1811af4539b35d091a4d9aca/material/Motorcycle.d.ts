import { StyledIcon, StyledIconProps } from '..';
export declare const Motorcycle: StyledIcon<any>;
export declare const MotorcycleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
