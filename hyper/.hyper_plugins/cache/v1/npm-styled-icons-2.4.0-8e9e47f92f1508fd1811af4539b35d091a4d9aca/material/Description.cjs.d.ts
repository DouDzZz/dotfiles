import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Description: StyledIcon<any>;
export declare const DescriptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
