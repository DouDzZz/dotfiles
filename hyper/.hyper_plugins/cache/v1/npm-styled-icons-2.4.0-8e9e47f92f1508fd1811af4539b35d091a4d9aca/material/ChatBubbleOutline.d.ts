import { StyledIcon, StyledIconProps } from '..';
export declare const ChatBubbleOutline: StyledIcon<any>;
export declare const ChatBubbleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
