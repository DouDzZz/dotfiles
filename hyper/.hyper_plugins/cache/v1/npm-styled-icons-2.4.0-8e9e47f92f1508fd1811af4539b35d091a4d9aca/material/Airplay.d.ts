import { StyledIcon, StyledIconProps } from '..';
export declare const Airplay: StyledIcon<any>;
export declare const AirplayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
