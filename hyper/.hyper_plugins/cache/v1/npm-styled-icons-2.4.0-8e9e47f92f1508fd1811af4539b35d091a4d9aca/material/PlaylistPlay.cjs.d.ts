import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlaylistPlay: StyledIcon<any>;
export declare const PlaylistPlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
