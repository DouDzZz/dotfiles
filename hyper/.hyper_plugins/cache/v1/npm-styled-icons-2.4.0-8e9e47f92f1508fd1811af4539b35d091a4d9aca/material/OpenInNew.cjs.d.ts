import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OpenInNew: StyledIcon<any>;
export declare const OpenInNewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
