import { StyledIcon, StyledIconProps } from '..';
export declare const HdrStrong: StyledIcon<any>;
export declare const HdrStrongDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
