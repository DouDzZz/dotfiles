import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalMall: StyledIcon<any>;
export declare const LocalMallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
