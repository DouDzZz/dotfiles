import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ZoomOut: StyledIcon<any>;
export declare const ZoomOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
