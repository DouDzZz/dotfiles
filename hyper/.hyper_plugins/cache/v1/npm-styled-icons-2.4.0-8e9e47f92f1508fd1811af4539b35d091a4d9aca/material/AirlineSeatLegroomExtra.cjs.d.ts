import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirlineSeatLegroomExtra: StyledIcon<any>;
export declare const AirlineSeatLegroomExtraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
