import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FlipToFront: StyledIcon<any>;
export declare const FlipToFrontDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
