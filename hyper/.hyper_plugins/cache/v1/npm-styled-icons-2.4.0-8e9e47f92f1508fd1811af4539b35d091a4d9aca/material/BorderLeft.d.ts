import { StyledIcon, StyledIconProps } from '..';
export declare const BorderLeft: StyledIcon<any>;
export declare const BorderLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
