import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DialerSip: StyledIcon<any>;
export declare const DialerSipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
