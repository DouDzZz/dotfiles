import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeUp: StyledIcon<any>;
export declare const VolumeUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
