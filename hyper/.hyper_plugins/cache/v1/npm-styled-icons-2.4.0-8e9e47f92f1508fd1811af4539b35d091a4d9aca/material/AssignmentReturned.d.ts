import { StyledIcon, StyledIconProps } from '..';
export declare const AssignmentReturned: StyledIcon<any>;
export declare const AssignmentReturnedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
