import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocationOn: StyledIcon<any>;
export declare const LocationOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
