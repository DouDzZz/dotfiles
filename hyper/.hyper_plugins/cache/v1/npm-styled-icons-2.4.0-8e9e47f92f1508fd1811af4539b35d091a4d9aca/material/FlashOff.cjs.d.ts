import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FlashOff: StyledIcon<any>;
export declare const FlashOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
