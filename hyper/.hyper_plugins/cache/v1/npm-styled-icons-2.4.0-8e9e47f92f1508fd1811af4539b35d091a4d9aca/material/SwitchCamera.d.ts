import { StyledIcon, StyledIconProps } from '..';
export declare const SwitchCamera: StyledIcon<any>;
export declare const SwitchCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
