import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalTaxi: StyledIcon<any>;
export declare const LocalTaxiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
