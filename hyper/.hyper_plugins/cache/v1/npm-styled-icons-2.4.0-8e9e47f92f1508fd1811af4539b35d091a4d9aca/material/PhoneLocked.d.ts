import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneLocked: StyledIcon<any>;
export declare const PhoneLockedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
