import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalLaundryService: StyledIcon<any>;
export declare const LocalLaundryServiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
