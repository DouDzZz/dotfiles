import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LibraryMusic: StyledIcon<any>;
export declare const LibraryMusicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
