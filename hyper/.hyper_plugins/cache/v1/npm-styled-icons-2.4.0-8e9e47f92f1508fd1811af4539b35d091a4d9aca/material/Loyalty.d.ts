import { StyledIcon, StyledIconProps } from '..';
export declare const Loyalty: StyledIcon<any>;
export declare const LoyaltyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
