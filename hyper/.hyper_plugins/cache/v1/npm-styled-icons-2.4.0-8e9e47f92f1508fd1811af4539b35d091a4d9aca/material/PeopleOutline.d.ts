import { StyledIcon, StyledIconProps } from '..';
export declare const PeopleOutline: StyledIcon<any>;
export declare const PeopleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
