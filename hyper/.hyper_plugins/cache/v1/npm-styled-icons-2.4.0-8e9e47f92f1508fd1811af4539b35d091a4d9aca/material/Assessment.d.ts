import { StyledIcon, StyledIconProps } from '..';
export declare const Assessment: StyledIcon<any>;
export declare const AssessmentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
