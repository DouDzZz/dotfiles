import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenLockRotation: StyledIcon<any>;
export declare const ScreenLockRotationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
