import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VerticalAlignBottom: StyledIcon<any>;
export declare const VerticalAlignBottomDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
