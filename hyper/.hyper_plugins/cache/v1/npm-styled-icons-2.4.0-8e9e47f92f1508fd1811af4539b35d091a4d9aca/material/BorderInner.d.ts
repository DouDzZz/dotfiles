import { StyledIcon, StyledIconProps } from '..';
export declare const BorderInner: StyledIcon<any>;
export declare const BorderInnerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
