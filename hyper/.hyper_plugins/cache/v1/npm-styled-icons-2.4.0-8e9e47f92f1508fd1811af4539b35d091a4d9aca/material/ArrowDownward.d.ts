import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDownward: StyledIcon<any>;
export declare const ArrowDownwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
