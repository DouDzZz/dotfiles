import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Textsms: StyledIcon<any>;
export declare const TextsmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
