import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardVoice: StyledIcon<any>;
export declare const KeyboardVoiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
