import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderOuter: StyledIcon<any>;
export declare const BorderOuterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
