import { StyledIcon, StyledIconProps } from '..';
export declare const ShowChart: StyledIcon<any>;
export declare const ShowChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
