import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneAndroid: StyledIcon<any>;
export declare const PhoneAndroidDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
