import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropOriginal: StyledIcon<any>;
export declare const CropOriginalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
