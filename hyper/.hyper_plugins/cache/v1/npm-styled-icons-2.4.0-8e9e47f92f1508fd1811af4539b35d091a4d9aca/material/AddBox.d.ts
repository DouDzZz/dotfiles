import { StyledIcon, StyledIconProps } from '..';
export declare const AddBox: StyledIcon<any>;
export declare const AddBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
