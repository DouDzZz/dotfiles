import { StyledIcon, StyledIconProps } from '..';
export declare const ErrorOutline: StyledIcon<any>;
export declare const ErrorOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
