import { StyledIcon, StyledIconProps } from '..';
export declare const FontDownload: StyledIcon<any>;
export declare const FontDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
