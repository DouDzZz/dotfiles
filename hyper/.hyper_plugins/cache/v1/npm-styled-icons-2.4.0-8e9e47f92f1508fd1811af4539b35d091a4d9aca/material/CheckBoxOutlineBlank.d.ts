import { StyledIcon, StyledIconProps } from '..';
export declare const CheckBoxOutlineBlank: StyledIcon<any>;
export declare const CheckBoxOutlineBlankDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
