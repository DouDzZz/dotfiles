import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeUp: StyledIcon<any>;
export declare const VolumeUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
