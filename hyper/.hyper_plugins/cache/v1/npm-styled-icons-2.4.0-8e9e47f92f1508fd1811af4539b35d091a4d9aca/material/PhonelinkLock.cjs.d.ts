import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonelinkLock: StyledIcon<any>;
export declare const PhonelinkLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
