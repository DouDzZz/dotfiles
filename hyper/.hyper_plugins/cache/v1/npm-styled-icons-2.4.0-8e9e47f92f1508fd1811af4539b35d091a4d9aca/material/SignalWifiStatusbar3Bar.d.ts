import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbar3Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
