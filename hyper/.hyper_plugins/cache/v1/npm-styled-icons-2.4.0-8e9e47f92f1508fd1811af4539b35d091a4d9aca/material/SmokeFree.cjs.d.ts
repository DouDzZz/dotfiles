import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SmokeFree: StyledIcon<any>;
export declare const SmokeFreeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
