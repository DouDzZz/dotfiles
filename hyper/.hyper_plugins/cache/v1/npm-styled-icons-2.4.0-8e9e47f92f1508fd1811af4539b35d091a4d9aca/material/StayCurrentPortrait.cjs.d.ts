import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StayCurrentPortrait: StyledIcon<any>;
export declare const StayCurrentPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
