import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SpeakerGroup: StyledIcon<any>;
export declare const SpeakerGroupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
