import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SelectAll: StyledIcon<any>;
export declare const SelectAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
