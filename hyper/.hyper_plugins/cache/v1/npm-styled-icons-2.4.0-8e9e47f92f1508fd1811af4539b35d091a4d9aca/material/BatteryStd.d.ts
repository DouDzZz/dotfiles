import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryStd: StyledIcon<any>;
export declare const BatteryStdDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
