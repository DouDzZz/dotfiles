import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Exposure: StyledIcon<any>;
export declare const ExposureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
