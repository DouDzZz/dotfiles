import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DoNotDisturb: StyledIcon<any>;
export declare const DoNotDisturbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
