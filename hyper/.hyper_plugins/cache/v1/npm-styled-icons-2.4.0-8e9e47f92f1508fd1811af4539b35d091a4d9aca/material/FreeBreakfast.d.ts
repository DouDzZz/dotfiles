import { StyledIcon, StyledIconProps } from '..';
export declare const FreeBreakfast: StyledIcon<any>;
export declare const FreeBreakfastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
