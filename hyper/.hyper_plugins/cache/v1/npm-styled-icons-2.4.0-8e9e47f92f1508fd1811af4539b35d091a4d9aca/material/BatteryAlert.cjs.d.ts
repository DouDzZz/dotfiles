import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryAlert: StyledIcon<any>;
export declare const BatteryAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
