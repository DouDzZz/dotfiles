import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SentimentNeutral: StyledIcon<any>;
export declare const SentimentNeutralDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
