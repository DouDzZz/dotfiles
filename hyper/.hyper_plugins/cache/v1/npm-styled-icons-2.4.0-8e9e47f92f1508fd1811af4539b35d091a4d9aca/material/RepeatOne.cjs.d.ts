import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RepeatOne: StyledIcon<any>;
export declare const RepeatOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
