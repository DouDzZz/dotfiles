import { StyledIcon, StyledIconProps } from '..';
export declare const MoodBad: StyledIcon<any>;
export declare const MoodBadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
