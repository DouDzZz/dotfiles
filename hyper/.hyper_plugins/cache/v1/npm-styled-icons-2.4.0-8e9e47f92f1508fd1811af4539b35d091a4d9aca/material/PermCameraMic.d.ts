import { StyledIcon, StyledIconProps } from '..';
export declare const PermCameraMic: StyledIcon<any>;
export declare const PermCameraMicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
