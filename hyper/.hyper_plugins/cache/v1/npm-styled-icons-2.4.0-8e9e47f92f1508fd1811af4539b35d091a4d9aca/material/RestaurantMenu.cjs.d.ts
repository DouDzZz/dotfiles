import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RestaurantMenu: StyledIcon<any>;
export declare const RestaurantMenuDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
