import { StyledIcon, StyledIconProps } from '..';
export declare const Translate: StyledIcon<any>;
export declare const TranslateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
