import { StyledIcon, StyledIconProps } from '..';
export declare const Tablet: StyledIcon<any>;
export declare const TabletDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
