import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LiveHelp: StyledIcon<any>;
export declare const LiveHelpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
