import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Security: StyledIcon<any>;
export declare const SecurityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
