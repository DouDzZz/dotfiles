import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EnhancedEncryption: StyledIcon<any>;
export declare const EnhancedEncryptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
