import { StyledIcon, StyledIconProps } from '..';
export declare const FormatTextdirectionRToL: StyledIcon<any>;
export declare const FormatTextdirectionRToLDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
