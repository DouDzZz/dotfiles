import { StyledIcon, StyledIconProps } from '..';
export declare const ExposurePlus2: StyledIcon<any>;
export declare const ExposurePlus2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
