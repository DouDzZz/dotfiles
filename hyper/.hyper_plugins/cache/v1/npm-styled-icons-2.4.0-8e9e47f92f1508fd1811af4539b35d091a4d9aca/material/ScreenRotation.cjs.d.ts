import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenRotation: StyledIcon<any>;
export declare const ScreenRotationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
