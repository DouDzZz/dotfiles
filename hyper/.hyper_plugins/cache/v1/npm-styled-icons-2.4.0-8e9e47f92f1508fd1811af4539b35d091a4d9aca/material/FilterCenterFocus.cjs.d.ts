import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterCenterFocus: StyledIcon<any>;
export declare const FilterCenterFocusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
