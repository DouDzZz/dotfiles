import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CollectionsBookmark: StyledIcon<any>;
export declare const CollectionsBookmarkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
