import { StyledIcon, StyledIconProps } from '..';
export declare const DiscFull: StyledIcon<any>;
export declare const DiscFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
