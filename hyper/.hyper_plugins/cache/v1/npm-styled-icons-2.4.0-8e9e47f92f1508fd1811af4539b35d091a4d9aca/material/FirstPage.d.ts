import { StyledIcon, StyledIconProps } from '..';
export declare const FirstPage: StyledIcon<any>;
export declare const FirstPageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
