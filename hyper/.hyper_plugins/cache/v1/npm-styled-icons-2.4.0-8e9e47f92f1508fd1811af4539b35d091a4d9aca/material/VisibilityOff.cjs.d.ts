import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VisibilityOff: StyledIcon<any>;
export declare const VisibilityOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
