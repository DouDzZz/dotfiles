import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifi4Bar: StyledIcon<any>;
export declare const SignalWifi4BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
