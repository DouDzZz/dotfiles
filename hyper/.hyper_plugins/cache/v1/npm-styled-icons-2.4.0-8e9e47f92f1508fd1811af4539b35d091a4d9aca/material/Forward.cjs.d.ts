import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Forward: StyledIcon<any>;
export declare const ForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
