import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OfflinePin: StyledIcon<any>;
export declare const OfflinePinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
