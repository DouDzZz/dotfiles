import { StyledIcon, StyledIconProps } from '..';
export declare const Iso: StyledIcon<any>;
export declare const IsoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
