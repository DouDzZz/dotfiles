import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifi3BarLock: StyledIcon<any>;
export declare const SignalWifi3BarLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
