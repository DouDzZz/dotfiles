import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryCharging20: StyledIcon<any>;
export declare const BatteryCharging20Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
