import { StyledIcon, StyledIconProps } from '..';
export declare const Rowing: StyledIcon<any>;
export declare const RowingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
