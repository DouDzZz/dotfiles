import { StyledIcon, StyledIconProps } from '..';
export declare const Snooze: StyledIcon<any>;
export declare const SnoozeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
