import { StyledIcon, StyledIconProps } from '..';
export declare const SdCard: StyledIcon<any>;
export declare const SdCardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
