import { StyledIcon, StyledIconProps } from '..';
export declare const LineWeight: StyledIcon<any>;
export declare const LineWeightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
