import { StyledIcon, StyledIconProps } from '..';
export declare const SentimentNeutral: StyledIcon<any>;
export declare const SentimentNeutralDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
