import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SubdirectoryArrowRight: StyledIcon<any>;
export declare const SubdirectoryArrowRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
