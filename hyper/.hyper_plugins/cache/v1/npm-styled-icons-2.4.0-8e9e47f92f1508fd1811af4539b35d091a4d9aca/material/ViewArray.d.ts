import { StyledIcon, StyledIconProps } from '..';
export declare const ViewArray: StyledIcon<any>;
export declare const ViewArrayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
