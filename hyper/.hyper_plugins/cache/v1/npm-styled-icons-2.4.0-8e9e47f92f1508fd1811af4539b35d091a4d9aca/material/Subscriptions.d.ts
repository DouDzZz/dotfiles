import { StyledIcon, StyledIconProps } from '..';
export declare const Subscriptions: StyledIcon<any>;
export declare const SubscriptionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
