import { StyledIcon, StyledIconProps } from '..';
export declare const Movie: StyledIcon<any>;
export declare const MovieDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
