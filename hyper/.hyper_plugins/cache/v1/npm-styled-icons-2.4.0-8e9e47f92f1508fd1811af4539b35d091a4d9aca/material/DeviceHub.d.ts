import { StyledIcon, StyledIconProps } from '..';
export declare const DeviceHub: StyledIcon<any>;
export declare const DeviceHubDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
