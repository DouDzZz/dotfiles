import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsVoice: StyledIcon<any>;
export declare const SettingsVoiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
