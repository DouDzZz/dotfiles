import { StyledIcon, StyledIconProps } from '..';
export declare const LabelOutline: StyledIcon<any>;
export declare const LabelOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
