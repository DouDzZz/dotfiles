import { StyledIcon, StyledIconProps } from '..';
export declare const DeveloperMode: StyledIcon<any>;
export declare const DeveloperModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
