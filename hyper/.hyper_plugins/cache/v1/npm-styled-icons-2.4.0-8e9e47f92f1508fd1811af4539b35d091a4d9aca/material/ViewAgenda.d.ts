import { StyledIcon, StyledIconProps } from '..';
export declare const ViewAgenda: StyledIcon<any>;
export declare const ViewAgendaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
