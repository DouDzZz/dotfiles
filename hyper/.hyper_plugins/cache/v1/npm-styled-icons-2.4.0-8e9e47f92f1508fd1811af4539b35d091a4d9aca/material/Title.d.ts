import { StyledIcon, StyledIconProps } from '..';
export declare const Title: StyledIcon<any>;
export declare const TitleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
