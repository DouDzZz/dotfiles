import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneBluetoothSpeaker: StyledIcon<any>;
export declare const PhoneBluetoothSpeakerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
