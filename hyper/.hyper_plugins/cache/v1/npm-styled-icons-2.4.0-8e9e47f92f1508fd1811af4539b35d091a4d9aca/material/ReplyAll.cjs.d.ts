import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ReplyAll: StyledIcon<any>;
export declare const ReplyAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
