import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbsUpDown: StyledIcon<any>;
export declare const ThumbsUpDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
