import { StyledIcon, StyledIconProps } from '..';
export declare const Filter3: StyledIcon<any>;
export declare const Filter3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
