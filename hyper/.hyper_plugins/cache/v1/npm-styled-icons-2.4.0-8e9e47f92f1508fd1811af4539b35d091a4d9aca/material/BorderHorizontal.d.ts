import { StyledIcon, StyledIconProps } from '..';
export declare const BorderHorizontal: StyledIcon<any>;
export declare const BorderHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
