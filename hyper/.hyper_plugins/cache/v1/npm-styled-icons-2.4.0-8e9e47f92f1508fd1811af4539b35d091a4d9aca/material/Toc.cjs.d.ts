import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Toc: StyledIcon<any>;
export declare const TocDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
