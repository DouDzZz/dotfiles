import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenShare: StyledIcon<any>;
export declare const ScreenShareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
