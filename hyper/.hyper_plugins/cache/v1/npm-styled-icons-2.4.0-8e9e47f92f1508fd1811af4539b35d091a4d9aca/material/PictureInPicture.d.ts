import { StyledIcon, StyledIconProps } from '..';
export declare const PictureInPicture: StyledIcon<any>;
export declare const PictureInPictureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
