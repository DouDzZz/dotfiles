import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mouse: StyledIcon<any>;
export declare const MouseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
