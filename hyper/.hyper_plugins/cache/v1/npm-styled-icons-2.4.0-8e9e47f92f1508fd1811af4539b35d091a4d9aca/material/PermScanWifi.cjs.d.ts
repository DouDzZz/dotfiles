import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermScanWifi: StyledIcon<any>;
export declare const PermScanWifiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
