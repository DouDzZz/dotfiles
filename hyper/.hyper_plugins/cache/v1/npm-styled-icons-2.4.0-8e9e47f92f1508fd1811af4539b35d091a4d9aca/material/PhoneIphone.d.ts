import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneIphone: StyledIcon<any>;
export declare const PhoneIphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
