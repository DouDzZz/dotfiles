import { StyledIcon, StyledIconProps } from '..';
export declare const ContentPaste: StyledIcon<any>;
export declare const ContentPasteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
