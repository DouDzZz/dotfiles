import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AttachMoney: StyledIcon<any>;
export declare const AttachMoneyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
