import { StyledIcon, StyledIconProps } from '..';
export declare const Flare: StyledIcon<any>;
export declare const FlareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
