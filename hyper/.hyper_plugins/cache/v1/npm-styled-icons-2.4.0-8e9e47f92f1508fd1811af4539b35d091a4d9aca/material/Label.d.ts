import { StyledIcon, StyledIconProps } from '..';
export declare const Label: StyledIcon<any>;
export declare const LabelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
