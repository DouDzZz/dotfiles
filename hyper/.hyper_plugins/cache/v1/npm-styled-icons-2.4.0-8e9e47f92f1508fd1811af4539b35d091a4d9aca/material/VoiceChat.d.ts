import { StyledIcon, StyledIconProps } from '..';
export declare const VoiceChat: StyledIcon<any>;
export declare const VoiceChatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
