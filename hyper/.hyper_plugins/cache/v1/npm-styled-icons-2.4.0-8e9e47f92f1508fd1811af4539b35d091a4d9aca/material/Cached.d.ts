import { StyledIcon, StyledIconProps } from '..';
export declare const Cached: StyledIcon<any>;
export declare const CachedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
