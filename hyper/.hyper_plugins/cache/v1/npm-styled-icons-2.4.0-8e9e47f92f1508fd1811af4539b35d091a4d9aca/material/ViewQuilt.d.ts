import { StyledIcon, StyledIconProps } from '..';
export declare const ViewQuilt: StyledIcon<any>;
export declare const ViewQuiltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
