import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SyncProblem: StyledIcon<any>;
export declare const SyncProblemDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
