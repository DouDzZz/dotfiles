import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness1: StyledIcon<any>;
export declare const Brightness1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
