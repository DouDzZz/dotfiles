import { StyledIcon, StyledIconProps } from '..';
export declare const HighlightOff: StyledIcon<any>;
export declare const HighlightOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
