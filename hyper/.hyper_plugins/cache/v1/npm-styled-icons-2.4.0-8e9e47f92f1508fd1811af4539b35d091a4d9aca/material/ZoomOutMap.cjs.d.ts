import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ZoomOutMap: StyledIcon<any>;
export declare const ZoomOutMapDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
