import { StyledIcon, StyledIconProps } from '..';
export declare const Toc: StyledIcon<any>;
export declare const TocDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
