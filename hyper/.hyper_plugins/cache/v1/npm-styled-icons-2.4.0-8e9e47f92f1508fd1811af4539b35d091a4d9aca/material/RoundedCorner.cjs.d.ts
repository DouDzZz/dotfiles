import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RoundedCorner: StyledIcon<any>;
export declare const RoundedCornerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
