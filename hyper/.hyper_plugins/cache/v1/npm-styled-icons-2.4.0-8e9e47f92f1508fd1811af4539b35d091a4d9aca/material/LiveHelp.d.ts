import { StyledIcon, StyledIconProps } from '..';
export declare const LiveHelp: StyledIcon<any>;
export declare const LiveHelpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
