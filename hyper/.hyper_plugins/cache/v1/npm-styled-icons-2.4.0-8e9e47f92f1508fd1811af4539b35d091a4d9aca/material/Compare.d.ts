import { StyledIcon, StyledIconProps } from '..';
export declare const Compare: StyledIcon<any>;
export declare const CompareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
