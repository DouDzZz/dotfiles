import { StyledIcon, StyledIconProps } from '..';
export declare const Receipt: StyledIcon<any>;
export declare const ReceiptDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
