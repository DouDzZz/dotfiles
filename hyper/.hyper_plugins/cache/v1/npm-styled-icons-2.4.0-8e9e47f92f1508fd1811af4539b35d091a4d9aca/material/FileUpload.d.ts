import { StyledIcon, StyledIconProps } from '..';
export declare const FileUpload: StyledIcon<any>;
export declare const FileUploadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
