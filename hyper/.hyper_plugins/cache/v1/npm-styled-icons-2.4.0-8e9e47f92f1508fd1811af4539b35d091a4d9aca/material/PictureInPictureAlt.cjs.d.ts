import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PictureInPictureAlt: StyledIcon<any>;
export declare const PictureInPictureAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
