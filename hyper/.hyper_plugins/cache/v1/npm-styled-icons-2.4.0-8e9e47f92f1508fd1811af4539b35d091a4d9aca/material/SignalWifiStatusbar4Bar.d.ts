import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbar4Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar4BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
