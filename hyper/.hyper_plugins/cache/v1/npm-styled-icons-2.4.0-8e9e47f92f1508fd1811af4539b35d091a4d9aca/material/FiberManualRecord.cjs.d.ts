import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiberManualRecord: StyledIcon<any>;
export declare const FiberManualRecordDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
