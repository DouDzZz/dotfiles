import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AttachFile: StyledIcon<any>;
export declare const AttachFileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
