import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Store: StyledIcon<any>;
export declare const StoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
