import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RadioButtonChecked: StyledIcon<any>;
export declare const RadioButtonCheckedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
