import { StyledIcon, StyledIconProps } from '..';
export declare const ChromeReaderMode: StyledIcon<any>;
export declare const ChromeReaderModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
