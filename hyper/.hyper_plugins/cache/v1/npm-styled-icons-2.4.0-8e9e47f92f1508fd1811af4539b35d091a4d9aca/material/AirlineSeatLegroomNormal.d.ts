import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatLegroomNormal: StyledIcon<any>;
export declare const AirlineSeatLegroomNormalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
