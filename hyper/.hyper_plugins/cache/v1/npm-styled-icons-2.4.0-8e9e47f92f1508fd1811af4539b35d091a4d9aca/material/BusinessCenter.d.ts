import { StyledIcon, StyledIconProps } from '..';
export declare const BusinessCenter: StyledIcon<any>;
export declare const BusinessCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
