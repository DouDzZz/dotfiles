import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Smartphone: StyledIcon<any>;
export declare const SmartphoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
