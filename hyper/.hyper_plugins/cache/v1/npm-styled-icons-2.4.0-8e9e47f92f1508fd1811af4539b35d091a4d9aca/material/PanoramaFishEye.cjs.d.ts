import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PanoramaFishEye: StyledIcon<any>;
export declare const PanoramaFishEyeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
