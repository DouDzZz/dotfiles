import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Payment: StyledIcon<any>;
export declare const PaymentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
