import { StyledIcon, StyledIconProps } from '..';
export declare const NotificationsNone: StyledIcon<any>;
export declare const NotificationsNoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
