import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermContactCalendar: StyledIcon<any>;
export declare const PermContactCalendarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
