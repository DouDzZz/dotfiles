import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SimCardAlert: StyledIcon<any>;
export declare const SimCardAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
