import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderStyle: StyledIcon<any>;
export declare const BorderStyleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
