import { StyledIcon, StyledIconProps } from '..';
export declare const FilterBAndW: StyledIcon<any>;
export declare const FilterBAndWDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
