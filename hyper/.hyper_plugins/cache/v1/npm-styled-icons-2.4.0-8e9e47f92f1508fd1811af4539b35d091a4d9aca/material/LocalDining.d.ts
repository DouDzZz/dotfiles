import { StyledIcon, StyledIconProps } from '..';
export declare const LocalDining: StyledIcon<any>;
export declare const LocalDiningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
