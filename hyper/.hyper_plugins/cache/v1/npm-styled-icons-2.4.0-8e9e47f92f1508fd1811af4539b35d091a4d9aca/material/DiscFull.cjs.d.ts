import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiscFull: StyledIcon<any>;
export declare const DiscFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
