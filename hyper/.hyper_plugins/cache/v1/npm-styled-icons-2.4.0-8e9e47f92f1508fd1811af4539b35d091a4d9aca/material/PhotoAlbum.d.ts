import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoAlbum: StyledIcon<any>;
export declare const PhotoAlbumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
