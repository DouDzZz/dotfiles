import { StyledIcon, StyledIconProps } from '..';
export declare const FastRewind: StyledIcon<any>;
export declare const FastRewindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
