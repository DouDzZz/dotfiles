import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddShoppingCart: StyledIcon<any>;
export declare const AddShoppingCartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
