import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardReturn: StyledIcon<any>;
export declare const KeyboardReturnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
