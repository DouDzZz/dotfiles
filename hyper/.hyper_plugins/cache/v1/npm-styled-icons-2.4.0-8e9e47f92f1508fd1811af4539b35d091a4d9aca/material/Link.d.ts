import { StyledIcon, StyledIconProps } from '..';
export declare const Link: StyledIcon<any>;
export declare const LinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
