import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellular3Bar: StyledIcon<any>;
export declare const SignalCellular3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
