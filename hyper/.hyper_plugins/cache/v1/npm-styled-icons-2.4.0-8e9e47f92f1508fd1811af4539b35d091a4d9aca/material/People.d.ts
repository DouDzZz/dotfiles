import { StyledIcon, StyledIconProps } from '..';
export declare const People: StyledIcon<any>;
export declare const PeopleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
