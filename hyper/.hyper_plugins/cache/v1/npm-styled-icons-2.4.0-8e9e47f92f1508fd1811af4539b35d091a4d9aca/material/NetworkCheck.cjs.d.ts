import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NetworkCheck: StyledIcon<any>;
export declare const NetworkCheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
