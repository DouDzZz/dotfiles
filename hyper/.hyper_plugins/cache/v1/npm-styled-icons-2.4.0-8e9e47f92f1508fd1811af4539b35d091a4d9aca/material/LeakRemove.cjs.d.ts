import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LeakRemove: StyledIcon<any>;
export declare const LeakRemoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
