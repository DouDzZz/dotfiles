import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiOff: StyledIcon<any>;
export declare const SignalWifiOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
