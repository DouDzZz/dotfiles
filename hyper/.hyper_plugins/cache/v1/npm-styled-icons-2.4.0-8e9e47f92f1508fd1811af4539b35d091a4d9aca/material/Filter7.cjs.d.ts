import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter7: StyledIcon<any>;
export declare const Filter7Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
