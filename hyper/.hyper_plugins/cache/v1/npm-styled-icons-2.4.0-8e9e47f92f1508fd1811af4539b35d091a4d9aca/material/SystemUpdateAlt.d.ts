import { StyledIcon, StyledIconProps } from '..';
export declare const SystemUpdateAlt: StyledIcon<any>;
export declare const SystemUpdateAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
