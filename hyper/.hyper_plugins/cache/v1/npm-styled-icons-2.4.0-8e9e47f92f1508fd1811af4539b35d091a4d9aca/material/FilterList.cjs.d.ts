import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterList: StyledIcon<any>;
export declare const FilterListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
