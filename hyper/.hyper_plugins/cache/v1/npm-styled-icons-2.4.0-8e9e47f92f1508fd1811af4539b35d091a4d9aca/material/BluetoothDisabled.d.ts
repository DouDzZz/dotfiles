import { StyledIcon, StyledIconProps } from '..';
export declare const BluetoothDisabled: StyledIcon<any>;
export declare const BluetoothDisabledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
