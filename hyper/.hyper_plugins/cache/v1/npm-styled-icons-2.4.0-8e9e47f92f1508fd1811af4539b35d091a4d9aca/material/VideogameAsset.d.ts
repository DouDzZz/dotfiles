import { StyledIcon, StyledIconProps } from '..';
export declare const VideogameAsset: StyledIcon<any>;
export declare const VideogameAssetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
