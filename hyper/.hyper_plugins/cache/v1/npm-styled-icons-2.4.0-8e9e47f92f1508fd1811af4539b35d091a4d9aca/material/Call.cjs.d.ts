import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Call: StyledIcon<any>;
export declare const CallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
