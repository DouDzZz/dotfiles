import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tab: StyledIcon<any>;
export declare const TabDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
