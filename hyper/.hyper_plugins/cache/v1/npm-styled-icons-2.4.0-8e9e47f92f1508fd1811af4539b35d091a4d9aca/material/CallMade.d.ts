import { StyledIcon, StyledIconProps } from '..';
export declare const CallMade: StyledIcon<any>;
export declare const CallMadeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
