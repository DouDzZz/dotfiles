import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ColorLens: StyledIcon<any>;
export declare const ColorLensDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
