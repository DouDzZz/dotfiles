import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsCar: StyledIcon<any>;
export declare const DirectionsCarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
