import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsInputComposite: StyledIcon<any>;
export declare const SettingsInputCompositeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
