import { StyledIcon, StyledIconProps } from '..';
export declare const School: StyledIcon<any>;
export declare const SchoolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
