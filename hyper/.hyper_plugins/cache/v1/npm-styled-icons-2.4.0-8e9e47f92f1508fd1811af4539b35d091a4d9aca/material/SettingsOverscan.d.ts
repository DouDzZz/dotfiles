import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsOverscan: StyledIcon<any>;
export declare const SettingsOverscanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
