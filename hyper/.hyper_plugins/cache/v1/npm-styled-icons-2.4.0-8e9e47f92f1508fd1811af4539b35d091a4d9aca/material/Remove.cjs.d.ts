import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Remove: StyledIcon<any>;
export declare const RemoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
