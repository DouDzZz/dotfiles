import { StyledIcon, StyledIconProps } from '..';
export declare const RestorePage: StyledIcon<any>;
export declare const RestorePageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
