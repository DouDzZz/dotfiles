import { StyledIcon, StyledIconProps } from '..';
export declare const FolderSpecial: StyledIcon<any>;
export declare const FolderSpecialDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
