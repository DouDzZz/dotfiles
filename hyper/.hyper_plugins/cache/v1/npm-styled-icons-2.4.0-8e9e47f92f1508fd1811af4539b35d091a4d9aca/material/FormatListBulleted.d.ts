import { StyledIcon, StyledIconProps } from '..';
export declare const FormatListBulleted: StyledIcon<any>;
export declare const FormatListBulletedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
