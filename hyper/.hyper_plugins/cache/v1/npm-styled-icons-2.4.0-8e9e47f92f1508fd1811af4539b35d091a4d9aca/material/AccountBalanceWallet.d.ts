import { StyledIcon, StyledIconProps } from '..';
export declare const AccountBalanceWallet: StyledIcon<any>;
export declare const AccountBalanceWalletDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
