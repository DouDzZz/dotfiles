import { StyledIcon, StyledIconProps } from '..';
export declare const ViewStream: StyledIcon<any>;
export declare const ViewStreamDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
