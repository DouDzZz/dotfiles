import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InfoOutline: StyledIcon<any>;
export declare const InfoOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
