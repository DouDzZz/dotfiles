import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const People: StyledIcon<any>;
export declare const PeopleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
