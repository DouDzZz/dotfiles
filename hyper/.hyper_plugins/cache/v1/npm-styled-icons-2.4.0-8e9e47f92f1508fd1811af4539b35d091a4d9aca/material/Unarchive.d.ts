import { StyledIcon, StyledIconProps } from '..';
export declare const Unarchive: StyledIcon<any>;
export declare const UnarchiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
