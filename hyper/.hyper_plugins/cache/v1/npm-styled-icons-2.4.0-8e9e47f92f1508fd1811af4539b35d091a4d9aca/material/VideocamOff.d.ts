import { StyledIcon, StyledIconProps } from '..';
export declare const VideocamOff: StyledIcon<any>;
export declare const VideocamOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
