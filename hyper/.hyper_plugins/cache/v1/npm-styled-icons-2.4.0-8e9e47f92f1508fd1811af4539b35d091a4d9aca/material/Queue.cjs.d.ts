import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Queue: StyledIcon<any>;
export declare const QueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
