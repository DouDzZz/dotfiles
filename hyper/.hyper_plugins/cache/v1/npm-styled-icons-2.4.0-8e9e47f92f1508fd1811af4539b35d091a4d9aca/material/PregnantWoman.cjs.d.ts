import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PregnantWoman: StyledIcon<any>;
export declare const PregnantWomanDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
