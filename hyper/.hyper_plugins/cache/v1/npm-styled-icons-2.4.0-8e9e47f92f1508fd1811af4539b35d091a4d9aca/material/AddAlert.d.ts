import { StyledIcon, StyledIconProps } from '..';
export declare const AddAlert: StyledIcon<any>;
export declare const AddAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
