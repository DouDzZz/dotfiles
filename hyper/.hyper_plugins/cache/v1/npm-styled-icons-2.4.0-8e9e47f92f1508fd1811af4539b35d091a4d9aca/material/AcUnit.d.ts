import { StyledIcon, StyledIconProps } from '..';
export declare const AcUnit: StyledIcon<any>;
export declare const AcUnitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
