import { StyledIcon, StyledIconProps } from '..';
export declare const ClearAll: StyledIcon<any>;
export declare const ClearAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
