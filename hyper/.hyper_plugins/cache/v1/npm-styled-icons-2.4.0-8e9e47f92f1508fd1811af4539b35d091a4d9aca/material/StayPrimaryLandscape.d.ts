import { StyledIcon, StyledIconProps } from '..';
export declare const StayPrimaryLandscape: StyledIcon<any>;
export declare const StayPrimaryLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
