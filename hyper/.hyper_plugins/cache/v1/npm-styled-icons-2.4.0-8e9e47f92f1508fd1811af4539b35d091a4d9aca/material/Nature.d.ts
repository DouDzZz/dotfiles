import { StyledIcon, StyledIconProps } from '..';
export declare const Nature: StyledIcon<any>;
export declare const NatureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
