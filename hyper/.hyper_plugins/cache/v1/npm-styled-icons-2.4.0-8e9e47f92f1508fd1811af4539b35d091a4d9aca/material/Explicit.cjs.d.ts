import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Explicit: StyledIcon<any>;
export declare const ExplicitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
