import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsInputSvideo: StyledIcon<any>;
export declare const SettingsInputSvideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
