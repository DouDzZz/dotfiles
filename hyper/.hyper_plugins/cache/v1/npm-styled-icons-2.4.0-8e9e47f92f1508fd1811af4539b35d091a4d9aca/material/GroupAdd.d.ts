import { StyledIcon, StyledIconProps } from '..';
export declare const GroupAdd: StyledIcon<any>;
export declare const GroupAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
