import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DragHandle: StyledIcon<any>;
export declare const DragHandleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
