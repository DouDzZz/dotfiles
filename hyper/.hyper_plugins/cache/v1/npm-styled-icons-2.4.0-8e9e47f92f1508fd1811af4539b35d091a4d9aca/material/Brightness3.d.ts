import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness3: StyledIcon<any>;
export declare const Brightness3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
