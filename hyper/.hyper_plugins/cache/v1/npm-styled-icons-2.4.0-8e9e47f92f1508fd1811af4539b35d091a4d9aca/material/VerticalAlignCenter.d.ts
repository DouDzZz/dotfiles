import { StyledIcon, StyledIconProps } from '..';
export declare const VerticalAlignCenter: StyledIcon<any>;
export declare const VerticalAlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
