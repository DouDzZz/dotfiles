import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirlineSeatReclineNormal: StyledIcon<any>;
export declare const AirlineSeatReclineNormalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
