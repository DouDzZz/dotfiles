import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudDone: StyledIcon<any>;
export declare const CloudDoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
