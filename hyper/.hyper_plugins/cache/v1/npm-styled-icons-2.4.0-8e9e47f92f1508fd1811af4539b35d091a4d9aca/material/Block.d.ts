import { StyledIcon, StyledIconProps } from '..';
export declare const Block: StyledIcon<any>;
export declare const BlockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
