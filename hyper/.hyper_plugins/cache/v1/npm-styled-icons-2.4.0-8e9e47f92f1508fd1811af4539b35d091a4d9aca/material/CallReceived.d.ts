import { StyledIcon, StyledIconProps } from '..';
export declare const CallReceived: StyledIcon<any>;
export declare const CallReceivedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
