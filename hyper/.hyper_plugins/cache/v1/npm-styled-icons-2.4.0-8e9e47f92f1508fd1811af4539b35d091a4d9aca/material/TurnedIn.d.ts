import { StyledIcon, StyledIconProps } from '..';
export declare const TurnedIn: StyledIcon<any>;
export declare const TurnedInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
