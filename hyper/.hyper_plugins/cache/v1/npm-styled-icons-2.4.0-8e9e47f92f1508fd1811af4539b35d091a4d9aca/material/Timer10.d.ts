import { StyledIcon, StyledIconProps } from '..';
export declare const Timer10: StyledIcon<any>;
export declare const Timer10Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
