import { StyledIcon, StyledIconProps } from '..';
export declare const Redeem: StyledIcon<any>;
export declare const RedeemDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
