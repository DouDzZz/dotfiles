import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ContactPhone: StyledIcon<any>;
export declare const ContactPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
