import { StyledIcon, StyledIconProps } from '..';
export declare const Chat: StyledIcon<any>;
export declare const ChatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
