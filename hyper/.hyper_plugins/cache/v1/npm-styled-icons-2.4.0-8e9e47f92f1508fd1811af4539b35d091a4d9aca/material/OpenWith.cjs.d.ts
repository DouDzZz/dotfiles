import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OpenWith: StyledIcon<any>;
export declare const OpenWithDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
