import { StyledIcon, StyledIconProps } from '..';
export declare const ArtTrack: StyledIcon<any>;
export declare const ArtTrackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
