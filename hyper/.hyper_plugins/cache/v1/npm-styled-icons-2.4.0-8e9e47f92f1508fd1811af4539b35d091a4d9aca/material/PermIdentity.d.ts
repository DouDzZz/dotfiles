import { StyledIcon, StyledIconProps } from '..';
export declare const PermIdentity: StyledIcon<any>;
export declare const PermIdentityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
