import { StyledIcon, StyledIconProps } from '..';
export declare const NetworkCell: StyledIcon<any>;
export declare const NetworkCellDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
