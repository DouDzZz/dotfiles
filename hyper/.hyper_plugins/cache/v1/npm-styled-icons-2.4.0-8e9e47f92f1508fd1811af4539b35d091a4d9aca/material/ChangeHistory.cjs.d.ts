import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChangeHistory: StyledIcon<any>;
export declare const ChangeHistoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
