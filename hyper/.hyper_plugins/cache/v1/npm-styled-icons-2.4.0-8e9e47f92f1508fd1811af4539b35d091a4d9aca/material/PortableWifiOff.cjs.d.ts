import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PortableWifiOff: StyledIcon<any>;
export declare const PortableWifiOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
