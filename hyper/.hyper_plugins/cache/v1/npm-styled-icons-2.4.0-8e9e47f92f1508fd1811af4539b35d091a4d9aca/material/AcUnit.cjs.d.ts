import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AcUnit: StyledIcon<any>;
export declare const AcUnitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
