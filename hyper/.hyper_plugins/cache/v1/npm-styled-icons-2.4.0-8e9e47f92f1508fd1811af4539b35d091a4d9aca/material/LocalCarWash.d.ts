import { StyledIcon, StyledIconProps } from '..';
export declare const LocalCarWash: StyledIcon<any>;
export declare const LocalCarWashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
