import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Collections: StyledIcon<any>;
export declare const CollectionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
