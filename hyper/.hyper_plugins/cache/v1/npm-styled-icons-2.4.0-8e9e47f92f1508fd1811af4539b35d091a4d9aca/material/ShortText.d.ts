import { StyledIcon, StyledIconProps } from '..';
export declare const ShortText: StyledIcon<any>;
export declare const ShortTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
