import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pool: StyledIcon<any>;
export declare const PoolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
