import { StyledIcon, StyledIconProps } from '..';
export declare const RateReview: StyledIcon<any>;
export declare const RateReviewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
