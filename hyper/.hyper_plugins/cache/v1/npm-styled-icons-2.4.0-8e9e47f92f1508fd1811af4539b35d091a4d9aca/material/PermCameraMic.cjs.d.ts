import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermCameraMic: StyledIcon<any>;
export declare const PermCameraMicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
