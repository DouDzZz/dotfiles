import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterTiltShift: StyledIcon<any>;
export declare const FilterTiltShiftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
