import { StyledIcon, StyledIconProps } from '..';
export declare const ViewColumn: StyledIcon<any>;
export declare const ViewColumnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
