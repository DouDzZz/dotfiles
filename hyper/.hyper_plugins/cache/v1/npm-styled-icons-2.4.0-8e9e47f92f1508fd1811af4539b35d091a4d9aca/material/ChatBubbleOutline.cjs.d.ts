import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChatBubbleOutline: StyledIcon<any>;
export declare const ChatBubbleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
