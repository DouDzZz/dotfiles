import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Portrait: StyledIcon<any>;
export declare const PortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
