import { StyledIcon, StyledIconProps } from '..';
export declare const Beenhere: StyledIcon<any>;
export declare const BeenhereDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
