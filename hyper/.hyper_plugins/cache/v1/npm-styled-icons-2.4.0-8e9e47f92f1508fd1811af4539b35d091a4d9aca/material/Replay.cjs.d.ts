import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Replay: StyledIcon<any>;
export declare const ReplayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
