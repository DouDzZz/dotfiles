import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatClear: StyledIcon<any>;
export declare const FormatClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
