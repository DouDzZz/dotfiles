import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalCarWash: StyledIcon<any>;
export declare const LocalCarWashDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
