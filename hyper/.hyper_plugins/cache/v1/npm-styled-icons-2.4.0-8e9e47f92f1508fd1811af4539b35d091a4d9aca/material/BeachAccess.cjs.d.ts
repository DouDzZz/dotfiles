import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BeachAccess: StyledIcon<any>;
export declare const BeachAccessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
