import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Done: StyledIcon<any>;
export declare const DoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
