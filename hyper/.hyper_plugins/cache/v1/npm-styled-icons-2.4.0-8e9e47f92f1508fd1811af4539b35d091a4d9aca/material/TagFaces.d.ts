import { StyledIcon, StyledIconProps } from '..';
export declare const TagFaces: StyledIcon<any>;
export declare const TagFacesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
