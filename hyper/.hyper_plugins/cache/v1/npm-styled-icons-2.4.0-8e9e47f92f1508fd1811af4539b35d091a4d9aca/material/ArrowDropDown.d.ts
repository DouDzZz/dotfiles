import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDropDown: StyledIcon<any>;
export declare const ArrowDropDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
