import { StyledIcon, StyledIconProps } from '..';
export declare const PictureInPictureAlt: StyledIcon<any>;
export declare const PictureInPictureAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
