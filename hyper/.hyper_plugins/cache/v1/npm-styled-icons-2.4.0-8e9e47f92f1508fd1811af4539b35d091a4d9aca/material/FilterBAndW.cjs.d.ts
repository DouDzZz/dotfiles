import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilterBAndW: StyledIcon<any>;
export declare const FilterBAndWDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
