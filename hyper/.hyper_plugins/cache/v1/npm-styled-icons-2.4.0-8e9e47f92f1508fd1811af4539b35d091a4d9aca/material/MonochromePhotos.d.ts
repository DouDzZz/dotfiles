import { StyledIcon, StyledIconProps } from '..';
export declare const MonochromePhotos: StyledIcon<any>;
export declare const MonochromePhotosDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
