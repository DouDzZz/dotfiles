import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsPower: StyledIcon<any>;
export declare const SettingsPowerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
