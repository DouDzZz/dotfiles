import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PersonPin: StyledIcon<any>;
export declare const PersonPinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
