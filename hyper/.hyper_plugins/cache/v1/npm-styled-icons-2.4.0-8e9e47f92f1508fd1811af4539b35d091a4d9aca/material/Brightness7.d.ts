import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness7: StyledIcon<any>;
export declare const Brightness7Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
