import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Stars: StyledIcon<any>;
export declare const StarsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
