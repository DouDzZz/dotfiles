import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ScreenLockLandscape: StyledIcon<any>;
export declare const ScreenLockLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
