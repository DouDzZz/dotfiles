import { StyledIcon, StyledIconProps } from '..';
export declare const RemoveCircle: StyledIcon<any>;
export declare const RemoveCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
