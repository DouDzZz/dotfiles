import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ReportProblem: StyledIcon<any>;
export declare const ReportProblemDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
