"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.PinDrop = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "PinDrop-title" }, props.title), react_1.default.createElement("path", { d: "M18 8c0-3.31-2.69-6-6-6S6 4.69 6 8c0 4.5 6 11 6 11s6-6.5 6-11zm-8 0c0-1.1.9-2 2-2s2 .9 2 2a2 2 0 1 1-4 0zM5 20v2h14v-2H5z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { d: "M18 8c0-3.31-2.69-6-6-6S6 4.69 6 8c0 4.5 6 11 6 11s6-6.5 6-11zm-8 0c0-1.1.9-2 2-2s2 .9 2 2a2 2 0 1 1-4 0zM5 20v2h14v-2H5z", key: "k0" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.PinDrop.displayName = 'PinDrop';
exports.PinDropDimensions = { height: 24, width: 24 };
var templateObject_1;
