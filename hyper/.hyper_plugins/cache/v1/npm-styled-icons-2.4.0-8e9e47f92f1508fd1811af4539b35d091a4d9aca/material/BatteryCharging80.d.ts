import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryCharging80: StyledIcon<any>;
export declare const BatteryCharging80Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
