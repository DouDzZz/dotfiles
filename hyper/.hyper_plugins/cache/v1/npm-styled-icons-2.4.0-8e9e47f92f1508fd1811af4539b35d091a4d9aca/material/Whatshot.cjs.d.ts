import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Whatshot: StyledIcon<any>;
export declare const WhatshotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
