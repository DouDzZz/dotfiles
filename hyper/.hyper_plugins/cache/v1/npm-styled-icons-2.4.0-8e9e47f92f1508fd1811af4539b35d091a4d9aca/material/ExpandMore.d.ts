import { StyledIcon, StyledIconProps } from '..';
export declare const ExpandMore: StyledIcon<any>;
export declare const ExpandMoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
