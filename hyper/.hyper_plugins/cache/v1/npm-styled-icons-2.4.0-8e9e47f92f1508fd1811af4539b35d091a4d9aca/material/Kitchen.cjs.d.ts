import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Kitchen: StyledIcon<any>;
export declare const KitchenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
