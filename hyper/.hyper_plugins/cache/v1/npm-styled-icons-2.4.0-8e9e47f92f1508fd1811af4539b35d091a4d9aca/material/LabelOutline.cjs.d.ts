import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LabelOutline: StyledIcon<any>;
export declare const LabelOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
