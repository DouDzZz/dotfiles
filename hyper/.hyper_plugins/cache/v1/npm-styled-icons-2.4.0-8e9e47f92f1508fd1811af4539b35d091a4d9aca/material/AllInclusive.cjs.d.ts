import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AllInclusive: StyledIcon<any>;
export declare const AllInclusiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
