import { StyledIcon, StyledIconProps } from '..';
export declare const ChatBubble: StyledIcon<any>;
export declare const ChatBubbleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
