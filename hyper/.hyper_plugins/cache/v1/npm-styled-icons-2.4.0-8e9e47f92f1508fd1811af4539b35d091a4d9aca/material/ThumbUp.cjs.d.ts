import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbUp: StyledIcon<any>;
export declare const ThumbUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
