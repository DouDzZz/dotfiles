import { StyledIcon, StyledIconProps } from '..';
export declare const Contacts: StyledIcon<any>;
export declare const ContactsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
