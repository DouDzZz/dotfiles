import { StyledIcon, StyledIconProps } from '..';
export declare const Room: StyledIcon<any>;
export declare const RoomDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
