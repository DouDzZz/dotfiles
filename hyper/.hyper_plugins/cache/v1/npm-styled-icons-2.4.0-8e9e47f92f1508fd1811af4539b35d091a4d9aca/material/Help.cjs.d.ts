import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Help: StyledIcon<any>;
export declare const HelpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
