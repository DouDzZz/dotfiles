import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Streetview: StyledIcon<any>;
export declare const StreetviewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
