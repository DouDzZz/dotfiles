import { StyledIcon, StyledIconProps } from '..';
export declare const ContentCopy: StyledIcon<any>;
export declare const ContentCopyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
