import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryCharging60: StyledIcon<any>;
export declare const BatteryCharging60Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
