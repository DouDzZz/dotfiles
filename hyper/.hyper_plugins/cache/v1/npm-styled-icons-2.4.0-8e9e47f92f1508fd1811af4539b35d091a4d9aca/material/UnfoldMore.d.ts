import { StyledIcon, StyledIconProps } from '..';
export declare const UnfoldMore: StyledIcon<any>;
export declare const UnfoldMoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
