import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatIndentDecrease: StyledIcon<any>;
export declare const FormatIndentDecreaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
