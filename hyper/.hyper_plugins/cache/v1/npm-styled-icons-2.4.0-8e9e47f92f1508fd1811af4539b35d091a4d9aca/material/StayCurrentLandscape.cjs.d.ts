import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StayCurrentLandscape: StyledIcon<any>;
export declare const StayCurrentLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
