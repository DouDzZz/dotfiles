import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UnfoldMore: StyledIcon<any>;
export declare const UnfoldMoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
