import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Face: StyledIcon<any>;
export declare const FaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
