import { StyledIcon, StyledIconProps } from '..';
export declare const MultilineChart: StyledIcon<any>;
export declare const MultilineChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
