import { StyledIcon, StyledIconProps } from '..';
export declare const SmokeFree: StyledIcon<any>;
export declare const SmokeFreeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
