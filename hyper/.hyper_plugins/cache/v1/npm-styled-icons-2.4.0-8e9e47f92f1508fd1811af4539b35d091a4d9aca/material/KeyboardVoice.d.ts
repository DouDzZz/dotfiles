import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardVoice: StyledIcon<any>;
export declare const KeyboardVoiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
