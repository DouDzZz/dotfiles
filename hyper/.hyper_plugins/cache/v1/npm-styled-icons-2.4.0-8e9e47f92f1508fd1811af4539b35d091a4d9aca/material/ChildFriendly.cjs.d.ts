import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChildFriendly: StyledIcon<any>;
export declare const ChildFriendlyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
