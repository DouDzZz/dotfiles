import { StyledIcon, StyledIconProps } from '..';
export declare const NotificationsOff: StyledIcon<any>;
export declare const NotificationsOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
