import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewWeek: StyledIcon<any>;
export declare const ViewWeekDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
