import { StyledIcon, StyledIconProps } from '..';
export declare const Tram: StyledIcon<any>;
export declare const TramDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
