import { StyledIcon, StyledIconProps } from '..';
export declare const Tab: StyledIcon<any>;
export declare const TabDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
