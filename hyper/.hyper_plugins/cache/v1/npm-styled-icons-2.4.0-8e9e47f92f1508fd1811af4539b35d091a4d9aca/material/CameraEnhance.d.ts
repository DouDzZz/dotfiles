import { StyledIcon, StyledIconProps } from '..';
export declare const CameraEnhance: StyledIcon<any>;
export declare const CameraEnhanceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
