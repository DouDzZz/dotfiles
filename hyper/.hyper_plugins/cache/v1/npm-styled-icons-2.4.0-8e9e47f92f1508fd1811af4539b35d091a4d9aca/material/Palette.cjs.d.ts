import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Palette: StyledIcon<any>;
export declare const PaletteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
