import { StyledIcon, StyledIconProps } from '..';
export declare const FiberPin: StyledIcon<any>;
export declare const FiberPinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
