import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter9Plus: StyledIcon<any>;
export declare const Filter9PlusDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
