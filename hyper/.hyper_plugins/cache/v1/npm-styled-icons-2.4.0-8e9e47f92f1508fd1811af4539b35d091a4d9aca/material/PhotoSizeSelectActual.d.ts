import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoSizeSelectActual: StyledIcon<any>;
export declare const PhotoSizeSelectActualDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
