import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirplanemodeActive: StyledIcon<any>;
export declare const AirplanemodeActiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
