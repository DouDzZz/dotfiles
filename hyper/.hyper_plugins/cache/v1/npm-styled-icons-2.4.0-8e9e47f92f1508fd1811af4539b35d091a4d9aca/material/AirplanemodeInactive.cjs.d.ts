import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirplanemodeInactive: StyledIcon<any>;
export declare const AirplanemodeInactiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
