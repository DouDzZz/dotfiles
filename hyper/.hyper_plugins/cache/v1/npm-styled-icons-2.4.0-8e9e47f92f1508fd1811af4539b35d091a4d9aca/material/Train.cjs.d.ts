import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Train: StyledIcon<any>;
export declare const TrainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
