import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallSplit: StyledIcon<any>;
export declare const CallSplitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
