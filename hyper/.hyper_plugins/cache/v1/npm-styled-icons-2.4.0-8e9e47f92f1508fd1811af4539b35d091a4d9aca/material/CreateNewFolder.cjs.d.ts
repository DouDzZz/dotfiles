import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CreateNewFolder: StyledIcon<any>;
export declare const CreateNewFolderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
