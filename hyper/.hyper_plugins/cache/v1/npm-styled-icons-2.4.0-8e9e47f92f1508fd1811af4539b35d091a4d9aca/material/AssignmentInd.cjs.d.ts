import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssignmentInd: StyledIcon<any>;
export declare const AssignmentIndDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
