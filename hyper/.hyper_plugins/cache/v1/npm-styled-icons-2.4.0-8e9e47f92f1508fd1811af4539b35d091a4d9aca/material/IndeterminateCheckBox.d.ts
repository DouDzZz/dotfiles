import { StyledIcon, StyledIconProps } from '..';
export declare const IndeterminateCheckBox: StyledIcon<any>;
export declare const IndeterminateCheckBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
