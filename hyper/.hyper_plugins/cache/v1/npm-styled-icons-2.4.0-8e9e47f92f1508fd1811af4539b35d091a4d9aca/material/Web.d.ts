import { StyledIcon, StyledIconProps } from '..';
export declare const Web: StyledIcon<any>;
export declare const WebDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
