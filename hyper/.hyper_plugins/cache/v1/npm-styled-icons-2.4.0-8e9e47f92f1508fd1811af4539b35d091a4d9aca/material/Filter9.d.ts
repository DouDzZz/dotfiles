import { StyledIcon, StyledIconProps } from '..';
export declare const Filter9: StyledIcon<any>;
export declare const Filter9Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
