import { StyledIcon, StyledIconProps } from '..';
export declare const Pages: StyledIcon<any>;
export declare const PagesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
