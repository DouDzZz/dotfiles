import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WbIridescent: StyledIcon<any>;
export declare const WbIridescentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
