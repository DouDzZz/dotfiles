import { StyledIcon, StyledIconProps } from '..';
export declare const StayCurrentPortrait: StyledIcon<any>;
export declare const StayCurrentPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
