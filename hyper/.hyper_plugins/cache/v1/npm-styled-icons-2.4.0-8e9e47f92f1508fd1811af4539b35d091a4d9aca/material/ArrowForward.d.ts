import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowForward: StyledIcon<any>;
export declare const ArrowForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
