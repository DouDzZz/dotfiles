import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardArrowRight: StyledIcon<any>;
export declare const KeyboardArrowRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
