import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsBoat: StyledIcon<any>;
export declare const DirectionsBoatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
