import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatStrikethrough: StyledIcon<any>;
export declare const FormatStrikethroughDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
