import { StyledIcon, StyledIconProps } from '..';
export declare const InsertChart: StyledIcon<any>;
export declare const InsertChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
