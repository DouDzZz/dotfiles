import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Https: StyledIcon<any>;
export declare const HttpsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
