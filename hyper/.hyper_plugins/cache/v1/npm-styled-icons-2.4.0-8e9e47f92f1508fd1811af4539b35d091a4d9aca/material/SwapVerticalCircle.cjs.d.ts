import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SwapVerticalCircle: StyledIcon<any>;
export declare const SwapVerticalCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
