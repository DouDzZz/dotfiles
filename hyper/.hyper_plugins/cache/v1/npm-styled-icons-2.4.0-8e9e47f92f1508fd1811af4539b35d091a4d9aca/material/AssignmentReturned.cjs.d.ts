import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssignmentReturned: StyledIcon<any>;
export declare const AssignmentReturnedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
