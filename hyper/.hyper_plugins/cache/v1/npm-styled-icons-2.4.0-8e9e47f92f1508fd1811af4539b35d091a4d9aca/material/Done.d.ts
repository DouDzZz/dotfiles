import { StyledIcon, StyledIconProps } from '..';
export declare const Done: StyledIcon<any>;
export declare const DoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
