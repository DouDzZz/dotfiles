var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var BubbleChart = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "BubbleChart-title" }, props.title), React.createElement("circle", { cx: 7.2, cy: 14.4, r: 3.2, key: "k0" }),
            React.createElement("circle", { cx: 14.8, cy: 18, r: 2, key: "k1" }),
            React.createElement("circle", { cx: 15.2, cy: 8.8, r: 4.8, key: "k2" })
        ]
        : [React.createElement("circle", { cx: 7.2, cy: 14.4, r: 3.2, key: "k0" }),
            React.createElement("circle", { cx: 14.8, cy: 18, r: 2, key: "k1" }),
            React.createElement("circle", { cx: 15.2, cy: 8.8, r: 4.8, key: "k2" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
BubbleChart.displayName = 'BubbleChart';
export var BubbleChartDimensions = { height: 24, width: 24 };
var templateObject_1;
