import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Grade: StyledIcon<any>;
export declare const GradeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
