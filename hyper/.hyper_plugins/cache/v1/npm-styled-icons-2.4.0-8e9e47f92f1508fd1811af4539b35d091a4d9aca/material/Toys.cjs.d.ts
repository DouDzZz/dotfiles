import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Toys: StyledIcon<any>;
export declare const ToysDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
