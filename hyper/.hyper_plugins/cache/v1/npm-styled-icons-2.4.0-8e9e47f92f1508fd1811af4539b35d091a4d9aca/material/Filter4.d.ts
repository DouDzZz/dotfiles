import { StyledIcon, StyledIconProps } from '..';
export declare const Filter4: StyledIcon<any>;
export declare const Filter4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
