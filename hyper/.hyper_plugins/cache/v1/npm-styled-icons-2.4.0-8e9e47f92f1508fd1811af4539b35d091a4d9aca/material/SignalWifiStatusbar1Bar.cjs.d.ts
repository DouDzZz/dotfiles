import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbar1Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
