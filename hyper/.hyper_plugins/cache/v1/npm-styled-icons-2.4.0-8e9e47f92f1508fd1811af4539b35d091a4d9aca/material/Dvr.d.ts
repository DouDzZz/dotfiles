import { StyledIcon, StyledIconProps } from '..';
export declare const Dvr: StyledIcon<any>;
export declare const DvrDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
