import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Android: StyledIcon<any>;
export declare const AndroidDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
