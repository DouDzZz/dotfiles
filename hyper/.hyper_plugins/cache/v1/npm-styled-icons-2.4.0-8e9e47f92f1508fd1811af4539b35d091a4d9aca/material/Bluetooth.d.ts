import { StyledIcon, StyledIconProps } from '..';
export declare const Bluetooth: StyledIcon<any>;
export declare const BluetoothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
