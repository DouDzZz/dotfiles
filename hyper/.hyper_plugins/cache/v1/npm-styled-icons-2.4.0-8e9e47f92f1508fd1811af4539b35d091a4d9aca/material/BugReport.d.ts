import { StyledIcon, StyledIconProps } from '..';
export declare const BugReport: StyledIcon<any>;
export declare const BugReportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
