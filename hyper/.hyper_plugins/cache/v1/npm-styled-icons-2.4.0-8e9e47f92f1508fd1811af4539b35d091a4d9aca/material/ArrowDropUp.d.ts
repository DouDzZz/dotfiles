import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDropUp: StyledIcon<any>;
export declare const ArrowDropUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
