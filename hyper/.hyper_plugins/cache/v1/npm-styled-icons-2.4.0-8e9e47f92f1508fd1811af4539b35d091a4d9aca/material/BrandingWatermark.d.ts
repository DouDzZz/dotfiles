import { StyledIcon, StyledIconProps } from '..';
export declare const BrandingWatermark: StyledIcon<any>;
export declare const BrandingWatermarkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
