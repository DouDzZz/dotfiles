import { StyledIcon, StyledIconProps } from '..';
export declare const AvTimer: StyledIcon<any>;
export declare const AvTimerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
