import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subscriptions: StyledIcon<any>;
export declare const SubscriptionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
