import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StrikethroughS: StyledIcon<any>;
export declare const StrikethroughSDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
