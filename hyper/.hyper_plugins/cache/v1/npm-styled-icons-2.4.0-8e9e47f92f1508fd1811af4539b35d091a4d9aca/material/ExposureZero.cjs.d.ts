import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExposureZero: StyledIcon<any>;
export declare const ExposureZeroDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
