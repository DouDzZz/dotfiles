import { StyledIcon, StyledIconProps } from '..';
export declare const SpaceBar: StyledIcon<any>;
export declare const SpaceBarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
