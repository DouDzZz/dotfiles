import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneInTalk: StyledIcon<any>;
export declare const PhoneInTalkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
