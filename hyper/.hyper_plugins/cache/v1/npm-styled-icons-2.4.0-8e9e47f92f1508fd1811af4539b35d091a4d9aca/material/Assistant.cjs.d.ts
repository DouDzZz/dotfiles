import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Assistant: StyledIcon<any>;
export declare const AssistantDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
