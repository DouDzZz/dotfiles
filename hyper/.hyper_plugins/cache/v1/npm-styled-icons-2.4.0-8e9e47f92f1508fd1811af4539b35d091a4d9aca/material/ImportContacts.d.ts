import { StyledIcon, StyledIconProps } from '..';
export declare const ImportContacts: StyledIcon<any>;
export declare const ImportContactsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
