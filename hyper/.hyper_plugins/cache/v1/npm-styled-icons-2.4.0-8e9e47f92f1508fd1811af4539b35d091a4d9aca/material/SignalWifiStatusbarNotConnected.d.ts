import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarNotConnected: StyledIcon<any>;
export declare const SignalWifiStatusbarNotConnectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
