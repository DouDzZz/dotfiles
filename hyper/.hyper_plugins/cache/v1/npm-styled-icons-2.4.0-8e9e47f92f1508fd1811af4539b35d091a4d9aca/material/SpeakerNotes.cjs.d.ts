import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SpeakerNotes: StyledIcon<any>;
export declare const SpeakerNotesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
