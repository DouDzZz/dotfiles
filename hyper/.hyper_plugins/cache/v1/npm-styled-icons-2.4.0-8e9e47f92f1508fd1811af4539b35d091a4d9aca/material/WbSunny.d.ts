import { StyledIcon, StyledIconProps } from '..';
export declare const WbSunny: StyledIcon<any>;
export declare const WbSunnyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
