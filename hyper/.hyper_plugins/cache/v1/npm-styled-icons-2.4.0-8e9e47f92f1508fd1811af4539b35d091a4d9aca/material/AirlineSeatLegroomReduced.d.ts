import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatLegroomReduced: StyledIcon<any>;
export declare const AirlineSeatLegroomReducedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
