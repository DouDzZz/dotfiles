import { StyledIcon, StyledIconProps } from '..';
export declare const EventAvailable: StyledIcon<any>;
export declare const EventAvailableDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
