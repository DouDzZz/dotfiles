import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SimCard: StyledIcon<any>;
export declare const SimCardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
