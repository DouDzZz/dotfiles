import { StyledIcon, StyledIconProps } from '..';
export declare const LocalOffer: StyledIcon<any>;
export declare const LocalOfferDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
