import { StyledIcon, StyledIconProps } from '..';
export declare const BlurOn: StyledIcon<any>;
export declare const BlurOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
