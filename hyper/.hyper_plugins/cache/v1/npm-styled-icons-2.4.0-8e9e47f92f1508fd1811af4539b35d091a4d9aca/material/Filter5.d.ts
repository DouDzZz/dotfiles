import { StyledIcon, StyledIconProps } from '..';
export declare const Filter5: StyledIcon<any>;
export declare const Filter5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
