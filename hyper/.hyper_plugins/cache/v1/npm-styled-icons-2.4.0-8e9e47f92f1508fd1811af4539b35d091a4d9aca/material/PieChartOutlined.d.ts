import { StyledIcon, StyledIconProps } from '..';
export declare const PieChartOutlined: StyledIcon<any>;
export declare const PieChartOutlinedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
