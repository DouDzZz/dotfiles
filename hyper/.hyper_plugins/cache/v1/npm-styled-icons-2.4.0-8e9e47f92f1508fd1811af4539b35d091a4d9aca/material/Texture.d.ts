import { StyledIcon, StyledIconProps } from '..';
export declare const Texture: StyledIcon<any>;
export declare const TextureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
