import { StyledIcon, StyledIconProps } from '..';
export declare const FormatIndentDecrease: StyledIcon<any>;
export declare const FormatIndentDecreaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
