import { StyledIcon, StyledIconProps } from '..';
export declare const NoSim: StyledIcon<any>;
export declare const NoSimDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
