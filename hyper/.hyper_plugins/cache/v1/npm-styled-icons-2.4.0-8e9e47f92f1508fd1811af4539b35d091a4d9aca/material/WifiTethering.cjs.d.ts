import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WifiTethering: StyledIcon<any>;
export declare const WifiTetheringDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
