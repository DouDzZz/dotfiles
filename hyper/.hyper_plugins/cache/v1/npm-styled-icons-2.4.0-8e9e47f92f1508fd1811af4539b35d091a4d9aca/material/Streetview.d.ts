import { StyledIcon, StyledIconProps } from '..';
export declare const Streetview: StyledIcon<any>;
export declare const StreetviewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
