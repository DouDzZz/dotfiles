import { StyledIcon, StyledIconProps } from '..';
export declare const AddToPhotos: StyledIcon<any>;
export declare const AddToPhotosDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
