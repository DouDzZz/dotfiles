var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var SkipPrevious = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "SkipPrevious-title" }, props.title), React.createElement("path", { d: "M6 6h2v12H6zm3.5 6l8.5 6V6z", key: "k0" })
        ]
        : [React.createElement("path", { d: "M6 6h2v12H6zm3.5 6l8.5 6V6z", key: "k0" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
SkipPrevious.displayName = 'SkipPrevious';
export var SkipPreviousDimensions = { height: 24, width: 24 };
var templateObject_1;
