import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter2: StyledIcon<any>;
export declare const Filter2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
