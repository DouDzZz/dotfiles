import { StyledIcon, StyledIconProps } from '..';
export declare const FormatBold: StyledIcon<any>;
export declare const FormatBoldDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
