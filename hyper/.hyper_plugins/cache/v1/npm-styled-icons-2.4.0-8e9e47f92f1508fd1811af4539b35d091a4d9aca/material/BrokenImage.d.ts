import { StyledIcon, StyledIconProps } from '..';
export declare const BrokenImage: StyledIcon<any>;
export declare const BrokenImageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
