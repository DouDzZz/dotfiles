import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NetworkCell: StyledIcon<any>;
export declare const NetworkCellDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
