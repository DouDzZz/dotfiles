import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rotate90DegreesCcw: StyledIcon<any>;
export declare const Rotate90DegreesCcwDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
