import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SyncDisabled: StyledIcon<any>;
export declare const SyncDisabledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
