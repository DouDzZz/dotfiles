import { StyledIcon, StyledIconProps } from '..';
export declare const PauseCircleFilled: StyledIcon<any>;
export declare const PauseCircleFilledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
