import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhotoSizeSelectSmall: StyledIcon<any>;
export declare const PhotoSizeSelectSmallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
