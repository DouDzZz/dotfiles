import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsVoice: StyledIcon<any>;
export declare const SettingsVoiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
