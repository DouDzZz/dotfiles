import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Timer10: StyledIcon<any>;
export declare const Timer10Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
