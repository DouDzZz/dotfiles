import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SupervisorAccount: StyledIcon<any>;
export declare const SupervisorAccountDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
