import { StyledIcon, StyledIconProps } from '..';
export declare const LibraryBooks: StyledIcon<any>;
export declare const LibraryBooksDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
