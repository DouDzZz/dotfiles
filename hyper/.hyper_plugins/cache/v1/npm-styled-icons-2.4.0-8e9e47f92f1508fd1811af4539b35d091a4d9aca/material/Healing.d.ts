import { StyledIcon, StyledIconProps } from '..';
export declare const Healing: StyledIcon<any>;
export declare const HealingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
