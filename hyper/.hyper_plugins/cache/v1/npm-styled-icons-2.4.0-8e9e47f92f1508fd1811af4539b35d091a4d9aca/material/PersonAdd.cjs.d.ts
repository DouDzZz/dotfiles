import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PersonAdd: StyledIcon<any>;
export declare const PersonAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
