import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HdrOn: StyledIcon<any>;
export declare const HdrOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
