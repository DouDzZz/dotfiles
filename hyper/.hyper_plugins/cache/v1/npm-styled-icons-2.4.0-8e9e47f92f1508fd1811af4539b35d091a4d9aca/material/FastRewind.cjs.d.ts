import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FastRewind: StyledIcon<any>;
export declare const FastRewindDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
