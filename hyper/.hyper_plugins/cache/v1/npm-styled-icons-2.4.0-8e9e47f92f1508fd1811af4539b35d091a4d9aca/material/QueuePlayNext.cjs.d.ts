import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QueuePlayNext: StyledIcon<any>;
export declare const QueuePlayNextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
