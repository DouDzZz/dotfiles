import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewCarousel: StyledIcon<any>;
export declare const ViewCarouselDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
