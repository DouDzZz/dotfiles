import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flip: StyledIcon<any>;
export declare const FlipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
