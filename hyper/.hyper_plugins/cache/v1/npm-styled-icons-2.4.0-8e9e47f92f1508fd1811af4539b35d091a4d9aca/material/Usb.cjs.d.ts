import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Usb: StyledIcon<any>;
export declare const UsbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
