import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VerticalAlignTop: StyledIcon<any>;
export declare const VerticalAlignTopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
