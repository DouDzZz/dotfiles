import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TagFaces: StyledIcon<any>;
export declare const TagFacesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
