import { StyledIcon, StyledIconProps } from '..';
export declare const LocalMovies: StyledIcon<any>;
export declare const LocalMoviesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
