import { StyledIcon, StyledIconProps } from '..';
export declare const HdrOff: StyledIcon<any>;
export declare const HdrOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
