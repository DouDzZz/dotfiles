import { StyledIcon, StyledIconProps } from '..';
export declare const Hd: StyledIcon<any>;
export declare const HdDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
