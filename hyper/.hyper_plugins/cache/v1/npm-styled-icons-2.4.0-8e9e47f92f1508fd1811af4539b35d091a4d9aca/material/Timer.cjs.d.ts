import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Timer: StyledIcon<any>;
export declare const TimerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
