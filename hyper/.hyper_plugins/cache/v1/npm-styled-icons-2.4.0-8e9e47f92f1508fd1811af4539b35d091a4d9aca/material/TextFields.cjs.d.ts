import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TextFields: StyledIcon<any>;
export declare const TextFieldsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
