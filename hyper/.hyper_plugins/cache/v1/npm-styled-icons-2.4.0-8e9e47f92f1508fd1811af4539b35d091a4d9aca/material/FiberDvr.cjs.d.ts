import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiberDvr: StyledIcon<any>;
export declare const FiberDvrDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
