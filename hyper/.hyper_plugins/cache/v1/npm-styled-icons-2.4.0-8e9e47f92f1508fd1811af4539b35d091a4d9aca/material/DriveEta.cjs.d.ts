import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DriveEta: StyledIcon<any>;
export declare const DriveEtaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
