import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter4: StyledIcon<any>;
export declare const Filter4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
