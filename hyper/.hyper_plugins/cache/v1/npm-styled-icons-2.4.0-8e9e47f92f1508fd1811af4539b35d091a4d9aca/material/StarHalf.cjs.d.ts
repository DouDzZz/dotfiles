import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StarHalf: StyledIcon<any>;
export declare const StarHalfDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
