import { StyledIcon, StyledIconProps } from '..';
export declare const BrightnessAuto: StyledIcon<any>;
export declare const BrightnessAutoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
