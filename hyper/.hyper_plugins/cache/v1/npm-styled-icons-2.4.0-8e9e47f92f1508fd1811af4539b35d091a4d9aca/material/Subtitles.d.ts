import { StyledIcon, StyledIconProps } from '..';
export declare const Subtitles: StyledIcon<any>;
export declare const SubtitlesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
