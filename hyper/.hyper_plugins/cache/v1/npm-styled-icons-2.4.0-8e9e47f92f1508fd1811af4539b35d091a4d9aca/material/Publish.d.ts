import { StyledIcon, StyledIconProps } from '..';
export declare const Publish: StyledIcon<any>;
export declare const PublishDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
