import { StyledIcon, StyledIconProps } from '..';
export declare const VerifiedUser: StyledIcon<any>;
export declare const VerifiedUserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
