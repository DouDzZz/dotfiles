import { StyledIcon, StyledIconProps } from '..';
export declare const BorderOuter: StyledIcon<any>;
export declare const BorderOuterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
