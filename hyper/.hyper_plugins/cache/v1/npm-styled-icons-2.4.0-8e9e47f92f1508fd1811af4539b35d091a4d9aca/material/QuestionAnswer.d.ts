import { StyledIcon, StyledIconProps } from '..';
export declare const QuestionAnswer: StyledIcon<any>;
export declare const QuestionAnswerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
