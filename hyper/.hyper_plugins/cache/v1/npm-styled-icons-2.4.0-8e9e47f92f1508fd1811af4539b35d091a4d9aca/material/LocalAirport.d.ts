import { StyledIcon, StyledIconProps } from '..';
export declare const LocalAirport: StyledIcon<any>;
export declare const LocalAirportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
