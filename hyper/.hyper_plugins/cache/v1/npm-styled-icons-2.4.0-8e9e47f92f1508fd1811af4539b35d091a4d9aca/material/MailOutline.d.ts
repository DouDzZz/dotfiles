import { StyledIcon, StyledIconProps } from '..';
export declare const MailOutline: StyledIcon<any>;
export declare const MailOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
