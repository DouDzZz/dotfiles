import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Opacity: StyledIcon<any>;
export declare const OpacityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
