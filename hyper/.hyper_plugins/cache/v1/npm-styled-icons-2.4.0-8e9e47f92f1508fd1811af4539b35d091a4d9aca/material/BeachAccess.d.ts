import { StyledIcon, StyledIconProps } from '..';
export declare const BeachAccess: StyledIcon<any>;
export declare const BeachAccessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
