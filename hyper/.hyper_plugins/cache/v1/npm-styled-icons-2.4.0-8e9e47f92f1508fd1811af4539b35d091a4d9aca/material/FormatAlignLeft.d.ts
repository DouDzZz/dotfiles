import { StyledIcon, StyledIconProps } from '..';
export declare const FormatAlignLeft: StyledIcon<any>;
export declare const FormatAlignLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
