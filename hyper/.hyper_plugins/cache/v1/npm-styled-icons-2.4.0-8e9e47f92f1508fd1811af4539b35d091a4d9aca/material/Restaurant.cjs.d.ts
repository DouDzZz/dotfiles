import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Restaurant: StyledIcon<any>;
export declare const RestaurantDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
