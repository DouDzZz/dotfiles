import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TrendingFlat: StyledIcon<any>;
export declare const TrendingFlatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
