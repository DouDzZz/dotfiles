import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardTab: StyledIcon<any>;
export declare const KeyboardTabDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
