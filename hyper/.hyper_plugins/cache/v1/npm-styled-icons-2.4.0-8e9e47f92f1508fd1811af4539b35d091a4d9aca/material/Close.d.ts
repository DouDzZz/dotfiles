import { StyledIcon, StyledIconProps } from '..';
export declare const Close: StyledIcon<any>;
export declare const CloseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
