import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellular2Bar: StyledIcon<any>;
export declare const SignalCellular2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
