import { StyledIcon, StyledIconProps } from '..';
export declare const LocationCity: StyledIcon<any>;
export declare const LocationCityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
