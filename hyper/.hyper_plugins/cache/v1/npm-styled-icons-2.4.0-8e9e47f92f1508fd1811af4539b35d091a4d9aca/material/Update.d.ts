import { StyledIcon, StyledIconProps } from '..';
export declare const Update: StyledIcon<any>;
export declare const UpdateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
