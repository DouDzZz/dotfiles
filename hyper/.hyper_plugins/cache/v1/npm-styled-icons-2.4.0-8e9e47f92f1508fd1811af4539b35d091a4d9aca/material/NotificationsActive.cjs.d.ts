import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotificationsActive: StyledIcon<any>;
export declare const NotificationsActiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
