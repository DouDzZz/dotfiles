import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DesktopWindows: StyledIcon<any>;
export declare const DesktopWindowsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
