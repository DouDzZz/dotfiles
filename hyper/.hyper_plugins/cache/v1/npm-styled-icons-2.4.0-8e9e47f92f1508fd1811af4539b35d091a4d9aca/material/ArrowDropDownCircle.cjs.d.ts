import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDropDownCircle: StyledIcon<any>;
export declare const ArrowDropDownCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
