import { StyledIcon, StyledIconProps } from '..';
export declare const Router: StyledIcon<any>;
export declare const RouterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
