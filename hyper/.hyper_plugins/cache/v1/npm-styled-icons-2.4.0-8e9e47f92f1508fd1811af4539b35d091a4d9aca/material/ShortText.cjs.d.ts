import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShortText: StyledIcon<any>;
export declare const ShortTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
