import { StyledIcon, StyledIconProps } from '..';
export declare const Refresh: StyledIcon<any>;
export declare const RefreshDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
