import { StyledIcon, StyledIconProps } from '..';
export declare const Map: StyledIcon<any>;
export declare const MapDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
