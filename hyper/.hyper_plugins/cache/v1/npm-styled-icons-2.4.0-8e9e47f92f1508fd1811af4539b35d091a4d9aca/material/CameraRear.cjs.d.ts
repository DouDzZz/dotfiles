import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CameraRear: StyledIcon<any>;
export declare const CameraRearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
