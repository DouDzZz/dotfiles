import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery20: StyledIcon<any>;
export declare const Battery20Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
