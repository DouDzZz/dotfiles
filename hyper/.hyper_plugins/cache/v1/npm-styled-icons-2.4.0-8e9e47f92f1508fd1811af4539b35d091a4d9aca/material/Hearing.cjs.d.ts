import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hearing: StyledIcon<any>;
export declare const HearingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
