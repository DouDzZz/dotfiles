import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalHotel: StyledIcon<any>;
export declare const LocalHotelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
