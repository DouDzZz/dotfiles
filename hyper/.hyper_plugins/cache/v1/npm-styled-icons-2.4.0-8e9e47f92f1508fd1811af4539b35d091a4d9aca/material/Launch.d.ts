import { StyledIcon, StyledIconProps } from '..';
export declare const Launch: StyledIcon<any>;
export declare const LaunchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
