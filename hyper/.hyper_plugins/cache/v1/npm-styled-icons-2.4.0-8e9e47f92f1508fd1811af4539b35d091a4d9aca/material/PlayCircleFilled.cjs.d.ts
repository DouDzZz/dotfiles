import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayCircleFilled: StyledIcon<any>;
export declare const PlayCircleFilledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
