import { StyledIcon, StyledIconProps } from '..';
export declare const RemoveRedEye: StyledIcon<any>;
export declare const RemoveRedEyeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
