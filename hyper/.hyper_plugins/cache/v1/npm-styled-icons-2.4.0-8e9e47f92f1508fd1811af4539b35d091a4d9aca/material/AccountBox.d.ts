import { StyledIcon, StyledIconProps } from '..';
export declare const AccountBox: StyledIcon<any>;
export declare const AccountBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
