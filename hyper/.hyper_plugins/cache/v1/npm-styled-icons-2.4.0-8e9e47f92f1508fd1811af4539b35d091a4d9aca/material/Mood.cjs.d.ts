import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mood: StyledIcon<any>;
export declare const MoodDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
