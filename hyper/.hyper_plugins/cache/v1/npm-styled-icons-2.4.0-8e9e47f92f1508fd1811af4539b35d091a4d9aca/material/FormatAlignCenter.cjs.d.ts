import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatAlignCenter: StyledIcon<any>;
export declare const FormatAlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
