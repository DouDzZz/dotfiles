import { StyledIcon, StyledIconProps } from '..';
export declare const Fingerprint: StyledIcon<any>;
export declare const FingerprintDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
