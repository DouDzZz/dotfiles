import { StyledIcon, StyledIconProps } from '..';
export declare const Accessibility: StyledIcon<any>;
export declare const AccessibilityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
