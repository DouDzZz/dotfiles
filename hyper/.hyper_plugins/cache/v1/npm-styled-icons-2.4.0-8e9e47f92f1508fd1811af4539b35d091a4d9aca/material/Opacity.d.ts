import { StyledIcon, StyledIconProps } from '..';
export declare const Opacity: StyledIcon<any>;
export declare const OpacityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
