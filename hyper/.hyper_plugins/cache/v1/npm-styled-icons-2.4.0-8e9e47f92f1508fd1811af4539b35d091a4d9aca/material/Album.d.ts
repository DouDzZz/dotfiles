import { StyledIcon, StyledIconProps } from '..';
export declare const Album: StyledIcon<any>;
export declare const AlbumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
