import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QuestionAnswer: StyledIcon<any>;
export declare const QuestionAnswerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
