import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery80: StyledIcon<any>;
export declare const Battery80Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
