import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudCircle: StyledIcon<any>;
export declare const CloudCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
