import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Announcement: StyledIcon<any>;
export declare const AnnouncementDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
