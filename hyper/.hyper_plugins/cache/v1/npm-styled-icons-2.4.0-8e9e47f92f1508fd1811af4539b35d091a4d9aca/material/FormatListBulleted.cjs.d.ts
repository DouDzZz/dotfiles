import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatListBulleted: StyledIcon<any>;
export declare const FormatListBulletedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
