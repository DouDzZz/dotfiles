import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CenterFocusStrong: StyledIcon<any>;
export declare const CenterFocusStrongDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
