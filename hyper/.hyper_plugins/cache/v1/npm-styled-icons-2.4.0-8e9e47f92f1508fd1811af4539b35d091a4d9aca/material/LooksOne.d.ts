import { StyledIcon, StyledIconProps } from '..';
export declare const LooksOne: StyledIcon<any>;
export declare const LooksOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
