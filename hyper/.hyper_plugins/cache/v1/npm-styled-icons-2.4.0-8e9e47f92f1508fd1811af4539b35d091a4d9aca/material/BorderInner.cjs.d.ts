import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderInner: StyledIcon<any>;
export declare const BorderInnerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
