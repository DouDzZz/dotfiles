import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LaptopMac: StyledIcon<any>;
export declare const LaptopMacDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
