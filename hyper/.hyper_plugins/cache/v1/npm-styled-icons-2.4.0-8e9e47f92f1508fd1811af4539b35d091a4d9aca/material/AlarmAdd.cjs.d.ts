import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlarmAdd: StyledIcon<any>;
export declare const AlarmAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
