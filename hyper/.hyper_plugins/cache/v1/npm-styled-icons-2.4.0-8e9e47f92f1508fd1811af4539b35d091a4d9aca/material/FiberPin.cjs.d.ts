import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FiberPin: StyledIcon<any>;
export declare const FiberPinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
