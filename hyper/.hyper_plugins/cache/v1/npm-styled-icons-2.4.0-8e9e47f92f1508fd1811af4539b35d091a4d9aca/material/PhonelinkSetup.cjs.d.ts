import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhonelinkSetup: StyledIcon<any>;
export declare const PhonelinkSetupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
