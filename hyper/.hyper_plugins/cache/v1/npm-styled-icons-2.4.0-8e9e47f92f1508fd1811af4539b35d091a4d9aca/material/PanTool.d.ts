import { StyledIcon, StyledIconProps } from '..';
export declare const PanTool: StyledIcon<any>;
export declare const PanToolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
