import { StyledIcon, StyledIconProps } from '..';
export declare const VideoCall: StyledIcon<any>;
export declare const VideoCallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
