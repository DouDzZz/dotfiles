import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsBackupRestore: StyledIcon<any>;
export declare const SettingsBackupRestoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
