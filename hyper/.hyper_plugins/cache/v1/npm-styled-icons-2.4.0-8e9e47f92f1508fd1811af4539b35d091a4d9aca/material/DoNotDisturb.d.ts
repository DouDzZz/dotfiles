import { StyledIcon, StyledIconProps } from '..';
export declare const DoNotDisturb: StyledIcon<any>;
export declare const DoNotDisturbDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
