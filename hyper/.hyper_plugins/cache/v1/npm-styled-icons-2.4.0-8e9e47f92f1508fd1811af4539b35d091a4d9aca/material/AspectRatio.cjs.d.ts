import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AspectRatio: StyledIcon<any>;
export declare const AspectRatioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
