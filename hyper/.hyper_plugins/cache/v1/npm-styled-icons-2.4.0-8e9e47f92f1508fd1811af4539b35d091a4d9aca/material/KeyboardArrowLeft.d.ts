import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardArrowLeft: StyledIcon<any>;
export declare const KeyboardArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
