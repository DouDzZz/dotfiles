import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlaylistAdd: StyledIcon<any>;
export declare const PlaylistAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
