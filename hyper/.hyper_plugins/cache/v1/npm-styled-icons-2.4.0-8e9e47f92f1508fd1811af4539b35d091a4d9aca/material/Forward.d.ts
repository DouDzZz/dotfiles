import { StyledIcon, StyledIconProps } from '..';
export declare const Forward: StyledIcon<any>;
export declare const ForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
