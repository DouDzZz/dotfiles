import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PersonPinCircle: StyledIcon<any>;
export declare const PersonPinCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
