import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SentimentDissatisfied: StyledIcon<any>;
export declare const SentimentDissatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
