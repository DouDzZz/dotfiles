import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WifiLock: StyledIcon<any>;
export declare const WifiLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
