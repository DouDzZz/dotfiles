import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatItalic: StyledIcon<any>;
export declare const FormatItalicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
