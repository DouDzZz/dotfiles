import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gesture: StyledIcon<any>;
export declare const GestureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
