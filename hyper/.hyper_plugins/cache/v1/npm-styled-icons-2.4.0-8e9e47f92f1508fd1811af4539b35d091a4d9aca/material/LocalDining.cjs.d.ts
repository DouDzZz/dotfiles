import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalDining: StyledIcon<any>;
export declare const LocalDiningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
