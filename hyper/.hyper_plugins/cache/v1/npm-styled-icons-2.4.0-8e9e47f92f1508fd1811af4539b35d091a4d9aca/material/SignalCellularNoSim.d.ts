import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularNoSim: StyledIcon<any>;
export declare const SignalCellularNoSimDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
