import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneLocked: StyledIcon<any>;
export declare const PhoneLockedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
