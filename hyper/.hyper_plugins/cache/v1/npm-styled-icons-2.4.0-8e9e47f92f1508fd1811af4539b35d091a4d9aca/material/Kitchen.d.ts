import { StyledIcon, StyledIconProps } from '..';
export declare const Kitchen: StyledIcon<any>;
export declare const KitchenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
