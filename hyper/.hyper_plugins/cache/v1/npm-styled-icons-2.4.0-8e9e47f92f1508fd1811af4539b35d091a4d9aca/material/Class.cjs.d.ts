import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Class: StyledIcon<any>;
export declare const ClassDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
