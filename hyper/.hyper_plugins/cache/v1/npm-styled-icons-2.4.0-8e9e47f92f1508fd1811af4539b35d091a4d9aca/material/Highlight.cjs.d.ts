import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Highlight: StyledIcon<any>;
export declare const HighlightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
