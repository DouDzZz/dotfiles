import { StyledIcon, StyledIconProps } from '..';
export declare const DriveEta: StyledIcon<any>;
export declare const DriveEtaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
