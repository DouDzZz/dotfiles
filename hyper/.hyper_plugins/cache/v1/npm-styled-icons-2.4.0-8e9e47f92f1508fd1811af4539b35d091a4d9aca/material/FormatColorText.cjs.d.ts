import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatColorText: StyledIcon<any>;
export declare const FormatColorTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
