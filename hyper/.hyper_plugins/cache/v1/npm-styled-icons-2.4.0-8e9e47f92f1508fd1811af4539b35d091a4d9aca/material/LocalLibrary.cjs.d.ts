import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalLibrary: StyledIcon<any>;
export declare const LocalLibraryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
