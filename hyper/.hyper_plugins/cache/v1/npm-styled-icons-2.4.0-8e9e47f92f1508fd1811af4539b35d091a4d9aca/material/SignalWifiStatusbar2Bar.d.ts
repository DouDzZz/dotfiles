import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbar2Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
