import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbar1Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
