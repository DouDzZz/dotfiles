import { StyledIcon, StyledIconProps } from '..';
export declare const BubbleChart: StyledIcon<any>;
export declare const BubbleChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
