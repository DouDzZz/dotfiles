import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FindInPage: StyledIcon<any>;
export declare const FindInPageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
