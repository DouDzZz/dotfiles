var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var NearMe = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "NearMe-title" }, props.title), React.createElement("path", { d: "M21 3L3 10.53v.98l6.84 2.65L12.48 21h.98L21 3z", key: "k0" })
        ]
        : [React.createElement("path", { d: "M21 3L3 10.53v.98l6.84 2.65L12.48 21h.98L21 3z", key: "k0" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
NearMe.displayName = 'NearMe';
export var NearMeDimensions = { height: 24, width: 24 };
var templateObject_1;
