import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi0Bar: StyledIcon<any>;
export declare const SignalWifi0BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
