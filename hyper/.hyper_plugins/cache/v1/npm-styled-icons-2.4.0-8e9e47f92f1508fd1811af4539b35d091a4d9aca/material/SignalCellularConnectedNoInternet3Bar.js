var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var SignalCellularConnectedNoInternet3Bar = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "SignalCellularConnectedNoInternet3Bar-title" }, props.title), React.createElement("path", { fillOpacity: .3, d: "M22 8V2L2 22h16V8z", key: "k0" }),
            React.createElement("path", { d: "M17 22V7L2 22h15zm3-12v8h2v-8h-2zm0 12h2v-2h-2v2z", key: "k1" })
        ]
        : [React.createElement("path", { fillOpacity: .3, d: "M22 8V2L2 22h16V8z", key: "k0" }),
            React.createElement("path", { d: "M17 22V7L2 22h15zm3-12v8h2v-8h-2zm0 12h2v-2h-2v2z", key: "k1" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
SignalCellularConnectedNoInternet3Bar.displayName = 'SignalCellularConnectedNoInternet3Bar';
export var SignalCellularConnectedNoInternet3BarDimensions = { height: 24, width: 24 };
var templateObject_1;
