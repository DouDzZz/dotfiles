import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalFlorist: StyledIcon<any>;
export declare const LocalFloristDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
