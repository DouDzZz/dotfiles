import { StyledIcon, StyledIconProps } from '..';
export declare const Gesture: StyledIcon<any>;
export declare const GestureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
