import { StyledIcon, StyledIconProps } from '..';
export declare const Shop: StyledIcon<any>;
export declare const ShopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
