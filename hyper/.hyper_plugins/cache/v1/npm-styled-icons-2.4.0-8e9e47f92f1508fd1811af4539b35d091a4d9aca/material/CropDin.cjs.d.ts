import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropDin: StyledIcon<any>;
export declare const CropDinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
