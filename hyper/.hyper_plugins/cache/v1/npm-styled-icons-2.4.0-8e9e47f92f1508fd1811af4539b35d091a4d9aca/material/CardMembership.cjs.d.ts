import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CardMembership: StyledIcon<any>;
export declare const CardMembershipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
