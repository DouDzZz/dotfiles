import { StyledIcon, StyledIconProps } from '..';
export declare const Favorite: StyledIcon<any>;
export declare const FavoriteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
