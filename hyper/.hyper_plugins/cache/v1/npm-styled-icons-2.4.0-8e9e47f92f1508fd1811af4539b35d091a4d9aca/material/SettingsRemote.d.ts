import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsRemote: StyledIcon<any>;
export declare const SettingsRemoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
