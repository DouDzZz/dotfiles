import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Replay5: StyledIcon<any>;
export declare const Replay5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
