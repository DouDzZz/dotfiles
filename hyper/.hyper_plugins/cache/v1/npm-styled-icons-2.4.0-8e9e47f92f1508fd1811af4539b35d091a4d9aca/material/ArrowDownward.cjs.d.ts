import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDownward: StyledIcon<any>;
export declare const ArrowDownwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
