import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Album: StyledIcon<any>;
export declare const AlbumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
