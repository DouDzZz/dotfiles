import { StyledIcon, StyledIconProps } from '..';
export declare const HelpOutline: StyledIcon<any>;
export declare const HelpOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
