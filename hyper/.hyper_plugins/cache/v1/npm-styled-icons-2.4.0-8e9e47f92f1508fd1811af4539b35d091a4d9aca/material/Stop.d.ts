import { StyledIcon, StyledIconProps } from '..';
export declare const Stop: StyledIcon<any>;
export declare const StopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
