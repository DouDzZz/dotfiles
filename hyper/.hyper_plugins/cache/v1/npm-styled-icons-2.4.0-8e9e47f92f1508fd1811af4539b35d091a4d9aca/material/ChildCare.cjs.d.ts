import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChildCare: StyledIcon<any>;
export declare const ChildCareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
