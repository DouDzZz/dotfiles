import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardCapslock: StyledIcon<any>;
export declare const KeyboardCapslockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
