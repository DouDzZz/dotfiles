import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalGroceryStore: StyledIcon<any>;
export declare const LocalGroceryStoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
