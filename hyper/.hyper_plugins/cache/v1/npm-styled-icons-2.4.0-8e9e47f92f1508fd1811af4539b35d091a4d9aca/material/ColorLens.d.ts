import { StyledIcon, StyledIconProps } from '..';
export declare const ColorLens: StyledIcon<any>;
export declare const ColorLensDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
