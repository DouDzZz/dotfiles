import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi3Bar: StyledIcon<any>;
export declare const SignalWifi3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
