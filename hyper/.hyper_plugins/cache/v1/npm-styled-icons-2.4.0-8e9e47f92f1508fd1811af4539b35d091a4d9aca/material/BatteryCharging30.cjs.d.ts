import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryCharging30: StyledIcon<any>;
export declare const BatteryCharging30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
