import { StyledIcon, StyledIconProps } from '..';
export declare const MovieFilter: StyledIcon<any>;
export declare const MovieFilterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
