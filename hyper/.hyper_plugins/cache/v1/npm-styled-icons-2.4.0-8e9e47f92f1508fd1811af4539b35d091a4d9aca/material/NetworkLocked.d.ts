import { StyledIcon, StyledIconProps } from '..';
export declare const NetworkLocked: StyledIcon<any>;
export declare const NetworkLockedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
