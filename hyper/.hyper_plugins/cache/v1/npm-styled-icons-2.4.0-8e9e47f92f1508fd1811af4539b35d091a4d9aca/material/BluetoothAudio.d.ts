import { StyledIcon, StyledIconProps } from '..';
export declare const BluetoothAudio: StyledIcon<any>;
export declare const BluetoothAudioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
