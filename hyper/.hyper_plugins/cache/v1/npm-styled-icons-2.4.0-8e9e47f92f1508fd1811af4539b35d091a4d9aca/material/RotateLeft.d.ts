import { StyledIcon, StyledIconProps } from '..';
export declare const RotateLeft: StyledIcon<any>;
export declare const RotateLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
