import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifi3Bar: StyledIcon<any>;
export declare const SignalWifi3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
