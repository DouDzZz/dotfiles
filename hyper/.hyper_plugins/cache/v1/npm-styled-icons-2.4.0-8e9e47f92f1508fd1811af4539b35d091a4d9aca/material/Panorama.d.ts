import { StyledIcon, StyledIconProps } from '..';
export declare const Panorama: StyledIcon<any>;
export declare const PanoramaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
