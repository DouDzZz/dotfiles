import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Place: StyledIcon<any>;
export declare const PlaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
