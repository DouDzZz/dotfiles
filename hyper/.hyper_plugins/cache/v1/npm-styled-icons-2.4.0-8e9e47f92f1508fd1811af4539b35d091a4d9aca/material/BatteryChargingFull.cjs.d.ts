import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryChargingFull: StyledIcon<any>;
export declare const BatteryChargingFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
