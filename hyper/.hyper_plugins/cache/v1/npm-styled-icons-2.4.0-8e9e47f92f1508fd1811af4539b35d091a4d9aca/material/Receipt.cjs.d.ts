import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Receipt: StyledIcon<any>;
export declare const ReceiptDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
