import { StyledIcon, StyledIconProps } from '..';
export declare const PersonOutline: StyledIcon<any>;
export declare const PersonOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
