import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Warning: StyledIcon<any>;
export declare const WarningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
