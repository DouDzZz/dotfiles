import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckBox: StyledIcon<any>;
export declare const CheckBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
