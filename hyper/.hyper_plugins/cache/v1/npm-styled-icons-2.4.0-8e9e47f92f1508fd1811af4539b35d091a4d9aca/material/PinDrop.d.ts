import { StyledIcon, StyledIconProps } from '..';
export declare const PinDrop: StyledIcon<any>;
export declare const PinDropDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
