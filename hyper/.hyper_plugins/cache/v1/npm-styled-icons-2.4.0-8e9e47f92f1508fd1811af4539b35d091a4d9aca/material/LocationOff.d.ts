import { StyledIcon, StyledIconProps } from '..';
export declare const LocationOff: StyledIcon<any>;
export declare const LocationOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
