import { StyledIcon, StyledIconProps } from '..';
export declare const PlayCircleOutline: StyledIcon<any>;
export declare const PlayCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
