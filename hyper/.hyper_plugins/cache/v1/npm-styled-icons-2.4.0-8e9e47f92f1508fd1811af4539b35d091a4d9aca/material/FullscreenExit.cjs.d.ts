import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FullscreenExit: StyledIcon<any>;
export declare const FullscreenExitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
