import { StyledIcon, StyledIconProps } from '..';
export declare const PhonelinkLock: StyledIcon<any>;
export declare const PhonelinkLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
