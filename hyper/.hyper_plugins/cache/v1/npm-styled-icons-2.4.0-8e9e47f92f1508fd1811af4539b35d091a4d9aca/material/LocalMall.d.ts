import { StyledIcon, StyledIconProps } from '..';
export declare const LocalMall: StyledIcon<any>;
export declare const LocalMallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
