import { StyledIcon, StyledIconProps } from '..';
export declare const Attachment: StyledIcon<any>;
export declare const AttachmentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
