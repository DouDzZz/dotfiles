import { StyledIcon, StyledIconProps } from '..';
export declare const FiberDvr: StyledIcon<any>;
export declare const FiberDvrDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
