import { StyledIcon, StyledIconProps } from '..';
export declare const PhonelinkOff: StyledIcon<any>;
export declare const PhonelinkOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
