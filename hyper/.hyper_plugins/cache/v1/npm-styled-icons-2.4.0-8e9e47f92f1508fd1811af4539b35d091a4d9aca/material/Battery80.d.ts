import { StyledIcon, StyledIconProps } from '..';
export declare const Battery80: StyledIcon<any>;
export declare const Battery80Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
