import { StyledIcon, StyledIconProps } from '..';
export declare const Straighten: StyledIcon<any>;
export declare const StraightenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
