import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayCircleOutline: StyledIcon<any>;
export declare const PlayCircleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
