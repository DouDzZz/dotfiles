import { StyledIcon, StyledIconProps } from '..';
export declare const SelectAll: StyledIcon<any>;
export declare const SelectAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
