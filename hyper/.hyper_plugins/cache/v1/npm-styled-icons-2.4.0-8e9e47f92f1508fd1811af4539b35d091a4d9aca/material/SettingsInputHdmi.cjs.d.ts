import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsInputHdmi: StyledIcon<any>;
export declare const SettingsInputHdmiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
