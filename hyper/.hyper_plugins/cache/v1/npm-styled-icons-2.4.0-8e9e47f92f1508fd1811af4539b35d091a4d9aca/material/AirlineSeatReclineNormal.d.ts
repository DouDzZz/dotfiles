import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatReclineNormal: StyledIcon<any>;
export declare const AirlineSeatReclineNormalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
