import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ControlPointDuplicate: StyledIcon<any>;
export declare const ControlPointDuplicateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
