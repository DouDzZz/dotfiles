import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RadioButtonUnchecked: StyledIcon<any>;
export declare const RadioButtonUncheckedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
