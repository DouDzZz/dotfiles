import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryAlert: StyledIcon<any>;
export declare const BatteryAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
