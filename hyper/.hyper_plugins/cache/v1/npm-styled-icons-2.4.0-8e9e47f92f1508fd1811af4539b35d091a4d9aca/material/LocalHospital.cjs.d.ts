import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalHospital: StyledIcon<any>;
export declare const LocalHospitalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
