import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Restore: StyledIcon<any>;
export declare const RestoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
