import { StyledIcon, StyledIconProps } from '..';
export declare const ThreeDRotation: StyledIcon<any>;
export declare const ThreeDRotationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
