import { StyledIcon, StyledIconProps } from '..';
export declare const History: StyledIcon<any>;
export declare const HistoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
