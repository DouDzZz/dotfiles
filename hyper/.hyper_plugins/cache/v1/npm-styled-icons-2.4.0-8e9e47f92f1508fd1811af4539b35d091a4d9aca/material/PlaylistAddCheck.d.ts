import { StyledIcon, StyledIconProps } from '..';
export declare const PlaylistAddCheck: StyledIcon<any>;
export declare const PlaylistAddCheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
