import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayArrow: StyledIcon<any>;
export declare const PlayArrowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
