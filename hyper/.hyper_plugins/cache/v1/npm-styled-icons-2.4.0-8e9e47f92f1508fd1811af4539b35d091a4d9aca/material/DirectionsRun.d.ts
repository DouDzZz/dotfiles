import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsRun: StyledIcon<any>;
export declare const DirectionsRunDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
