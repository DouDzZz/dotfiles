import { StyledIcon, StyledIconProps } from '..';
export declare const RecordVoiceOver: StyledIcon<any>;
export declare const RecordVoiceOverDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
