import { StyledIcon, StyledIconProps } from '..';
export declare const AllOut: StyledIcon<any>;
export declare const AllOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
