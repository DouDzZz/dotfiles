import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Devices: StyledIcon<any>;
export declare const DevicesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
