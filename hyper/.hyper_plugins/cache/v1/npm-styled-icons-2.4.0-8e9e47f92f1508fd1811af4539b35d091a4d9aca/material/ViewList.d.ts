import { StyledIcon, StyledIconProps } from '..';
export declare const ViewList: StyledIcon<any>;
export declare const ViewListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
