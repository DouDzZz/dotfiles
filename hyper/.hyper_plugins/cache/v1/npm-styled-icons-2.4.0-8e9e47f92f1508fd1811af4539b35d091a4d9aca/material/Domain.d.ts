import { StyledIcon, StyledIconProps } from '..';
export declare const Domain: StyledIcon<any>;
export declare const DomainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
