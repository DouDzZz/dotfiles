import { StyledIcon, StyledIconProps } from '..';
export declare const InsertComment: StyledIcon<any>;
export declare const InsertCommentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
