import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QueryBuilder: StyledIcon<any>;
export declare const QueryBuilderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
