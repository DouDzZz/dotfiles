import { StyledIcon, StyledIconProps } from '..';
export declare const Hearing: StyledIcon<any>;
export declare const HearingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
