import { StyledIcon, StyledIconProps } from '..';
export declare const Gamepad: StyledIcon<any>;
export declare const GamepadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
