import { StyledIcon, StyledIconProps } from '..';
export declare const Filter6: StyledIcon<any>;
export declare const Filter6Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
