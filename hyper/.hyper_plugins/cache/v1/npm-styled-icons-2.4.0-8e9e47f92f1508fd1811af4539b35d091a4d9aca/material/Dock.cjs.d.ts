import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dock: StyledIcon<any>;
export declare const DockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
