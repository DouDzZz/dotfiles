import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TimerOff: StyledIcon<any>;
export declare const TimerOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
