import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SmokingRooms: StyledIcon<any>;
export declare const SmokingRoomsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
