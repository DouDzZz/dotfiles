import { StyledIcon, StyledIconProps } from '..';
export declare const TrendingFlat: StyledIcon<any>;
export declare const TrendingFlatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
