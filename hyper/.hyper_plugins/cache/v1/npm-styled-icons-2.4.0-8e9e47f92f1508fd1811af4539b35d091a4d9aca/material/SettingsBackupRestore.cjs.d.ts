import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsBackupRestore: StyledIcon<any>;
export declare const SettingsBackupRestoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
