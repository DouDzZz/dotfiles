import { StyledIcon, StyledIconProps } from '..';
export declare const PersonAdd: StyledIcon<any>;
export declare const PersonAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
