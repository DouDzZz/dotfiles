import { StyledIcon, StyledIconProps } from '..';
export declare const FlipToFront: StyledIcon<any>;
export declare const FlipToFrontDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
