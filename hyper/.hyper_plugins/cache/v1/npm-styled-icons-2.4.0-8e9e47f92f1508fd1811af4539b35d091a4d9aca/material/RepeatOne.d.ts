import { StyledIcon, StyledIconProps } from '..';
export declare const RepeatOne: StyledIcon<any>;
export declare const RepeatOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
