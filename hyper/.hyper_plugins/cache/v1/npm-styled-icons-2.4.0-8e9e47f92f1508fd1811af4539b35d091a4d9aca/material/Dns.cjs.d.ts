import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dns: StyledIcon<any>;
export declare const DnsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
