import { StyledIcon, StyledIconProps } from '..';
export declare const ControlPoint: StyledIcon<any>;
export declare const ControlPointDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
