import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShoppingBasket: StyledIcon<any>;
export declare const ShoppingBasketDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
