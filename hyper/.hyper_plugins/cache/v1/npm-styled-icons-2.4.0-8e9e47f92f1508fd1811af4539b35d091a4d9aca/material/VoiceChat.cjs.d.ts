import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VoiceChat: StyledIcon<any>;
export declare const VoiceChatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
