import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Looks6: StyledIcon<any>;
export declare const Looks6Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
