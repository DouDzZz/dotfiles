import { StyledIcon, StyledIconProps } from '..';
export declare const LocalShipping: StyledIcon<any>;
export declare const LocalShippingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
