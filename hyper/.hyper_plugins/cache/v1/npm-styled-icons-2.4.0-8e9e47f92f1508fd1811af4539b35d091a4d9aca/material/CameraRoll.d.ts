import { StyledIcon, StyledIconProps } from '..';
export declare const CameraRoll: StyledIcon<any>;
export declare const CameraRollDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
