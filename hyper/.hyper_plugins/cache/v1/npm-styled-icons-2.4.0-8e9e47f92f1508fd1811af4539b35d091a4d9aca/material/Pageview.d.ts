import { StyledIcon, StyledIconProps } from '..';
export declare const Pageview: StyledIcon<any>;
export declare const PageviewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
