import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Power: StyledIcon<any>;
export declare const PowerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
