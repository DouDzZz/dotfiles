import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GolfCourse: StyledIcon<any>;
export declare const GolfCourseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
