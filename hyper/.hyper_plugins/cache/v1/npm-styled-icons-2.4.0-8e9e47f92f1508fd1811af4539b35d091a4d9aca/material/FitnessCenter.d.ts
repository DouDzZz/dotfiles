import { StyledIcon, StyledIconProps } from '..';
export declare const FitnessCenter: StyledIcon<any>;
export declare const FitnessCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
