import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DoNotDisturbOn: StyledIcon<any>;
export declare const DoNotDisturbOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
