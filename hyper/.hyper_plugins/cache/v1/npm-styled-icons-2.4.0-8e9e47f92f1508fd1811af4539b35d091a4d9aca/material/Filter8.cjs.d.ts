import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter8: StyledIcon<any>;
export declare const Filter8Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
