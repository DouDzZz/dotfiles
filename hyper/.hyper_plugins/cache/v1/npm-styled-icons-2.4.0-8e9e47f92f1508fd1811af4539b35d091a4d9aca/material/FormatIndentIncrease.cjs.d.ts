import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatIndentIncrease: StyledIcon<any>;
export declare const FormatIndentIncreaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
