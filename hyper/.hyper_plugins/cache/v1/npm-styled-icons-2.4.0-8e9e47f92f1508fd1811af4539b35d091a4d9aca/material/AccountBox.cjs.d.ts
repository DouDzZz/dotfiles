import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AccountBox: StyledIcon<any>;
export declare const AccountBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
