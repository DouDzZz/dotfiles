import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowBack: StyledIcon<any>;
export declare const ArrowBackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
