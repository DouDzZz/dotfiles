import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideoLibrary: StyledIcon<any>;
export declare const VideoLibraryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
