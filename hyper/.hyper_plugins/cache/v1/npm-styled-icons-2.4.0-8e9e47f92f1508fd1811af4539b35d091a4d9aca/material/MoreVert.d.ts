import { StyledIcon, StyledIconProps } from '..';
export declare const MoreVert: StyledIcon<any>;
export declare const MoreVertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
