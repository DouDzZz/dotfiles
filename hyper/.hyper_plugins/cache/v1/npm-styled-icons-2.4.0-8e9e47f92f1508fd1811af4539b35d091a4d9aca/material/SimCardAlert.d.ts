import { StyledIcon, StyledIconProps } from '..';
export declare const SimCardAlert: StyledIcon<any>;
export declare const SimCardAlertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
