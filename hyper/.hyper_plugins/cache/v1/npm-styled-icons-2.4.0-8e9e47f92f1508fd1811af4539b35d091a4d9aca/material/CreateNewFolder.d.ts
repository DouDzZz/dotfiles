import { StyledIcon, StyledIconProps } from '..';
export declare const CreateNewFolder: StyledIcon<any>;
export declare const CreateNewFolderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
