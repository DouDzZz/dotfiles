import { StyledIcon, StyledIconProps } from '..';
export declare const FormatAlignJustify: StyledIcon<any>;
export declare const FormatAlignJustifyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
