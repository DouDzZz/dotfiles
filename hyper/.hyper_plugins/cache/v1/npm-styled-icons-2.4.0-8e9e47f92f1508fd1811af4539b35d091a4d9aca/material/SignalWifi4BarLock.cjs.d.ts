import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi4BarLock: StyledIcon<any>;
export declare const SignalWifi4BarLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
