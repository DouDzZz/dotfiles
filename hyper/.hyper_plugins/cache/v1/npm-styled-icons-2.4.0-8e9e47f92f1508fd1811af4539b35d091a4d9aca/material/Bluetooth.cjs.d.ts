import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bluetooth: StyledIcon<any>;
export declare const BluetoothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
