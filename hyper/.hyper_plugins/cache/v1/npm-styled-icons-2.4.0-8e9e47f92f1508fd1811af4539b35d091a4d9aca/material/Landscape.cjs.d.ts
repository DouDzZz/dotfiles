import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Landscape: StyledIcon<any>;
export declare const LandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
