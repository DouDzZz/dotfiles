import { StyledIcon, StyledIconProps } from '..';
export declare const NotificationsPaused: StyledIcon<any>;
export declare const NotificationsPausedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
