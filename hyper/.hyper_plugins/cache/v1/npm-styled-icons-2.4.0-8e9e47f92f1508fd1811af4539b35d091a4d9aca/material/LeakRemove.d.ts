import { StyledIcon, StyledIconProps } from '..';
export declare const LeakRemove: StyledIcon<any>;
export declare const LeakRemoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
