import { StyledIcon, StyledIconProps } from '..';
export declare const ImportExport: StyledIcon<any>;
export declare const ImportExportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
