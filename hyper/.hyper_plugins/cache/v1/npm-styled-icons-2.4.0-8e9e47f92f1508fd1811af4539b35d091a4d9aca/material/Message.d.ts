import { StyledIcon, StyledIconProps } from '..';
export declare const Message: StyledIcon<any>;
export declare const MessageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
