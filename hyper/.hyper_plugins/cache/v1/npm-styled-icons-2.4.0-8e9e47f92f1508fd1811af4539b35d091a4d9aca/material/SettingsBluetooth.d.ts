import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsBluetooth: StyledIcon<any>;
export declare const SettingsBluetoothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
