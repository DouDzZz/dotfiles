import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatQuote: StyledIcon<any>;
export declare const FormatQuoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
