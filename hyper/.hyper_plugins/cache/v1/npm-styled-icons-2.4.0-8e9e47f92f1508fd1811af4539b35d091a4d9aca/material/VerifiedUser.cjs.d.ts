import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VerifiedUser: StyledIcon<any>;
export declare const VerifiedUserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
