import { StyledIcon, StyledIconProps } from '..';
export declare const LeakAdd: StyledIcon<any>;
export declare const LeakAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
