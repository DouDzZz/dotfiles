import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryStd: StyledIcon<any>;
export declare const BatteryStdDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
