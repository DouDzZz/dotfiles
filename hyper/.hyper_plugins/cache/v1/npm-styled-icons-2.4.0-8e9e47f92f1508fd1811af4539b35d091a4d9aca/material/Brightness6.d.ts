import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness6: StyledIcon<any>;
export declare const Brightness6Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
