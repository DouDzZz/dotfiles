import { StyledIcon, StyledIconProps } from '..';
export declare const Person: StyledIcon<any>;
export declare const PersonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
