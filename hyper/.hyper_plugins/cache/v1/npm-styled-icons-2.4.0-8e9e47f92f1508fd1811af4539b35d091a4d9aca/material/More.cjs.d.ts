import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const More: StyledIcon<any>;
export declare const MoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
