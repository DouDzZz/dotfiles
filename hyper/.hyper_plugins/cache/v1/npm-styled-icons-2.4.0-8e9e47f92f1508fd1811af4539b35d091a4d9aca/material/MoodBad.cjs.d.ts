import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoodBad: StyledIcon<any>;
export declare const MoodBadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
