import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalConvenienceStore: StyledIcon<any>;
export declare const LocalConvenienceStoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
