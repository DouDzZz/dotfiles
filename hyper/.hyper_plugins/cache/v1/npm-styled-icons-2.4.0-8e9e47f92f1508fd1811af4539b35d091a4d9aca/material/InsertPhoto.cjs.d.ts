import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertPhoto: StyledIcon<any>;
export declare const InsertPhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
