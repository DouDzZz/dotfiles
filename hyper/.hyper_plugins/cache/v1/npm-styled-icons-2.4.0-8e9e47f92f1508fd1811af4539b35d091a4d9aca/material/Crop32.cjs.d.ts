import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crop32: StyledIcon<any>;
export declare const Crop32Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
