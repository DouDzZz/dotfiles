import { StyledIcon, StyledIconProps } from '..';
export declare const Toys: StyledIcon<any>;
export declare const ToysDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
