import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LockOpen: StyledIcon<any>;
export declare const LockOpenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
