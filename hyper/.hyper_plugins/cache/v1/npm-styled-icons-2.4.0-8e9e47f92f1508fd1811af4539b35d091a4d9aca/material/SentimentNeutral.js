var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var SentimentNeutral = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "SentimentNeutral-title" }, props.title), React.createElement("path", { d: "M9 14h6v1.5H9z", key: "k0" }),
            React.createElement("circle", { cx: 15.5, cy: 9.5, r: 1.5, key: "k1" }),
            React.createElement("circle", { cx: 8.5, cy: 9.5, r: 1.5, key: "k2" }),
            React.createElement("path", { d: "M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z", key: "k3" })
        ]
        : [React.createElement("path", { d: "M9 14h6v1.5H9z", key: "k0" }),
            React.createElement("circle", { cx: 15.5, cy: 9.5, r: 1.5, key: "k1" }),
            React.createElement("circle", { cx: 8.5, cy: 9.5, r: 1.5, key: "k2" }),
            React.createElement("path", { d: "M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z", key: "k3" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
SentimentNeutral.displayName = 'SentimentNeutral';
export var SentimentNeutralDimensions = { height: 24, width: 24 };
var templateObject_1;
