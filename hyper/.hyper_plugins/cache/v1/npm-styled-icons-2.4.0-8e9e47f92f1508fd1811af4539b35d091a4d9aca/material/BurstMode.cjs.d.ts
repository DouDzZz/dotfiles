import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BurstMode: StyledIcon<any>;
export declare const BurstModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
