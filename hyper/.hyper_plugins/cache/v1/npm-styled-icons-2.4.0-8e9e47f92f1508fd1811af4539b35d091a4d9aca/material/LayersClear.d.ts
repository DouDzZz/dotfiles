import { StyledIcon, StyledIconProps } from '..';
export declare const LayersClear: StyledIcon<any>;
export declare const LayersClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
