import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MicNone: StyledIcon<any>;
export declare const MicNoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
