import { StyledIcon, StyledIconProps } from '..';
export declare const Palette: StyledIcon<any>;
export declare const PaletteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
