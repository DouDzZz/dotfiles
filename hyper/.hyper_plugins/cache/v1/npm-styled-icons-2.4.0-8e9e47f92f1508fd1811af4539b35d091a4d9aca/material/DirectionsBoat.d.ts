import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsBoat: StyledIcon<any>;
export declare const DirectionsBoatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
