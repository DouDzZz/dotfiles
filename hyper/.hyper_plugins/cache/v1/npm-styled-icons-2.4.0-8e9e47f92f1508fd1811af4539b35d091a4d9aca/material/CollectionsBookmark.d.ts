import { StyledIcon, StyledIconProps } from '..';
export declare const CollectionsBookmark: StyledIcon<any>;
export declare const CollectionsBookmarkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
