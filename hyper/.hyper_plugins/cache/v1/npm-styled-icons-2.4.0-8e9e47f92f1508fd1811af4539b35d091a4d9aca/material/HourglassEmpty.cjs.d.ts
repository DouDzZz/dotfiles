import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HourglassEmpty: StyledIcon<any>;
export declare const HourglassEmptyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
