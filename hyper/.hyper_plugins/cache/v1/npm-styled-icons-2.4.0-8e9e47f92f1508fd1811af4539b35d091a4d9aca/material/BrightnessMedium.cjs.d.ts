import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BrightnessMedium: StyledIcon<any>;
export declare const BrightnessMediumDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
