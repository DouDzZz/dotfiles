import { StyledIcon, StyledIconProps } from '..';
export declare const VerticalAlignTop: StyledIcon<any>;
export declare const VerticalAlignTopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
