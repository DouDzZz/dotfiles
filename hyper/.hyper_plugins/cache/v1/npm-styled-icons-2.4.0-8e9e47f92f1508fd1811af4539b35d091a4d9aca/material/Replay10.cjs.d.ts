import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Replay10: StyledIcon<any>;
export declare const Replay10Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
