import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Games: StyledIcon<any>;
export declare const GamesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
