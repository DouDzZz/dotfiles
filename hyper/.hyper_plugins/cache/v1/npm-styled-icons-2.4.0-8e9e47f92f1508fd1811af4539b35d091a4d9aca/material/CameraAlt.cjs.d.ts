import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CameraAlt: StyledIcon<any>;
export declare const CameraAltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
