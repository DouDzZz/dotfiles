import { StyledIcon, StyledIconProps } from '..';
export declare const Star: StyledIcon<any>;
export declare const StarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
