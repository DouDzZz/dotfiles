import { StyledIcon, StyledIconProps } from '..';
export declare const BlurLinear: StyledIcon<any>;
export declare const BlurLinearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
