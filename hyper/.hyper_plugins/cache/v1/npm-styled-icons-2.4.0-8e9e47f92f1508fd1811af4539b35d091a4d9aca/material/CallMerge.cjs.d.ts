import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallMerge: StyledIcon<any>;
export declare const CallMergeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
