import { StyledIcon, StyledIconProps } from '..';
export declare const LocalPharmacy: StyledIcon<any>;
export declare const LocalPharmacyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
