import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BlurOn: StyledIcon<any>;
export declare const BlurOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
