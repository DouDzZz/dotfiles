import { StyledIcon, StyledIconProps } from '..';
export declare const Looks: StyledIcon<any>;
export declare const LooksDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
