import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PowerInput: StyledIcon<any>;
export declare const PowerInputDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
