import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NaturePeople: StyledIcon<any>;
export declare const NaturePeopleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
