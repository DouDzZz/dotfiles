import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Backspace: StyledIcon<any>;
export declare const BackspaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
