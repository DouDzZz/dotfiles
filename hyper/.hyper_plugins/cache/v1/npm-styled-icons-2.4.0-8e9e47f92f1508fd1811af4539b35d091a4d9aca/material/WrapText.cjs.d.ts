import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WrapText: StyledIcon<any>;
export declare const WrapTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
