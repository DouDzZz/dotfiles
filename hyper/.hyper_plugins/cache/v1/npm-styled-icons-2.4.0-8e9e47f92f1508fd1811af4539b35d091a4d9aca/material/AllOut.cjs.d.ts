import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AllOut: StyledIcon<any>;
export declare const AllOutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
