import { StyledIcon, StyledIconProps } from '..';
export declare const ViewModule: StyledIcon<any>;
export declare const ViewModuleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
