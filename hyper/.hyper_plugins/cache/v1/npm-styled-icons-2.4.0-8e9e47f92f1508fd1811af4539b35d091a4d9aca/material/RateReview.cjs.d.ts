import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RateReview: StyledIcon<any>;
export declare const RateReviewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
