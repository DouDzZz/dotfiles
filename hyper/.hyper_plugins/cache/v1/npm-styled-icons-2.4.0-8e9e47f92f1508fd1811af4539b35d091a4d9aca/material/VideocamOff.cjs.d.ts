import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideocamOff: StyledIcon<any>;
export declare const VideocamOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
