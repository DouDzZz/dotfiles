import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DevicesOther: StyledIcon<any>;
export declare const DevicesOtherDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
