import { StyledIcon, StyledIconProps } from '..';
export declare const TimeToLeave: StyledIcon<any>;
export declare const TimeToLeaveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
