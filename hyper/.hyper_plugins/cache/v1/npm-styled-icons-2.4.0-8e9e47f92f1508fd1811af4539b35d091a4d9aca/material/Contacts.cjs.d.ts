import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Contacts: StyledIcon<any>;
export declare const ContactsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
