import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Vignette: StyledIcon<any>;
export declare const VignetteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
