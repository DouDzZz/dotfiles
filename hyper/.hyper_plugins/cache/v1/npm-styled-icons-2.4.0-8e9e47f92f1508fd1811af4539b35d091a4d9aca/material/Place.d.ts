import { StyledIcon, StyledIconProps } from '..';
export declare const Place: StyledIcon<any>;
export declare const PlaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
