import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Traffic: StyledIcon<any>;
export declare const TrafficDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
