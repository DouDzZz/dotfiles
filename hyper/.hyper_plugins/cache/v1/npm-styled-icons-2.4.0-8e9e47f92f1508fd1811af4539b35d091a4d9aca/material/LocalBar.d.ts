import { StyledIcon, StyledIconProps } from '..';
export declare const LocalBar: StyledIcon<any>;
export declare const LocalBarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
