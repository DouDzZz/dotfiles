import { StyledIcon, StyledIconProps } from '..';
export declare const Headset: StyledIcon<any>;
export declare const HeadsetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
