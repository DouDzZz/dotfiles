import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unarchive: StyledIcon<any>;
export declare const UnarchiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
