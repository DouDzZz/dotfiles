import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PanoramaWideAngle: StyledIcon<any>;
export declare const PanoramaWideAngleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
