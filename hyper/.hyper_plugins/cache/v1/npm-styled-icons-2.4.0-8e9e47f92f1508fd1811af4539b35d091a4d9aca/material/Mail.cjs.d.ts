import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mail: StyledIcon<any>;
export declare const MailDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
