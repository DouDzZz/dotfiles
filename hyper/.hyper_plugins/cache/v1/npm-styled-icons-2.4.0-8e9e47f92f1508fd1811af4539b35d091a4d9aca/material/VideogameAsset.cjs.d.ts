import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideogameAsset: StyledIcon<any>;
export declare const VideogameAssetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
