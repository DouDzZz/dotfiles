import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LooksTwo: StyledIcon<any>;
export declare const LooksTwoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
