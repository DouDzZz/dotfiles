import { StyledIcon, StyledIconProps } from '..';
export declare const Add: StyledIcon<any>;
export declare const AddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
