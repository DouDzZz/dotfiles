import { StyledIcon, StyledIconProps } from '..';
export declare const InsertDriveFile: StyledIcon<any>;
export declare const InsertDriveFileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
