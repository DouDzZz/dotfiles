import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ImportExport: StyledIcon<any>;
export declare const ImportExportDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
