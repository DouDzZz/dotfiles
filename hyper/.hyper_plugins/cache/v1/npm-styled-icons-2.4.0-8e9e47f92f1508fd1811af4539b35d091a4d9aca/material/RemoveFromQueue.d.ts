import { StyledIcon, StyledIconProps } from '..';
export declare const RemoveFromQueue: StyledIcon<any>;
export declare const RemoveFromQueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
