import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoSizeSelectLarge: StyledIcon<any>;
export declare const PhotoSizeSelectLargeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
