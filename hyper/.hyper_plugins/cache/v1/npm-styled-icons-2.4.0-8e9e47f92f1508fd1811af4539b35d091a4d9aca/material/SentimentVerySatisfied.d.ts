import { StyledIcon, StyledIconProps } from '..';
export declare const SentimentVerySatisfied: StyledIcon<any>;
export declare const SentimentVerySatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
