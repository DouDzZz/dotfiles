import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Update: StyledIcon<any>;
export declare const UpdateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
