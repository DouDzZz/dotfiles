import { StyledIcon, StyledIconProps } from '..';
export declare const Highlight: StyledIcon<any>;
export declare const HighlightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
