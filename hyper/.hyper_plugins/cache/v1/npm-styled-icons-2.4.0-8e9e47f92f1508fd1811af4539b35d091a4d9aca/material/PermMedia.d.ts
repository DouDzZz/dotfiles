import { StyledIcon, StyledIconProps } from '..';
export declare const PermMedia: StyledIcon<any>;
export declare const PermMediaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
