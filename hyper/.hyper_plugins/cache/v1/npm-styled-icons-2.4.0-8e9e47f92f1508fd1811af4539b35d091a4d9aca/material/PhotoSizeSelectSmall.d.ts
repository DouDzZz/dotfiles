import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoSizeSelectSmall: StyledIcon<any>;
export declare const PhotoSizeSelectSmallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
