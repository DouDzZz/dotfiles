import { StyledIcon, StyledIconProps } from '..';
export declare const LocalTaxi: StyledIcon<any>;
export declare const LocalTaxiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
