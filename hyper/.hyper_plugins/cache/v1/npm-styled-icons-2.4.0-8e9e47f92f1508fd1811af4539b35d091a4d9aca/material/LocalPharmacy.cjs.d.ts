import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPharmacy: StyledIcon<any>;
export declare const LocalPharmacyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
