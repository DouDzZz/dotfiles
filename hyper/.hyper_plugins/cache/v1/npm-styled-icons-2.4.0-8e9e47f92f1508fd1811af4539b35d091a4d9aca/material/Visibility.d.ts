import { StyledIcon, StyledIconProps } from '..';
export declare const Visibility: StyledIcon<any>;
export declare const VisibilityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
