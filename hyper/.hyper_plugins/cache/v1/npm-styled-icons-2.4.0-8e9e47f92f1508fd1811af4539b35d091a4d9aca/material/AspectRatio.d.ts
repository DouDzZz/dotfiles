import { StyledIcon, StyledIconProps } from '..';
export declare const AspectRatio: StyledIcon<any>;
export declare const AspectRatioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
