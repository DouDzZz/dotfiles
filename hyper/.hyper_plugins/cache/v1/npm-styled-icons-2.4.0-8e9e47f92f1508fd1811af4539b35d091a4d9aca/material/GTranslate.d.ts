import { StyledIcon, StyledIconProps } from '..';
export declare const GTranslate: StyledIcon<any>;
export declare const GTranslateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
