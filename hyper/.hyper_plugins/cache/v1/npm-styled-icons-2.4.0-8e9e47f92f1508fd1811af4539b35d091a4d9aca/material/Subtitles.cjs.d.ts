import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subtitles: StyledIcon<any>;
export declare const SubtitlesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
