import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BluetoothDisabled: StyledIcon<any>;
export declare const BluetoothDisabledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
