import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Nfc: StyledIcon<any>;
export declare const NfcDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
