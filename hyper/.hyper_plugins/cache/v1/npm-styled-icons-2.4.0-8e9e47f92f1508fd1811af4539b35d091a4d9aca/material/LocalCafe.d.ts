import { StyledIcon, StyledIconProps } from '..';
export declare const LocalCafe: StyledIcon<any>;
export declare const LocalCafeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
