import { StyledIcon, StyledIconProps } from '..';
export declare const Dehaze: StyledIcon<any>;
export declare const DehazeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
