import { StyledIcon, StyledIconProps } from '..';
export declare const Dock: StyledIcon<any>;
export declare const DockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
