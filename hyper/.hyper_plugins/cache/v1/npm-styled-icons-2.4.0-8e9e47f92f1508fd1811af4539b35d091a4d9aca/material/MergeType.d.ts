import { StyledIcon, StyledIconProps } from '..';
export declare const MergeType: StyledIcon<any>;
export declare const MergeTypeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
