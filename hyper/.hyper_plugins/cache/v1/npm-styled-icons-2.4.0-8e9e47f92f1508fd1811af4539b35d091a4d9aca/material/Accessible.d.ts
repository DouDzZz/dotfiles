import { StyledIcon, StyledIconProps } from '..';
export declare const Accessible: StyledIcon<any>;
export declare const AccessibleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
