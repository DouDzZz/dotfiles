import { StyledIcon, StyledIconProps } from '..';
export declare const Terrain: StyledIcon<any>;
export declare const TerrainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
