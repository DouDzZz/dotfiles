import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DeleteForever: StyledIcon<any>;
export declare const DeleteForeverDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
