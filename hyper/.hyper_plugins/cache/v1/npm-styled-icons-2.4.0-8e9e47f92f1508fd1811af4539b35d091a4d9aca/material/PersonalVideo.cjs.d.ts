import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PersonalVideo: StyledIcon<any>;
export declare const PersonalVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
