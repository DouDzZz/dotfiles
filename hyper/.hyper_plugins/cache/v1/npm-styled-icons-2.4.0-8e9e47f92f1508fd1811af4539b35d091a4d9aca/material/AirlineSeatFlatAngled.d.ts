import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatFlatAngled: StyledIcon<any>;
export declare const AirlineSeatFlatAngledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
