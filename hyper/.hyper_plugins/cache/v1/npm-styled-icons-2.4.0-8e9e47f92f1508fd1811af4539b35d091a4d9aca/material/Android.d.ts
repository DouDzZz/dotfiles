import { StyledIcon, StyledIconProps } from '..';
export declare const Android: StyledIcon<any>;
export declare const AndroidDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
