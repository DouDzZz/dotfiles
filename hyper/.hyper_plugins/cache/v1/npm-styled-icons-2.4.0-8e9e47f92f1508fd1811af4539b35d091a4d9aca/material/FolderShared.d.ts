import { StyledIcon, StyledIconProps } from '..';
export declare const FolderShared: StyledIcon<any>;
export declare const FolderSharedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
