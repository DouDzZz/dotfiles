import { StyledIcon, StyledIconProps } from '..';
export declare const Keyboard: StyledIcon<any>;
export declare const KeyboardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
