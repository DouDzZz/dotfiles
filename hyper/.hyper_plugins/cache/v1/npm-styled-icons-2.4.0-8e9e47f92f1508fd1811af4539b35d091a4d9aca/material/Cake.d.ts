import { StyledIcon, StyledIconProps } from '..';
export declare const Cake: StyledIcon<any>;
export declare const CakeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
