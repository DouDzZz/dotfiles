import { StyledIcon, StyledIconProps } from '..';
export declare const WbCloudy: StyledIcon<any>;
export declare const WbCloudyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
