import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Today: StyledIcon<any>;
export declare const TodayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
