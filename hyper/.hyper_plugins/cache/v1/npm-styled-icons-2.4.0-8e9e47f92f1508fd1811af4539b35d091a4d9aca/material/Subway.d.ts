import { StyledIcon, StyledIconProps } from '..';
export declare const Subway: StyledIcon<any>;
export declare const SubwayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
