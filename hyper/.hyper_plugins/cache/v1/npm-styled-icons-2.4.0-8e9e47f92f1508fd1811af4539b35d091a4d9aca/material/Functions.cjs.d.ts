import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Functions: StyledIcon<any>;
export declare const FunctionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
