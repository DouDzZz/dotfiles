import { StyledIcon, StyledIconProps } from '..';
export declare const SlowMotionVideo: StyledIcon<any>;
export declare const SlowMotionVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
