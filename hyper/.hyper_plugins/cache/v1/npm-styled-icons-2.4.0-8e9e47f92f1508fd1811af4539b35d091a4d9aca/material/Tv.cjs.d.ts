import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tv: StyledIcon<any>;
export declare const TvDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
