import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LineWeight: StyledIcon<any>;
export declare const LineWeightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
