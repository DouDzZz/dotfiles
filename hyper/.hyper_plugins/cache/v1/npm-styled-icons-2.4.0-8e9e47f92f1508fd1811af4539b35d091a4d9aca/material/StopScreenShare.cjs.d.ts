import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StopScreenShare: StyledIcon<any>;
export declare const StopScreenShareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
