import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tune: StyledIcon<any>;
export declare const TuneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
