import { StyledIcon, StyledIconProps } from '..';
export declare const AirportShuttle: StyledIcon<any>;
export declare const AirportShuttleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
