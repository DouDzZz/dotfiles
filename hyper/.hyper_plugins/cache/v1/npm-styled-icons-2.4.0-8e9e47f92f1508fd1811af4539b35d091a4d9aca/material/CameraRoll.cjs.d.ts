import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CameraRoll: StyledIcon<any>;
export declare const CameraRollDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
