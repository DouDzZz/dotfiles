import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CardGiftcard: StyledIcon<any>;
export declare const CardGiftcardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
