import { StyledIcon, StyledIconProps } from '..';
export declare const RadioButtonUnchecked: StyledIcon<any>;
export declare const RadioButtonUncheckedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
