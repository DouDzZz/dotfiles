import { StyledIcon, StyledIconProps } from '..';
export declare const Laptop: StyledIcon<any>;
export declare const LaptopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
