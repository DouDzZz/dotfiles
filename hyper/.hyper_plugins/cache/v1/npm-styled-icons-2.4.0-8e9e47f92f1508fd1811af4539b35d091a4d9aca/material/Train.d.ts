import { StyledIcon, StyledIconProps } from '..';
export declare const Train: StyledIcon<any>;
export declare const TrainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
