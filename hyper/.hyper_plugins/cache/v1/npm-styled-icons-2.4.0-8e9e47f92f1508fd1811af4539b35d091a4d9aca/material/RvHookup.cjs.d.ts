import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RvHookup: StyledIcon<any>;
export declare const RvHookupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
