import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Compare: StyledIcon<any>;
export declare const CompareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
