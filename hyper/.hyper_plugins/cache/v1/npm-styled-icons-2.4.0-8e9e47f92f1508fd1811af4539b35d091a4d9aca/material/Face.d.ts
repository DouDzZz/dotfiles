import { StyledIcon, StyledIconProps } from '..';
export declare const Face: StyledIcon<any>;
export declare const FaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
