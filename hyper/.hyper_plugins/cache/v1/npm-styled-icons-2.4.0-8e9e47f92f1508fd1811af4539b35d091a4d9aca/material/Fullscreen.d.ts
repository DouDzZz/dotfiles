import { StyledIcon, StyledIconProps } from '..';
export declare const Fullscreen: StyledIcon<any>;
export declare const FullscreenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
