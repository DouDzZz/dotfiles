import { StyledIcon, StyledIconProps } from '..';
export declare const Input: StyledIcon<any>;
export declare const InputDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
