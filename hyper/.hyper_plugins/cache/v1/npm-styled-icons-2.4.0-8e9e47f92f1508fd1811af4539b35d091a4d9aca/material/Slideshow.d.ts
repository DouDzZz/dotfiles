import { StyledIcon, StyledIconProps } from '..';
export declare const Slideshow: StyledIcon<any>;
export declare const SlideshowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
