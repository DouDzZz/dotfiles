import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi1BarLock: StyledIcon<any>;
export declare const SignalWifi1BarLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
