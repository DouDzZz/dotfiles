import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneAndroid: StyledIcon<any>;
export declare const PhoneAndroidDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
