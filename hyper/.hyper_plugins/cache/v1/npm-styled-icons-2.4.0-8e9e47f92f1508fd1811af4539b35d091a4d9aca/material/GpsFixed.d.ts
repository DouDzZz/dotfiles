import { StyledIcon, StyledIconProps } from '..';
export declare const GpsFixed: StyledIcon<any>;
export declare const GpsFixedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
