import { StyledIcon, StyledIconProps } from '..';
export declare const Battery30: StyledIcon<any>;
export declare const Battery30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
