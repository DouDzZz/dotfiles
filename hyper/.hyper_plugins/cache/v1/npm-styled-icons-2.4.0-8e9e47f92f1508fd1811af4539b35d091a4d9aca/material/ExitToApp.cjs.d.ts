import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExitToApp: StyledIcon<any>;
export declare const ExitToAppDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
