import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Reorder: StyledIcon<any>;
export declare const ReorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
