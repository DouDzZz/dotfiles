import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneInTalk: StyledIcon<any>;
export declare const PhoneInTalkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
