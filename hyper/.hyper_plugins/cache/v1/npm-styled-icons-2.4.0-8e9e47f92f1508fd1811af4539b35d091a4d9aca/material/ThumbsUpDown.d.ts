import { StyledIcon, StyledIconProps } from '..';
export declare const ThumbsUpDown: StyledIcon<any>;
export declare const ThumbsUpDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
