import { StyledIcon, StyledIconProps } from '..';
export declare const Announcement: StyledIcon<any>;
export declare const AnnouncementDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
