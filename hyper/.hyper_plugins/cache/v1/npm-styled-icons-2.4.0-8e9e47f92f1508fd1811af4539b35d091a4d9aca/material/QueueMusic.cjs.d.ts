import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QueueMusic: StyledIcon<any>;
export declare const QueueMusicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
