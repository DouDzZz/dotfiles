import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewColumn: StyledIcon<any>;
export declare const ViewColumnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
