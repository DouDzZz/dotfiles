import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Spa: StyledIcon<any>;
export declare const SpaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
