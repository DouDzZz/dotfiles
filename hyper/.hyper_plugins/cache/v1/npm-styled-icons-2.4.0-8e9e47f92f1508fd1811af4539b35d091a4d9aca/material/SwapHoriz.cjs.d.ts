import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SwapHoriz: StyledIcon<any>;
export declare const SwapHorizDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
