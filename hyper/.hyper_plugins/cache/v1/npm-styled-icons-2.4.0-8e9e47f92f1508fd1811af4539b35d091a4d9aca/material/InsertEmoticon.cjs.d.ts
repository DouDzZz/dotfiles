import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertEmoticon: StyledIcon<any>;
export declare const InsertEmoticonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
