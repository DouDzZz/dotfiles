import { StyledIcon, StyledIconProps } from '..';
export declare const RotateRight: StyledIcon<any>;
export declare const RotateRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
