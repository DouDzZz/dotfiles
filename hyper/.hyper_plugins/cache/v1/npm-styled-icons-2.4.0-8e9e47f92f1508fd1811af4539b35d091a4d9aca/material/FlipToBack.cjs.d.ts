import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FlipToBack: StyledIcon<any>;
export declare const FlipToBackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
