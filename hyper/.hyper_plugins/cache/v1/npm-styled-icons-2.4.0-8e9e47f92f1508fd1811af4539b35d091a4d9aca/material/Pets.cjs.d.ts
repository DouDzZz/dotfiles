import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pets: StyledIcon<any>;
export declare const PetsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
