import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsSystemDaydream: StyledIcon<any>;
export declare const SettingsSystemDaydreamDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
