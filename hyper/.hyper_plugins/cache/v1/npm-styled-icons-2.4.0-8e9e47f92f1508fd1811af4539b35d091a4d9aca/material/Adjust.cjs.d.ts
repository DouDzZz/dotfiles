import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Adjust: StyledIcon<any>;
export declare const AdjustDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
