import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SpeakerNotesOff: StyledIcon<any>;
export declare const SpeakerNotesOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
