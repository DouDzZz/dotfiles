import { StyledIcon, StyledIconProps } from '..';
export declare const WbIridescent: StyledIcon<any>;
export declare const WbIridescentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
