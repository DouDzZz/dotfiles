import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowDropDown: StyledIcon<any>;
export declare const ArrowDropDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
