import { StyledIcon, StyledIconProps } from '..';
export declare const ControlPointDuplicate: StyledIcon<any>;
export declare const ControlPointDuplicateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
