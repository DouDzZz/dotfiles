import { StyledIcon, StyledIconProps } from '..';
export declare const DoNotDisturbOff: StyledIcon<any>;
export declare const DoNotDisturbOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
