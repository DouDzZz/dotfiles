import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DoneAll: StyledIcon<any>;
export declare const DoneAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
