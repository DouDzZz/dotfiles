import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularConnectedNoInternet4Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet4BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
