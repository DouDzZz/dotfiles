import { StyledIcon, StyledIconProps } from '..';
export declare const Crop75: StyledIcon<any>;
export declare const Crop75Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
