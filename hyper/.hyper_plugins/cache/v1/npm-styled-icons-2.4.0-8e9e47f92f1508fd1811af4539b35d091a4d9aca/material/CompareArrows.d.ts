import { StyledIcon, StyledIconProps } from '..';
export declare const CompareArrows: StyledIcon<any>;
export declare const CompareArrowsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
