import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Beenhere: StyledIcon<any>;
export declare const BeenhereDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
