import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Texture: StyledIcon<any>;
export declare const TextureDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
