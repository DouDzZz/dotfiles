import { StyledIcon, StyledIconProps } from '..';
export declare const Gradient: StyledIcon<any>;
export declare const GradientDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
