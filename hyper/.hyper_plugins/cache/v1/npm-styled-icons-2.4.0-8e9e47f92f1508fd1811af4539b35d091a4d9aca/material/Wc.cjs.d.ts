import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wc: StyledIcon<any>;
export declare const WcDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
