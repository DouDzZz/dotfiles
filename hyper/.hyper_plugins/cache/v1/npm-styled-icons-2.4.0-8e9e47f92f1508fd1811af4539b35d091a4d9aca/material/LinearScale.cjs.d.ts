import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LinearScale: StyledIcon<any>;
export declare const LinearScaleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
