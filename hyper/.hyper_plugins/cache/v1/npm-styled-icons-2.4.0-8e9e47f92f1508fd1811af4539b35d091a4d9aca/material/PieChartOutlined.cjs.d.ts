import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PieChartOutlined: StyledIcon<any>;
export declare const PieChartOutlinedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
