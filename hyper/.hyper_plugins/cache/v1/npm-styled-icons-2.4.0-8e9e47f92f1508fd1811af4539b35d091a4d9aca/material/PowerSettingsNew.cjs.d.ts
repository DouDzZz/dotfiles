import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PowerSettingsNew: StyledIcon<any>;
export declare const PowerSettingsNewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
