import { StyledIcon, StyledIconProps } from '..';
export declare const HighQuality: StyledIcon<any>;
export declare const HighQualityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
