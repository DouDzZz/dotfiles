import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FreeBreakfast: StyledIcon<any>;
export declare const FreeBreakfastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
