import { StyledIcon, StyledIconProps } from '..';
export declare const Traffic: StyledIcon<any>;
export declare const TrafficDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
