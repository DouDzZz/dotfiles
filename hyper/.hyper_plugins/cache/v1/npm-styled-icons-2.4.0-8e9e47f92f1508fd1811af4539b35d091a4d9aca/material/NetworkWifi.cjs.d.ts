import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NetworkWifi: StyledIcon<any>;
export declare const NetworkWifiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
