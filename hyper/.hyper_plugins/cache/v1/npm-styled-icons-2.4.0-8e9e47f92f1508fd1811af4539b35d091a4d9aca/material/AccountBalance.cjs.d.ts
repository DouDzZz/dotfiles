import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AccountBalance: StyledIcon<any>;
export declare const AccountBalanceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
