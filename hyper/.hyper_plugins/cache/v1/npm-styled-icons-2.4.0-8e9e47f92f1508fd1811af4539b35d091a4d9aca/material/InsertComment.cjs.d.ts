import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertComment: StyledIcon<any>;
export declare const InsertCommentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
