import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LayersClear: StyledIcon<any>;
export declare const LayersClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
