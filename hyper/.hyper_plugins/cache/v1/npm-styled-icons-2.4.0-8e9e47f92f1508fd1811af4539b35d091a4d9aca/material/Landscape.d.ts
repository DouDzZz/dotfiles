import { StyledIcon, StyledIconProps } from '..';
export declare const Landscape: StyledIcon<any>;
export declare const LandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
