import { StyledIcon, StyledIconProps } from '..';
export declare const PresentToAll: StyledIcon<any>;
export declare const PresentToAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
