import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPlay: StyledIcon<any>;
export declare const LocalPlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
