import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsPhone: StyledIcon<any>;
export declare const SettingsPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
