import { StyledIcon, StyledIconProps } from '..';
export declare const BlurCircular: StyledIcon<any>;
export declare const BlurCircularDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
