import { StyledIcon, StyledIconProps } from '..';
export declare const Security: StyledIcon<any>;
export declare const SecurityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
