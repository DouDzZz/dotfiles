import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Loupe: StyledIcon<any>;
export declare const LoupeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
