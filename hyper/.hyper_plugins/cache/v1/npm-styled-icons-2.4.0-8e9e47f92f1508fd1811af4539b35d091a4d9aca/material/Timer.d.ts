import { StyledIcon, StyledIconProps } from '..';
export declare const Timer: StyledIcon<any>;
export declare const TimerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
