import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Repeat: StyledIcon<any>;
export declare const RepeatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
