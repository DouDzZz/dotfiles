import { StyledIcon, StyledIconProps } from '..';
export declare const Wallpaper: StyledIcon<any>;
export declare const WallpaperDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
