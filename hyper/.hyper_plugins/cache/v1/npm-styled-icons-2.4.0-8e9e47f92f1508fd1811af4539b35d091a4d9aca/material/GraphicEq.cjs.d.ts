import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GraphicEq: StyledIcon<any>;
export declare const GraphicEqDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
