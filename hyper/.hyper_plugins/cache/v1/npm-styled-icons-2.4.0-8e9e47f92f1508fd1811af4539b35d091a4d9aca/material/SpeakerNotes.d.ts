import { StyledIcon, StyledIconProps } from '..';
export declare const SpeakerNotes: StyledIcon<any>;
export declare const SpeakerNotesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
