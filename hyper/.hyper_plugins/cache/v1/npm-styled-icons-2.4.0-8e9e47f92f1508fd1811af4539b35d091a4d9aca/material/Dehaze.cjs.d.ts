import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dehaze: StyledIcon<any>;
export declare const DehazeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
