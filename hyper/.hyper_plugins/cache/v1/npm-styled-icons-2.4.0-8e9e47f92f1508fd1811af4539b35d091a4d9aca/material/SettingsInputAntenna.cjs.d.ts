import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsInputAntenna: StyledIcon<any>;
export declare const SettingsInputAntennaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
