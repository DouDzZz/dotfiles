import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RemoveShoppingCart: StyledIcon<any>;
export declare const RemoveShoppingCartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
