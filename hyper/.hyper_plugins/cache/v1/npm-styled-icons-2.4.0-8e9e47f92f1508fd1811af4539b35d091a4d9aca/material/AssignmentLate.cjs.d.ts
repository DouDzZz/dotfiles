import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssignmentLate: StyledIcon<any>;
export declare const AssignmentLateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
