import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Scanner: StyledIcon<any>;
export declare const ScannerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
