import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermDataSetting: StyledIcon<any>;
export declare const PermDataSettingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
