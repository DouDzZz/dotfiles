import { StyledIcon, StyledIconProps } from '..';
export declare const CheckBox: StyledIcon<any>;
export declare const CheckBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
