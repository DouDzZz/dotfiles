import { StyledIcon, StyledIconProps } from '..';
export declare const BurstMode: StyledIcon<any>;
export declare const BurstModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
