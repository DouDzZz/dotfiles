import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsRemote: StyledIcon<any>;
export declare const SettingsRemoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
