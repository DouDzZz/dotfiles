import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TabUnselected: StyledIcon<any>;
export declare const TabUnselectedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
