import { StyledIcon, StyledIconProps } from '..';
export declare const SwapVert: StyledIcon<any>;
export declare const SwapVertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
