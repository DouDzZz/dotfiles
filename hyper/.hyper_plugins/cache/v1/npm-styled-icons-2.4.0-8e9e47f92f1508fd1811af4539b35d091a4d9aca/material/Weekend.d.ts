import { StyledIcon, StyledIconProps } from '..';
export declare const Weekend: StyledIcon<any>;
export declare const WeekendDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
