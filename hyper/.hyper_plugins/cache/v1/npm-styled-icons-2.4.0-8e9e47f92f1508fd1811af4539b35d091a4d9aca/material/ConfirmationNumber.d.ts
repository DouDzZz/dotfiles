import { StyledIcon, StyledIconProps } from '..';
export declare const ConfirmationNumber: StyledIcon<any>;
export declare const ConfirmationNumberDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
