import { StyledIcon, StyledIconProps } from '..';
export declare const BorderTop: StyledIcon<any>;
export declare const BorderTopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
