import { StyledIcon, StyledIconProps } from '..';
export declare const RoundedCorner: StyledIcon<any>;
export declare const RoundedCornerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
