import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PauseCircleFilled: StyledIcon<any>;
export declare const PauseCircleFilledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
