import { StyledIcon, StyledIconProps } from '..';
export declare const EditLocation: StyledIcon<any>;
export declare const EditLocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
