import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhotoSizeSelectLarge: StyledIcon<any>;
export declare const PhotoSizeSelectLargeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
