import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalShipping: StyledIcon<any>;
export declare const LocalShippingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
