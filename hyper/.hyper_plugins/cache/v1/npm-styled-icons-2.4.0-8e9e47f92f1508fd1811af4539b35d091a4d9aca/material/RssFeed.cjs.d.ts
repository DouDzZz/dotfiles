import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RssFeed: StyledIcon<any>;
export declare const RssFeedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
