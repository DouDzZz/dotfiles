import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EvStation: StyledIcon<any>;
export declare const EvStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
