import { StyledIcon, StyledIconProps } from '..';
export declare const PhonePaused: StyledIcon<any>;
export declare const PhonePausedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
