import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Equalizer: StyledIcon<any>;
export declare const EqualizerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
