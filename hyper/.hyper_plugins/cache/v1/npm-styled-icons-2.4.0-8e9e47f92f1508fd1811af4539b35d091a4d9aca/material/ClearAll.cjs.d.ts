import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ClearAll: StyledIcon<any>;
export declare const ClearAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
