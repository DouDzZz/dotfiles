import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPhone: StyledIcon<any>;
export declare const LocalPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
