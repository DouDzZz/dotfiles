import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalSee: StyledIcon<any>;
export declare const LocalSeeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
