import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WbAuto: StyledIcon<any>;
export declare const WbAutoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
