import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SurroundSound: StyledIcon<any>;
export declare const SurroundSoundDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
