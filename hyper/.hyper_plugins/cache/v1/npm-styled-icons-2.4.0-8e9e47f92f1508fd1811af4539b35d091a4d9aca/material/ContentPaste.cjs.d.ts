import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ContentPaste: StyledIcon<any>;
export declare const ContentPasteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
