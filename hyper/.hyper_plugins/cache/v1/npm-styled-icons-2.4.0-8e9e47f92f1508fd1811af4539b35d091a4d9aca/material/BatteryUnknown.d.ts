import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryUnknown: StyledIcon<any>;
export declare const BatteryUnknownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
