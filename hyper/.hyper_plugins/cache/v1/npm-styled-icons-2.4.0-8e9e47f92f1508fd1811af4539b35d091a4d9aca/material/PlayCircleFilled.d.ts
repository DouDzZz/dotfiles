import { StyledIcon, StyledIconProps } from '..';
export declare const PlayCircleFilled: StyledIcon<any>;
export declare const PlayCircleFilledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
