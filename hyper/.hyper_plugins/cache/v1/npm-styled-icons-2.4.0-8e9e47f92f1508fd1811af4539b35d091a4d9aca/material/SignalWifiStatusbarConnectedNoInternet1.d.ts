import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarConnectedNoInternet1: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternet1Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
