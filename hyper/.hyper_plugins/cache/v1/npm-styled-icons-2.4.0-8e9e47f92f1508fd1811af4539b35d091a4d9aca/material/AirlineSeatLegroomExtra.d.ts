import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatLegroomExtra: StyledIcon<any>;
export declare const AirlineSeatLegroomExtraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
