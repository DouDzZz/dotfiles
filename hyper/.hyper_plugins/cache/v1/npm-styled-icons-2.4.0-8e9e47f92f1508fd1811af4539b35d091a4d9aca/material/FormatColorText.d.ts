import { StyledIcon, StyledIconProps } from '..';
export declare const FormatColorText: StyledIcon<any>;
export declare const FormatColorTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
