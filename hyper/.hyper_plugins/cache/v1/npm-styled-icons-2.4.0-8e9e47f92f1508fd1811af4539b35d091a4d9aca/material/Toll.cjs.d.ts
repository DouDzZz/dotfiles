import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Toll: StyledIcon<any>;
export declare const TollDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
