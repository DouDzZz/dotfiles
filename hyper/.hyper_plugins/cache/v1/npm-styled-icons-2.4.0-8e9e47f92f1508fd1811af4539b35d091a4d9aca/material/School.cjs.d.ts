import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const School: StyledIcon<any>;
export declare const SchoolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
