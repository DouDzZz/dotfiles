import { StyledIcon, StyledIconProps } from '..';
export declare const HeadsetMic: StyledIcon<any>;
export declare const HeadsetMicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
