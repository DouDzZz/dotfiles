import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocationOff: StyledIcon<any>;
export declare const LocationOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
