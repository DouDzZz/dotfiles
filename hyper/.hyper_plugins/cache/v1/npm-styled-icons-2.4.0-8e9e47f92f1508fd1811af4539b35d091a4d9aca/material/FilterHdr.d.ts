import { StyledIcon, StyledIconProps } from '..';
export declare const FilterHdr: StyledIcon<any>;
export declare const FilterHdrDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
