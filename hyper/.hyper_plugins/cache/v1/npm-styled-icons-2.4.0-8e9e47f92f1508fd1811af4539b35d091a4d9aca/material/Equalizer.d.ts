import { StyledIcon, StyledIconProps } from '..';
export declare const Equalizer: StyledIcon<any>;
export declare const EqualizerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
