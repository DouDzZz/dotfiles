import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CameraFront: StyledIcon<any>;
export declare const CameraFrontDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
