import { StyledIcon, StyledIconProps } from '..';
export declare const Collections: StyledIcon<any>;
export declare const CollectionsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
