import { StyledIcon, StyledIconProps } from '..';
export declare const HdrWeak: StyledIcon<any>;
export declare const HdrWeakDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
