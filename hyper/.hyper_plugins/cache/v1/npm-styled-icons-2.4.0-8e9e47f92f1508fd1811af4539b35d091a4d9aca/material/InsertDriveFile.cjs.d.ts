import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertDriveFile: StyledIcon<any>;
export declare const InsertDriveFileDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
