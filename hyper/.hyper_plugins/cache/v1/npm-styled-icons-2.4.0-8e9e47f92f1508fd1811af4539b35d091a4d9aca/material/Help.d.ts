import { StyledIcon, StyledIconProps } from '..';
export declare const Help: StyledIcon<any>;
export declare const HelpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
