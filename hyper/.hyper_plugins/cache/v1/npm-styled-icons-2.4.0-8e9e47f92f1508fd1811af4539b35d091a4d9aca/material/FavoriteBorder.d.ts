import { StyledIcon, StyledIconProps } from '..';
export declare const FavoriteBorder: StyledIcon<any>;
export declare const FavoriteBorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
