import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallMade: StyledIcon<any>;
export declare const CallMadeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
