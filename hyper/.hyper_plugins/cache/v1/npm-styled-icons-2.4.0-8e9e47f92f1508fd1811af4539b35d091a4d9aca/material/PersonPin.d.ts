import { StyledIcon, StyledIconProps } from '..';
export declare const PersonPin: StyledIcon<any>;
export declare const PersonPinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
