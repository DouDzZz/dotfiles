import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EventAvailable: StyledIcon<any>;
export declare const EventAvailableDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
