import { StyledIcon, StyledIconProps } from '..';
export declare const VisibilityOff: StyledIcon<any>;
export declare const VisibilityOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
