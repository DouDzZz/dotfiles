import { StyledIcon, StyledIconProps } from '..';
export declare const Eject: StyledIcon<any>;
export declare const EjectDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
