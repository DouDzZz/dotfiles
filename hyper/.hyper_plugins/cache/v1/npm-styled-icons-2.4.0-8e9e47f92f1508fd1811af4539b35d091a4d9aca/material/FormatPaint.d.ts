import { StyledIcon, StyledIconProps } from '..';
export declare const FormatPaint: StyledIcon<any>;
export declare const FormatPaintDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
