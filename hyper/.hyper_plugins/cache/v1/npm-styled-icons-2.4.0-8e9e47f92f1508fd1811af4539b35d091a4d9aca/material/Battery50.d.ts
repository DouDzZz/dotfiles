import { StyledIcon, StyledIconProps } from '..';
export declare const Battery50: StyledIcon<any>;
export declare const Battery50Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
