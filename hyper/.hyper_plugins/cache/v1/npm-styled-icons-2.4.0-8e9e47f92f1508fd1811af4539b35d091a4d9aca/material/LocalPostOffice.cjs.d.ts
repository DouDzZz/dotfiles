import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPostOffice: StyledIcon<any>;
export declare const LocalPostOfficeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
