import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChromeReaderMode: StyledIcon<any>;
export declare const ChromeReaderModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
