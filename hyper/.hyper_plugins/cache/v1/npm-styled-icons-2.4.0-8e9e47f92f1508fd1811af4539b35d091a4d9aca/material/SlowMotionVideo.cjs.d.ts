import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SlowMotionVideo: StyledIcon<any>;
export declare const SlowMotionVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
