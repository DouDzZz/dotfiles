import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertLink: StyledIcon<any>;
export declare const InsertLinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
