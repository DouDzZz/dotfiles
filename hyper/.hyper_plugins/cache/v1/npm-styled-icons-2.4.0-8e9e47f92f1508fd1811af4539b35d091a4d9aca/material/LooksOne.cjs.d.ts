import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LooksOne: StyledIcon<any>;
export declare const LooksOneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
