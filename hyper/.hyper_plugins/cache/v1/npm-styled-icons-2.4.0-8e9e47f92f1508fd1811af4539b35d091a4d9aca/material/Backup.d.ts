import { StyledIcon, StyledIconProps } from '..';
export declare const Backup: StyledIcon<any>;
export declare const BackupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
