import { StyledIcon, StyledIconProps } from '..';
export declare const Business: StyledIcon<any>;
export declare const BusinessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
