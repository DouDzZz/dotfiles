import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardArrowUp: StyledIcon<any>;
export declare const KeyboardArrowUpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
