import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Visibility: StyledIcon<any>;
export declare const VisibilityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
