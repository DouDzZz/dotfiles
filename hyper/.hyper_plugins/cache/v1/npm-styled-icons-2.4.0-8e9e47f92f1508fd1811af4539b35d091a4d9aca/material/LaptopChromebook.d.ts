import { StyledIcon, StyledIconProps } from '..';
export declare const LaptopChromebook: StyledIcon<any>;
export declare const LaptopChromebookDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
