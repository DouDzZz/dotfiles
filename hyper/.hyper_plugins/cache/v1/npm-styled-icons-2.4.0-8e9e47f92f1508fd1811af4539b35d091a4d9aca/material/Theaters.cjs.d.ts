import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Theaters: StyledIcon<any>;
export declare const TheatersDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
