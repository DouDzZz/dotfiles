import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsRailway: StyledIcon<any>;
export declare const DirectionsRailwayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
