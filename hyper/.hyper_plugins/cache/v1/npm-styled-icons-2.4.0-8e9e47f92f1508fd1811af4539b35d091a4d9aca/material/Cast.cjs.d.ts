import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cast: StyledIcon<any>;
export declare const CastDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
