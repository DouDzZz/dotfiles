import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudQueue: StyledIcon<any>;
export declare const CloudQueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
