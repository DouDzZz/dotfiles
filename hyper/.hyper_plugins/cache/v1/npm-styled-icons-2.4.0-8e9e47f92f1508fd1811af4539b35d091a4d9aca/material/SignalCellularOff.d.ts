import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularOff: StyledIcon<any>;
export declare const SignalCellularOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
