import { StyledIcon, StyledIconProps } from '..';
export declare const Warning: StyledIcon<any>;
export declare const WarningDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
