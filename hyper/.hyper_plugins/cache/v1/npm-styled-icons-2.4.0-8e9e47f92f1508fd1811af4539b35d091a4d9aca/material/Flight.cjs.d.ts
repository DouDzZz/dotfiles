import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flight: StyledIcon<any>;
export declare const FlightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
