import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbar3Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
