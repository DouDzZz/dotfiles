import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HighQuality: StyledIcon<any>;
export declare const HighQualityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
