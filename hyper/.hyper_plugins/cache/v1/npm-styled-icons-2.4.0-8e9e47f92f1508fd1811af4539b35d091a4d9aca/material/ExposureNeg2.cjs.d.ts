import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExposureNeg2: StyledIcon<any>;
export declare const ExposureNeg2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
