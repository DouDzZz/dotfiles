import { StyledIcon, StyledIconProps } from '..';
export declare const SortByAlpha: StyledIcon<any>;
export declare const SortByAlphaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
