import { StyledIcon, StyledIconProps } from '..';
export declare const AssignmentReturn: StyledIcon<any>;
export declare const AssignmentReturnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
