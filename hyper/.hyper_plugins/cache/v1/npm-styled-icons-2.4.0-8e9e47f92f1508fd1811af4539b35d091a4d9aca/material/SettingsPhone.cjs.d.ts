import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsPhone: StyledIcon<any>;
export declare const SettingsPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
