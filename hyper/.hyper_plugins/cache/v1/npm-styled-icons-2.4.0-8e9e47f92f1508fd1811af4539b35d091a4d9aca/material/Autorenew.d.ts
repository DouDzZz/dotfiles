import { StyledIcon, StyledIconProps } from '..';
export declare const Autorenew: StyledIcon<any>;
export declare const AutorenewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
