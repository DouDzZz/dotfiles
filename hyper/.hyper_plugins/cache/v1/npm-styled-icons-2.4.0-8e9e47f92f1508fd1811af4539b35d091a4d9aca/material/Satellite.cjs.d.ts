import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Satellite: StyledIcon<any>;
export declare const SatelliteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
