import { StyledIcon, StyledIconProps } from '..';
export declare const PlaylistPlay: StyledIcon<any>;
export declare const PlaylistPlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
