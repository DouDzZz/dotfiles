import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Polymer: StyledIcon<any>;
export declare const PolymerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
