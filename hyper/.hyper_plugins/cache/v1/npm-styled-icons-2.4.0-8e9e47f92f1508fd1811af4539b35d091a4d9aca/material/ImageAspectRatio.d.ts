import { StyledIcon, StyledIconProps } from '..';
export declare const ImageAspectRatio: StyledIcon<any>;
export declare const ImageAspectRatioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
