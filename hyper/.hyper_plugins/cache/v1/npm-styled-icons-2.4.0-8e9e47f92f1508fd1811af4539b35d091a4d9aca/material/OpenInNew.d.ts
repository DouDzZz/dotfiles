import { StyledIcon, StyledIconProps } from '..';
export declare const OpenInNew: StyledIcon<any>;
export declare const OpenInNewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
