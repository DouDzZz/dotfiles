import { StyledIcon, StyledIconProps } from '..';
export declare const Replay30: StyledIcon<any>;
export declare const Replay30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
