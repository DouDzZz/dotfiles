import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Favorite: StyledIcon<any>;
export declare const FavoriteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
