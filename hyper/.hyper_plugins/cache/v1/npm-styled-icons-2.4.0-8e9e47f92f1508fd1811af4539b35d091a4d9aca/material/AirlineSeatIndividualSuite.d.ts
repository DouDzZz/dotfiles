import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatIndividualSuite: StyledIcon<any>;
export declare const AirlineSeatIndividualSuiteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
