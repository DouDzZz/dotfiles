import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subway: StyledIcon<any>;
export declare const SubwayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
