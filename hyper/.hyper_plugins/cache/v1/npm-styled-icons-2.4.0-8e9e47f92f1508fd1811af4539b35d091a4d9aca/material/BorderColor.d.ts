import { StyledIcon, StyledIconProps } from '..';
export declare const BorderColor: StyledIcon<any>;
export declare const BorderColorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
