import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssignmentReturn: StyledIcon<any>;
export declare const AssignmentReturnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
