import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flare: StyledIcon<any>;
export declare const FlareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
