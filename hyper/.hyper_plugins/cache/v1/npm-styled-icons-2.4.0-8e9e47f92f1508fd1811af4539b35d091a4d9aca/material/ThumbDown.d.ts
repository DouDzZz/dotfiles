import { StyledIcon, StyledIconProps } from '..';
export declare const ThumbDown: StyledIcon<any>;
export declare const ThumbDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
