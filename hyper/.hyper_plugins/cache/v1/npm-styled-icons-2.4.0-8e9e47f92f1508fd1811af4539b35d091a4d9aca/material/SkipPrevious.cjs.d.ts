import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SkipPrevious: StyledIcon<any>;
export declare const SkipPreviousDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
