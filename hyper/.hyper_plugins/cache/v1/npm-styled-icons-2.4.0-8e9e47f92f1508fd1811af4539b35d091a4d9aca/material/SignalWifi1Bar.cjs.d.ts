import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi1Bar: StyledIcon<any>;
export declare const SignalWifi1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
