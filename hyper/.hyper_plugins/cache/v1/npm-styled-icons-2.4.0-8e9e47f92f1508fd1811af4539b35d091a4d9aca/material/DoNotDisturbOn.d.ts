import { StyledIcon, StyledIconProps } from '..';
export declare const DoNotDisturbOn: StyledIcon<any>;
export declare const DoNotDisturbOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
