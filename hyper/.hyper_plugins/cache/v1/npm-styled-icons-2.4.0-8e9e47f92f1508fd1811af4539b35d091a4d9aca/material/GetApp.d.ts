import { StyledIcon, StyledIconProps } from '..';
export declare const GetApp: StyledIcon<any>;
export declare const GetAppDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
