import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Battery30: StyledIcon<any>;
export declare const Battery30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
