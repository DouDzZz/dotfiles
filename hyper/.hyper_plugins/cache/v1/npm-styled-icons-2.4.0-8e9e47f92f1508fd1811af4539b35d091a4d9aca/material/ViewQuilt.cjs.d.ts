import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewQuilt: StyledIcon<any>;
export declare const ViewQuiltDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
