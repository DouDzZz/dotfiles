import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BubbleChart: StyledIcon<any>;
export declare const BubbleChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
