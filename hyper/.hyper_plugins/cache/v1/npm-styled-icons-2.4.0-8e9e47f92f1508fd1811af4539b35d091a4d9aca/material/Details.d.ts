import { StyledIcon, StyledIconProps } from '..';
export declare const Details: StyledIcon<any>;
export declare const DetailsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
