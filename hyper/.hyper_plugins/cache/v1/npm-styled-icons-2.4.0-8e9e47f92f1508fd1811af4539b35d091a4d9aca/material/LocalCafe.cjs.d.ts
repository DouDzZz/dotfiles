import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalCafe: StyledIcon<any>;
export declare const LocalCafeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
