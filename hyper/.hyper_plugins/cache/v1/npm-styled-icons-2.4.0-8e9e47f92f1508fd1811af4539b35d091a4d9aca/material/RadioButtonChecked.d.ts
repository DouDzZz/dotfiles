import { StyledIcon, StyledIconProps } from '..';
export declare const RadioButtonChecked: StyledIcon<any>;
export declare const RadioButtonCheckedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
