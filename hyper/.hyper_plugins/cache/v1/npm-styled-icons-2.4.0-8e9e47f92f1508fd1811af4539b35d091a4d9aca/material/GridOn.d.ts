import { StyledIcon, StyledIconProps } from '..';
export declare const GridOn: StyledIcon<any>;
export declare const GridOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
