import { StyledIcon, StyledIconProps } from '..';
export declare const Store: StyledIcon<any>;
export declare const StoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
