import { StyledIcon, StyledIconProps } from '..';
export declare const CallSplit: StyledIcon<any>;
export declare const CallSplitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
