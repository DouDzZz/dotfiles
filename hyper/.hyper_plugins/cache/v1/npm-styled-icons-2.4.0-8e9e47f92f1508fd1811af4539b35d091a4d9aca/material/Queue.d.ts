import { StyledIcon, StyledIconProps } from '..';
export declare const Queue: StyledIcon<any>;
export declare const QueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
