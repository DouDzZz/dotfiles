import { StyledIcon, StyledIconProps } from '..';
export declare const Spellcheck: StyledIcon<any>;
export declare const SpellcheckDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
