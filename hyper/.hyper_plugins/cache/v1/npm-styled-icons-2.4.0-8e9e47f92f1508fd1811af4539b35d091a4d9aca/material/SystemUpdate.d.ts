import { StyledIcon, StyledIconProps } from '..';
export declare const SystemUpdate: StyledIcon<any>;
export declare const SystemUpdateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
