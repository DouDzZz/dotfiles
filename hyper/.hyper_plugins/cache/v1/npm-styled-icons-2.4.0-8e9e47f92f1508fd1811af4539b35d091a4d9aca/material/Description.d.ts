import { StyledIcon, StyledIconProps } from '..';
export declare const Description: StyledIcon<any>;
export declare const DescriptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
