import { StyledIcon, StyledIconProps } from '..';
export declare const ChangeHistory: StyledIcon<any>;
export declare const ChangeHistoryDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
