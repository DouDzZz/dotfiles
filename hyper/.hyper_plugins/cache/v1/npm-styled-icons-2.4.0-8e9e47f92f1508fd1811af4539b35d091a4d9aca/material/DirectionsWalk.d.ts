import { StyledIcon, StyledIconProps } from '..';
export declare const DirectionsWalk: StyledIcon<any>;
export declare const DirectionsWalkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
