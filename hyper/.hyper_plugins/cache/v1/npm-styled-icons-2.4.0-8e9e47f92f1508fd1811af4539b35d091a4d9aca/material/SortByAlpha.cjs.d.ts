import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortByAlpha: StyledIcon<any>;
export declare const SortByAlphaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
