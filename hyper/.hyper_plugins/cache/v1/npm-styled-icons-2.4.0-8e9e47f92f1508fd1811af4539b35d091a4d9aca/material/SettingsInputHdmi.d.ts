import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsInputHdmi: StyledIcon<any>;
export declare const SettingsInputHdmiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
