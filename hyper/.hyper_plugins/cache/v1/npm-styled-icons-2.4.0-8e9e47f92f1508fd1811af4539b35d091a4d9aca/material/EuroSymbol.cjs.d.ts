import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EuroSymbol: StyledIcon<any>;
export declare const EuroSymbolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
