import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GpsNotFixed: StyledIcon<any>;
export declare const GpsNotFixedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
