import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sms: StyledIcon<any>;
export declare const SmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
