import { StyledIcon, StyledIconProps } from '..';
export declare const Clear: StyledIcon<any>;
export declare const ClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
