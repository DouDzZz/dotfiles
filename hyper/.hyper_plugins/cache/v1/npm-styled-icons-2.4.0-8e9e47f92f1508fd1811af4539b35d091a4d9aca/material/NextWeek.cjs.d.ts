import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NextWeek: StyledIcon<any>;
export declare const NextWeekDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
