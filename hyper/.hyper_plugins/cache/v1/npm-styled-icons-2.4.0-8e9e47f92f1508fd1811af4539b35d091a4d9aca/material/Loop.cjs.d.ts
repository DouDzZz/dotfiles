import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Loop: StyledIcon<any>;
export declare const LoopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
