import { StyledIcon, StyledIconProps } from '..';
export declare const Reply: StyledIcon<any>;
export declare const ReplyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
