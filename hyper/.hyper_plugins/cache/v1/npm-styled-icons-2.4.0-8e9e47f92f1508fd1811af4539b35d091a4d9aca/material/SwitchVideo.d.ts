import { StyledIcon, StyledIconProps } from '..';
export declare const SwitchVideo: StyledIcon<any>;
export declare const SwitchVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
