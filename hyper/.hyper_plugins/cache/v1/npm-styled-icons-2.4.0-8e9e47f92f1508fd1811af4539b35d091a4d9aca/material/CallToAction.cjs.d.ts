import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallToAction: StyledIcon<any>;
export declare const CallToActionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
