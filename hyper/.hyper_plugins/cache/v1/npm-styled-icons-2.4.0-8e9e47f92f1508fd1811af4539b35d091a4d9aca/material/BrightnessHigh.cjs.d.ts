import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BrightnessHigh: StyledIcon<any>;
export declare const BrightnessHighDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
