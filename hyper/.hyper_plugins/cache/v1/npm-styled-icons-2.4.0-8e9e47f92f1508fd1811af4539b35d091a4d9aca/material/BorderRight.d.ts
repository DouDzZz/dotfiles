import { StyledIcon, StyledIconProps } from '..';
export declare const BorderRight: StyledIcon<any>;
export declare const BorderRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
