import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GroupWork: StyledIcon<any>;
export declare const GroupWorkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
