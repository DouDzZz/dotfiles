import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MailOutline: StyledIcon<any>;
export declare const MailOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
