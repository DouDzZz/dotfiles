import { StyledIcon, StyledIconProps } from '..';
export declare const TimerOff: StyledIcon<any>;
export declare const TimerOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
