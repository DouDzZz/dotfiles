import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OpenInBrowser: StyledIcon<any>;
export declare const OpenInBrowserDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
