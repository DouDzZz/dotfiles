import { StyledIcon, StyledIconProps } from '..';
export declare const Loop: StyledIcon<any>;
export declare const LoopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
