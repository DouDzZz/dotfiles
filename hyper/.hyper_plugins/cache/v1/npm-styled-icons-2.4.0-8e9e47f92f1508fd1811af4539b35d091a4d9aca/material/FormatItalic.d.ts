import { StyledIcon, StyledIconProps } from '..';
export declare const FormatItalic: StyledIcon<any>;
export declare const FormatItalicDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
