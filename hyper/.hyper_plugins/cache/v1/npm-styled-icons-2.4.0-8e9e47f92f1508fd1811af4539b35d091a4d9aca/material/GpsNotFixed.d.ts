import { StyledIcon, StyledIconProps } from '..';
export declare const GpsNotFixed: StyledIcon<any>;
export declare const GpsNotFixedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
