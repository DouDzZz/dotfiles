import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HourglassFull: StyledIcon<any>;
export declare const HourglassFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
