import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RestorePage: StyledIcon<any>;
export declare const RestorePageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
