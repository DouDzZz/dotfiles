import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GetApp: StyledIcon<any>;
export declare const GetAppDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
