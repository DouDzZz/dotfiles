import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HighlightOff: StyledIcon<any>;
export declare const HighlightOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
