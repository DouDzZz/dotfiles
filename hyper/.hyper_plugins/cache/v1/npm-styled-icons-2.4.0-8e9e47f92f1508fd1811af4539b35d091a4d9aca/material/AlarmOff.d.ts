import { StyledIcon, StyledIconProps } from '..';
export declare const AlarmOff: StyledIcon<any>;
export declare const AlarmOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
