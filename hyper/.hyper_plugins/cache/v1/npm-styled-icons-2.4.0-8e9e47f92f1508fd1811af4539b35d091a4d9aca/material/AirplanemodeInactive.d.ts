import { StyledIcon, StyledIconProps } from '..';
export declare const AirplanemodeInactive: StyledIcon<any>;
export declare const AirplanemodeInactiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
