import { StyledIcon, StyledIconProps } from '..';
export declare const AssignmentInd: StyledIcon<any>;
export declare const AssignmentIndDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
