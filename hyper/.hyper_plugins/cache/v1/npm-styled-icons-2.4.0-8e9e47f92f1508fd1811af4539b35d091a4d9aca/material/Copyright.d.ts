import { StyledIcon, StyledIconProps } from '..';
export declare const Copyright: StyledIcon<any>;
export declare const CopyrightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
