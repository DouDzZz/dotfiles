import { StyledIcon, StyledIconProps } from '..';
export declare const Timelapse: StyledIcon<any>;
export declare const TimelapseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
