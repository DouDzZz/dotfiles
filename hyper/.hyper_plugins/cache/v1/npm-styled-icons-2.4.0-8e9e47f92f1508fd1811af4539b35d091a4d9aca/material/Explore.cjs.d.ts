import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Explore: StyledIcon<any>;
export declare const ExploreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
