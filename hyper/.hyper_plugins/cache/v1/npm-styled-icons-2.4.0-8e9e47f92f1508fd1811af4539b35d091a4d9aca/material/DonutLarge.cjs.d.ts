import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DonutLarge: StyledIcon<any>;
export declare const DonutLargeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
