import { StyledIcon, StyledIconProps } from '..';
export declare const LaptopWindows: StyledIcon<any>;
export declare const LaptopWindowsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
