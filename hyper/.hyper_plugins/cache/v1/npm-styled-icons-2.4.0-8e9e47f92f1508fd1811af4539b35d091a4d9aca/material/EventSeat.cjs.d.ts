import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EventSeat: StyledIcon<any>;
export declare const EventSeatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
