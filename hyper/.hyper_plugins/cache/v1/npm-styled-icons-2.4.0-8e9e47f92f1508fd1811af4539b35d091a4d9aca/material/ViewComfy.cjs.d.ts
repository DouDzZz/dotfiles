import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewComfy: StyledIcon<any>;
export declare const ViewComfyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
