import { StyledIcon, StyledIconProps } from '..';
export declare const LocalAtm: StyledIcon<any>;
export declare const LocalAtmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
