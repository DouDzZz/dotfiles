import { StyledIcon, StyledIconProps } from '..';
export declare const AssignmentLate: StyledIcon<any>;
export declare const AssignmentLateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
