import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifi2Bar: StyledIcon<any>;
export declare const SignalWifi2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
