import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Timer3: StyledIcon<any>;
export declare const Timer3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
