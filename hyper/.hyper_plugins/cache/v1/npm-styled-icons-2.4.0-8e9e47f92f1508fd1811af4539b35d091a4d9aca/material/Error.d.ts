import { StyledIcon, StyledIconProps } from '..';
export declare const Error: StyledIcon<any>;
export declare const ErrorDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
