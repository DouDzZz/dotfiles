import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BluetoothSearching: StyledIcon<any>;
export declare const BluetoothSearchingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
