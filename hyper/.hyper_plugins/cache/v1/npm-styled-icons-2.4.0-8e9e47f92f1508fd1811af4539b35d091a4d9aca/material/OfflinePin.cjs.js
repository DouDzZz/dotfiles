"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.OfflinePin = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "OfflinePin-title" }, props.title), react_1.default.createElement("path", { d: "M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm5 16H7v-2h10v2zm-6.7-4L7 10.7l1.4-1.4 1.9 1.9 5.3-5.3L17 7.3 10.3 14z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { d: "M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm5 16H7v-2h10v2zm-6.7-4L7 10.7l1.4-1.4 1.9 1.9 5.3-5.3L17 7.3 10.3 14z", key: "k0" })
        ]); },
    viewBox: '0 0 24 24',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: middle;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.OfflinePin.displayName = 'OfflinePin';
exports.OfflinePinDimensions = { height: 24, width: 24 };
var templateObject_1;
