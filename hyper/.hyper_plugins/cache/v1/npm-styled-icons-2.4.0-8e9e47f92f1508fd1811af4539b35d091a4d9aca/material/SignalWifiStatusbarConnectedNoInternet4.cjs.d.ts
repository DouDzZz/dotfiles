import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbarConnectedNoInternet4: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternet4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
