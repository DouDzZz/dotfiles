import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewAgenda: StyledIcon<any>;
export declare const ViewAgendaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
