import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SwapCalls: StyledIcon<any>;
export declare const SwapCallsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
