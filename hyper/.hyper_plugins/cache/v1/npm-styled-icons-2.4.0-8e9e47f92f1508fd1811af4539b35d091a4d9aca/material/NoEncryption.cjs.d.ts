import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NoEncryption: StyledIcon<any>;
export declare const NoEncryptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
