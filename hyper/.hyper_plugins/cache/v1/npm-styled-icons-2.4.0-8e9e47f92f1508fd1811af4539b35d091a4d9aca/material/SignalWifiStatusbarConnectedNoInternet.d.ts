import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarConnectedNoInternet: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
