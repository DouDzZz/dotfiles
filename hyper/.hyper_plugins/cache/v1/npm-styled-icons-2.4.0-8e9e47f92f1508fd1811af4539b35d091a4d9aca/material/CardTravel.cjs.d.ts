import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CardTravel: StyledIcon<any>;
export declare const CardTravelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
