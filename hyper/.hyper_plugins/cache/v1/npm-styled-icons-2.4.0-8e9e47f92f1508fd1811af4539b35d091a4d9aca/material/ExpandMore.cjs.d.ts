import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExpandMore: StyledIcon<any>;
export declare const ExpandMoreDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
