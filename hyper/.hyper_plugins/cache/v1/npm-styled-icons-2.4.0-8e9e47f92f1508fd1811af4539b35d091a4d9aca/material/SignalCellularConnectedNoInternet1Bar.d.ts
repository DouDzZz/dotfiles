import { StyledIcon, StyledIconProps } from '..';
export declare const SignalCellularConnectedNoInternet1Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
