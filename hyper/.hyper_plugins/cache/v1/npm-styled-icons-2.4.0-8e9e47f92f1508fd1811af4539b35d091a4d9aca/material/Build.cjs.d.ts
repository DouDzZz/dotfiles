import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Build: StyledIcon<any>;
export declare const BuildDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
