import { StyledIcon, StyledIconProps } from '..';
export declare const RoomService: StyledIcon<any>;
export declare const RoomServiceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
