import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssignmentTurnedIn: StyledIcon<any>;
export declare const AssignmentTurnedInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
