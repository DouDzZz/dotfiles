import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AirportShuttle: StyledIcon<any>;
export declare const AirportShuttleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
