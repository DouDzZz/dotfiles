import { StyledIcon, StyledIconProps } from '..';
export declare const FormatColorReset: StyledIcon<any>;
export declare const FormatColorResetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
