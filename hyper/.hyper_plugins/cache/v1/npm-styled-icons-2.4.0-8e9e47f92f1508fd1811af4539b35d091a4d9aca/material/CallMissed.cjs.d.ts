import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallMissed: StyledIcon<any>;
export declare const CallMissedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
