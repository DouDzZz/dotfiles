import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsApplications: StyledIcon<any>;
export declare const SettingsApplicationsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
