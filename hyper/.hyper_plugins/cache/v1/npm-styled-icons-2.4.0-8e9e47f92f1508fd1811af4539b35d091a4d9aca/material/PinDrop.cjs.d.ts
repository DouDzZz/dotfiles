import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PinDrop: StyledIcon<any>;
export declare const PinDropDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
