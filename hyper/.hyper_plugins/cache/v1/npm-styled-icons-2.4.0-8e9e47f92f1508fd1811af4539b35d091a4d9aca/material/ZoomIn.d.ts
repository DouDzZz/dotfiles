import { StyledIcon, StyledIconProps } from '..';
export declare const ZoomIn: StyledIcon<any>;
export declare const ZoomInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
