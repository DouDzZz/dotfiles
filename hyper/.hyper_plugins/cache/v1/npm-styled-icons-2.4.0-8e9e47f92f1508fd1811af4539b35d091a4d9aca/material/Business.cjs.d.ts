import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Business: StyledIcon<any>;
export declare const BusinessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
