import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Person: StyledIcon<any>;
export declare const PersonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
