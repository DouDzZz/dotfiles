import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryCharging50: StyledIcon<any>;
export declare const BatteryCharging50Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
