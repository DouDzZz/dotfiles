import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsRun: StyledIcon<any>;
export declare const DirectionsRunDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
