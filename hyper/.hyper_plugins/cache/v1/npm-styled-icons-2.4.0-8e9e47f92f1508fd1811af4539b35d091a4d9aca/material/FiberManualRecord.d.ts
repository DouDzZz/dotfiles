import { StyledIcon, StyledIconProps } from '..';
export declare const FiberManualRecord: StyledIcon<any>;
export declare const FiberManualRecordDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
