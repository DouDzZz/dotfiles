import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Laptop: StyledIcon<any>;
export declare const LaptopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
