import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatAlignRight: StyledIcon<any>;
export declare const FormatAlignRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
