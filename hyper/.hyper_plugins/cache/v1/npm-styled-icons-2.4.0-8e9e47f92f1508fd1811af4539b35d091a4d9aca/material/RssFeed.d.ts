import { StyledIcon, StyledIconProps } from '..';
export declare const RssFeed: StyledIcon<any>;
export declare const RssFeedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
