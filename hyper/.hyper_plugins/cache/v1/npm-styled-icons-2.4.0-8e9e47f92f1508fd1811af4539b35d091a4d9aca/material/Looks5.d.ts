import { StyledIcon, StyledIconProps } from '..';
export declare const Looks5: StyledIcon<any>;
export declare const Looks5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
