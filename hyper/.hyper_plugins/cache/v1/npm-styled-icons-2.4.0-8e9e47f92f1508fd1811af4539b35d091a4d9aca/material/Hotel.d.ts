import { StyledIcon, StyledIconProps } from '..';
export declare const Hotel: StyledIcon<any>;
export declare const HotelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
