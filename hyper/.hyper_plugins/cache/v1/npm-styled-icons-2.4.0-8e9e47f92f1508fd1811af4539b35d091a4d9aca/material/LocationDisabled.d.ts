import { StyledIcon, StyledIconProps } from '..';
export declare const LocationDisabled: StyledIcon<any>;
export declare const LocationDisabledDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
