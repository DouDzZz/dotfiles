import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tablet: StyledIcon<any>;
export declare const TabletDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
