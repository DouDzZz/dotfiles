import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderLeft: StyledIcon<any>;
export declare const BorderLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
