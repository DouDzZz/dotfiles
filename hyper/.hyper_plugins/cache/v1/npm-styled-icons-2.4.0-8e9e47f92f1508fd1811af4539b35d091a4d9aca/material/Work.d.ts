import { StyledIcon, StyledIconProps } from '..';
export declare const Work: StyledIcon<any>;
export declare const WorkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
