import { StyledIcon, StyledIconProps } from '..';
export declare const LocalGasStation: StyledIcon<any>;
export declare const LocalGasStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
