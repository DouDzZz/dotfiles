import { StyledIcon, StyledIconProps } from '..';
export declare const ViewWeek: StyledIcon<any>;
export declare const ViewWeekDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
