import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderHorizontal: StyledIcon<any>;
export declare const BorderHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
