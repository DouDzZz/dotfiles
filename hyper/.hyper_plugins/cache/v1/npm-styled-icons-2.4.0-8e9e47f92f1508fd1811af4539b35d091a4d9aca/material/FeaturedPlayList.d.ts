import { StyledIcon, StyledIconProps } from '..';
export declare const FeaturedPlayList: StyledIcon<any>;
export declare const FeaturedPlayListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
