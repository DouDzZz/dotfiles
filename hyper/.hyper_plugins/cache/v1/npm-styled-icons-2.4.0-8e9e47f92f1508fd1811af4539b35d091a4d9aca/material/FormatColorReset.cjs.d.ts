import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatColorReset: StyledIcon<any>;
export declare const FormatColorResetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
