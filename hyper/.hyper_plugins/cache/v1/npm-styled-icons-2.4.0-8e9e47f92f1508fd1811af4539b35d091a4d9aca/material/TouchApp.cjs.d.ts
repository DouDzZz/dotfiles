import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TouchApp: StyledIcon<any>;
export declare const TouchAppDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
