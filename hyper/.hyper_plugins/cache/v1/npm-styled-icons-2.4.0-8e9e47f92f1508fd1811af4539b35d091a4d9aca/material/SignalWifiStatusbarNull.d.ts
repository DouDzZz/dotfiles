import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarNull: StyledIcon<any>;
export declare const SignalWifiStatusbarNullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
