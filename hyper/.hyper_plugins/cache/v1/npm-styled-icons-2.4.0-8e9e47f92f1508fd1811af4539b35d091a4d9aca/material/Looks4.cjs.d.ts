import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Looks4: StyledIcon<any>;
export declare const Looks4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
