import { StyledIcon, StyledIconProps } from '..';
export declare const MusicNote: StyledIcon<any>;
export declare const MusicNoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
