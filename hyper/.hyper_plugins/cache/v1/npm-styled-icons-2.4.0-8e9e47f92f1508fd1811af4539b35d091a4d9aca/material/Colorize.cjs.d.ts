import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Colorize: StyledIcon<any>;
export declare const ColorizeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
