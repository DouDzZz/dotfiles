import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ErrorOutline: StyledIcon<any>;
export declare const ErrorOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
