import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FlightLand: StyledIcon<any>;
export declare const FlightLandDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
