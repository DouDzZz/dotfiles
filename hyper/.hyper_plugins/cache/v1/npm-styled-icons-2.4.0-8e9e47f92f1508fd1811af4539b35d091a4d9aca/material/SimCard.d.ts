import { StyledIcon, StyledIconProps } from '..';
export declare const SimCard: StyledIcon<any>;
export declare const SimCardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
