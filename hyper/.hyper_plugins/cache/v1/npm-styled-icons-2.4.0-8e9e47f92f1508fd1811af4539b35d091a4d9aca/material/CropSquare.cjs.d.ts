import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CropSquare: StyledIcon<any>;
export declare const CropSquareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
