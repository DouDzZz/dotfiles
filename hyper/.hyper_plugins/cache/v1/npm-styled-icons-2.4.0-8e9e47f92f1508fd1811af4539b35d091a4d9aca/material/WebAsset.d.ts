import { StyledIcon, StyledIconProps } from '..';
export declare const WebAsset: StyledIcon<any>;
export declare const WebAssetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
