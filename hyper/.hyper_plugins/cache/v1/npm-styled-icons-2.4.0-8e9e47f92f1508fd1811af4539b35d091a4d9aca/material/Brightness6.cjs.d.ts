import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Brightness6: StyledIcon<any>;
export declare const Brightness6Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
