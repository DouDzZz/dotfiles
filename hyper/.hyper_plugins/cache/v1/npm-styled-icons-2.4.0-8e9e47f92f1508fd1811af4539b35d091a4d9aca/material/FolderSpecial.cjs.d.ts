import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FolderSpecial: StyledIcon<any>;
export declare const FolderSpecialDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
