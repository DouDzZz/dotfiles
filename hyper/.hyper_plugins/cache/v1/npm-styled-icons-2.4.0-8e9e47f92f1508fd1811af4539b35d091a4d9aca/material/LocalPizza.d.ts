import { StyledIcon, StyledIconProps } from '..';
export declare const LocalPizza: StyledIcon<any>;
export declare const LocalPizzaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
