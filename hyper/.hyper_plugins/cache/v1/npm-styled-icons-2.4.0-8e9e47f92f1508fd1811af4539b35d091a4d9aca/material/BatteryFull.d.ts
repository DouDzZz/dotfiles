import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryFull: StyledIcon<any>;
export declare const BatteryFullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
