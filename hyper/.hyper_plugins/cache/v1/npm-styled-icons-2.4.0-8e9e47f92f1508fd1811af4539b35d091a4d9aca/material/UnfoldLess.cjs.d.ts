import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UnfoldLess: StyledIcon<any>;
export declare const UnfoldLessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
