import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const YoutubeSearchedFor: StyledIcon<any>;
export declare const YoutubeSearchedForDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
