import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardTab: StyledIcon<any>;
export declare const KeyboardTabDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
