import { StyledIcon, StyledIconProps } from '..';
export declare const TabletMac: StyledIcon<any>;
export declare const TabletMacDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
