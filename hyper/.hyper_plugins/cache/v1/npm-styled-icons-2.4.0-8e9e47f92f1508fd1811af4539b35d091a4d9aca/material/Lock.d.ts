import { StyledIcon, StyledIconProps } from '..';
export declare const Lock: StyledIcon<any>;
export declare const LockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
