import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Photo: StyledIcon<any>;
export declare const PhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
