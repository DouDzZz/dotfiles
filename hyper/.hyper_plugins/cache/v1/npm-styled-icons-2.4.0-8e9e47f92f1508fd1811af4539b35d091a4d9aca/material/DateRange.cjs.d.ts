import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DateRange: StyledIcon<any>;
export declare const DateRangeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
