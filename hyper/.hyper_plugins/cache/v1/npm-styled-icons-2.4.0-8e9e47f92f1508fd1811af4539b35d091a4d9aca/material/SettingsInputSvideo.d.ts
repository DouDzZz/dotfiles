import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsInputSvideo: StyledIcon<any>;
export declare const SettingsInputSvideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
