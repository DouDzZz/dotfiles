import { StyledIcon, StyledIconProps } from '..';
export declare const CropRotate: StyledIcon<any>;
export declare const CropRotateDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
