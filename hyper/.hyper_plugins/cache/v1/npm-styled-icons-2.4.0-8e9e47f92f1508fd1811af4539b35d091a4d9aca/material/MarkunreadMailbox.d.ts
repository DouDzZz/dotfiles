import { StyledIcon, StyledIconProps } from '..';
export declare const MarkunreadMailbox: StyledIcon<any>;
export declare const MarkunreadMailboxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
