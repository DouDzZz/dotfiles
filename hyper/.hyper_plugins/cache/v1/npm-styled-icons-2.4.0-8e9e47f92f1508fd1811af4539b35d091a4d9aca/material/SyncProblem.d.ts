import { StyledIcon, StyledIconProps } from '..';
export declare const SyncProblem: StyledIcon<any>;
export declare const SyncProblemDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
