import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotificationsOff: StyledIcon<any>;
export declare const NotificationsOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
