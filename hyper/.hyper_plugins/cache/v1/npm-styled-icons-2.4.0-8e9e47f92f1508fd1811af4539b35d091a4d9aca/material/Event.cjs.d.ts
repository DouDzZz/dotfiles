import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Event: StyledIcon<any>;
export declare const EventDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
