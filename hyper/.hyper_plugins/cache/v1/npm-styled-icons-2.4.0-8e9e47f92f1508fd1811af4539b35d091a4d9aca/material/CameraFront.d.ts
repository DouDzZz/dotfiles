import { StyledIcon, StyledIconProps } from '..';
export declare const CameraFront: StyledIcon<any>;
export declare const CameraFrontDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
