import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DirectionsBike: StyledIcon<any>;
export declare const DirectionsBikeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
