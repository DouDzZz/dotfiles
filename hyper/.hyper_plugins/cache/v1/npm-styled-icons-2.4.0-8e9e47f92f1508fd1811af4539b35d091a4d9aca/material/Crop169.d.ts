import { StyledIcon, StyledIconProps } from '..';
export declare const Crop169: StyledIcon<any>;
export declare const Crop169Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
