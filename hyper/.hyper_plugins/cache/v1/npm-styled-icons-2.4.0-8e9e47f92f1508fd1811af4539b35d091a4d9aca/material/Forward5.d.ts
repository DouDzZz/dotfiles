import { StyledIcon, StyledIconProps } from '..';
export declare const Forward5: StyledIcon<any>;
export declare const Forward5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
