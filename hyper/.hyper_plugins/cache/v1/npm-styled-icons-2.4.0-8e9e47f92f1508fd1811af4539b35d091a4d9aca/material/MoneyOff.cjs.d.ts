import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoneyOff: StyledIcon<any>;
export declare const MoneyOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
