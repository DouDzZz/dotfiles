import { StyledIcon, StyledIconProps } from '..';
export declare const AllInclusive: StyledIcon<any>;
export declare const AllInclusiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
