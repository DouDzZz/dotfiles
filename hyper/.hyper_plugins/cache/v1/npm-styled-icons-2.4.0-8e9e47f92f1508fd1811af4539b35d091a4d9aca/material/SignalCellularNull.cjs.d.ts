import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularNull: StyledIcon<any>;
export declare const SignalCellularNullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
