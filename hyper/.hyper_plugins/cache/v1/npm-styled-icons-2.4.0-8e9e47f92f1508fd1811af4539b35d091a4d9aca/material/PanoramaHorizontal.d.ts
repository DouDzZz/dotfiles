import { StyledIcon, StyledIconProps } from '..';
export declare const PanoramaHorizontal: StyledIcon<any>;
export declare const PanoramaHorizontalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
