import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Email: StyledIcon<any>;
export declare const EmailDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
