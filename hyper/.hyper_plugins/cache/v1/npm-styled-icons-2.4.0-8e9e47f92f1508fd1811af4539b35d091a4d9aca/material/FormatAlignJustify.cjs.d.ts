import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatAlignJustify: StyledIcon<any>;
export declare const FormatAlignJustifyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
