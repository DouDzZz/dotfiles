import { StyledIcon, StyledIconProps } from '..';
export declare const SpeakerGroup: StyledIcon<any>;
export declare const SpeakerGroupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
