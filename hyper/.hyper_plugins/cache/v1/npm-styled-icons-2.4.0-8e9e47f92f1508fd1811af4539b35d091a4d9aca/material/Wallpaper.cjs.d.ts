import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wallpaper: StyledIcon<any>;
export declare const WallpaperDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
