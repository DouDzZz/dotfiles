import { StyledIcon, StyledIconProps } from '..';
export declare const FormatIndentIncrease: StyledIcon<any>;
export declare const FormatIndentIncreaseDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
