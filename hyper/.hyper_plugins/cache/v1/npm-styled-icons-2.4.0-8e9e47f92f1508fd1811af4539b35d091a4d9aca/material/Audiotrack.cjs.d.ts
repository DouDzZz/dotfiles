import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Audiotrack: StyledIcon<any>;
export declare const AudiotrackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
