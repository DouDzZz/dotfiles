import { StyledIcon, StyledIconProps } from '..';
export declare const AddAlarm: StyledIcon<any>;
export declare const AddAlarmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
