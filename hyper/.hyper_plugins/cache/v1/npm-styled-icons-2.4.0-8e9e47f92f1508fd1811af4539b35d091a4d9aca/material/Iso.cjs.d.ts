import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Iso: StyledIcon<any>;
export declare const IsoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
