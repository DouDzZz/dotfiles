import { StyledIcon, StyledIconProps } from '..';
export declare const SubdirectoryArrowLeft: StyledIcon<any>;
export declare const SubdirectoryArrowLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
