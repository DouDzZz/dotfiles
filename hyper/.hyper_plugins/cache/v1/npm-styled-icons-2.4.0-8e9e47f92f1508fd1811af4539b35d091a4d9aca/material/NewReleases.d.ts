import { StyledIcon, StyledIconProps } from '..';
export declare const NewReleases: StyledIcon<any>;
export declare const NewReleasesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
