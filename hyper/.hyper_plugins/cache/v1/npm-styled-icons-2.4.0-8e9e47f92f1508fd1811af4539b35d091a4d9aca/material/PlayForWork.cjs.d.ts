import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayForWork: StyledIcon<any>;
export declare const PlayForWorkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
