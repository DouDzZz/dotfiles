import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Straighten: StyledIcon<any>;
export declare const StraightenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
