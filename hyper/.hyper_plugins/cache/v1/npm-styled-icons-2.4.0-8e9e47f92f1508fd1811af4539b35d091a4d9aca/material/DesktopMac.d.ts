import { StyledIcon, StyledIconProps } from '..';
export declare const DesktopMac: StyledIcon<any>;
export declare const DesktopMacDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
