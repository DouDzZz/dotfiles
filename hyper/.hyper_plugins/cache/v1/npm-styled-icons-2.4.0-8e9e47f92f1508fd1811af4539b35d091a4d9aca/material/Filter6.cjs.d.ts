import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter6: StyledIcon<any>;
export declare const Filter6Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
