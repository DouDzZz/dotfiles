import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GroupAdd: StyledIcon<any>;
export declare const GroupAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
