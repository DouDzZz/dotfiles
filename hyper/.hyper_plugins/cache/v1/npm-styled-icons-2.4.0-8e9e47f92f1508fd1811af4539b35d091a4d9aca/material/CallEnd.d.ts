import { StyledIcon, StyledIconProps } from '..';
export declare const CallEnd: StyledIcon<any>;
export declare const CallEndDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
