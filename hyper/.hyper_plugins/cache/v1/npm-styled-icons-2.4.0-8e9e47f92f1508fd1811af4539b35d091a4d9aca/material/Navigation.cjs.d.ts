import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Navigation: StyledIcon<any>;
export declare const NavigationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
