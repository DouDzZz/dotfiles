import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MicOff: StyledIcon<any>;
export declare const MicOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
