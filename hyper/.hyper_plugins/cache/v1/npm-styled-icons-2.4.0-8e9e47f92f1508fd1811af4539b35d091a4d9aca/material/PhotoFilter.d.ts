import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoFilter: StyledIcon<any>;
export declare const PhotoFilterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
