import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Alarm: StyledIcon<any>;
export declare const AlarmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
