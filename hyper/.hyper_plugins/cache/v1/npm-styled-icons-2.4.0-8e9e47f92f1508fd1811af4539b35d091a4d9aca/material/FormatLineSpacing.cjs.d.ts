import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FormatLineSpacing: StyledIcon<any>;
export declare const FormatLineSpacingDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
