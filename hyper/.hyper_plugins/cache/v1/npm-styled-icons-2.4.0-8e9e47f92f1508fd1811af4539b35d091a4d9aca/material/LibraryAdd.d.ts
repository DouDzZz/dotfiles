import { StyledIcon, StyledIconProps } from '..';
export declare const LibraryAdd: StyledIcon<any>;
export declare const LibraryAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
