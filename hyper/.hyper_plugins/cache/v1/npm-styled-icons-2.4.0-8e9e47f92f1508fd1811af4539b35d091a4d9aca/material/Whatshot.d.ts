import { StyledIcon, StyledIconProps } from '..';
export declare const Whatshot: StyledIcon<any>;
export declare const WhatshotDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
