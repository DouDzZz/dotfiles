import { StyledIcon, StyledIconProps } from '..';
export declare const AirlineSeatFlat: StyledIcon<any>;
export declare const AirlineSeatFlatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
