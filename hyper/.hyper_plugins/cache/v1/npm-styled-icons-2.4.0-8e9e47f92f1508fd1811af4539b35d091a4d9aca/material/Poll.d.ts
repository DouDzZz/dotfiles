import { StyledIcon, StyledIconProps } from '..';
export declare const Poll: StyledIcon<any>;
export declare const PollDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
