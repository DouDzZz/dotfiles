import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MonetizationOn: StyledIcon<any>;
export declare const MonetizationOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
