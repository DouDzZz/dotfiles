import { StyledIcon, StyledIconProps } from '..';
export declare const VpnKey: StyledIcon<any>;
export declare const VpnKeyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
