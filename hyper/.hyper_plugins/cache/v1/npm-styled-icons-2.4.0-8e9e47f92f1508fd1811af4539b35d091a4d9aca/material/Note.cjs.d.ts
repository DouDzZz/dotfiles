import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Note: StyledIcon<any>;
export declare const NoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
