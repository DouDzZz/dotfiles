import { StyledIcon, StyledIconProps } from '..';
export declare const PhotoCamera: StyledIcon<any>;
export declare const PhotoCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
