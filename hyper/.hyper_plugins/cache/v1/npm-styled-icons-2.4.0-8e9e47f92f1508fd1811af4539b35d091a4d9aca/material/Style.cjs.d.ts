import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Style: StyledIcon<any>;
export declare const StyleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
