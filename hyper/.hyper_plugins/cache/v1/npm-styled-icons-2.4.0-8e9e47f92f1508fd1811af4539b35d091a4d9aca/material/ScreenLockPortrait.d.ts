import { StyledIcon, StyledIconProps } from '..';
export declare const ScreenLockPortrait: StyledIcon<any>;
export declare const ScreenLockPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
