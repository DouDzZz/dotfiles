import { StyledIcon, StyledIconProps } from '..';
export declare const CloudQueue: StyledIcon<any>;
export declare const CloudQueueDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
