import { StyledIcon, StyledIconProps } from '..';
export declare const NotificationsActive: StyledIcon<any>;
export declare const NotificationsActiveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
