import { StyledIcon, StyledIconProps } from '..';
export declare const InsertEmoticon: StyledIcon<any>;
export declare const InsertEmoticonDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
