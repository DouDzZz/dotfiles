import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Add: StyledIcon<any>;
export declare const AddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
