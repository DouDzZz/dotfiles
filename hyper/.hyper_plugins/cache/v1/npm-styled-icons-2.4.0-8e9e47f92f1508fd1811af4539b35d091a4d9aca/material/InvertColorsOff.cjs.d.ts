import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InvertColorsOff: StyledIcon<any>;
export declare const InvertColorsOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
