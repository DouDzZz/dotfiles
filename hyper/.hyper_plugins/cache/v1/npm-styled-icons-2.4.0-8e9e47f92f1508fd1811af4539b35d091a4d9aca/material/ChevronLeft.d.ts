import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronLeft: StyledIcon<any>;
export declare const ChevronLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
