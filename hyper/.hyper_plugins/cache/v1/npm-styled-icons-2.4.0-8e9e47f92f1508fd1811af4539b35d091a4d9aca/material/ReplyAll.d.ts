import { StyledIcon, StyledIconProps } from '..';
export declare const ReplyAll: StyledIcon<any>;
export declare const ReplyAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
