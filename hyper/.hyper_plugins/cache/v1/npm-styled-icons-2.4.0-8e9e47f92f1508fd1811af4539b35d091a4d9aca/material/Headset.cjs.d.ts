import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Headset: StyledIcon<any>;
export declare const HeadsetDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
