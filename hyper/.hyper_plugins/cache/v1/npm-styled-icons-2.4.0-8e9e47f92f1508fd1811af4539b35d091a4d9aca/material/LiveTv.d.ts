import { StyledIcon, StyledIconProps } from '..';
export declare const LiveTv: StyledIcon<any>;
export declare const LiveTvDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
