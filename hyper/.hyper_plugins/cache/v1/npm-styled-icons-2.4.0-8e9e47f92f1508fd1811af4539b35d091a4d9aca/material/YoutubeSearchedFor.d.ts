import { StyledIcon, StyledIconProps } from '..';
export declare const YoutubeSearchedFor: StyledIcon<any>;
export declare const YoutubeSearchedForDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
