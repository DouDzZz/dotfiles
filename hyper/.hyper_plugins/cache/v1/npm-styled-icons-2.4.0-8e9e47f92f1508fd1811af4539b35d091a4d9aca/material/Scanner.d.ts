import { StyledIcon, StyledIconProps } from '..';
export declare const Scanner: StyledIcon<any>;
export declare const ScannerDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
