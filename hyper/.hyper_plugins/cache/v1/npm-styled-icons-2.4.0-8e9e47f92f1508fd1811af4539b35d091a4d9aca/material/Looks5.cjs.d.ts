import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Looks5: StyledIcon<any>;
export declare const Looks5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
