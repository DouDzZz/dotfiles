import { StyledIcon, StyledIconProps } from '..';
export declare const Explicit: StyledIcon<any>;
export declare const ExplicitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
