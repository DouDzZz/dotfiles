import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Looks: StyledIcon<any>;
export declare const LooksDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
