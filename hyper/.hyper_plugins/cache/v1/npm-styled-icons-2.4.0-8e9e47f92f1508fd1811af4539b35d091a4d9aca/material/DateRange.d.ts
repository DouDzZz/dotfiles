import { StyledIcon, StyledIconProps } from '..';
export declare const DateRange: StyledIcon<any>;
export declare const DateRangeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
