import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhotoSizeSelectActual: StyledIcon<any>;
export declare const PhotoSizeSelectActualDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
