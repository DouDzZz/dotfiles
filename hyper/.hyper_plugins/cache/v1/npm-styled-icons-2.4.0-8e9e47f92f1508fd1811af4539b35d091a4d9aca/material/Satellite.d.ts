import { StyledIcon, StyledIconProps } from '..';
export declare const Satellite: StyledIcon<any>;
export declare const SatelliteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
