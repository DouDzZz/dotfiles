import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewArray: StyledIcon<any>;
export declare const ViewArrayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
