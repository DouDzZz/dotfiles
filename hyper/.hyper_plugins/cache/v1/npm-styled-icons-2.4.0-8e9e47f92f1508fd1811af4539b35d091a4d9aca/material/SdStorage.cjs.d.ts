import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SdStorage: StyledIcon<any>;
export declare const SdStorageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
