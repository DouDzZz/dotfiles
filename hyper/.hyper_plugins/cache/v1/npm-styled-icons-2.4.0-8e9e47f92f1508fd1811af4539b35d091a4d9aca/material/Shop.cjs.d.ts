import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Shop: StyledIcon<any>;
export declare const ShopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
