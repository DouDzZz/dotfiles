import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VerticalAlignCenter: StyledIcon<any>;
export declare const VerticalAlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
