import { StyledIcon, StyledIconProps } from '..';
export declare const LockOutline: StyledIcon<any>;
export declare const LockOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
