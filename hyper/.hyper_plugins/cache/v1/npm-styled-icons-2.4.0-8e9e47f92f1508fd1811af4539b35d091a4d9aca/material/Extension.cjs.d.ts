import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Extension: StyledIcon<any>;
export declare const ExtensionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
