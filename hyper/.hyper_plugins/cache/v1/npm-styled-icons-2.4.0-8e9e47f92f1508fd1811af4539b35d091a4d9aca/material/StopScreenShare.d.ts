import { StyledIcon, StyledIconProps } from '..';
export declare const StopScreenShare: StyledIcon<any>;
export declare const StopScreenShareDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
