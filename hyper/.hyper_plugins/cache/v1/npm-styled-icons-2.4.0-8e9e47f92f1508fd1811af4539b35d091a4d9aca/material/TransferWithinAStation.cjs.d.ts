import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TransferWithinAStation: StyledIcon<any>;
export declare const TransferWithinAStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
