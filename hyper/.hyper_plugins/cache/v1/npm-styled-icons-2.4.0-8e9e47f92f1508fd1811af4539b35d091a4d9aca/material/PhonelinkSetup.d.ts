import { StyledIcon, StyledIconProps } from '..';
export declare const PhonelinkSetup: StyledIcon<any>;
export declare const PhonelinkSetupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
