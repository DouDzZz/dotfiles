import { StyledIcon, StyledIconProps } from '..';
export declare const Grain: StyledIcon<any>;
export declare const GrainDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
