import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Widgets: StyledIcon<any>;
export declare const WidgetsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
