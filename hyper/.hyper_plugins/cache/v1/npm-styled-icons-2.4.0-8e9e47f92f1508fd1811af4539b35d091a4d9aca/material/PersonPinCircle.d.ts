import { StyledIcon, StyledIconProps } from '..';
export declare const PersonPinCircle: StyledIcon<any>;
export declare const PersonPinCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
