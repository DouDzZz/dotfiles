import { StyledIcon, StyledIconProps } from '..';
export declare const Brightness5: StyledIcon<any>;
export declare const Brightness5Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
