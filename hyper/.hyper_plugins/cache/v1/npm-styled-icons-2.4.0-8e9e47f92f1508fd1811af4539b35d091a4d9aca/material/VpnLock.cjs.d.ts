import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VpnLock: StyledIcon<any>;
export declare const VpnLockDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
