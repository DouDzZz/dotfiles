import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TextFormat: StyledIcon<any>;
export declare const TextFormatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
