import { StyledIcon, StyledIconProps } from '..';
export declare const Vibration: StyledIcon<any>;
export declare const VibrationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
