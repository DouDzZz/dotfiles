import { StyledIcon, StyledIconProps } from '..';
export declare const AddLocation: StyledIcon<any>;
export declare const AddLocationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
