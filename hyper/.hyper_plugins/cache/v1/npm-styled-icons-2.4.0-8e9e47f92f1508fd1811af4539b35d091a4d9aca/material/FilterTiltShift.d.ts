import { StyledIcon, StyledIconProps } from '..';
export declare const FilterTiltShift: StyledIcon<any>;
export declare const FilterTiltShiftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
