import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RemoveRedEye: StyledIcon<any>;
export declare const RemoveRedEyeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
