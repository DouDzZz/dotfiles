import { StyledIcon, StyledIconProps } from '..';
export declare const AddCircle: StyledIcon<any>;
export declare const AddCircleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
