import { StyledIcon, StyledIconProps } from '..';
export declare const ImportantDevices: StyledIcon<any>;
export declare const ImportantDevicesDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
