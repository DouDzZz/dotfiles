import { StyledIcon, StyledIconProps } from '..';
export declare const FilterList: StyledIcon<any>;
export declare const FilterListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
