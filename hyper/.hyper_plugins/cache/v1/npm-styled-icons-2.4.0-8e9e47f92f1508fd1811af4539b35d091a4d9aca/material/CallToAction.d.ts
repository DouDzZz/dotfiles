import { StyledIcon, StyledIconProps } from '..';
export declare const CallToAction: StyledIcon<any>;
export declare const CallToActionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
