import { StyledIcon, StyledIconProps } from '..';
export declare const StayCurrentLandscape: StyledIcon<any>;
export declare const StayCurrentLandscapeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
