import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PeopleOutline: StyledIcon<any>;
export declare const PeopleOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
