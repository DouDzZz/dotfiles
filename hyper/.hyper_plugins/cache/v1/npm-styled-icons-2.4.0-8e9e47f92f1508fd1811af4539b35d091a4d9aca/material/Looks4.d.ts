import { StyledIcon, StyledIconProps } from '..';
export declare const Looks4: StyledIcon<any>;
export declare const Looks4Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
