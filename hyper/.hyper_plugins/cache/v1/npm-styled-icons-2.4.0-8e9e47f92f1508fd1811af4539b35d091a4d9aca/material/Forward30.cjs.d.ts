import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Forward30: StyledIcon<any>;
export declare const Forward30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
