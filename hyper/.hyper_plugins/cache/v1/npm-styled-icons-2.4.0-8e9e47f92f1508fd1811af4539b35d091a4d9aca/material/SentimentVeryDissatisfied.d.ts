import { StyledIcon, StyledIconProps } from '..';
export declare const SentimentVeryDissatisfied: StyledIcon<any>;
export declare const SentimentVeryDissatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
