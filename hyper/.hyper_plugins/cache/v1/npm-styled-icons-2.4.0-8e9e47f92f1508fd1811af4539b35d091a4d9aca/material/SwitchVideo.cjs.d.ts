import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SwitchVideo: StyledIcon<any>;
export declare const SwitchVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
