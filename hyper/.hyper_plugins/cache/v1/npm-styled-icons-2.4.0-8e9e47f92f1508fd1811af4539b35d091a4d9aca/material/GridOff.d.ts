import { StyledIcon, StyledIconProps } from '..';
export declare const GridOff: StyledIcon<any>;
export declare const GridOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
