import { StyledIcon, StyledIconProps } from '..';
export declare const HdrOn: StyledIcon<any>;
export declare const HdrOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
