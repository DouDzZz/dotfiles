import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ImageAspectRatio: StyledIcon<any>;
export declare const ImageAspectRatioDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
