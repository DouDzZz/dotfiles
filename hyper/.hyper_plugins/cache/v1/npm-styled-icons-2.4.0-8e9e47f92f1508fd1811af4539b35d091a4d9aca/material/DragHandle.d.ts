import { StyledIcon, StyledIconProps } from '..';
export declare const DragHandle: StyledIcon<any>;
export declare const DragHandleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
