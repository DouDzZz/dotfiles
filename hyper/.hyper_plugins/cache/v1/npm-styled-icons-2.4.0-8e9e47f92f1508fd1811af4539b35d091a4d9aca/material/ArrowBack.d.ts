import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowBack: StyledIcon<any>;
export declare const ArrowBackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
