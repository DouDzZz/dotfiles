import { StyledIcon, StyledIconProps } from '..';
export declare const FastForward: StyledIcon<any>;
export declare const FastForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
