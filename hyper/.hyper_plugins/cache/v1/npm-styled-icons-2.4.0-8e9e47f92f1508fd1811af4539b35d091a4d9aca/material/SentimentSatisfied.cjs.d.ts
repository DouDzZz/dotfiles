import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SentimentSatisfied: StyledIcon<any>;
export declare const SentimentSatisfiedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
