import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const OndemandVideo: StyledIcon<any>;
export declare const OndemandVideoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
