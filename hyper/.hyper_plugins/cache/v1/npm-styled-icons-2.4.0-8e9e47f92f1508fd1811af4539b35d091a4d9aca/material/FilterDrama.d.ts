import { StyledIcon, StyledIconProps } from '..';
export declare const FilterDrama: StyledIcon<any>;
export declare const FilterDramaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
