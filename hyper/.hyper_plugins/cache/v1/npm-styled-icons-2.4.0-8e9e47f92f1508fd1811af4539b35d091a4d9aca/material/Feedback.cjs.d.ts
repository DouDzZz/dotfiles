import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Feedback: StyledIcon<any>;
export declare const FeedbackDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
