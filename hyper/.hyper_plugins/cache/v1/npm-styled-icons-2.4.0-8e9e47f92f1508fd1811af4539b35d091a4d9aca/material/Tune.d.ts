import { StyledIcon, StyledIconProps } from '..';
export declare const Tune: StyledIcon<any>;
export declare const TuneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
