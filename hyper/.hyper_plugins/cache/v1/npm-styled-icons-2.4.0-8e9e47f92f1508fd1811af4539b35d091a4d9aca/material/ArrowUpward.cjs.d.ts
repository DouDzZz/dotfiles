import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowUpward: StyledIcon<any>;
export declare const ArrowUpwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
