import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RotateLeft: StyledIcon<any>;
export declare const RotateLeftDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
