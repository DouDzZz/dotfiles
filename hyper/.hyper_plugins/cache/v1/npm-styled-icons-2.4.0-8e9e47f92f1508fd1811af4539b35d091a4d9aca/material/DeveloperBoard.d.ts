import { StyledIcon, StyledIconProps } from '..';
export declare const DeveloperBoard: StyledIcon<any>;
export declare const DeveloperBoardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
