import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewHeadline: StyledIcon<any>;
export declare const ViewHeadlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
