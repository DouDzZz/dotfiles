import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Chat: StyledIcon<any>;
export declare const ChatDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
