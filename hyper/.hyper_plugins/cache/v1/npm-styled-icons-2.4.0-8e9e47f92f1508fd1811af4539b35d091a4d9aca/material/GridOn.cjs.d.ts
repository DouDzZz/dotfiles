import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GridOn: StyledIcon<any>;
export declare const GridOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
