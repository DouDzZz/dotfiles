import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderRight: StyledIcon<any>;
export declare const BorderRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
