import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EventBusy: StyledIcon<any>;
export declare const EventBusyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
