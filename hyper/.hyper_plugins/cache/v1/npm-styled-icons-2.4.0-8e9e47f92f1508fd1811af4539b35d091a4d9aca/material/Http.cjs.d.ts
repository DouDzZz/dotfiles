import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Http: StyledIcon<any>;
export declare const HttpDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
