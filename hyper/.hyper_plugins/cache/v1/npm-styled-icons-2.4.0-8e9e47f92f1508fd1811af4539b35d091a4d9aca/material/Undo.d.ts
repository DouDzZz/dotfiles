import { StyledIcon, StyledIconProps } from '..';
export declare const Undo: StyledIcon<any>;
export declare const UndoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
