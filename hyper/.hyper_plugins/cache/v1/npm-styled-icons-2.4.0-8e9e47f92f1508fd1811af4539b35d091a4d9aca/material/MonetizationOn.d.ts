import { StyledIcon, StyledIconProps } from '..';
export declare const MonetizationOn: StyledIcon<any>;
export declare const MonetizationOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
