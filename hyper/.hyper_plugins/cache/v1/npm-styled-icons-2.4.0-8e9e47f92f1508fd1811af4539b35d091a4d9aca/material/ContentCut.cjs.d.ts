import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ContentCut: StyledIcon<any>;
export declare const ContentCutDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
