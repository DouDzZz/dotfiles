import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DeleteSweep: StyledIcon<any>;
export declare const DeleteSweepDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
