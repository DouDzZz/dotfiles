import { StyledIcon, StyledIconProps } from '..';
export declare const FormatQuote: StyledIcon<any>;
export declare const FormatQuoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
