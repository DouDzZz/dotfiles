import { StyledIcon, StyledIconProps } from '..';
export declare const ExposureNeg2: StyledIcon<any>;
export declare const ExposureNeg2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
