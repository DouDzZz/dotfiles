import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WbSunny: StyledIcon<any>;
export declare const WbSunnyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
