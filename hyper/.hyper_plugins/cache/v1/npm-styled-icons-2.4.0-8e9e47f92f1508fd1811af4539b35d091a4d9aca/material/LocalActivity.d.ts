import { StyledIcon, StyledIconProps } from '..';
export declare const LocalActivity: StyledIcon<any>;
export declare const LocalActivityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
