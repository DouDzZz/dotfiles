import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SwapVert: StyledIcon<any>;
export declare const SwapVertDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
