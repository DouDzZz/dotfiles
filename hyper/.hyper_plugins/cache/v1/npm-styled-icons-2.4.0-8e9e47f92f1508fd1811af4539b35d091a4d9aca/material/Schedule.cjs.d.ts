import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Schedule: StyledIcon<any>;
export declare const ScheduleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
