import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Markunread: StyledIcon<any>;
export declare const MarkunreadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
