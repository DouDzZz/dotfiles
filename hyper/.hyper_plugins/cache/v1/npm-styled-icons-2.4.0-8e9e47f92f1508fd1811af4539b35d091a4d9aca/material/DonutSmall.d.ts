import { StyledIcon, StyledIconProps } from '..';
export declare const DonutSmall: StyledIcon<any>;
export declare const DonutSmallDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
