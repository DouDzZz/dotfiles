import { StyledIcon, StyledIconProps } from '..';
export declare const Watch: StyledIcon<any>;
export declare const WatchDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
