import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SpaceBar: StyledIcon<any>;
export declare const SpaceBarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
