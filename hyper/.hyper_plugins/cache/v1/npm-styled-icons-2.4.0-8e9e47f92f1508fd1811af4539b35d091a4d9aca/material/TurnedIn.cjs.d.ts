import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TurnedIn: StyledIcon<any>;
export declare const TurnedInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
