import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LightbulbOutline: StyledIcon<any>;
export declare const LightbulbOutlineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
