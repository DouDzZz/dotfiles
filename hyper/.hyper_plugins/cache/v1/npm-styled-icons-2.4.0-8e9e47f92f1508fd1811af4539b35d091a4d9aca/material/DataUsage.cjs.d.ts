import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DataUsage: StyledIcon<any>;
export declare const DataUsageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
