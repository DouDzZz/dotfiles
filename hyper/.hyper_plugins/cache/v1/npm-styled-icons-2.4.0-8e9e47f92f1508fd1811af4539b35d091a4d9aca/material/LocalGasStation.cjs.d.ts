import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalGasStation: StyledIcon<any>;
export declare const LocalGasStationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
