import { StyledIcon, StyledIconProps } from '..';
export declare const FlashOn: StyledIcon<any>;
export declare const FlashOnDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
