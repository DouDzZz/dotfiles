import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fullscreen: StyledIcon<any>;
export declare const FullscreenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
