import { StyledIcon, StyledIconProps } from '..';
export declare const UnfoldLess: StyledIcon<any>;
export declare const UnfoldLessDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
