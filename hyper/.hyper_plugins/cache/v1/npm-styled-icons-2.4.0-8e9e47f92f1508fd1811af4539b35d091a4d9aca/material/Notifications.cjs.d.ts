import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Notifications: StyledIcon<any>;
export declare const NotificationsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
