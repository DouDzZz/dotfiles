import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Language: StyledIcon<any>;
export declare const LanguageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
