import { StyledIcon, StyledIconProps } from '..';
export declare const WifiTethering: StyledIcon<any>;
export declare const WifiTetheringDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
