import { StyledIcon, StyledIconProps } from '..';
export declare const Markunread: StyledIcon<any>;
export declare const MarkunreadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
