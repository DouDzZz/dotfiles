import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbar2Bar: StyledIcon<any>;
export declare const SignalWifiStatusbar2BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
