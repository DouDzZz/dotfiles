import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Title: StyledIcon<any>;
export declare const TitleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
