import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarConnectedNoInternet2: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternet2Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
