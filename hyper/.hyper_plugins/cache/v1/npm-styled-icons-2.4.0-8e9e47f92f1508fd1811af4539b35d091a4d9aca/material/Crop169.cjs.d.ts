import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crop169: StyledIcon<any>;
export declare const Crop169Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
