import { StyledIcon, StyledIconProps } from '..';
export declare const Note: StyledIcon<any>;
export declare const NoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
