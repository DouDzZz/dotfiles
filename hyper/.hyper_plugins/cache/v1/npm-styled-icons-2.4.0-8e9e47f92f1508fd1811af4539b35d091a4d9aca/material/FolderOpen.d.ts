import { StyledIcon, StyledIconProps } from '..';
export declare const FolderOpen: StyledIcon<any>;
export declare const FolderOpenDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
