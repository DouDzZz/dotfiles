import { StyledIcon, StyledIconProps } from '..';
export declare const Apps: StyledIcon<any>;
export declare const AppsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
