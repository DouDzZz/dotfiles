import { StyledIcon, StyledIconProps } from '..';
export declare const Today: StyledIcon<any>;
export declare const TodayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
