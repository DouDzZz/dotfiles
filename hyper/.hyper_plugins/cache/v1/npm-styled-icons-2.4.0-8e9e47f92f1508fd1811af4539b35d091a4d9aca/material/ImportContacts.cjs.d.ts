import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ImportContacts: StyledIcon<any>;
export declare const ImportContactsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
