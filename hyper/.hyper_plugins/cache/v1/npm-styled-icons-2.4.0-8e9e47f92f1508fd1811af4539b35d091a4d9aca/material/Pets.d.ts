import { StyledIcon, StyledIconProps } from '..';
export declare const Pets: StyledIcon<any>;
export declare const PetsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
