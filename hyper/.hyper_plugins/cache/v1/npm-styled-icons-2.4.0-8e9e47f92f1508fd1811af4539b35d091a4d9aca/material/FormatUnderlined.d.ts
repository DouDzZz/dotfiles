import { StyledIcon, StyledIconProps } from '..';
export declare const FormatUnderlined: StyledIcon<any>;
export declare const FormatUnderlinedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
