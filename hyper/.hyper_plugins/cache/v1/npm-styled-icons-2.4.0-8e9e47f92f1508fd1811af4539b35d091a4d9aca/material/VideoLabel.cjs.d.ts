import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideoLabel: StyledIcon<any>;
export declare const VideoLabelDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
