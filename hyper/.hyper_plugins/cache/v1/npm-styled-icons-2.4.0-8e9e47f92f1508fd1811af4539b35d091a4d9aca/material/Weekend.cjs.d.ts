import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Weekend: StyledIcon<any>;
export declare const WeekendDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
