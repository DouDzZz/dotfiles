import { StyledIcon, StyledIconProps } from '..';
export declare const Mood: StyledIcon<any>;
export declare const MoodDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
