import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gif: StyledIcon<any>;
export declare const GifDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
