import { StyledIcon, StyledIconProps } from '..';
export declare const KeyboardHide: StyledIcon<any>;
export declare const KeyboardHideDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
