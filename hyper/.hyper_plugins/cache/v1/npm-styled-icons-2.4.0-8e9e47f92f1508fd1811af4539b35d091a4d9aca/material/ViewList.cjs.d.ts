import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewList: StyledIcon<any>;
export declare const ViewListDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
