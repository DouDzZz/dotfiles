import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KeyboardArrowDown: StyledIcon<any>;
export declare const KeyboardArrowDownDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
