import { StyledIcon, StyledIconProps } from '..';
export declare const SpeakerPhone: StyledIcon<any>;
export declare const SpeakerPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
