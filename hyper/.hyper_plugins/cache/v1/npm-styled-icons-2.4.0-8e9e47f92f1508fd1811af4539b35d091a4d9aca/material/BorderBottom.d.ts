import { StyledIcon, StyledIconProps } from '..';
export declare const BorderBottom: StyledIcon<any>;
export declare const BorderBottomDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
