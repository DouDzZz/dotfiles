import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsInputAntenna: StyledIcon<any>;
export declare const SettingsInputAntennaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
