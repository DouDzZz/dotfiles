import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CallReceived: StyledIcon<any>;
export declare const CallReceivedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
