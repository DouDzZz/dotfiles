import { StyledIcon, StyledIconProps } from '..';
export declare const Redo: StyledIcon<any>;
export declare const RedoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
