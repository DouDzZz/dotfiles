import { StyledIcon, StyledIconProps } from '..';
export declare const Storage: StyledIcon<any>;
export declare const StorageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
