import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsBluetooth: StyledIcon<any>;
export declare const SettingsBluetoothDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
