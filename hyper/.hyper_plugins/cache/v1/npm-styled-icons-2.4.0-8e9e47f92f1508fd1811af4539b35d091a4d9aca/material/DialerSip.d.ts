import { StyledIcon, StyledIconProps } from '..';
export declare const DialerSip: StyledIcon<any>;
export declare const DialerSipDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
