import { StyledIcon, StyledIconProps } from '..';
export declare const FormatAlignRight: StyledIcon<any>;
export declare const FormatAlignRightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
