import { StyledIcon, StyledIconProps } from '..';
export declare const LocalSee: StyledIcon<any>;
export declare const LocalSeeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
