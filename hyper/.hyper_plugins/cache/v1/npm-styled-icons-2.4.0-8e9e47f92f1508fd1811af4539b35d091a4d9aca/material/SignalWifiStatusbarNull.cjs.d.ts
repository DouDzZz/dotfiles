import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalWifiStatusbarNull: StyledIcon<any>;
export declare const SignalWifiStatusbarNullDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
