import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotInterested: StyledIcon<any>;
export declare const NotInterestedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
