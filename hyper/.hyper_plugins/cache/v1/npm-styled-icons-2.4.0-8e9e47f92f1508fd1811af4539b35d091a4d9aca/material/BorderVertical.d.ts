import { StyledIcon, StyledIconProps } from '..';
export declare const BorderVertical: StyledIcon<any>;
export declare const BorderVerticalDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
