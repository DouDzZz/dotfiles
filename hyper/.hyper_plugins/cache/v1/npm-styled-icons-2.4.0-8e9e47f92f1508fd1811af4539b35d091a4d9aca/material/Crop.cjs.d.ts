import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crop: StyledIcon<any>;
export declare const CropDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
