import { StyledIcon, StyledIconProps } from '..';
export declare const Notifications: StyledIcon<any>;
export declare const NotificationsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
