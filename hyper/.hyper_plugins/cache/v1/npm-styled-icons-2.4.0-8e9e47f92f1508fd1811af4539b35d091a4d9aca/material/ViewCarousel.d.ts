import { StyledIcon, StyledIconProps } from '..';
export declare const ViewCarousel: StyledIcon<any>;
export declare const ViewCarouselDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
