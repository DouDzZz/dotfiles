import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subject: StyledIcon<any>;
export declare const SubjectDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
