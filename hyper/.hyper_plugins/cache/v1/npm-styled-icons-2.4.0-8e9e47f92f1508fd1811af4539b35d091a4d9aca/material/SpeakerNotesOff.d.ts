import { StyledIcon, StyledIconProps } from '..';
export declare const SpeakerNotesOff: StyledIcon<any>;
export declare const SpeakerNotesOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
