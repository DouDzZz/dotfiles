import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LibraryBooks: StyledIcon<any>;
export declare const LibraryBooksDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
