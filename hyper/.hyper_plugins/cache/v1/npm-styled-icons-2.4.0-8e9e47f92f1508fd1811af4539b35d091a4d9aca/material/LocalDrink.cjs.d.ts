import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalDrink: StyledIcon<any>;
export declare const LocalDrinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
