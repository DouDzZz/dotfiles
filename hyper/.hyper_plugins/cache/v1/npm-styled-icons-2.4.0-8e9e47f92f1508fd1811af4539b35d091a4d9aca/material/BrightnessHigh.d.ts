import { StyledIcon, StyledIconProps } from '..';
export declare const BrightnessHigh: StyledIcon<any>;
export declare const BrightnessHighDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
