import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AssistantPhoto: StyledIcon<any>;
export declare const AssistantPhotoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
