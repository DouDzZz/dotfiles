import { StyledIcon, StyledIconProps } from '..';
export declare const Print: StyledIcon<any>;
export declare const PrintDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
