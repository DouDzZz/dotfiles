import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Timeline: StyledIcon<any>;
export declare const TimelineDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
