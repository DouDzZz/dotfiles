import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IndeterminateCheckBox: StyledIcon<any>;
export declare const IndeterminateCheckBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
