import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Brush: StyledIcon<any>;
export declare const BrushDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
