import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MusicNote: StyledIcon<any>;
export declare const MusicNoteDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
