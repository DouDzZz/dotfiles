import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SmsFailed: StyledIcon<any>;
export declare const SmsFailedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
