import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PartyMode: StyledIcon<any>;
export declare const PartyModeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
