import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Brightness7: StyledIcon<any>;
export declare const Brightness7Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
