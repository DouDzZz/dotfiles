import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AccessAlarms: StyledIcon<any>;
export declare const AccessAlarmsDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
