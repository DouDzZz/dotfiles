import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PriorityHigh: StyledIcon<any>;
export declare const PriorityHighDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
