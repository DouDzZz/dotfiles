import { StyledIcon, StyledIconProps } from '..';
export declare const Alarm: StyledIcon<any>;
export declare const AlarmDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
