import { StyledIcon, StyledIconProps } from '..';
export declare const LocalPrintshop: StyledIcon<any>;
export declare const LocalPrintshopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
