import { StyledIcon, StyledIconProps } from '..';
export declare const Language: StyledIcon<any>;
export declare const LanguageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
