import { StyledIcon, StyledIconProps } from '..';
export declare const FlightLand: StyledIcon<any>;
export declare const FlightLandDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
