import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Redeem: StyledIcon<any>;
export declare const RedeemDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
