import { StyledIcon, StyledIconProps } from '..';
export declare const NotInterested: StyledIcon<any>;
export declare const NotInterestedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
