import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StayPrimaryPortrait: StyledIcon<any>;
export declare const StayPrimaryPortraitDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
