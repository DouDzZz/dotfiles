import { StyledIcon, StyledIconProps } from '..';
export declare const NetworkWifi: StyledIcon<any>;
export declare const NetworkWifiDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
