import { StyledIcon, StyledIconProps } from '..';
export declare const RestaurantMenu: StyledIcon<any>;
export declare const RestaurantMenuDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
