import { StyledIcon, StyledIconProps } from '..';
export declare const FormatAlignCenter: StyledIcon<any>;
export declare const FormatAlignCenterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
