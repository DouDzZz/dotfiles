import { StyledIcon, StyledIconProps } from '..';
export declare const Dashboard: StyledIcon<any>;
export declare const DashboardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
