import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellular3Bar: StyledIcon<any>;
export declare const SignalCellular3BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
