import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewDay: StyledIcon<any>;
export declare const ViewDayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
