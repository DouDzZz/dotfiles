import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowForward: StyledIcon<any>;
export declare const ArrowForwardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
