import { StyledIcon, StyledIconProps } from '..';
export declare const Phone: StyledIcon<any>;
export declare const PhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
