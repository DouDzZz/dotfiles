import { StyledIcon, StyledIconProps } from '..';
export declare const Pool: StyledIcon<any>;
export declare const PoolDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
