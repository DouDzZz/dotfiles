import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ViewCompact: StyledIcon<any>;
export declare const ViewCompactDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
