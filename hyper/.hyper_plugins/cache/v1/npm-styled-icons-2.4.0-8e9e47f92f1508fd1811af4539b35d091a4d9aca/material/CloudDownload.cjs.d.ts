import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudDownload: StyledIcon<any>;
export declare const CloudDownloadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
