import { StyledIcon, StyledIconProps } from '..';
export declare const AccessTime: StyledIcon<any>;
export declare const AccessTimeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
