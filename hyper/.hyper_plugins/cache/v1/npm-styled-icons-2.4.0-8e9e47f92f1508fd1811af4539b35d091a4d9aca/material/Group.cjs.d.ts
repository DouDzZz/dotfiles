import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Group: StyledIcon<any>;
export declare const GroupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
