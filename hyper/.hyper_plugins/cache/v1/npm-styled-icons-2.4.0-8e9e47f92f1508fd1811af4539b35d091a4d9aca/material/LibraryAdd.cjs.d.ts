import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LibraryAdd: StyledIcon<any>;
export declare const LibraryAddDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
