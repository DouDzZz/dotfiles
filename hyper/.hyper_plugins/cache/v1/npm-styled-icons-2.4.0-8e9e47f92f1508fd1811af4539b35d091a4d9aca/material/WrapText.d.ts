import { StyledIcon, StyledIconProps } from '..';
export declare const WrapText: StyledIcon<any>;
export declare const WrapTextDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
