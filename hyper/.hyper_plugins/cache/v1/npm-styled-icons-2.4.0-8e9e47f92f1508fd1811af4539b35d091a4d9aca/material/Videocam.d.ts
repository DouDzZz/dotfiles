import { StyledIcon, StyledIconProps } from '..';
export declare const Videocam: StyledIcon<any>;
export declare const VideocamDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
