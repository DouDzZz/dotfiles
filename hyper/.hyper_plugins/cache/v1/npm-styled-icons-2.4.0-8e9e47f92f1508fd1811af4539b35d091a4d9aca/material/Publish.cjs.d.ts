import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Publish: StyledIcon<any>;
export declare const PublishDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
