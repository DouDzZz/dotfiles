import { StyledIcon, StyledIconProps } from '..';
export declare const Sort: StyledIcon<any>;
export declare const SortDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
