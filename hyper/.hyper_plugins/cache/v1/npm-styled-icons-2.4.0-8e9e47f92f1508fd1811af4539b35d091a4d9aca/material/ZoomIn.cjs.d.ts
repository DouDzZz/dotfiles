import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ZoomIn: StyledIcon<any>;
export declare const ZoomInDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
