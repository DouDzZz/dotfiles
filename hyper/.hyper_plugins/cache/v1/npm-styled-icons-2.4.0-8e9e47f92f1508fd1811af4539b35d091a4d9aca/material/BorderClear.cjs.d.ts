import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BorderClear: StyledIcon<any>;
export declare const BorderClearDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
