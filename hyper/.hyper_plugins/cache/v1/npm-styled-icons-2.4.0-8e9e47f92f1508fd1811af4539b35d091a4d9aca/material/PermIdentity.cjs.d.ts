import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PermIdentity: StyledIcon<any>;
export declare const PermIdentityDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
