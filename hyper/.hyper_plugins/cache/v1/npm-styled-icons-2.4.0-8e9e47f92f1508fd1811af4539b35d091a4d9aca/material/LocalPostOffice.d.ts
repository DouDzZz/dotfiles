import { StyledIcon, StyledIconProps } from '..';
export declare const LocalPostOffice: StyledIcon<any>;
export declare const LocalPostOfficeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
