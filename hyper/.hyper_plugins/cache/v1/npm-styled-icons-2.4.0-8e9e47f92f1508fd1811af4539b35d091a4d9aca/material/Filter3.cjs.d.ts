import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter3: StyledIcon<any>;
export declare const Filter3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
