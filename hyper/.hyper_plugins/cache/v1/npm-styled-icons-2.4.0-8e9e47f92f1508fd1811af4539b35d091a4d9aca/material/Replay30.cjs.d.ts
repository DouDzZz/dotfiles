import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Replay30: StyledIcon<any>;
export declare const Replay30Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
