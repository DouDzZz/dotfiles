import { StyledIcon, StyledIconProps } from '..';
export declare const SettingsInputComposite: StyledIcon<any>;
export declare const SettingsInputCompositeDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
