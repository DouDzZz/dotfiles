import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPizza: StyledIcon<any>;
export declare const LocalPizzaDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
