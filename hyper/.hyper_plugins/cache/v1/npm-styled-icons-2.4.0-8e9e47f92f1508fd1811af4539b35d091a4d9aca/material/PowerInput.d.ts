import { StyledIcon, StyledIconProps } from '..';
export declare const PowerInput: StyledIcon<any>;
export declare const PowerInputDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
