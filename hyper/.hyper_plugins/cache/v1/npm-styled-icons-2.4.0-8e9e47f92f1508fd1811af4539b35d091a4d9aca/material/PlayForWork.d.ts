import { StyledIcon, StyledIconProps } from '..';
export declare const PlayForWork: StyledIcon<any>;
export declare const PlayForWorkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
