import { StyledIcon, StyledIconProps } from '..';
export declare const FlightTakeoff: StyledIcon<any>;
export declare const FlightTakeoffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
