import { StyledIcon, StyledIconProps } from '..';
export declare const FormatColorFill: StyledIcon<any>;
export declare const FormatColorFillDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
