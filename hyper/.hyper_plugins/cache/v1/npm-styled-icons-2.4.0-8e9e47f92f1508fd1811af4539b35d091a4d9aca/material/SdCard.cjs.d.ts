import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SdCard: StyledIcon<any>;
export declare const SdCardDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
