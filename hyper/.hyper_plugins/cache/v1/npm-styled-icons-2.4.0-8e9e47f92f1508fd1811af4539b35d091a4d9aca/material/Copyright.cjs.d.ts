import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Copyright: StyledIcon<any>;
export declare const CopyrightDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
