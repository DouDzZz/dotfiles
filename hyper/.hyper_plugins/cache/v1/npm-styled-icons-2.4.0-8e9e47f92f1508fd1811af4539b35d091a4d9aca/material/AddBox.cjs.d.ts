import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddBox: StyledIcon<any>;
export declare const AddBoxDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
