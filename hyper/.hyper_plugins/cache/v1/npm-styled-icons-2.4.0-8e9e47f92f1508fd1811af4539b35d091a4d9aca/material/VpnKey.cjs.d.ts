import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VpnKey: StyledIcon<any>;
export declare const VpnKeyDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
