import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotificationsPaused: StyledIcon<any>;
export declare const NotificationsPausedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
