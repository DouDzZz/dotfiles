import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Accessible: StyledIcon<any>;
export declare const AccessibleDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
