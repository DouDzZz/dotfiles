import { StyledIcon, StyledIconProps } from '..';
export declare const MicNone: StyledIcon<any>;
export declare const MicNoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
