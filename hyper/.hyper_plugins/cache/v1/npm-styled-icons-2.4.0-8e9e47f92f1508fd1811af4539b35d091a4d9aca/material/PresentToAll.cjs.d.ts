import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PresentToAll: StyledIcon<any>;
export declare const PresentToAllDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
