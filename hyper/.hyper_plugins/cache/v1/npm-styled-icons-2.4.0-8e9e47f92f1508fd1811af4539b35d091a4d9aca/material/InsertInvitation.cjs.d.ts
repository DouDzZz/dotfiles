import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InsertInvitation: StyledIcon<any>;
export declare const InsertInvitationDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
