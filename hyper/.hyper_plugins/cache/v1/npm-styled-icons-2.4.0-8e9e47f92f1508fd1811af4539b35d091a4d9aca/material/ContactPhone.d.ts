import { StyledIcon, StyledIconProps } from '..';
export declare const ContactPhone: StyledIcon<any>;
export declare const ContactPhoneDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
