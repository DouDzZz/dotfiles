import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Router: StyledIcon<any>;
export declare const RouterDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
