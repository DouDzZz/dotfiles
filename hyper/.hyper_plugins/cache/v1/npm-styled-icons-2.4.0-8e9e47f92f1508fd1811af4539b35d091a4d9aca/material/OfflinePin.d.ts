import { StyledIcon, StyledIconProps } from '..';
export declare const OfflinePin: StyledIcon<any>;
export declare const OfflinePinDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
