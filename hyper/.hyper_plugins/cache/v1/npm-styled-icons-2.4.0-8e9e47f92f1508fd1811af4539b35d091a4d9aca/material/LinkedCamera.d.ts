import { StyledIcon, StyledIconProps } from '..';
export declare const LinkedCamera: StyledIcon<any>;
export declare const LinkedCameraDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
