import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Movie: StyledIcon<any>;
export declare const MovieDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
