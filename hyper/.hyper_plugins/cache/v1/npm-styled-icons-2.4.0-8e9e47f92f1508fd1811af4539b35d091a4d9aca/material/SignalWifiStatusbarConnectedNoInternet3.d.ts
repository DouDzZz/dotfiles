import { StyledIcon, StyledIconProps } from '..';
export declare const SignalWifiStatusbarConnectedNoInternet3: StyledIcon<any>;
export declare const SignalWifiStatusbarConnectedNoInternet3Dimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
