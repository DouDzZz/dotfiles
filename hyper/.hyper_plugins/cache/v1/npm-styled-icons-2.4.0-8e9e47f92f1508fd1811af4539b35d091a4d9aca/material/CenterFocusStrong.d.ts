import { StyledIcon, StyledIconProps } from '..';
export declare const CenterFocusStrong: StyledIcon<any>;
export declare const CenterFocusStrongDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
