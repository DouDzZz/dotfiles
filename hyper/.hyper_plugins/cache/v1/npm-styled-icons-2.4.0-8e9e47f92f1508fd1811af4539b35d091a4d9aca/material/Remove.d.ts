import { StyledIcon, StyledIconProps } from '..';
export declare const Remove: StyledIcon<any>;
export declare const RemoveDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
