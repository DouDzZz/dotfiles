import { StyledIcon, StyledIconProps } from '..';
export declare const BlurOff: StyledIcon<any>;
export declare const BlurOffDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
