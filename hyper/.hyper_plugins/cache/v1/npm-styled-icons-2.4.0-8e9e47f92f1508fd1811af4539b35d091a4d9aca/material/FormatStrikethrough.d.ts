import { StyledIcon, StyledIconProps } from '..';
export declare const FormatStrikethrough: StyledIcon<any>;
export declare const FormatStrikethroughDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
