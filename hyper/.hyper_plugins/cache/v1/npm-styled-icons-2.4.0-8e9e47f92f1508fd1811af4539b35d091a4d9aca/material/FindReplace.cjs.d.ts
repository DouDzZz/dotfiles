import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FindReplace: StyledIcon<any>;
export declare const FindReplaceDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
