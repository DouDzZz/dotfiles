import { StyledIcon, StyledIconProps } from '..';
export declare const DeleteForever: StyledIcon<any>;
export declare const DeleteForeverDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
