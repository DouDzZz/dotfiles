import { StyledIcon, StyledIconProps } from '..';
export declare const SwapHoriz: StyledIcon<any>;
export declare const SwapHorizDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
