import { StyledIcon, StyledIconProps } from '..';
export declare const ShopTwo: StyledIcon<any>;
export declare const ShopTwoDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
