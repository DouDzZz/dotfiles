import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StarBorder: StyledIcon<any>;
export declare const StarBorderDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
