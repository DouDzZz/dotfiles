import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Backup: StyledIcon<any>;
export declare const BackupDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
