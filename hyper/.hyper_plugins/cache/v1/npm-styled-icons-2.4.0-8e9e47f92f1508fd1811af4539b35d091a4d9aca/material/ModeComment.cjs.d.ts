import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ModeComment: StyledIcon<any>;
export declare const ModeCommentDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
