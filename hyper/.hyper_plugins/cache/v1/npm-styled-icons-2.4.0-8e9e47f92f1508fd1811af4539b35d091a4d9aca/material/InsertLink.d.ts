import { StyledIcon, StyledIconProps } from '..';
export declare const InsertLink: StyledIcon<any>;
export declare const InsertLinkDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
