import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LocalPrintshop: StyledIcon<any>;
export declare const LocalPrintshopDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
