import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GpsFixed: StyledIcon<any>;
export declare const GpsFixedDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
