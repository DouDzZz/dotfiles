import { StyledIcon, StyledIconProps } from '..';
export declare const PieChart: StyledIcon<any>;
export declare const PieChartDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
