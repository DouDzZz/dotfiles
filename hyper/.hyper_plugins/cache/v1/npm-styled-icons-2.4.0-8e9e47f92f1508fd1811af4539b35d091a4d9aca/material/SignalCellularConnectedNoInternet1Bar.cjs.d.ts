import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignalCellularConnectedNoInternet1Bar: StyledIcon<any>;
export declare const SignalCellularConnectedNoInternet1BarDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
