import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TapAndPlay: StyledIcon<any>;
export declare const TapAndPlayDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
