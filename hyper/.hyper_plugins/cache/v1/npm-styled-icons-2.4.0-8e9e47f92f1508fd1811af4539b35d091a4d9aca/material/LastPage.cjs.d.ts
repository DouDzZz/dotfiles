import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LastPage: StyledIcon<any>;
export declare const LastPageDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
