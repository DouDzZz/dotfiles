import { StyledIcon, StyledIconProps } from '..';
export declare const BrightnessLow: StyledIcon<any>;
export declare const BrightnessLowDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
