import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Autorenew: StyledIcon<any>;
export declare const AutorenewDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
