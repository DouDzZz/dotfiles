import { StyledIcon, StyledIconProps } from '..';
export declare const Dialpad: StyledIcon<any>;
export declare const DialpadDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
