import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SettingsCell: StyledIcon<any>;
export declare const SettingsCellDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
