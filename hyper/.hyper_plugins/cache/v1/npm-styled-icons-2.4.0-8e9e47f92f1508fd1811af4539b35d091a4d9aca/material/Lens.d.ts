import { StyledIcon, StyledIconProps } from '..';
export declare const Lens: StyledIcon<any>;
export declare const LensDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
