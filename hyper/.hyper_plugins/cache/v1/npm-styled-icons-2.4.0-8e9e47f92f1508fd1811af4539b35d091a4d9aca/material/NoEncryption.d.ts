import { StyledIcon, StyledIconProps } from '..';
export declare const NoEncryption: StyledIcon<any>;
export declare const NoEncryptionDimensions: {
    height: number;
    width: number;
};
export { StyledIcon, StyledIconProps };
