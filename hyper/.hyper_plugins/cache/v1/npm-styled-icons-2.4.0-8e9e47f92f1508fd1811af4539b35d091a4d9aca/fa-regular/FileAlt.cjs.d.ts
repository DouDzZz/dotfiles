import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileAlt: StyledIcon<any>;
export declare const FileAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
