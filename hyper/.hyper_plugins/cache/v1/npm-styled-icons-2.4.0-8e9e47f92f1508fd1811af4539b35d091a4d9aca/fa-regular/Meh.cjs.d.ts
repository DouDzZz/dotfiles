import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Meh: StyledIcon<any>;
export declare const MehDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
