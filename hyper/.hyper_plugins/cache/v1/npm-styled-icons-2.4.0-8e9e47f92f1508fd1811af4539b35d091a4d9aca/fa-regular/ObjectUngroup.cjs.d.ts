import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ObjectUngroup: StyledIcon<any>;
export declare const ObjectUngroupDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
