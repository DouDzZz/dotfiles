import { StyledIcon, StyledIconProps } from '..';
export declare const FilePowerpoint: StyledIcon<any>;
export declare const FilePowerpointDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
