import { StyledIcon, StyledIconProps } from '..';
export declare const CheckCircle: StyledIcon<any>;
export declare const CheckCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
