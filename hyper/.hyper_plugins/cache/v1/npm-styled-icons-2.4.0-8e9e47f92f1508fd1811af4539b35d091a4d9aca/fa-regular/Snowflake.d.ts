import { StyledIcon, StyledIconProps } from '..';
export declare const Snowflake: StyledIcon<any>;
export declare const SnowflakeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
