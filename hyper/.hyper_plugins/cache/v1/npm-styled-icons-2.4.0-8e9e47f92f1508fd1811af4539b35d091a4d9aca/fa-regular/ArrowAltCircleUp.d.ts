import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowAltCircleUp: StyledIcon<any>;
export declare const ArrowAltCircleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
