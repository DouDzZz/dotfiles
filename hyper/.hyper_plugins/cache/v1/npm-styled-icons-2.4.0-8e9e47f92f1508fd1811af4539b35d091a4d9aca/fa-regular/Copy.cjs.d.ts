import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Copy: StyledIcon<any>;
export declare const CopyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
