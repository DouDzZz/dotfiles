import { StyledIcon, StyledIconProps } from '..';
export declare const StickyNote: StyledIcon<any>;
export declare const StickyNoteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
