import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TimesCircle: StyledIcon<any>;
export declare const TimesCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
