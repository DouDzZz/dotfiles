import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ListAlt: StyledIcon<any>;
export declare const ListAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
