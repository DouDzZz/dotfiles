import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Images: StyledIcon<any>;
export declare const ImagesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
