import { StyledIcon, StyledIconProps } from '..';
export declare const CalendarCheck: StyledIcon<any>;
export declare const CalendarCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
