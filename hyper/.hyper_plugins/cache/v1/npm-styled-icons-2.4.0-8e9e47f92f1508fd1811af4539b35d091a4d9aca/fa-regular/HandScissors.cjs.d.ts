import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandScissors: StyledIcon<any>;
export declare const HandScissorsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
