import { StyledIcon, StyledIconProps } from '..';
export declare const Circle: StyledIcon<any>;
export declare const CircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
