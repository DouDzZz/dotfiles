import { StyledIcon, StyledIconProps } from '..';
export declare const FileAudio: StyledIcon<any>;
export declare const FileAudioDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
