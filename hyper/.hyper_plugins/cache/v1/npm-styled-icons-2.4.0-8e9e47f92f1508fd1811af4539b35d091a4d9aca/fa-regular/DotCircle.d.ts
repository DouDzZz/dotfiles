import { StyledIcon, StyledIconProps } from '..';
export declare const DotCircle: StyledIcon<any>;
export declare const DotCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
