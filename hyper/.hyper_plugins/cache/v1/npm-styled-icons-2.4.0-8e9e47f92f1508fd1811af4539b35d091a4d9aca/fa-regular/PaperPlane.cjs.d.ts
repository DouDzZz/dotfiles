import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PaperPlane: StyledIcon<any>;
export declare const PaperPlaneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
