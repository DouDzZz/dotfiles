import { StyledIcon, StyledIconProps } from '..';
export declare const User: StyledIcon<any>;
export declare const UserDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
