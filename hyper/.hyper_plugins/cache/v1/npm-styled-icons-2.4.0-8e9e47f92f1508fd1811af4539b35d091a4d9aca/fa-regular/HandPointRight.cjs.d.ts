import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandPointRight: StyledIcon<any>;
export declare const HandPointRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
