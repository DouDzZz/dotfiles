import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Circle: StyledIcon<any>;
export declare const CircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
