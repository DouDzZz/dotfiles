import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DotCircle: StyledIcon<any>;
export declare const DotCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
