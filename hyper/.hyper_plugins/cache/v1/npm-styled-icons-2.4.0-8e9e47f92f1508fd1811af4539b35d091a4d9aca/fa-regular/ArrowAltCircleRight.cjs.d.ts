import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowAltCircleRight: StyledIcon<any>;
export declare const ArrowAltCircleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
