import { StyledIcon, StyledIconProps } from '..';
export declare const Moon: StyledIcon<any>;
export declare const MoonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
