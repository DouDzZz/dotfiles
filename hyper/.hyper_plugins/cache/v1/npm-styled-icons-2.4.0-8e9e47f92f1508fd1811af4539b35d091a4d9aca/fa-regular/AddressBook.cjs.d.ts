import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddressBook: StyledIcon<any>;
export declare const AddressBookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
