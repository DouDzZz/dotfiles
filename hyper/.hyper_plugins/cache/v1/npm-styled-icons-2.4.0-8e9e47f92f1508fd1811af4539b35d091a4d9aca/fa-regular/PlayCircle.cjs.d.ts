import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlayCircle: StyledIcon<any>;
export declare const PlayCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
