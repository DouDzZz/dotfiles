import { StyledIcon, StyledIconProps } from '..';
export declare const FileAlt: StyledIcon<any>;
export declare const FileAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
