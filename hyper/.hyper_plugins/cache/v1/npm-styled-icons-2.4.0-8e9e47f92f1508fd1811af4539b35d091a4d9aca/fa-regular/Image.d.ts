import { StyledIcon, StyledIconProps } from '..';
export declare const Image: StyledIcon<any>;
export declare const ImageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
