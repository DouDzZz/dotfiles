import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowAltCircleDown: StyledIcon<any>;
export declare const ArrowAltCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
