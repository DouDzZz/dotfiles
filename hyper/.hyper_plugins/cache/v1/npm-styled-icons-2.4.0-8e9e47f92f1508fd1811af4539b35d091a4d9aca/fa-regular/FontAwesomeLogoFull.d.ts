import { StyledIcon, StyledIconProps } from '..';
export declare const FontAwesomeLogoFull: StyledIcon<any>;
export declare const FontAwesomeLogoFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
