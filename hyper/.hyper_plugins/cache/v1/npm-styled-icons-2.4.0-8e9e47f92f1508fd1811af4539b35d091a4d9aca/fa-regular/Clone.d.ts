import { StyledIcon, StyledIconProps } from '..';
export declare const Clone: StyledIcon<any>;
export declare const CloneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
