import { StyledIcon, StyledIconProps } from '..';
export declare const Sun: StyledIcon<any>;
export declare const SunDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
