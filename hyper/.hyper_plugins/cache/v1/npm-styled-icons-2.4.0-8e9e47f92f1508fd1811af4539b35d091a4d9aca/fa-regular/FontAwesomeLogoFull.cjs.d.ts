import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FontAwesomeLogoFull: StyledIcon<any>;
export declare const FontAwesomeLogoFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
