import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Square: StyledIcon<any>;
export declare const SquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
