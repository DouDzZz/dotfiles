import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bell: StyledIcon<any>;
export declare const BellDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
