import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyBillAlt: StyledIcon<any>;
export declare const MoneyBillAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
