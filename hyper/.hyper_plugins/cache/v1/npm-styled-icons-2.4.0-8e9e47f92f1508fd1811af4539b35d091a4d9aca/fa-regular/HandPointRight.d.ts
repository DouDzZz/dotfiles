import { StyledIcon, StyledIconProps } from '..';
export declare const HandPointRight: StyledIcon<any>;
export declare const HandPointRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
