import { StyledIcon, StyledIconProps } from '..';
export declare const FolderOpen: StyledIcon<any>;
export declare const FolderOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
