import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowAltCircleRight: StyledIcon<any>;
export declare const ArrowAltCircleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
