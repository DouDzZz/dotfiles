import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileExcel: StyledIcon<any>;
export declare const FileExcelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
