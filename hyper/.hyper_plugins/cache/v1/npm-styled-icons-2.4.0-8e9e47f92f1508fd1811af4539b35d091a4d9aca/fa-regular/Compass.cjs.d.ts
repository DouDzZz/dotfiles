import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Compass: StyledIcon<any>;
export declare const CompassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
