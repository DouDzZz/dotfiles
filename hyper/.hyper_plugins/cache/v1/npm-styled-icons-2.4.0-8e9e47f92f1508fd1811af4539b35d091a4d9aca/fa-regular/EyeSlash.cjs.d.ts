import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EyeSlash: StyledIcon<any>;
export declare const EyeSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
