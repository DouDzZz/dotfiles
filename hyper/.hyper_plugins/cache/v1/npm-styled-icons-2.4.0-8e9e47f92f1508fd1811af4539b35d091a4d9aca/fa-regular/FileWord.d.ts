import { StyledIcon, StyledIconProps } from '..';
export declare const FileWord: StyledIcon<any>;
export declare const FileWordDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
