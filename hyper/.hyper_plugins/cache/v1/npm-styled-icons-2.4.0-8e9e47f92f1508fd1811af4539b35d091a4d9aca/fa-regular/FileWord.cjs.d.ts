import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileWord: StyledIcon<any>;
export declare const FileWordDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
