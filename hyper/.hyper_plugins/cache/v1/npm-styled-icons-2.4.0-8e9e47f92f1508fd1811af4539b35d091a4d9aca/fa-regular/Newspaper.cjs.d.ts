import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Newspaper: StyledIcon<any>;
export declare const NewspaperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
