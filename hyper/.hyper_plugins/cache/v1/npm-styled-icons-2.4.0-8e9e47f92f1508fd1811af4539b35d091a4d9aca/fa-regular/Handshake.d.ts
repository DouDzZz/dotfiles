import { StyledIcon, StyledIconProps } from '..';
export declare const Handshake: StyledIcon<any>;
export declare const HandshakeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
