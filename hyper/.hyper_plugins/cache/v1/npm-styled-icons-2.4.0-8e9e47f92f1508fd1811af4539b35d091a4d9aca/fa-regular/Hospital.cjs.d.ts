import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hospital: StyledIcon<any>;
export declare const HospitalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
