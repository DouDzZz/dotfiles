import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckCircle: StyledIcon<any>;
export declare const CheckCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
