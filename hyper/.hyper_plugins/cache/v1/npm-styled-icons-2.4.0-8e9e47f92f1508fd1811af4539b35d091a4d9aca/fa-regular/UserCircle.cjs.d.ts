import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserCircle: StyledIcon<any>;
export declare const UserCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
