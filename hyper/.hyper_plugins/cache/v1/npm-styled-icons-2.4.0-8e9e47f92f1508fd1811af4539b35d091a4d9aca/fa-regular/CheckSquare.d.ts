import { StyledIcon, StyledIconProps } from '..';
export declare const CheckSquare: StyledIcon<any>;
export declare const CheckSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
