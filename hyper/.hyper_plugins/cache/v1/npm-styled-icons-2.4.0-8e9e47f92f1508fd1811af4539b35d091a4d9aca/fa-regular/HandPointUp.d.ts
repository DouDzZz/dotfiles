import { StyledIcon, StyledIconProps } from '..';
export declare const HandPointUp: StyledIcon<any>;
export declare const HandPointUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
