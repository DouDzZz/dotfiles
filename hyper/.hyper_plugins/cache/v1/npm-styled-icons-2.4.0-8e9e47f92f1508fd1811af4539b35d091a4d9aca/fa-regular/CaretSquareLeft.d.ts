import { StyledIcon, StyledIconProps } from '..';
export declare const CaretSquareLeft: StyledIcon<any>;
export declare const CaretSquareLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
