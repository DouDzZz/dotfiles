import { StyledIcon, StyledIconProps } from '..';
export declare const PauseCircle: StyledIcon<any>;
export declare const PauseCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
