import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WindowClose: StyledIcon<any>;
export declare const WindowCloseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
