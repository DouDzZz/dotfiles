import { StyledIcon, StyledIconProps } from '..';
export declare const Heart: StyledIcon<any>;
export declare const HeartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
