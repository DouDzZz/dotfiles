import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShareSquare: StyledIcon<any>;
export declare const ShareSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
