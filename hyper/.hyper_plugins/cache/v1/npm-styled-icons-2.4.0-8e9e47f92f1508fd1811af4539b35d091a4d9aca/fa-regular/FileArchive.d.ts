import { StyledIcon, StyledIconProps } from '..';
export declare const FileArchive: StyledIcon<any>;
export declare const FileArchiveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
