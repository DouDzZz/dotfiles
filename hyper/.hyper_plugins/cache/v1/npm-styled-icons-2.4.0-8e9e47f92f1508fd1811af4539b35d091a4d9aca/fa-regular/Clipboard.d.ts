import { StyledIcon, StyledIconProps } from '..';
export declare const Clipboard: StyledIcon<any>;
export declare const ClipboardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
