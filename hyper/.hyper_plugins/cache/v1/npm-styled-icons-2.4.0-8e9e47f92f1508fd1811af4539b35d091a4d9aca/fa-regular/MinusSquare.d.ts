import { StyledIcon, StyledIconProps } from '..';
export declare const MinusSquare: StyledIcon<any>;
export declare const MinusSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
