import { StyledIcon, StyledIconProps } from '..';
export declare const HandPointDown: StyledIcon<any>;
export declare const HandPointDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
