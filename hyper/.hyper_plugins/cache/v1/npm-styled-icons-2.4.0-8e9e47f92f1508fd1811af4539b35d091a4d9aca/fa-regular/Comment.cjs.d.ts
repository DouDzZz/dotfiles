import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Comment: StyledIcon<any>;
export declare const CommentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
