import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoneyBillAlt: StyledIcon<any>;
export declare const MoneyBillAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
