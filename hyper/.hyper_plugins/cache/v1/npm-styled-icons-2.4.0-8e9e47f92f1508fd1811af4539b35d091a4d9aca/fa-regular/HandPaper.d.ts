import { StyledIcon, StyledIconProps } from '..';
export declare const HandPaper: StyledIcon<any>;
export declare const HandPaperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
