import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileImage: StyledIcon<any>;
export declare const FileImageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
