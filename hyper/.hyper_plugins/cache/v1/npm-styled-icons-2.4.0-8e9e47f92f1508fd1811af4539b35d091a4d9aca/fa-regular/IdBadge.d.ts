import { StyledIcon, StyledIconProps } from '..';
export declare const IdBadge: StyledIcon<any>;
export declare const IdBadgeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
