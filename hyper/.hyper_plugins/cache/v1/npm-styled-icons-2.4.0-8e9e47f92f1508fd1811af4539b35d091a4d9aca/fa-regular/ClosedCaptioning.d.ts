import { StyledIcon, StyledIconProps } from '..';
export declare const ClosedCaptioning: StyledIcon<any>;
export declare const ClosedCaptioningDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
