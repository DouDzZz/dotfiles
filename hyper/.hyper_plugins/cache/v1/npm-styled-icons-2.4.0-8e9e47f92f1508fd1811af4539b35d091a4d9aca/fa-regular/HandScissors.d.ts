import { StyledIcon, StyledIconProps } from '..';
export declare const HandScissors: StyledIcon<any>;
export declare const HandScissorsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
