import { StyledIcon, StyledIconProps } from '..';
export declare const EnvelopeOpen: StyledIcon<any>;
export declare const EnvelopeOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
