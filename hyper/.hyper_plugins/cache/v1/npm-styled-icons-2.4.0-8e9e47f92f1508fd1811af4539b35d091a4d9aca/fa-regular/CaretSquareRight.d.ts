import { StyledIcon, StyledIconProps } from '..';
export declare const CaretSquareRight: StyledIcon<any>;
export declare const CaretSquareRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
