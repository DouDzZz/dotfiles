import { StyledIcon, StyledIconProps } from '..';
export declare const Eye: StyledIcon<any>;
export declare const EyeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
