import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Copyright: StyledIcon<any>;
export declare const CopyrightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
