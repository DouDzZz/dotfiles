import { StyledIcon, StyledIconProps } from '..';
export declare const Gem: StyledIcon<any>;
export declare const GemDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
