import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TrashAlt: StyledIcon<any>;
export declare const TrashAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
