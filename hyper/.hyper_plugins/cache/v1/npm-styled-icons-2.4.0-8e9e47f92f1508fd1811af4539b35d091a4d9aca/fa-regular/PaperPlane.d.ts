import { StyledIcon, StyledIconProps } from '..';
export declare const PaperPlane: StyledIcon<any>;
export declare const PaperPlaneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
