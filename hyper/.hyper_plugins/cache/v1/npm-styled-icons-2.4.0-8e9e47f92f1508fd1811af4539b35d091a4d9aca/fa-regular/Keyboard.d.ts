import { StyledIcon, StyledIconProps } from '..';
export declare const Keyboard: StyledIcon<any>;
export declare const KeyboardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
