import * as faRegular from './fa-regular';
import * as faSolid from './fa-solid';
import * as faBrands from './fa-brands';
import * as feather from './feather';
import * as material from './material';
import * as octicons from './octicons';
export { faRegular, faSolid, faBrands, feather, material, octicons };
