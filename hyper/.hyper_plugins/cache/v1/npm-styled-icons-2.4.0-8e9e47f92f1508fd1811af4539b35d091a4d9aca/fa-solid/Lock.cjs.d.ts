import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lock: StyledIcon<any>;
export declare const LockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
