import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bars: StyledIcon<any>;
export declare const BarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
