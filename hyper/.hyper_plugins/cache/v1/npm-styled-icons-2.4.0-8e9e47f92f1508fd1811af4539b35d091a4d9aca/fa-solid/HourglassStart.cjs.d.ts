import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HourglassStart: StyledIcon<any>;
export declare const HourglassStartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
