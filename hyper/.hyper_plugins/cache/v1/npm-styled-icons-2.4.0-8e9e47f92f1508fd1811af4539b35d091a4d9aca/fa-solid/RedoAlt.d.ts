import { StyledIcon, StyledIconProps } from '..';
export declare const RedoAlt: StyledIcon<any>;
export declare const RedoAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
