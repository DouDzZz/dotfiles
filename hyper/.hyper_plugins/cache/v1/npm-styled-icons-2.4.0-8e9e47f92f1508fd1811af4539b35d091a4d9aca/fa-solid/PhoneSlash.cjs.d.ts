import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PhoneSlash: StyledIcon<any>;
export declare const PhoneSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
