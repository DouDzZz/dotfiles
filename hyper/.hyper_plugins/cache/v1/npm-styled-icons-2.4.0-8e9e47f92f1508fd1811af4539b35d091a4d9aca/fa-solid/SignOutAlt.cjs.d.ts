import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignOutAlt: StyledIcon<any>;
export declare const SignOutAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
