import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Screwdriver: StyledIcon<any>;
export declare const ScrewdriverDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
