import { StyledIcon, StyledIconProps } from '..';
export declare const FileCode: StyledIcon<any>;
export declare const FileCodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
