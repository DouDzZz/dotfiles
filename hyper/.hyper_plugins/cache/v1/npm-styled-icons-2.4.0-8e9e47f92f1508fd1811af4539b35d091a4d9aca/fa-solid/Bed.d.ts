import { StyledIcon, StyledIconProps } from '..';
export declare const Bed: StyledIcon<any>;
export declare const BedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
