import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Coffee: StyledIcon<any>;
export declare const CoffeeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
