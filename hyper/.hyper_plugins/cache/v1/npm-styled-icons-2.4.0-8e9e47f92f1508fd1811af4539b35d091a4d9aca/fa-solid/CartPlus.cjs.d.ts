import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CartPlus: StyledIcon<any>;
export declare const CartPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
