import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Pills: StyledIcon<any>;
export declare const PillsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
