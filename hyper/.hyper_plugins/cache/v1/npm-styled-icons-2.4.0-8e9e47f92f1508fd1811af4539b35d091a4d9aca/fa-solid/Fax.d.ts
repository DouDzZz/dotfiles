import { StyledIcon, StyledIconProps } from '..';
export declare const Fax: StyledIcon<any>;
export declare const FaxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
