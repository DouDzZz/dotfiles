import { StyledIcon, StyledIconProps } from '..';
export declare const Truck: StyledIcon<any>;
export declare const TruckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
