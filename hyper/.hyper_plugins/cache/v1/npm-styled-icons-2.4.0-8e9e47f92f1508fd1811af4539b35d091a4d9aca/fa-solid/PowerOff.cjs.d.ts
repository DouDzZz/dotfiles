import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PowerOff: StyledIcon<any>;
export declare const PowerOffDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
