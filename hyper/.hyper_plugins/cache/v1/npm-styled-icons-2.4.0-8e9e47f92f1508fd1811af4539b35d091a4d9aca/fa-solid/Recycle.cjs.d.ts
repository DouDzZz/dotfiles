import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Recycle: StyledIcon<any>;
export declare const RecycleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
