import { StyledIcon, StyledIconProps } from '..';
export declare const Warehouse: StyledIcon<any>;
export declare const WarehouseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
