import { StyledIcon, StyledIconProps } from '..';
export declare const HandRock: StyledIcon<any>;
export declare const HandRockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
