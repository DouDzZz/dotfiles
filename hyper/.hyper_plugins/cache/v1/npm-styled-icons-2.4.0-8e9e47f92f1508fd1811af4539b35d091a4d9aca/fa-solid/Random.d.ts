import { StyledIcon, StyledIconProps } from '..';
export declare const Random: StyledIcon<any>;
export declare const RandomDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
