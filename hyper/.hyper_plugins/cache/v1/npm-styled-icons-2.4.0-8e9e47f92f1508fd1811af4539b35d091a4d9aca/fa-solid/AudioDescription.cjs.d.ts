import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AudioDescription: StyledIcon<any>;
export declare const AudioDescriptionDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
