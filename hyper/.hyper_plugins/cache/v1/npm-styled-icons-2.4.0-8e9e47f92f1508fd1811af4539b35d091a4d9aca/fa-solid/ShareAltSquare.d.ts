import { StyledIcon, StyledIconProps } from '..';
export declare const ShareAltSquare: StyledIcon<any>;
export declare const ShareAltSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
