import { StyledIcon, StyledIconProps } from '..';
export declare const ExternalLinkAlt: StyledIcon<any>;
export declare const ExternalLinkAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
