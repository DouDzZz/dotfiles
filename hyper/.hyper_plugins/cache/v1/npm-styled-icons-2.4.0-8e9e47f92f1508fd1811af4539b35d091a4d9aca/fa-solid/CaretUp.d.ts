import { StyledIcon, StyledIconProps } from '..';
export declare const CaretUp: StyledIcon<any>;
export declare const CaretUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
