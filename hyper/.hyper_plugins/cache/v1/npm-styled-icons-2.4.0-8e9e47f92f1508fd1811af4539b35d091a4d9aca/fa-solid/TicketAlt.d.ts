import { StyledIcon, StyledIconProps } from '..';
export declare const TicketAlt: StyledIcon<any>;
export declare const TicketAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
