import { StyledIcon, StyledIconProps } from '..';
export declare const GreaterThanEqual: StyledIcon<any>;
export declare const GreaterThanEqualDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
