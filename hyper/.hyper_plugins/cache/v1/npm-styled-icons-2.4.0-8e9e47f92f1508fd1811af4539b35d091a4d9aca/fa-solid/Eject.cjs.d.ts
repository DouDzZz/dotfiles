import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Eject: StyledIcon<any>;
export declare const EjectDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
