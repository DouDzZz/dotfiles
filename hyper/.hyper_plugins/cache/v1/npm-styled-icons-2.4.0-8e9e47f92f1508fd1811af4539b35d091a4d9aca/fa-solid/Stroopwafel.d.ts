import { StyledIcon, StyledIconProps } from '..';
export declare const Stroopwafel: StyledIcon<any>;
export declare const StroopwafelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
