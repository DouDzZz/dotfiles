import { StyledIcon, StyledIconProps } from '..';
export declare const Syringe: StyledIcon<any>;
export declare const SyringeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
