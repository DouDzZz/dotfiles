import { StyledIcon, StyledIconProps } from '..';
export declare const RssSquare: StyledIcon<any>;
export declare const RssSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
