import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowAltCircleLeft: StyledIcon<any>;
export declare const ArrowAltCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
