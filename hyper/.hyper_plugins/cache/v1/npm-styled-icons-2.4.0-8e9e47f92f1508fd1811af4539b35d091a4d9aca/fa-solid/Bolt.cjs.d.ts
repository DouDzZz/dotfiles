import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bolt: StyledIcon<any>;
export declare const BoltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
