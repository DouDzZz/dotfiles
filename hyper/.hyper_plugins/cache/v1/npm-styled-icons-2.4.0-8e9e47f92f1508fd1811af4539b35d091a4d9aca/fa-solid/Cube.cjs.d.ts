import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cube: StyledIcon<any>;
export declare const CubeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
