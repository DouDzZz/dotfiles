import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarsStrokeH: StyledIcon<any>;
export declare const MarsStrokeHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
