import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowUp: StyledIcon<any>;
export declare const ArrowUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
