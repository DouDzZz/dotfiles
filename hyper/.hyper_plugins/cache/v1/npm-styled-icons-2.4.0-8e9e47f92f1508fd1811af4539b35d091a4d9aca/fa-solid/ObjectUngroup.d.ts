import { StyledIcon, StyledIconProps } from '..';
export declare const ObjectUngroup: StyledIcon<any>;
export declare const ObjectUngroupDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
