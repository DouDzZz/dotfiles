import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Microphone: StyledIcon<any>;
export declare const MicrophoneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
