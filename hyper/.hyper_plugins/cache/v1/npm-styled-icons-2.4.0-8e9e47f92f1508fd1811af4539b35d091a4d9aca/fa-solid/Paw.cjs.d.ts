import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paw: StyledIcon<any>;
export declare const PawDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
