import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Medkit: StyledIcon<any>;
export declare const MedkitDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
