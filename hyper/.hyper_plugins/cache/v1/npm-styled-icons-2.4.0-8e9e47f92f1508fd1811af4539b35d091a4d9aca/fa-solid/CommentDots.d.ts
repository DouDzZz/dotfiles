import { StyledIcon, StyledIconProps } from '..';
export declare const CommentDots: StyledIcon<any>;
export declare const CommentDotsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
