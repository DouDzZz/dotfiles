import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowAltCircleDown: StyledIcon<any>;
export declare const ArrowAltCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
