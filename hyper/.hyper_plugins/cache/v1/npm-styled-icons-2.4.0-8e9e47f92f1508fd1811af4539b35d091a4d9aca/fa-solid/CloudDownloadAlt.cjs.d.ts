import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudDownloadAlt: StyledIcon<any>;
export declare const CloudDownloadAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
