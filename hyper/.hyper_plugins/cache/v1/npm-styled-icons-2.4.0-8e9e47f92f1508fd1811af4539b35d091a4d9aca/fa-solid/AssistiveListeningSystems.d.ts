import { StyledIcon, StyledIconProps } from '..';
export declare const AssistiveListeningSystems: StyledIcon<any>;
export declare const AssistiveListeningSystemsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
