import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Eraser: StyledIcon<any>;
export declare const EraserDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
