import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BaseballBall: StyledIcon<any>;
export declare const BaseballBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
