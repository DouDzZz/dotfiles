import { StyledIcon, StyledIconProps } from '..';
export declare const SquareFull: StyledIcon<any>;
export declare const SquareFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
