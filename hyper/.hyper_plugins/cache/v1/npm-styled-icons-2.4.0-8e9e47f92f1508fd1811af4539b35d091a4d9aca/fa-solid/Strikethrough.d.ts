import { StyledIcon, StyledIconProps } from '..';
export declare const Strikethrough: StyledIcon<any>;
export declare const StrikethroughDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
