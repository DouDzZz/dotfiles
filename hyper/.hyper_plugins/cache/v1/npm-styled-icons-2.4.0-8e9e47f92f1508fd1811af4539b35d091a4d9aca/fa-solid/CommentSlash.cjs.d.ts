import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CommentSlash: StyledIcon<any>;
export declare const CommentSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
