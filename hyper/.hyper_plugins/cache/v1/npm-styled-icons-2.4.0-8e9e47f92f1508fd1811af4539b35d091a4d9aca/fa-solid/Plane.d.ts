import { StyledIcon, StyledIconProps } from '..';
export declare const Plane: StyledIcon<any>;
export declare const PlaneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
