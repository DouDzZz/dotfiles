import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dna: StyledIcon<any>;
export declare const DnaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
