import { StyledIcon, StyledIconProps } from '..';
export declare const ThList: StyledIcon<any>;
export declare const ThListDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
