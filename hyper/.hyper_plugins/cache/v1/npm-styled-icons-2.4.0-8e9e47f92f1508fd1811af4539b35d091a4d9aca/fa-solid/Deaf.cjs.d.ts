import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Deaf: StyledIcon<any>;
export declare const DeafDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
