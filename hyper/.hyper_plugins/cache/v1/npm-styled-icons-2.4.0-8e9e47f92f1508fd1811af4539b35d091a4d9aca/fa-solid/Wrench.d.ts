import { StyledIcon, StyledIconProps } from '..';
export declare const Wrench: StyledIcon<any>;
export declare const WrenchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
