import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarsStroke: StyledIcon<any>;
export declare const MarsStrokeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
