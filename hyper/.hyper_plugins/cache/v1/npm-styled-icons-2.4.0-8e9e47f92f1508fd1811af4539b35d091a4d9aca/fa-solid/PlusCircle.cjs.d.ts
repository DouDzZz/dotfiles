import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PlusCircle: StyledIcon<any>;
export declare const PlusCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
