import { StyledIcon, StyledIconProps } from '..';
export declare const Tshirt: StyledIcon<any>;
export declare const TshirtDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
