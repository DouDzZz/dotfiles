import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LongArrowAltLeft: StyledIcon<any>;
export declare const LongArrowAltLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
