import { StyledIcon, StyledIconProps } from '..';
export declare const Paperclip: StyledIcon<any>;
export declare const PaperclipDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
