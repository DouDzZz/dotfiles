import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiceSix: StyledIcon<any>;
export declare const DiceSixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
