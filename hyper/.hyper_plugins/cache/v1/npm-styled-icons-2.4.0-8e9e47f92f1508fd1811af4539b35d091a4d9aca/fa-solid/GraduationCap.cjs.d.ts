import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GraduationCap: StyledIcon<any>;
export declare const GraduationCapDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
