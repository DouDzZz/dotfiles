import { StyledIcon, StyledIconProps } from '..';
export declare const Coins: StyledIcon<any>;
export declare const CoinsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
