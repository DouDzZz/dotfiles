import { StyledIcon, StyledIconProps } from '..';
export declare const Mercury: StyledIcon<any>;
export declare const MercuryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
