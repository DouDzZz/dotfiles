import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Backward: StyledIcon<any>;
export declare const BackwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
