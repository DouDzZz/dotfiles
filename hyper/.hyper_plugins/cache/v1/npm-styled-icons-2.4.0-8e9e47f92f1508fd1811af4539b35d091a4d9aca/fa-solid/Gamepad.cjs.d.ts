import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gamepad: StyledIcon<any>;
export declare const GamepadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
