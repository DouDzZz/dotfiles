import { StyledIcon, StyledIconProps } from '..';
export declare const MapSigns: StyledIcon<any>;
export declare const MapSignsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
