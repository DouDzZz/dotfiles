import { StyledIcon, StyledIconProps } from '..';
export declare const Store: StyledIcon<any>;
export declare const StoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
