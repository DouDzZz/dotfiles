import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Magic: StyledIcon<any>;
export declare const MagicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
