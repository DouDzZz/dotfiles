import { StyledIcon, StyledIconProps } from '..';
export declare const RupeeSign: StyledIcon<any>;
export declare const RupeeSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
