import { StyledIcon, StyledIconProps } from '..';
export declare const ToggleOff: StyledIcon<any>;
export declare const ToggleOffDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
