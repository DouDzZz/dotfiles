import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Signal: StyledIcon<any>;
export declare const SignalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
