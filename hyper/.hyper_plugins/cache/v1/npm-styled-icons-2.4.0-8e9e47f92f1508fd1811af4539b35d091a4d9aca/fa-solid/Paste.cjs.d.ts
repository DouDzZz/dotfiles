import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paste: StyledIcon<any>;
export declare const PasteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
