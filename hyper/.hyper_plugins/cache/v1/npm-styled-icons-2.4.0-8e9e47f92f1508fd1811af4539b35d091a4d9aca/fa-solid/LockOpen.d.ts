import { StyledIcon, StyledIconProps } from '..';
export declare const LockOpen: StyledIcon<any>;
export declare const LockOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
