import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiceFive: StyledIcon<any>;
export declare const DiceFiveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
