import { StyledIcon, StyledIconProps } from '..';
export declare const Download: StyledIcon<any>;
export declare const DownloadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
