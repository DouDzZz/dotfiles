import { StyledIcon, StyledIconProps } from '..';
export declare const Tint: StyledIcon<any>;
export declare const TintDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
