import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowsAlt: StyledIcon<any>;
export declare const ArrowsAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
