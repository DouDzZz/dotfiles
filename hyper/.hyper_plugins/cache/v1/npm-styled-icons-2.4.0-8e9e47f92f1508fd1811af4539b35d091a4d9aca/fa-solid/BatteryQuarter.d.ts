import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryQuarter: StyledIcon<any>;
export declare const BatteryQuarterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
