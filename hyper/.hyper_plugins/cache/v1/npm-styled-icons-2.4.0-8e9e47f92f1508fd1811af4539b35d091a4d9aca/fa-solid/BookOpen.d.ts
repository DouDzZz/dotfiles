import { StyledIcon, StyledIconProps } from '..';
export declare const BookOpen: StyledIcon<any>;
export declare const BookOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
