import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TableTennis: StyledIcon<any>;
export declare const TableTennisDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
