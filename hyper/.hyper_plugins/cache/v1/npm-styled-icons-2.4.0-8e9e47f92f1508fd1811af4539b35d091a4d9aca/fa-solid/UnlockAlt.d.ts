import { StyledIcon, StyledIconProps } from '..';
export declare const UnlockAlt: StyledIcon<any>;
export declare const UnlockAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
