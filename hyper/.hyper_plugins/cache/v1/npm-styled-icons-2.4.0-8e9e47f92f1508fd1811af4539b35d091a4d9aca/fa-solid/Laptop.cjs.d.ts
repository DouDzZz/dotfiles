import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Laptop: StyledIcon<any>;
export declare const LaptopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
