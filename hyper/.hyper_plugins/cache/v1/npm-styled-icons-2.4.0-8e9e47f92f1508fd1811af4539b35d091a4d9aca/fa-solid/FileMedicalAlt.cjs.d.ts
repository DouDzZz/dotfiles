import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileMedicalAlt: StyledIcon<any>;
export declare const FileMedicalAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
