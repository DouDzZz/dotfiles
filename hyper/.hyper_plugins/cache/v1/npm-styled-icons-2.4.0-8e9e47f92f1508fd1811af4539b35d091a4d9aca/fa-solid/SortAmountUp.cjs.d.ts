import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortAmountUp: StyledIcon<any>;
export declare const SortAmountUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
