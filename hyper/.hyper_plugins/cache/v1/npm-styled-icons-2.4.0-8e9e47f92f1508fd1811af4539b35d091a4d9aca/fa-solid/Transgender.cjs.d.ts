import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Transgender: StyledIcon<any>;
export declare const TransgenderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
