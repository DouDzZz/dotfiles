import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dice: StyledIcon<any>;
export declare const DiceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
