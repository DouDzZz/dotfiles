import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessRook: StyledIcon<any>;
export declare const ChessRookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
