import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronCircleUp: StyledIcon<any>;
export declare const ChevronCircleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
