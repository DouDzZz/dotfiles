import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const At: StyledIcon<any>;
export declare const AtDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
