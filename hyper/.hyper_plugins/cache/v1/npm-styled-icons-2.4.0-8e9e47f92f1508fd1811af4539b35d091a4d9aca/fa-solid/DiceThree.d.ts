import { StyledIcon, StyledIconProps } from '..';
export declare const DiceThree: StyledIcon<any>;
export declare const DiceThreeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
