import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronDown: StyledIcon<any>;
export declare const ChevronDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
