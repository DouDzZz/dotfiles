import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Neuter: StyledIcon<any>;
export declare const NeuterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
