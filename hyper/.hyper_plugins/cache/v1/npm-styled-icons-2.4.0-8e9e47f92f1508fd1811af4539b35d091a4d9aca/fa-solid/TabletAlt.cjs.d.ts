import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TabletAlt: StyledIcon<any>;
export declare const TabletAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
