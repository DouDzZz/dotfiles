import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShareAltSquare: StyledIcon<any>;
export declare const ShareAltSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
