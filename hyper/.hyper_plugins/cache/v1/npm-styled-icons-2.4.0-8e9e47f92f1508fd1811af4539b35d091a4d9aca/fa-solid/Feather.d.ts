import { StyledIcon, StyledIconProps } from '..';
export declare const Feather: StyledIcon<any>;
export declare const FeatherDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
