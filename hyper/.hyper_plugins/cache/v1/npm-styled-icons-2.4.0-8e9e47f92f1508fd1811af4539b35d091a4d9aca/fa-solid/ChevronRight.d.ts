import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronRight: StyledIcon<any>;
export declare const ChevronRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
