import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignLanguage: StyledIcon<any>;
export declare const SignLanguageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
