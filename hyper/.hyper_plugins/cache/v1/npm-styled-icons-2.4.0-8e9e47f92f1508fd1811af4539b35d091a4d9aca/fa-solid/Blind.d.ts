import { StyledIcon, StyledIconProps } from '..';
export declare const Blind: StyledIcon<any>;
export declare const BlindDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
