import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BasketballBall: StyledIcon<any>;
export declare const BasketballBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
