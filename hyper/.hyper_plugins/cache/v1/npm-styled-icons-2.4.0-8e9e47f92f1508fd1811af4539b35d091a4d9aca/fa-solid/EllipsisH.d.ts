import { StyledIcon, StyledIconProps } from '..';
export declare const EllipsisH: StyledIcon<any>;
export declare const EllipsisHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
