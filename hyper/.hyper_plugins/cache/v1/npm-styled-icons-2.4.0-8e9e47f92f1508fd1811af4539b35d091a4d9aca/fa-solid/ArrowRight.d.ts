import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowRight: StyledIcon<any>;
export declare const ArrowRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
