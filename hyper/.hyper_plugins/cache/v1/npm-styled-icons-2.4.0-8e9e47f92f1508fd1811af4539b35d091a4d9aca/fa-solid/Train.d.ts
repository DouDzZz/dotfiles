import { StyledIcon, StyledIconProps } from '..';
export declare const Train: StyledIcon<any>;
export declare const TrainDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
