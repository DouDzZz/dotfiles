import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TextHeight: StyledIcon<any>;
export declare const TextHeightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
