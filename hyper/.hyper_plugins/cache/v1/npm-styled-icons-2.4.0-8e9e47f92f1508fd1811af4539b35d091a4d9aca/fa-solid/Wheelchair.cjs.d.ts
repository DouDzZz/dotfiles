import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wheelchair: StyledIcon<any>;
export declare const WheelchairDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
