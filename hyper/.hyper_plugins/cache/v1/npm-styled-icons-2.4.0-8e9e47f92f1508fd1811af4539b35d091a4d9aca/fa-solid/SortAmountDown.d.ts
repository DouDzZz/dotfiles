import { StyledIcon, StyledIconProps } from '..';
export declare const SortAmountDown: StyledIcon<any>;
export declare const SortAmountDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
