import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LongArrowAltUp: StyledIcon<any>;
export declare const LongArrowAltUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
