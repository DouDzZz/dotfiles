import { StyledIcon, StyledIconProps } from '..';
export declare const Divide: StyledIcon<any>;
export declare const DivideDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
