import { StyledIcon, StyledIconProps } from '..';
export declare const CaretLeft: StyledIcon<any>;
export declare const CaretLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
