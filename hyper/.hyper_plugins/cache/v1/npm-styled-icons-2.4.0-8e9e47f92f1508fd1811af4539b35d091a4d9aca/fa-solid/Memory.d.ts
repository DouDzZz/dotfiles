import { StyledIcon, StyledIconProps } from '..';
export declare const Memory: StyledIcon<any>;
export declare const MemoryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
