import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wrench: StyledIcon<any>;
export declare const WrenchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
