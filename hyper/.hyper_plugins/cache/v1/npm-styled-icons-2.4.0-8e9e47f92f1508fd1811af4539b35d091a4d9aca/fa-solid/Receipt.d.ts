import { StyledIcon, StyledIconProps } from '..';
export declare const Receipt: StyledIcon<any>;
export declare const ReceiptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
