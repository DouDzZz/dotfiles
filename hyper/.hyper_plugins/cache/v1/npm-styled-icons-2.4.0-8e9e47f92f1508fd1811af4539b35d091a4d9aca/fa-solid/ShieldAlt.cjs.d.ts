import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShieldAlt: StyledIcon<any>;
export declare const ShieldAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
