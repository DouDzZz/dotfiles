import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WindowMinimize: StyledIcon<any>;
export declare const WindowMinimizeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
