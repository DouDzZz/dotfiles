import { StyledIcon, StyledIconProps } from '..';
export declare const XRay: StyledIcon<any>;
export declare const XRayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
