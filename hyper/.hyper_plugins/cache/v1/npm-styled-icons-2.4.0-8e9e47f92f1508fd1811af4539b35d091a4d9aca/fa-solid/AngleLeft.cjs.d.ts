import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AngleLeft: StyledIcon<any>;
export declare const AngleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
