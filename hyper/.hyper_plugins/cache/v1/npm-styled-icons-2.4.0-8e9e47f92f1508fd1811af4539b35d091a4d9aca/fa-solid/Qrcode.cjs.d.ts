import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Qrcode: StyledIcon<any>;
export declare const QrcodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
