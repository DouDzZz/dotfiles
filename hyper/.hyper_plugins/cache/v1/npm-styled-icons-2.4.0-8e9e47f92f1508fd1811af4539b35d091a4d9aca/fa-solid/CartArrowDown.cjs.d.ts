import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CartArrowDown: StyledIcon<any>;
export declare const CartArrowDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
