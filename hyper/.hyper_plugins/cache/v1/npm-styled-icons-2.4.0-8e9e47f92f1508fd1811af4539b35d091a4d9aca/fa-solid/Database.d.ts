import { StyledIcon, StyledIconProps } from '..';
export declare const Database: StyledIcon<any>;
export declare const DatabaseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
