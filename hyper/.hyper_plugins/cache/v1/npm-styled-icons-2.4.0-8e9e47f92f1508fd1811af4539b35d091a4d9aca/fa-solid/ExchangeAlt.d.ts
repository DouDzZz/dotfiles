import { StyledIcon, StyledIconProps } from '..';
export declare const ExchangeAlt: StyledIcon<any>;
export declare const ExchangeAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
