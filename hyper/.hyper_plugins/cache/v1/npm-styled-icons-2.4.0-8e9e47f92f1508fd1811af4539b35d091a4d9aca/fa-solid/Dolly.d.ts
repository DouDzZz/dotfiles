import { StyledIcon, StyledIconProps } from '..';
export declare const Dolly: StyledIcon<any>;
export declare const DollyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
