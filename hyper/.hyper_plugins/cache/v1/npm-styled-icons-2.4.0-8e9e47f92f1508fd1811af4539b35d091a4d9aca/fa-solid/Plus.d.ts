import { StyledIcon, StyledIconProps } from '..';
export declare const Plus: StyledIcon<any>;
export declare const PlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
