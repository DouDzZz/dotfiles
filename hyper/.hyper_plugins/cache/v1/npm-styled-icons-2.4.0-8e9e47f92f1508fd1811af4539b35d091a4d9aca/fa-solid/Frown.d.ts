import { StyledIcon, StyledIconProps } from '..';
export declare const Frown: StyledIcon<any>;
export declare const FrownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
