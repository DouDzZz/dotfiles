import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const School: StyledIcon<any>;
export declare const SchoolDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
