import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolleyballBall: StyledIcon<any>;
export declare const VolleyballBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
