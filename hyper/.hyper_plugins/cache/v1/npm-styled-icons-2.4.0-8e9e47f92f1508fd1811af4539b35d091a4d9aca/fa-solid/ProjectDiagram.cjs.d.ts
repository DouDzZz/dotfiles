import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ProjectDiagram: StyledIcon<any>;
export declare const ProjectDiagramDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
