import { StyledIcon, StyledIconProps } from '..';
export declare const Barcode: StyledIcon<any>;
export declare const BarcodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
