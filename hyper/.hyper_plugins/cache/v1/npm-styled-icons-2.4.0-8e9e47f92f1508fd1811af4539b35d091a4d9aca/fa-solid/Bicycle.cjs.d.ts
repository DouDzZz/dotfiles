import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bicycle: StyledIcon<any>;
export declare const BicycleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
