import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiceTwo: StyledIcon<any>;
export declare const DiceTwoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
