import { StyledIcon, StyledIconProps } from '..';
export declare const Exclamation: StyledIcon<any>;
export declare const ExclamationDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
