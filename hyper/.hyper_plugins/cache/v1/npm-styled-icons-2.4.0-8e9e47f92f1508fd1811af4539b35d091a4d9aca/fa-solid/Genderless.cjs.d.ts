import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Genderless: StyledIcon<any>;
export declare const GenderlessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
