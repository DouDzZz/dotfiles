import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paperclip: StyledIcon<any>;
export declare const PaperclipDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
