import { StyledIcon, StyledIconProps } from '..';
export declare const UserTimes: StyledIcon<any>;
export declare const UserTimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
