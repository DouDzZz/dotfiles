import { StyledIcon, StyledIconProps } from '..';
export declare const Play: StyledIcon<any>;
export declare const PlayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
