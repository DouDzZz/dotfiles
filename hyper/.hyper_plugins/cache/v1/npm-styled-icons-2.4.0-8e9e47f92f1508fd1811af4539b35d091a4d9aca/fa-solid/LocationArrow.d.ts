import { StyledIcon, StyledIconProps } from '..';
export declare const LocationArrow: StyledIcon<any>;
export declare const LocationArrowDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
