import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ListUl: StyledIcon<any>;
export declare const ListUlDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
