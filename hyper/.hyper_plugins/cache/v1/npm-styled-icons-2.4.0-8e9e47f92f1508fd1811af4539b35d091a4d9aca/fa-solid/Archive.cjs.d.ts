import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Archive: StyledIcon<any>;
export declare const ArchiveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
