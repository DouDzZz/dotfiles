import { StyledIcon, StyledIconProps } from '..';
export declare const Chess: StyledIcon<any>;
export declare const ChessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
