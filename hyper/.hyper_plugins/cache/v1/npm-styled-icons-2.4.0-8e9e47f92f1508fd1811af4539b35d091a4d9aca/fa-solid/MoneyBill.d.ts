import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyBill: StyledIcon<any>;
export declare const MoneyBillDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
