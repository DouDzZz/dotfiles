import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CalendarPlus: StyledIcon<any>;
export declare const CalendarPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
