import { StyledIcon, StyledIconProps } from '..';
export declare const VenusDouble: StyledIcon<any>;
export declare const VenusDoubleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
