import { StyledIcon, StyledIconProps } from '..';
export declare const TextWidth: StyledIcon<any>;
export declare const TextWidthDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
