import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sort: StyledIcon<any>;
export declare const SortDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
