import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Globe: StyledIcon<any>;
export declare const GlobeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
