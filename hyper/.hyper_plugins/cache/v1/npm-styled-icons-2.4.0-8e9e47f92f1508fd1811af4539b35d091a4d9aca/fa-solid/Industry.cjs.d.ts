import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Industry: StyledIcon<any>;
export declare const IndustryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
