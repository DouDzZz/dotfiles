import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Robot: StyledIcon<any>;
export declare const RobotDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
