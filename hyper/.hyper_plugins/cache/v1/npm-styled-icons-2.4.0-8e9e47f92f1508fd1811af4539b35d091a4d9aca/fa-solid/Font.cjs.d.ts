import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Font: StyledIcon<any>;
export declare const FontDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
