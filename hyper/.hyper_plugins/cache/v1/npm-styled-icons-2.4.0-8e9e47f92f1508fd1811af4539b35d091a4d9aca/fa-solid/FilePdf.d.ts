import { StyledIcon, StyledIconProps } from '..';
export declare const FilePdf: StyledIcon<any>;
export declare const FilePdfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
