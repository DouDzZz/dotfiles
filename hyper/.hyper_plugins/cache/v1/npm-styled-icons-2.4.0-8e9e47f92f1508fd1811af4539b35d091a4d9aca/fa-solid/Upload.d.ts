import { StyledIcon, StyledIconProps } from '..';
export declare const Upload: StyledIcon<any>;
export declare const UploadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
