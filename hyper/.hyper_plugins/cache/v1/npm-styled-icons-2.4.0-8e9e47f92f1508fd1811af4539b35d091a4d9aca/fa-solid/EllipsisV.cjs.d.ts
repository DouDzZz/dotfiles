import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EllipsisV: StyledIcon<any>;
export declare const EllipsisVDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
