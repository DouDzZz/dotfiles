import { StyledIcon, StyledIconProps } from '..';
export declare const Th: StyledIcon<any>;
export declare const ThDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
