import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyCheckAlt: StyledIcon<any>;
export declare const MoneyCheckAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
