import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Server: StyledIcon<any>;
export declare const ServerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
