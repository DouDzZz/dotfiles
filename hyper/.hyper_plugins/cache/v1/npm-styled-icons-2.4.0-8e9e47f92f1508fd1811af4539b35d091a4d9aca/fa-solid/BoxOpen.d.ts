import { StyledIcon, StyledIconProps } from '..';
export declare const BoxOpen: StyledIcon<any>;
export declare const BoxOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
