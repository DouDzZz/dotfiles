import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryThreeQuarters: StyledIcon<any>;
export declare const BatteryThreeQuartersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
