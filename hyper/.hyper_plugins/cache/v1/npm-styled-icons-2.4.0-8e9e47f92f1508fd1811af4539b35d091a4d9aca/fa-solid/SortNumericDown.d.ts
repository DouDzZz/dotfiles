import { StyledIcon, StyledIconProps } from '..';
export declare const SortNumericDown: StyledIcon<any>;
export declare const SortNumericDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
