import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Leaf: StyledIcon<any>;
export declare const LeafDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
