import { StyledIcon, StyledIconProps } from '..';
export declare const Filter: StyledIcon<any>;
export declare const FilterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
