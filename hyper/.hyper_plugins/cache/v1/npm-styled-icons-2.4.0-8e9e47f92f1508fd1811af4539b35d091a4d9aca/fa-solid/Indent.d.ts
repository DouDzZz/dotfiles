import { StyledIcon, StyledIconProps } from '..';
export declare const Indent: StyledIcon<any>;
export declare const IndentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
