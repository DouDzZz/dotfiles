import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThumbsUp: StyledIcon<any>;
export declare const ThumbsUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
