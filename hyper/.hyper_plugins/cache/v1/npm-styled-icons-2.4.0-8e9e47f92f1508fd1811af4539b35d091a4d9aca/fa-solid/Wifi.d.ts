import { StyledIcon, StyledIconProps } from '..';
export declare const Wifi: StyledIcon<any>;
export declare const WifiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
