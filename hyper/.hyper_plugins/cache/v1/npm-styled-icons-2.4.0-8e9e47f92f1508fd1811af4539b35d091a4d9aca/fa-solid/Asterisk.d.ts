import { StyledIcon, StyledIconProps } from '..';
export declare const Asterisk: StyledIcon<any>;
export declare const AsteriskDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
