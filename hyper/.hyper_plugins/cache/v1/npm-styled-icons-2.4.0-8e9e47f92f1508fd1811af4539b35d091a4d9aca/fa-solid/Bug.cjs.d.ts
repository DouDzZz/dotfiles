import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bug: StyledIcon<any>;
export declare const BugDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
