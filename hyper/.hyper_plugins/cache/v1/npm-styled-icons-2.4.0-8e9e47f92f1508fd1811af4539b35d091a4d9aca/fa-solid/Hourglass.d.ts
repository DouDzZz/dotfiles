import { StyledIcon, StyledIconProps } from '..';
export declare const Hourglass: StyledIcon<any>;
export declare const HourglassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
