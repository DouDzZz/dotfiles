import { StyledIcon, StyledIconProps } from '..';
export declare const SortNumericUp: StyledIcon<any>;
export declare const SortNumericUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
