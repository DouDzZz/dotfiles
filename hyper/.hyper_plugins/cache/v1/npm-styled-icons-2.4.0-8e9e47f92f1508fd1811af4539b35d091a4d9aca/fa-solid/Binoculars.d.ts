import { StyledIcon, StyledIconProps } from '..';
export declare const Binoculars: StyledIcon<any>;
export declare const BinocularsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
