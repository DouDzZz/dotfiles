import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ban: StyledIcon<any>;
export declare const BanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
