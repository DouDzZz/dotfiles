import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Calendar: StyledIcon<any>;
export declare const CalendarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
