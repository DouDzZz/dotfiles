import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ParachuteBox: StyledIcon<any>;
export declare const ParachuteBoxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
