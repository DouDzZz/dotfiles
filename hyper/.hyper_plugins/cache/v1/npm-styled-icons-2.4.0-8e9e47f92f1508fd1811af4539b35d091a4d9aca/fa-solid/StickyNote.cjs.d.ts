import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StickyNote: StyledIcon<any>;
export declare const StickyNoteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
