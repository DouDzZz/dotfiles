import { StyledIcon, StyledIconProps } from '..';
export declare const File: StyledIcon<any>;
export declare const FileDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
