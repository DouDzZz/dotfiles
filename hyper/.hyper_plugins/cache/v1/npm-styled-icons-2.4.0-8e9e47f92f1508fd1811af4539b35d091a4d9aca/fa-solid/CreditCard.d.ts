import { StyledIcon, StyledIconProps } from '..';
export declare const CreditCard: StyledIcon<any>;
export declare const CreditCardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
