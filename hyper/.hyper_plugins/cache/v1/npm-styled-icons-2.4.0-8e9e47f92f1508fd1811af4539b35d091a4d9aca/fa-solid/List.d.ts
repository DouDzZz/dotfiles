import { StyledIcon, StyledIconProps } from '..';
export declare const List: StyledIcon<any>;
export declare const ListDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
