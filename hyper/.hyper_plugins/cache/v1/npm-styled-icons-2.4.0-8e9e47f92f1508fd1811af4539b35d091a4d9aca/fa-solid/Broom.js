var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import React from 'react';
import styled from 'styled-components';
export var Broom = styled.svg.attrs({
    children: function (props) { return (props.title != null
        ? [React.createElement("title", { key: "Broom-title" }, props.title), React.createElement("path", { fill: "currentColor", d: "M10.8 247.2C-.7 251.8-3.7 266.7 5 275.4l54.8 54.8 73.2-24.4-24.4 73.2 128 128c8.8 8.8 23.6 5.7 28.2-5.8l98.1-243.7-108.4-108.4-243.7 98.1zM507.3 27.3L484.7 4.7c-6.2-6.3-16.4-6.3-22.6 0L359.8 106.9l-40.4-40.4c-4.2-4.3-11.4-3-13.9 2.5l-27.2 58.6 106.1 106.1 58.6-27.2c5.4-2.5 6.7-9.7 2.5-13.9l-40.4-40.4L507.3 49.9c6.3-6.2 6.3-16.3 0-22.6z", key: "k0" })
        ]
        : [React.createElement("path", { fill: "currentColor", d: "M10.8 247.2C-.7 251.8-3.7 266.7 5 275.4l54.8 54.8 73.2-24.4-24.4 73.2 128 128c8.8 8.8 23.6 5.7 28.2-5.8l98.1-243.7-108.4-108.4-243.7 98.1zM507.3 27.3L484.7 4.7c-6.2-6.3-16.4-6.3-22.6 0L359.8 106.9l-40.4-40.4c-4.2-4.3-11.4-3-13.9 2.5l-27.2 58.6 106.1 106.1 58.6-27.2c5.4-2.5 6.7-9.7 2.5-13.9l-40.4-40.4L507.3 49.9c6.3-6.2 6.3-16.3 0-22.6z", key: "k0" })
        ]); },
    viewBox: '0 0 512 512',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
Broom.displayName = 'Broom';
export var BroomDimensions = { height: undefined, width: undefined };
var templateObject_1;
