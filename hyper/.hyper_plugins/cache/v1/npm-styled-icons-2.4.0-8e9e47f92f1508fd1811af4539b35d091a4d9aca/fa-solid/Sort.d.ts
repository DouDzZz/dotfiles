import { StyledIcon, StyledIconProps } from '..';
export declare const Sort: StyledIcon<any>;
export declare const SortDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
