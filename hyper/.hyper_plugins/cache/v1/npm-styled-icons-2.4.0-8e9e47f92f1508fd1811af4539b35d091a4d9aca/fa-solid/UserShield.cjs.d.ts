import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserShield: StyledIcon<any>;
export declare const UserShieldDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
