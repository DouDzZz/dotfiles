import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChartArea: StyledIcon<any>;
export declare const ChartAreaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
