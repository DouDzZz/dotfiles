import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Folder: StyledIcon<any>;
export declare const FolderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
