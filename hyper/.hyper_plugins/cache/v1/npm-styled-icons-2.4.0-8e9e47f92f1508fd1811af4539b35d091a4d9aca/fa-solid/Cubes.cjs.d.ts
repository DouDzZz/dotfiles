import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cubes: StyledIcon<any>;
export declare const CubesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
