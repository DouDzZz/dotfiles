import { StyledIcon, StyledIconProps } from '..';
export declare const PiggyBank: StyledIcon<any>;
export declare const PiggyBankDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
