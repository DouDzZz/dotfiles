import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Rss: StyledIcon<any>;
export declare const RssDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
