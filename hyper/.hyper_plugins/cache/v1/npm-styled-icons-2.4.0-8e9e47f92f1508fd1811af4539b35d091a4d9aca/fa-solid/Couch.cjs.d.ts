import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Couch: StyledIcon<any>;
export declare const CouchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
