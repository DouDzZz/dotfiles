import { StyledIcon, StyledIconProps } from '..';
export declare const Stethoscope: StyledIcon<any>;
export declare const StethoscopeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
