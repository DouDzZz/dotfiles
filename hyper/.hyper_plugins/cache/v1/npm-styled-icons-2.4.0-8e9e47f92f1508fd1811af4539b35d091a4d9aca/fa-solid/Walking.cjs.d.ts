import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Walking: StyledIcon<any>;
export declare const WalkingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
