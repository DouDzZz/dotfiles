import { StyledIcon, StyledIconProps } from '..';
export declare const Columns: StyledIcon<any>;
export declare const ColumnsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
