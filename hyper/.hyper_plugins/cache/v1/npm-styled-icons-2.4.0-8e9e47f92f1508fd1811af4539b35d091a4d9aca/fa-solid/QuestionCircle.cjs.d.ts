import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QuestionCircle: StyledIcon<any>;
export declare const QuestionCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
