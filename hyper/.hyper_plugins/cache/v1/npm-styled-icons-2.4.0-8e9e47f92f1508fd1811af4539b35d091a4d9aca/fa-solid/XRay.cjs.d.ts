import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const XRay: StyledIcon<any>;
export declare const XRayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
