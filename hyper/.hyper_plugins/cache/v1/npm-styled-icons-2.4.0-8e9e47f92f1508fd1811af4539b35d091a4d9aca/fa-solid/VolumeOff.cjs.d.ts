import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeOff: StyledIcon<any>;
export declare const VolumeOffDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
