import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crop: StyledIcon<any>;
export declare const CropDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
