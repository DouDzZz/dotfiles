import { StyledIcon, StyledIconProps } from '..';
export declare const FastForward: StyledIcon<any>;
export declare const FastForwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
