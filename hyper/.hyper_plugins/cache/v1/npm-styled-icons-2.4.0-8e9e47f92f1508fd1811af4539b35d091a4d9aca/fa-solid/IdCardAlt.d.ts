import { StyledIcon, StyledIconProps } from '..';
export declare const IdCardAlt: StyledIcon<any>;
export declare const IdCardAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
