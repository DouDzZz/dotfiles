import { StyledIcon, StyledIconProps } from '..';
export declare const Capsules: StyledIcon<any>;
export declare const CapsulesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
