import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExclamationCircle: StyledIcon<any>;
export declare const ExclamationCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
