import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Syringe: StyledIcon<any>;
export declare const SyringeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
