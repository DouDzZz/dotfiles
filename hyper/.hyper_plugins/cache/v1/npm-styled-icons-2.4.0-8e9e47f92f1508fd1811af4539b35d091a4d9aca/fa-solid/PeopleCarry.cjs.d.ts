import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PeopleCarry: StyledIcon<any>;
export declare const PeopleCarryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
