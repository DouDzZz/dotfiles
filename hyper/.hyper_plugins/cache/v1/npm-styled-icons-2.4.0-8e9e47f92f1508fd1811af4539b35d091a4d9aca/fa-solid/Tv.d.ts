import { StyledIcon, StyledIconProps } from '..';
export declare const Tv: StyledIcon<any>;
export declare const TvDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
