import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PrescriptionBottleAlt: StyledIcon<any>;
export declare const PrescriptionBottleAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
