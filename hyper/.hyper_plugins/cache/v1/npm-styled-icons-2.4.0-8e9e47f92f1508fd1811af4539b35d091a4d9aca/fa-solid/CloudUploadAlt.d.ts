import { StyledIcon, StyledIconProps } from '..';
export declare const CloudUploadAlt: StyledIcon<any>;
export declare const CloudUploadAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
