import { StyledIcon, StyledIconProps } from '..';
export declare const Medkit: StyledIcon<any>;
export declare const MedkitDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
