import { StyledIcon, StyledIconProps } from '..';
export declare const Key: StyledIcon<any>;
export declare const KeyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
