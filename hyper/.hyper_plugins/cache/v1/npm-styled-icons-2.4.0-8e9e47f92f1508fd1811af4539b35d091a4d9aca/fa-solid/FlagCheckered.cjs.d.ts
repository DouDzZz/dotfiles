import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FlagCheckered: StyledIcon<any>;
export declare const FlagCheckeredDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
