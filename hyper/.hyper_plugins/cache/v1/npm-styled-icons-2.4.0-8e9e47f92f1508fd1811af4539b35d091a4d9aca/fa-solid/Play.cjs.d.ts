import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Play: StyledIcon<any>;
export declare const PlayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
