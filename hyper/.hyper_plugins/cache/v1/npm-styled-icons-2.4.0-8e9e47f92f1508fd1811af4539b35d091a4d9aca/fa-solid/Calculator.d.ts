import { StyledIcon, StyledIconProps } from '..';
export declare const Calculator: StyledIcon<any>;
export declare const CalculatorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
