import { StyledIcon, StyledIconProps } from '..';
export declare const AngleDoubleUp: StyledIcon<any>;
export declare const AngleDoubleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
