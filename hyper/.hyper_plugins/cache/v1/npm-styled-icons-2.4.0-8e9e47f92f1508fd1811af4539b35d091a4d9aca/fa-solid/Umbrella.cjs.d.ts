import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Umbrella: StyledIcon<any>;
export declare const UmbrellaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
