import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowLeft: StyledIcon<any>;
export declare const ArrowLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
