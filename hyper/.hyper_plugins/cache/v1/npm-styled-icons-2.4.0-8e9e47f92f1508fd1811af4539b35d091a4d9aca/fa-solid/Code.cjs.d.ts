import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Code: StyledIcon<any>;
export declare const CodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
