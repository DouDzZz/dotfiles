import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MobileAlt: StyledIcon<any>;
export declare const MobileAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
