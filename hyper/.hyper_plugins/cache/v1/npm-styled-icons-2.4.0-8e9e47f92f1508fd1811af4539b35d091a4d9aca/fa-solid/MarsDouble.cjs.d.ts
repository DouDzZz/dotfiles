import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarsDouble: StyledIcon<any>;
export declare const MarsDoubleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
