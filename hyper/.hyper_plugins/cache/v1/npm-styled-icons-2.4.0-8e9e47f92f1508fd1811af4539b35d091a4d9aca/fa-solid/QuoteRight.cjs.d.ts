import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QuoteRight: StyledIcon<any>;
export declare const QuoteRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
