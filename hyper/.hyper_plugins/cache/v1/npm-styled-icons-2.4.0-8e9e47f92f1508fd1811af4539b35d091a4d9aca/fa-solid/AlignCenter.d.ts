import { StyledIcon, StyledIconProps } from '..';
export declare const AlignCenter: StyledIcon<any>;
export declare const AlignCenterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
