import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThermometerThreeQuarters: StyledIcon<any>;
export declare const ThermometerThreeQuartersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
