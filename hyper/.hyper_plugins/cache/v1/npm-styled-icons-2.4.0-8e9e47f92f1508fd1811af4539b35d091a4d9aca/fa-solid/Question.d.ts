import { StyledIcon, StyledIconProps } from '..';
export declare const Question: StyledIcon<any>;
export declare const QuestionDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
