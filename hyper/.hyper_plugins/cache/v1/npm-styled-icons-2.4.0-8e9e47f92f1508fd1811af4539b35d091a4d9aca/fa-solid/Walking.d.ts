import { StyledIcon, StyledIconProps } from '..';
export declare const Walking: StyledIcon<any>;
export declare const WalkingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
