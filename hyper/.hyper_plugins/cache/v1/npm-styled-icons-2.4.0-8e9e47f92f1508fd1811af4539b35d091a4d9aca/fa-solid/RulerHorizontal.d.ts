import { StyledIcon, StyledIconProps } from '..';
export declare const RulerHorizontal: StyledIcon<any>;
export declare const RulerHorizontalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
