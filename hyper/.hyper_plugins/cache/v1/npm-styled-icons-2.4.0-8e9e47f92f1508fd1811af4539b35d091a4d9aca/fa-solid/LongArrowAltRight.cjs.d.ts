import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LongArrowAltRight: StyledIcon<any>;
export declare const LongArrowAltRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
