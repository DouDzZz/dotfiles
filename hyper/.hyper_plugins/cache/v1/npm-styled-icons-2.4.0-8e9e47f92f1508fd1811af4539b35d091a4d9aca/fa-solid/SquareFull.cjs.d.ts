import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SquareFull: StyledIcon<any>;
export declare const SquareFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
