import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Moon: StyledIcon<any>;
export declare const MoonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
