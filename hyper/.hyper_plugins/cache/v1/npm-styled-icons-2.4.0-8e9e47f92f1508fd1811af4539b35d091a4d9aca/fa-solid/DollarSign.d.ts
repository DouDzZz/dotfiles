import { StyledIcon, StyledIconProps } from '..';
export declare const DollarSign: StyledIcon<any>;
export declare const DollarSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
