import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeOff: StyledIcon<any>;
export declare const VolumeOffDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
