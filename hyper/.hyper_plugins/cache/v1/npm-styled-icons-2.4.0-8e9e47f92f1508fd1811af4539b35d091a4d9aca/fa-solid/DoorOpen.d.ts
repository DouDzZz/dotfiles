import { StyledIcon, StyledIconProps } from '..';
export declare const DoorOpen: StyledIcon<any>;
export declare const DoorOpenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
