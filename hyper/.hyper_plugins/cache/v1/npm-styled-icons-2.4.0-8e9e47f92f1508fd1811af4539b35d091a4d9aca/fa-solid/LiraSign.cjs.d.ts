import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LiraSign: StyledIcon<any>;
export declare const LiraSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
