import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortAlphaDown: StyledIcon<any>;
export declare const SortAlphaDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
