import { StyledIcon, StyledIconProps } from '..';
export declare const Vials: StyledIcon<any>;
export declare const VialsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
