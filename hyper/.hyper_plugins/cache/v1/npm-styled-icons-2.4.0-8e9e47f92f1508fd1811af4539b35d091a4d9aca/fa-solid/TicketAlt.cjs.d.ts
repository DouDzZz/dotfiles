import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TicketAlt: StyledIcon<any>;
export declare const TicketAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
