import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserNinja: StyledIcon<any>;
export declare const UserNinjaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
