import { StyledIcon, StyledIconProps } from '..';
export declare const RulerVertical: StyledIcon<any>;
export declare const RulerVerticalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
