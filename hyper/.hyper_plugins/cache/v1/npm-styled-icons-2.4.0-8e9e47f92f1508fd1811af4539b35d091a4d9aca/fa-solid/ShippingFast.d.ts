import { StyledIcon, StyledIconProps } from '..';
export declare const ShippingFast: StyledIcon<any>;
export declare const ShippingFastDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
