import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HourglassEnd: StyledIcon<any>;
export declare const HourglassEndDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
