import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RulerCombined: StyledIcon<any>;
export declare const RulerCombinedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
