import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CalendarTimes: StyledIcon<any>;
export declare const CalendarTimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
