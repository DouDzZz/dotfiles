import { StyledIcon, StyledIconProps } from '..';
export declare const Crosshairs: StyledIcon<any>;
export declare const CrosshairsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
