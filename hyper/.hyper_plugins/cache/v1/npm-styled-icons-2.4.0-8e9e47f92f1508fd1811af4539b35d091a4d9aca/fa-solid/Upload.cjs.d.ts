import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Upload: StyledIcon<any>;
export declare const UploadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
