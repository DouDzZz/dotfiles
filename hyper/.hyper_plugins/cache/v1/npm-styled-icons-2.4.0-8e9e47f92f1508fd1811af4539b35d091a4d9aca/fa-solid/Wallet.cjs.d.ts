import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Wallet: StyledIcon<any>;
export declare const WalletDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
