import { StyledIcon, StyledIconProps } from '..';
export declare const Parking: StyledIcon<any>;
export declare const ParkingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
