import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserClock: StyledIcon<any>;
export declare const UserClockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
