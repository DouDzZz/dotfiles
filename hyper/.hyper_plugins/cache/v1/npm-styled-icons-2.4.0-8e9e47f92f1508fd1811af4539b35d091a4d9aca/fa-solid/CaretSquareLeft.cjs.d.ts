import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretSquareLeft: StyledIcon<any>;
export declare const CaretSquareLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
