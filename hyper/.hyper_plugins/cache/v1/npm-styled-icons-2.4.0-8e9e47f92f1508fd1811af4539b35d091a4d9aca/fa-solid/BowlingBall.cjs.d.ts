import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BowlingBall: StyledIcon<any>;
export declare const BowlingBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
