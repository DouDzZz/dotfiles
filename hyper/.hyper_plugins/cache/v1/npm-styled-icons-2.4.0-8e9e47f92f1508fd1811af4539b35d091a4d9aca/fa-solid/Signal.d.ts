import { StyledIcon, StyledIconProps } from '..';
export declare const Signal: StyledIcon<any>;
export declare const SignalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
