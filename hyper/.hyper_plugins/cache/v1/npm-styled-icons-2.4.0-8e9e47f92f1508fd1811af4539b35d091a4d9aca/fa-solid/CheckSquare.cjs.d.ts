import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CheckSquare: StyledIcon<any>;
export declare const CheckSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
