import { StyledIcon, StyledIconProps } from '..';
export declare const UserLock: StyledIcon<any>;
export declare const UserLockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
