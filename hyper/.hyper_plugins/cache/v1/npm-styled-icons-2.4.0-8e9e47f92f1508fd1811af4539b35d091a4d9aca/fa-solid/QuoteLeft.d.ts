import { StyledIcon, StyledIconProps } from '..';
export declare const QuoteLeft: StyledIcon<any>;
export declare const QuoteLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
