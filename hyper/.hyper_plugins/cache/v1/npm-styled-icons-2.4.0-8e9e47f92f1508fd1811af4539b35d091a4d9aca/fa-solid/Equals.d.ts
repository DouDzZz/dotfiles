import { StyledIcon, StyledIconProps } from '..';
export declare const Equals: StyledIcon<any>;
export declare const EqualsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
