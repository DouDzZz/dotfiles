import { StyledIcon, StyledIconProps } from '..';
export declare const Camera: StyledIcon<any>;
export declare const CameraDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
