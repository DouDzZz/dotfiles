import { StyledIcon, StyledIconProps } from '..';
export declare const Burn: StyledIcon<any>;
export declare const BurnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
