import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowCircleUp: StyledIcon<any>;
export declare const ArrowCircleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
