import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gavel: StyledIcon<any>;
export declare const GavelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
