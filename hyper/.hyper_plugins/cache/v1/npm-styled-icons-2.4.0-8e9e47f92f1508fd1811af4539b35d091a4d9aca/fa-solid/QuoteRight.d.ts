import { StyledIcon, StyledIconProps } from '..';
export declare const QuoteRight: StyledIcon<any>;
export declare const QuoteRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
