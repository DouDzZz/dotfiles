import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Allergies: StyledIcon<any>;
export declare const AllergiesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
