import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThermometerQuarter: StyledIcon<any>;
export declare const ThermometerQuarterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
