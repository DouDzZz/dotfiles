import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ribbon: StyledIcon<any>;
export declare const RibbonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
