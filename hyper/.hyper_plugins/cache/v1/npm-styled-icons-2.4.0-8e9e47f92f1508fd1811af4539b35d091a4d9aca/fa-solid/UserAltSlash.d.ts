import { StyledIcon, StyledIconProps } from '..';
export declare const UserAltSlash: StyledIcon<any>;
export declare const UserAltSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
