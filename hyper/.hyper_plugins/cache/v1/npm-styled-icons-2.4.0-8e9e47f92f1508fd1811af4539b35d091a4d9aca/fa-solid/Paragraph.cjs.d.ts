import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Paragraph: StyledIcon<any>;
export declare const ParagraphDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
