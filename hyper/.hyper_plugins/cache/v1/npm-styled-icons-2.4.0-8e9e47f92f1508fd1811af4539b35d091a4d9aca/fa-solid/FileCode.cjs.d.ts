import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileCode: StyledIcon<any>;
export declare const FileCodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
