import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fire: StyledIcon<any>;
export declare const FireDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
