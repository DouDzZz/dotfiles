import { StyledIcon, StyledIconProps } from '..';
export declare const Music: StyledIcon<any>;
export declare const MusicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
