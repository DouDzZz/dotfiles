import { StyledIcon, StyledIconProps } from '..';
export declare const Reply: StyledIcon<any>;
export declare const ReplyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
