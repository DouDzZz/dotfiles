import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExternalLinkSquareAlt: StyledIcon<any>;
export declare const ExternalLinkSquareAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
