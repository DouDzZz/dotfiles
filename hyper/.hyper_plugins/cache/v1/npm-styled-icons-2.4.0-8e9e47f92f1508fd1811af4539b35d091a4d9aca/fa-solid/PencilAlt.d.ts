import { StyledIcon, StyledIconProps } from '..';
export declare const PencilAlt: StyledIcon<any>;
export declare const PencilAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
