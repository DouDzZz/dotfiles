import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignJustify: StyledIcon<any>;
export declare const AlignJustifyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
