import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortAlphaUp: StyledIcon<any>;
export declare const SortAlphaUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
