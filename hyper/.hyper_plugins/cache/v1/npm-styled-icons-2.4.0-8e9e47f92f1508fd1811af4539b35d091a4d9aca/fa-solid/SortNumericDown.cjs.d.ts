import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortNumericDown: StyledIcon<any>;
export declare const SortNumericDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
