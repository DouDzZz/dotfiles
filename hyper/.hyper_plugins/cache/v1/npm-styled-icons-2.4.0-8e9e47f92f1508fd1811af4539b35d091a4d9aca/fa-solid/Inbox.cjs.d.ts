import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Inbox: StyledIcon<any>;
export declare const InboxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
