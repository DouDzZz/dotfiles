import { StyledIcon, StyledIconProps } from '..';
export declare const Unlock: StyledIcon<any>;
export declare const UnlockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
