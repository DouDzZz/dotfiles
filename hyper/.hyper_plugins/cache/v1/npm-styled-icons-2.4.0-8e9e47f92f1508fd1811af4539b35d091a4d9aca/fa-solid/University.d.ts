import { StyledIcon, StyledIconProps } from '..';
export declare const University: StyledIcon<any>;
export declare const UniversityDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
