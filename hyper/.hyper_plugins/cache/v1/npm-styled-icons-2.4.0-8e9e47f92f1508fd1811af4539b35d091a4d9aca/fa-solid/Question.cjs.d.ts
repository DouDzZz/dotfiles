import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Question: StyledIcon<any>;
export declare const QuestionDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
