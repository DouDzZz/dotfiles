import { StyledIcon, StyledIconProps } from '..';
export declare const CircleNotch: StyledIcon<any>;
export declare const CircleNotchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
