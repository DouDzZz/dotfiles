import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const User: StyledIcon<any>;
export declare const UserDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
