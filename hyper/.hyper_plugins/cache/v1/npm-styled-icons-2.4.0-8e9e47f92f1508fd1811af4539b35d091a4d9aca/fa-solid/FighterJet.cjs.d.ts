import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FighterJet: StyledIcon<any>;
export declare const FighterJetDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
