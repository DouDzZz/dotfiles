import { StyledIcon, StyledIconProps } from '..';
export declare const Tag: StyledIcon<any>;
export declare const TagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
