import { StyledIcon, StyledIconProps } from '..';
export declare const GolfBall: StyledIcon<any>;
export declare const GolfBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
