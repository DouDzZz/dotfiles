import { StyledIcon, StyledIconProps } from '..';
export declare const Subscript: StyledIcon<any>;
export declare const SubscriptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
