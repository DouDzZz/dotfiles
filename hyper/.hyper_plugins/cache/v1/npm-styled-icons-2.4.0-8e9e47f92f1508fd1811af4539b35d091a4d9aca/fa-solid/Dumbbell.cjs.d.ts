import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dumbbell: StyledIcon<any>;
export declare const DumbbellDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
