import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CommentDots: StyledIcon<any>;
export declare const CommentDotsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
