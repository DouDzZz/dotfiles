import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyBillWave: StyledIcon<any>;
export declare const MoneyBillWaveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
