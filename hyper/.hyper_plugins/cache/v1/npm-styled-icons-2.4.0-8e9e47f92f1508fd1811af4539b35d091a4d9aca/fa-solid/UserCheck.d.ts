import { StyledIcon, StyledIconProps } from '..';
export declare const UserCheck: StyledIcon<any>;
export declare const UserCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
