import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Terminal: StyledIcon<any>;
export declare const TerminalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
