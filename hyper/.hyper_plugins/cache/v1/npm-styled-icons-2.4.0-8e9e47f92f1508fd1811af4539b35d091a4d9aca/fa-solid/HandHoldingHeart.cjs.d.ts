import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandHoldingHeart: StyledIcon<any>;
export declare const HandHoldingHeartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
