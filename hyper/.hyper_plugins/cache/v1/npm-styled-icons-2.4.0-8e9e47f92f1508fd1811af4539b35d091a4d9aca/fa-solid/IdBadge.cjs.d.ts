import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IdBadge: StyledIcon<any>;
export declare const IdBadgeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
