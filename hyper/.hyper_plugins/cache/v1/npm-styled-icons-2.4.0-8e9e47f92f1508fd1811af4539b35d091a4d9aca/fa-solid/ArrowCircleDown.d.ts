import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowCircleDown: StyledIcon<any>;
export declare const ArrowCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
