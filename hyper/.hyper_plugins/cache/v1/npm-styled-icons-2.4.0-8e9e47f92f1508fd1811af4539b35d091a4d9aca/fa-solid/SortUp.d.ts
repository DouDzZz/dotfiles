import { StyledIcon, StyledIconProps } from '..';
export declare const SortUp: StyledIcon<any>;
export declare const SortUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
