import { StyledIcon, StyledIconProps } from '..';
export declare const VenusMars: StyledIcon<any>;
export declare const VenusMarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
