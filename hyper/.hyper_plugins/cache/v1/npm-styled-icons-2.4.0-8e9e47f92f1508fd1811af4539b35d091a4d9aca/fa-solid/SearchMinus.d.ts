import { StyledIcon, StyledIconProps } from '..';
export declare const SearchMinus: StyledIcon<any>;
export declare const SearchMinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
