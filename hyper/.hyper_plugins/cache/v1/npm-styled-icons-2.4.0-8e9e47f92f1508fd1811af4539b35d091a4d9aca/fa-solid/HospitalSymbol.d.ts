import { StyledIcon, StyledIconProps } from '..';
export declare const HospitalSymbol: StyledIcon<any>;
export declare const HospitalSymbolDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
