import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UsersCog: StyledIcon<any>;
export declare const UsersCogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
