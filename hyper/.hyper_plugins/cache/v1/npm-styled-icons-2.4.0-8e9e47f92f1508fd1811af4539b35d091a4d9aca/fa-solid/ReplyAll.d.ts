import { StyledIcon, StyledIconProps } from '..';
export declare const ReplyAll: StyledIcon<any>;
export declare const ReplyAllDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
