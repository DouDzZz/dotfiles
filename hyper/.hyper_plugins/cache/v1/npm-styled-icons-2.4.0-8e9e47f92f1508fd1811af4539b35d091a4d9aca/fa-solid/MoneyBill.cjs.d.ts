import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoneyBill: StyledIcon<any>;
export declare const MoneyBillDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
