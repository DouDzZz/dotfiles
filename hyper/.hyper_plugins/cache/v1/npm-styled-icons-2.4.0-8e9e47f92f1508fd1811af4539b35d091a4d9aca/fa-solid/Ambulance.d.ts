import { StyledIcon, StyledIconProps } from '..';
export declare const Ambulance: StyledIcon<any>;
export declare const AmbulanceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
