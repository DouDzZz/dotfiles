import { StyledIcon, StyledIconProps } from '..';
export declare const ThumbsDown: StyledIcon<any>;
export declare const ThumbsDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
