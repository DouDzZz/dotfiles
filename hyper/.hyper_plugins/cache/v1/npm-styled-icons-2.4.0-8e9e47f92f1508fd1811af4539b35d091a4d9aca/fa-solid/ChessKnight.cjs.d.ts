import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessKnight: StyledIcon<any>;
export declare const ChessKnightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
