import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Gift: StyledIcon<any>;
export declare const GiftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
