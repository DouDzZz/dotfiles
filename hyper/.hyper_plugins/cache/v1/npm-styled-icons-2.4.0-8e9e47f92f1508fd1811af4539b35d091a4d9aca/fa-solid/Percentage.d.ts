import { StyledIcon, StyledIconProps } from '..';
export declare const Percentage: StyledIcon<any>;
export declare const PercentageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
