import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Superscript: StyledIcon<any>;
export declare const SuperscriptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
