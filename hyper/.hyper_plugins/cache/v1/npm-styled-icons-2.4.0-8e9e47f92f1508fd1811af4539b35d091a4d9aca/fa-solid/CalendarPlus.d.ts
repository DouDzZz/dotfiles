import { StyledIcon, StyledIconProps } from '..';
export declare const CalendarPlus: StyledIcon<any>;
export declare const CalendarPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
