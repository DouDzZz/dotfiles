import { StyledIcon, StyledIconProps } from '..';
export declare const Copyright: StyledIcon<any>;
export declare const CopyrightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
