import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretSquareRight: StyledIcon<any>;
export declare const CaretSquareRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
