import { StyledIcon, StyledIconProps } from '..';
export declare const Certificate: StyledIcon<any>;
export declare const CertificateDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
