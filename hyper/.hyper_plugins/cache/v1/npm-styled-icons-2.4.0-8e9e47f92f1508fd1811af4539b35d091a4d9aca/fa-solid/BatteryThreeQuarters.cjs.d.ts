import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryThreeQuarters: StyledIcon<any>;
export declare const BatteryThreeQuartersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
