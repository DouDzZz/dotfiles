import { StyledIcon, StyledIconProps } from '..';
export declare const ChessPawn: StyledIcon<any>;
export declare const ChessPawnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
