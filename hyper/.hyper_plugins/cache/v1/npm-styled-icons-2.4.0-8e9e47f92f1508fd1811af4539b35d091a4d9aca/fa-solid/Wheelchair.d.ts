import { StyledIcon, StyledIconProps } from '..';
export declare const Wheelchair: StyledIcon<any>;
export declare const WheelchairDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
