import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BroadcastTower: StyledIcon<any>;
export declare const BroadcastTowerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
