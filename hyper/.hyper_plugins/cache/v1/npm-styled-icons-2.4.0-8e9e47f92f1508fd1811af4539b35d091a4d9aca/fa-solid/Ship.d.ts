import { StyledIcon, StyledIconProps } from '..';
export declare const Ship: StyledIcon<any>;
export declare const ShipDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
