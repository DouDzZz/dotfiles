import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UndoAlt: StyledIcon<any>;
export declare const UndoAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
