import { StyledIcon, StyledIconProps } from '..';
export declare const DiceOne: StyledIcon<any>;
export declare const DiceOneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
