import { StyledIcon, StyledIconProps } from '..';
export declare const Backward: StyledIcon<any>;
export declare const BackwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
