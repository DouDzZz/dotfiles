import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowLeft: StyledIcon<any>;
export declare const ArrowLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
