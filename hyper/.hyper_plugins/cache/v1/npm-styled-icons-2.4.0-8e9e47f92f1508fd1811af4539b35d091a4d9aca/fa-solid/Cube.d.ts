import { StyledIcon, StyledIconProps } from '..';
export declare const Cube: StyledIcon<any>;
export declare const CubeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
