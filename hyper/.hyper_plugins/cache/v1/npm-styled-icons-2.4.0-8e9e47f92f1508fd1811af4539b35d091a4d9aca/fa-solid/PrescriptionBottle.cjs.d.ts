import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PrescriptionBottle: StyledIcon<any>;
export declare const PrescriptionBottleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
