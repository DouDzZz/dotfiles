import { StyledIcon, StyledIconProps } from '..';
export declare const MapPin: StyledIcon<any>;
export declare const MapPinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
