import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DoorClosed: StyledIcon<any>;
export declare const DoorClosedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
