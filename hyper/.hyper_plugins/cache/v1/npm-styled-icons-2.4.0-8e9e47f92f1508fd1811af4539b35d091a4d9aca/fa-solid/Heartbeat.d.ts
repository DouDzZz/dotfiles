import { StyledIcon, StyledIconProps } from '..';
export declare const Heartbeat: StyledIcon<any>;
export declare const HeartbeatDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
