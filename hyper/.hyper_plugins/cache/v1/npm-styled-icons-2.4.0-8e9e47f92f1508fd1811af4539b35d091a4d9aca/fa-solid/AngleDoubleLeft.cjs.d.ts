import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AngleDoubleLeft: StyledIcon<any>;
export declare const AngleDoubleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
