import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tty: StyledIcon<any>;
export declare const TtyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
