import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LessThan: StyledIcon<any>;
export declare const LessThanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
