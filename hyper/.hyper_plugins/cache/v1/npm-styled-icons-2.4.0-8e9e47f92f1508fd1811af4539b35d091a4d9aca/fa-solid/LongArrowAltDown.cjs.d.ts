import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LongArrowAltDown: StyledIcon<any>;
export declare const LongArrowAltDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
