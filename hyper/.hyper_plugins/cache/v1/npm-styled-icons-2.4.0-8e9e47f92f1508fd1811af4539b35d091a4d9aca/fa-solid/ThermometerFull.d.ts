import { StyledIcon, StyledIconProps } from '..';
export declare const ThermometerFull: StyledIcon<any>;
export declare const ThermometerFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
