import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandHolding: StyledIcon<any>;
export declare const HandHoldingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
