import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SignInAlt: StyledIcon<any>;
export declare const SignInAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
