import { StyledIcon, StyledIconProps } from '..';
export declare const Palette: StyledIcon<any>;
export declare const PaletteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
