import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MousePointer: StyledIcon<any>;
export declare const MousePointerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
