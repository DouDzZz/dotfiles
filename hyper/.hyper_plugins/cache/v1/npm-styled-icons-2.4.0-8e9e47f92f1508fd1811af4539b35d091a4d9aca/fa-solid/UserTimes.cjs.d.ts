import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserTimes: StyledIcon<any>;
export declare const UserTimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
