import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Table: StyledIcon<any>;
export declare const TableDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
