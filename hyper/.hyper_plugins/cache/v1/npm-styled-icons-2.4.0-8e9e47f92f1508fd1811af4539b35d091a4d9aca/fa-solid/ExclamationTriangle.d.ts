import { StyledIcon, StyledIconProps } from '..';
export declare const ExclamationTriangle: StyledIcon<any>;
export declare const ExclamationTriangleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
