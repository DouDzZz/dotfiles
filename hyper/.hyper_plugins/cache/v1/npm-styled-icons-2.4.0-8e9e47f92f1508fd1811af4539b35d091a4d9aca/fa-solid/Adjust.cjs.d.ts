import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Adjust: StyledIcon<any>;
export declare const AdjustDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
