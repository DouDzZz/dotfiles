import { StyledIcon, StyledIconProps } from '..';
export declare const HandPointer: StyledIcon<any>;
export declare const HandPointerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
