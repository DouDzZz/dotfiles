import { StyledIcon, StyledIconProps } from '..';
export declare const LongArrowAltLeft: StyledIcon<any>;
export declare const LongArrowAltLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
