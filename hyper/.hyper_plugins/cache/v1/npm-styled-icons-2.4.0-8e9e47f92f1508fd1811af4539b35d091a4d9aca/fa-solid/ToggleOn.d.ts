import { StyledIcon, StyledIconProps } from '..';
export declare const ToggleOn: StyledIcon<any>;
export declare const ToggleOnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
