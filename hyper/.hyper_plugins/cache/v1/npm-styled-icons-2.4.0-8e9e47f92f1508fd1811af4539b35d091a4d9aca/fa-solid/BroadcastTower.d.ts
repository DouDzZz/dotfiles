import { StyledIcon, StyledIconProps } from '..';
export declare const BroadcastTower: StyledIcon<any>;
export declare const BroadcastTowerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
