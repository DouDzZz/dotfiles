import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tasks: StyledIcon<any>;
export declare const TasksDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
