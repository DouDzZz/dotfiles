import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const KiwiBird: StyledIcon<any>;
export declare const KiwiBirdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
