import { StyledIcon, StyledIconProps } from '..';
export declare const Unlink: StyledIcon<any>;
export declare const UnlinkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
