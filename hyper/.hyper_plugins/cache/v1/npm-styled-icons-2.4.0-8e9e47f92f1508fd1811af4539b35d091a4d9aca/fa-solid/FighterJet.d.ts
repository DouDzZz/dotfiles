import { StyledIcon, StyledIconProps } from '..';
export declare const FighterJet: StyledIcon<any>;
export declare const FighterJetDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
