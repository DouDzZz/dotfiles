import { StyledIcon, StyledIconProps } from '..';
export declare const Cogs: StyledIcon<any>;
export declare const CogsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
