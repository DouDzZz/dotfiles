import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SearchMinus: StyledIcon<any>;
export declare const SearchMinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
