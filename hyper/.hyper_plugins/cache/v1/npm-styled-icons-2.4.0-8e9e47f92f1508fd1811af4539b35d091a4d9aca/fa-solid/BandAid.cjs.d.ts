import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BandAid: StyledIcon<any>;
export declare const BandAidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
