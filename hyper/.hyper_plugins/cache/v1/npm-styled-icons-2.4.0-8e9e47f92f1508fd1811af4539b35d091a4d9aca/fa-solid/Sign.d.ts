import { StyledIcon, StyledIconProps } from '..';
export declare const Sign: StyledIcon<any>;
export declare const SignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
