import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bold: StyledIcon<any>;
export declare const BoldDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
