import { StyledIcon, StyledIconProps } from '..';
export declare const ChessBishop: StyledIcon<any>;
export declare const ChessBishopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
