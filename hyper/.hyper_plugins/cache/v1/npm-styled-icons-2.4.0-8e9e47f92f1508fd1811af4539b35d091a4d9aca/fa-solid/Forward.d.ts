import { StyledIcon, StyledIconProps } from '..';
export declare const Forward: StyledIcon<any>;
export declare const ForwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
