import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WineGlass: StyledIcon<any>;
export declare const WineGlassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
