import { StyledIcon, StyledIconProps } from '..';
export declare const FirstAid: StyledIcon<any>;
export declare const FirstAidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
