import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserTie: StyledIcon<any>;
export declare const UserTieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
