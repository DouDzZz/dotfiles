import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MicrophoneAltSlash: StyledIcon<any>;
export declare const MicrophoneAltSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
