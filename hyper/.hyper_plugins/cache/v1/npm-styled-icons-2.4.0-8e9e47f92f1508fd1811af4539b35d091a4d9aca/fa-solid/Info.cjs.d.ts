import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Info: StyledIcon<any>;
export declare const InfoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
