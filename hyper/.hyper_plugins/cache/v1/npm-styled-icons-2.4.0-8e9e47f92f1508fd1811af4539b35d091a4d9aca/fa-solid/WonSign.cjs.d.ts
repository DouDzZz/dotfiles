import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WonSign: StyledIcon<any>;
export declare const WonSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
