import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Desktop: StyledIcon<any>;
export declare const DesktopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
