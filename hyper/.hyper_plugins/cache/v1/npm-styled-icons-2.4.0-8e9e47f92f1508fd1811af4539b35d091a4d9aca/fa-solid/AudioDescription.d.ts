import { StyledIcon, StyledIconProps } from '..';
export declare const AudioDescription: StyledIcon<any>;
export declare const AudioDescriptionDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
