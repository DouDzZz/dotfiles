import { StyledIcon, StyledIconProps } from '..';
export declare const ThermometerEmpty: StyledIcon<any>;
export declare const ThermometerEmptyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
