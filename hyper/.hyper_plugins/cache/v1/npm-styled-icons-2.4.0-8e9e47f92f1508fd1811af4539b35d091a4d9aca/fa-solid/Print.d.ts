import { StyledIcon, StyledIconProps } from '..';
export declare const Print: StyledIcon<any>;
export declare const PrintDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
