import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronCircleLeft: StyledIcon<any>;
export declare const ChevronCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
