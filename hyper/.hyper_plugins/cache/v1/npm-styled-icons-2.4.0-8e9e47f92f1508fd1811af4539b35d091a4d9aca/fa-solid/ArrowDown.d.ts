import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowDown: StyledIcon<any>;
export declare const ArrowDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
