import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RulerVertical: StyledIcon<any>;
export declare const RulerVerticalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
