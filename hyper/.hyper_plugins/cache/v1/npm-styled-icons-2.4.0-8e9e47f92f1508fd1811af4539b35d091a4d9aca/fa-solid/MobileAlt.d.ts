import { StyledIcon, StyledIconProps } from '..';
export declare const MobileAlt: StyledIcon<any>;
export declare const MobileAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
