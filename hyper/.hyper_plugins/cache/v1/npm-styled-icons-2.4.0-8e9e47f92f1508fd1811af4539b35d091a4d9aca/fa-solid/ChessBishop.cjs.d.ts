import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessBishop: StyledIcon<any>;
export declare const ChessBishopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
