import { StyledIcon, StyledIconProps } from '..';
export declare const Dice: StyledIcon<any>;
export declare const DiceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
