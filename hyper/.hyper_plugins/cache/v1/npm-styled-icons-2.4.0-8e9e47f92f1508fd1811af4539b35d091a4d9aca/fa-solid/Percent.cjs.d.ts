import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Percent: StyledIcon<any>;
export declare const PercentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
