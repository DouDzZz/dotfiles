import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PauseCircle: StyledIcon<any>;
export declare const PauseCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
