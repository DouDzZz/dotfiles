import { StyledIcon, StyledIconProps } from '..';
export declare const Bars: StyledIcon<any>;
export declare const BarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
