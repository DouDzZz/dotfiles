import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WindowRestore: StyledIcon<any>;
export declare const WindowRestoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
