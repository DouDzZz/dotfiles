import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tablet: StyledIcon<any>;
export declare const TabletDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
