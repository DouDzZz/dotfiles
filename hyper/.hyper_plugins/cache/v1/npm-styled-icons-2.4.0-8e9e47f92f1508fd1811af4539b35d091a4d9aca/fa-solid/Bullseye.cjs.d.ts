import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bullseye: StyledIcon<any>;
export declare const BullseyeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
