import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Stop: StyledIcon<any>;
export declare const StopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
