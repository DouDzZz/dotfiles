import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Memory: StyledIcon<any>;
export declare const MemoryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
