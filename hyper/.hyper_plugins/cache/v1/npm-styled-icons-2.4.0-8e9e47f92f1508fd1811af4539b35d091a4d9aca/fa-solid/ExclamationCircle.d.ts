import { StyledIcon, StyledIconProps } from '..';
export declare const ExclamationCircle: StyledIcon<any>;
export declare const ExclamationCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
