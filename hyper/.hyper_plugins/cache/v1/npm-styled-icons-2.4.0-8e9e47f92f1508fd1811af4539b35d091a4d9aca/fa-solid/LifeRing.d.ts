import { StyledIcon, StyledIconProps } from '..';
export declare const LifeRing: StyledIcon<any>;
export declare const LifeRingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
