import { StyledIcon, StyledIconProps } from '..';
export declare const Plug: StyledIcon<any>;
export declare const PlugDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
