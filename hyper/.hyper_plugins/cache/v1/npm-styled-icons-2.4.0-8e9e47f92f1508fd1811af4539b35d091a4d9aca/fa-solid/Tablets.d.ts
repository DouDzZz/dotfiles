import { StyledIcon, StyledIconProps } from '..';
export declare const Tablets: StyledIcon<any>;
export declare const TabletsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
