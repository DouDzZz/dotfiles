import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Edit: StyledIcon<any>;
export declare const EditDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
