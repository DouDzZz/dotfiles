import { StyledIcon, StyledIconProps } from '..';
export declare const Crop: StyledIcon<any>;
export declare const CropDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
