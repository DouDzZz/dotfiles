import { StyledIcon, StyledIconProps } from '..';
export declare const Diagnoses: StyledIcon<any>;
export declare const DiagnosesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
