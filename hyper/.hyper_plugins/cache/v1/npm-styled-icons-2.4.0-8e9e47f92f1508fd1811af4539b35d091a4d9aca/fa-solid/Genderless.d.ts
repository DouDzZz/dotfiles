import { StyledIcon, StyledIconProps } from '..';
export declare const Genderless: StyledIcon<any>;
export declare const GenderlessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
