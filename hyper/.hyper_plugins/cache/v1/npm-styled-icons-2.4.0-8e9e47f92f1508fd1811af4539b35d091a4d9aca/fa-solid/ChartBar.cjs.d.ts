import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChartBar: StyledIcon<any>;
export declare const ChartBarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
