import { StyledIcon, StyledIconProps } from '..';
export declare const Industry: StyledIcon<any>;
export declare const IndustryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
