import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Chess: StyledIcon<any>;
export declare const ChessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
