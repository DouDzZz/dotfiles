import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Suitcase: StyledIcon<any>;
export declare const SuitcaseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
