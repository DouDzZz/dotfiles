import { StyledIcon, StyledIconProps } from '..';
export declare const Qrcode: StyledIcon<any>;
export declare const QrcodeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
