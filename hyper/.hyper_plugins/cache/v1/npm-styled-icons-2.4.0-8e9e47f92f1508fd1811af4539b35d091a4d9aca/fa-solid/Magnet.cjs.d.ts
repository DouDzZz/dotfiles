import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Magnet: StyledIcon<any>;
export declare const MagnetDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
