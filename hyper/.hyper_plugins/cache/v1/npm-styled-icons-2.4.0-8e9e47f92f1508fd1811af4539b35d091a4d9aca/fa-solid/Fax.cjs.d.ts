import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Fax: StyledIcon<any>;
export declare const FaxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
