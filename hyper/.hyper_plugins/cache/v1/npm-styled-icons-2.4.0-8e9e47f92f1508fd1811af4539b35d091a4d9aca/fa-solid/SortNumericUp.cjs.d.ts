import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortNumericUp: StyledIcon<any>;
export declare const SortNumericUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
