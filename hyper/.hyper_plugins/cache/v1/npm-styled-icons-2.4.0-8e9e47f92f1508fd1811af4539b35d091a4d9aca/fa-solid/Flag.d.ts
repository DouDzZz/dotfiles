import { StyledIcon, StyledIconProps } from '..';
export declare const Flag: StyledIcon<any>;
export declare const FlagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
