import { StyledIcon, StyledIconProps } from '..';
export declare const Mars: StyledIcon<any>;
export declare const MarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
