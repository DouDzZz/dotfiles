import { StyledIcon, StyledIconProps } from '..';
export declare const SlidersH: StyledIcon<any>;
export declare const SlidersHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
