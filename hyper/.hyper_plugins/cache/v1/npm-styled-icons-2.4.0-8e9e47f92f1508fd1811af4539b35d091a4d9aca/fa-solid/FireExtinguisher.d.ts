import { StyledIcon, StyledIconProps } from '..';
export declare const FireExtinguisher: StyledIcon<any>;
export declare const FireExtinguisherDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
