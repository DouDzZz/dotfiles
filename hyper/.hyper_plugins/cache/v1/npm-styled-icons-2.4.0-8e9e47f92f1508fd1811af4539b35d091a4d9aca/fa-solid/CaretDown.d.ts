import { StyledIcon, StyledIconProps } from '..';
export declare const CaretDown: StyledIcon<any>;
export declare const CaretDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
