import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MicrophoneAlt: StyledIcon<any>;
export declare const MicrophoneAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
