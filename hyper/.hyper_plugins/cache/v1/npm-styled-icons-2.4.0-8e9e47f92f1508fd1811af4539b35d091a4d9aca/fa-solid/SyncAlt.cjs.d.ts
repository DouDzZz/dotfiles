import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SyncAlt: StyledIcon<any>;
export declare const SyncAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
