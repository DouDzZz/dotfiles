import { StyledIcon, StyledIconProps } from '..';
export declare const Helicopter: StyledIcon<any>;
export declare const HelicopterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
