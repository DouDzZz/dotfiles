import { StyledIcon, StyledIconProps } from '..';
export declare const Check: StyledIcon<any>;
export declare const CheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
