import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GreaterThan: StyledIcon<any>;
export declare const GreaterThanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
