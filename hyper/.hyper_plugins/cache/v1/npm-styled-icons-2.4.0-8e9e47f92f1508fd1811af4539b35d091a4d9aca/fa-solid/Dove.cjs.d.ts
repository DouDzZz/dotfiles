import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Dove: StyledIcon<any>;
export declare const DoveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
