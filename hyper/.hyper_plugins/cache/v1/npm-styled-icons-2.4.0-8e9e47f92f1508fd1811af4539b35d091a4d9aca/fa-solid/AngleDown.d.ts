import { StyledIcon, StyledIconProps } from '..';
export declare const AngleDown: StyledIcon<any>;
export declare const AngleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
