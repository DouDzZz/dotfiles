import { StyledIcon, StyledIconProps } from '..';
export declare const LessThanEqual: StyledIcon<any>;
export declare const LessThanEqualDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
