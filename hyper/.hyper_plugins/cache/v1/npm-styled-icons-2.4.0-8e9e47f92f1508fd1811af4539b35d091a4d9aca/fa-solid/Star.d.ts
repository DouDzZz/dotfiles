import { StyledIcon, StyledIconProps } from '..';
export declare const Star: StyledIcon<any>;
export declare const StarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
