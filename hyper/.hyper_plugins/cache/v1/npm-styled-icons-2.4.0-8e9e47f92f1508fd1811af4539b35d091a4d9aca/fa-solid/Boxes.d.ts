import { StyledIcon, StyledIconProps } from '..';
export declare const Boxes: StyledIcon<any>;
export declare const BoxesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
