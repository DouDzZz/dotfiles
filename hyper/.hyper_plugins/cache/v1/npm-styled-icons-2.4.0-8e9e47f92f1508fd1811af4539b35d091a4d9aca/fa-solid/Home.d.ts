import { StyledIcon, StyledIconProps } from '..';
export declare const Home: StyledIcon<any>;
export declare const HomeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
