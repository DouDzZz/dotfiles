import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Language: StyledIcon<any>;
export declare const LanguageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
