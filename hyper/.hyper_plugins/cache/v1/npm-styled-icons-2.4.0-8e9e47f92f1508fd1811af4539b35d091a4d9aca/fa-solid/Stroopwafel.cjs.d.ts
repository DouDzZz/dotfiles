import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Stroopwafel: StyledIcon<any>;
export declare const StroopwafelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
