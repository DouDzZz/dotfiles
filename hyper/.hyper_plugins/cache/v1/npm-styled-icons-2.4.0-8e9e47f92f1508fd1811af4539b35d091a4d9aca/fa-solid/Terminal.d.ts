import { StyledIcon, StyledIconProps } from '..';
export declare const Terminal: StyledIcon<any>;
export declare const TerminalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
