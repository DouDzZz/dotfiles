import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VenusMars: StyledIcon<any>;
export declare const VenusMarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
