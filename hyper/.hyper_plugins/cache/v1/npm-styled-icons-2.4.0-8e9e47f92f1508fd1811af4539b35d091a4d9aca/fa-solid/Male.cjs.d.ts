import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Male: StyledIcon<any>;
export declare const MaleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
