import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MoneyBillWave: StyledIcon<any>;
export declare const MoneyBillWaveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
