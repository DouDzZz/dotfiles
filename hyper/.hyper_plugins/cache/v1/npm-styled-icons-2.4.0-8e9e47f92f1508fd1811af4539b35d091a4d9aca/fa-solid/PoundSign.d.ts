import { StyledIcon, StyledIconProps } from '..';
export declare const PoundSign: StyledIcon<any>;
export declare const PoundSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
