import { StyledIcon, StyledIconProps } from '..';
export declare const PlusCircle: StyledIcon<any>;
export declare const PlusCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
