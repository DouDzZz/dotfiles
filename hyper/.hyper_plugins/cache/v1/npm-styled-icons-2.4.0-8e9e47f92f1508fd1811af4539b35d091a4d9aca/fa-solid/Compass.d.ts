import { StyledIcon, StyledIconProps } from '..';
export declare const Compass: StyledIcon<any>;
export declare const CompassDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
