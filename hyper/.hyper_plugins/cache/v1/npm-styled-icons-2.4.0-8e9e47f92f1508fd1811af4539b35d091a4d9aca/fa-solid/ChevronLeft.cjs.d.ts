import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronLeft: StyledIcon<any>;
export declare const ChevronLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
