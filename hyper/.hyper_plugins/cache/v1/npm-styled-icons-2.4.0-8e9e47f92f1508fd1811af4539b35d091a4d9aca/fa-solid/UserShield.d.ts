import { StyledIcon, StyledIconProps } from '..';
export declare const UserShield: StyledIcon<any>;
export declare const UserShieldDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
