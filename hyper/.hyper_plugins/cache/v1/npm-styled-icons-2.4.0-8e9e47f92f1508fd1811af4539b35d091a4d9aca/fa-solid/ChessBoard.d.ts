import { StyledIcon, StyledIconProps } from '..';
export declare const ChessBoard: StyledIcon<any>;
export declare const ChessBoardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
