import { StyledIcon, StyledIconProps } from '..';
export declare const Sitemap: StyledIcon<any>;
export declare const SitemapDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
