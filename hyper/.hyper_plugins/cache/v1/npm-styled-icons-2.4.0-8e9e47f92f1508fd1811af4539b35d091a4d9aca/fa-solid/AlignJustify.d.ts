import { StyledIcon, StyledIconProps } from '..';
export declare const AlignJustify: StyledIcon<any>;
export declare const AlignJustifyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
