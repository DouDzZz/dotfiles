import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserMinus: StyledIcon<any>;
export declare const UserMinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
