import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AmericanSignLanguageInterpreting: StyledIcon<any>;
export declare const AmericanSignLanguageInterpretingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
