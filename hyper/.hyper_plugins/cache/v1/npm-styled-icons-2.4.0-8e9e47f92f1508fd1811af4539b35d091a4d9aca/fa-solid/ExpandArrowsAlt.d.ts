import { StyledIcon, StyledIconProps } from '..';
export declare const ExpandArrowsAlt: StyledIcon<any>;
export declare const ExpandArrowsAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
