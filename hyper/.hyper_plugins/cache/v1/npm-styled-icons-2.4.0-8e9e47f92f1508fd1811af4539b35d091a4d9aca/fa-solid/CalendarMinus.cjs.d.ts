import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CalendarMinus: StyledIcon<any>;
export declare const CalendarMinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
