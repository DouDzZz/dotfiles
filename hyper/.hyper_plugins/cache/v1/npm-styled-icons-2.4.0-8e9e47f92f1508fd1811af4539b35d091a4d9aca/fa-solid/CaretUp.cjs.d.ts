import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretUp: StyledIcon<any>;
export declare const CaretUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
