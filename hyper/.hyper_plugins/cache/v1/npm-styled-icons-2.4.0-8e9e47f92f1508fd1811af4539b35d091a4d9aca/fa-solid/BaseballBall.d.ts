import { StyledIcon, StyledIconProps } from '..';
export declare const BaseballBall: StyledIcon<any>;
export declare const BaseballBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
