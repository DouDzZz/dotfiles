import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MapMarkerAlt: StyledIcon<any>;
export declare const MapMarkerAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
