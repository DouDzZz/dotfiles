import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeDown: StyledIcon<any>;
export declare const VolumeDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
