import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThermometerHalf: StyledIcon<any>;
export declare const ThermometerHalfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
