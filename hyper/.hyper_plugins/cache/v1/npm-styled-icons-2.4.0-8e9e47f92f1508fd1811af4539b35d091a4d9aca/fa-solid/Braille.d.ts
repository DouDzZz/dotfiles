import { StyledIcon, StyledIconProps } from '..';
export declare const Braille: StyledIcon<any>;
export declare const BrailleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
