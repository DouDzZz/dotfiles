import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const List: StyledIcon<any>;
export declare const ListDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
