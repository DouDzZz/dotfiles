import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UtensilSpoon: StyledIcon<any>;
export declare const UtensilSpoonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
