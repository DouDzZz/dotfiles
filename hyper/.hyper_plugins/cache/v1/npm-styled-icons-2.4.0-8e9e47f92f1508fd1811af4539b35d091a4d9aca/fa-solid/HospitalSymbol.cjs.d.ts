import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HospitalSymbol: StyledIcon<any>;
export declare const HospitalSymbolDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
