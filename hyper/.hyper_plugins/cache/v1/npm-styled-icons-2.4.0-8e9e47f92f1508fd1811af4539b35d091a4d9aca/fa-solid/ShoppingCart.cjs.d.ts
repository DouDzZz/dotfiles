import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShoppingCart: StyledIcon<any>;
export declare const ShoppingCartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
