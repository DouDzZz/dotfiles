import { StyledIcon, StyledIconProps } from '..';
export declare const Bookmark: StyledIcon<any>;
export declare const BookmarkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
