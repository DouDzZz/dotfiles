import { StyledIcon, StyledIconProps } from '..';
export declare const Bold: StyledIcon<any>;
export declare const BoldDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
