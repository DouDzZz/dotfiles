import { StyledIcon, StyledIconProps } from '..';
export declare const Percent: StyledIcon<any>;
export declare const PercentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
