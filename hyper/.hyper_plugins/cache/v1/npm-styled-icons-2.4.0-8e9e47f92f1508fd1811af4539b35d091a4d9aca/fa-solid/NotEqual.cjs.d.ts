import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const NotEqual: StyledIcon<any>;
export declare const NotEqualDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
