import { StyledIcon, StyledIconProps } from '..';
export declare const Superscript: StyledIcon<any>;
export declare const SuperscriptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
