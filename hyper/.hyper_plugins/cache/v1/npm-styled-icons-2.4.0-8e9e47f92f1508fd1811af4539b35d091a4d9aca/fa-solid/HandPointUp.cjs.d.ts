import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandPointUp: StyledIcon<any>;
export declare const HandPointUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
