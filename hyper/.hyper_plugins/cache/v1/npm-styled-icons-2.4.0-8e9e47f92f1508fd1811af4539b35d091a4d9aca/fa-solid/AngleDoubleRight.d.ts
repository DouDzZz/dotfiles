import { StyledIcon, StyledIconProps } from '..';
export declare const AngleDoubleRight: StyledIcon<any>;
export declare const AngleDoubleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
