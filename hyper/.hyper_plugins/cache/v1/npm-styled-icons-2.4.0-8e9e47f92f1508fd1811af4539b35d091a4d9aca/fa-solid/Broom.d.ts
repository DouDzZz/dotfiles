import { StyledIcon, StyledIconProps } from '..';
export declare const Broom: StyledIcon<any>;
export declare const BroomDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
