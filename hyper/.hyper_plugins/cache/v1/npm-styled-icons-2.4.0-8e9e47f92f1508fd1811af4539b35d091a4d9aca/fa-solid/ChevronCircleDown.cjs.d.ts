import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronCircleDown: StyledIcon<any>;
export declare const ChevronCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
