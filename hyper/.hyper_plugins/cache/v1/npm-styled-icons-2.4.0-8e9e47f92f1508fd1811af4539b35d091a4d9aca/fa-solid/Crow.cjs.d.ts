import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crow: StyledIcon<any>;
export declare const CrowDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
