import { StyledIcon, StyledIconProps } from '..';
export declare const Server: StyledIcon<any>;
export declare const ServerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
