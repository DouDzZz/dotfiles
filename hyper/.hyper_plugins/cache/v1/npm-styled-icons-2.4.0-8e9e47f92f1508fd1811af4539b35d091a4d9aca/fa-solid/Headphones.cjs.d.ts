import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Headphones: StyledIcon<any>;
export declare const HeadphonesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
