import { StyledIcon, StyledIconProps } from '..';
export declare const AddressBook: StyledIcon<any>;
export declare const AddressBookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
