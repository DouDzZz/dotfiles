import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ambulance: StyledIcon<any>;
export declare const AmbulanceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
