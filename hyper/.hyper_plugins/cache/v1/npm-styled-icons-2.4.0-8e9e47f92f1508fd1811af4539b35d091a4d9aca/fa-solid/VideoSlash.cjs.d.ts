import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VideoSlash: StyledIcon<any>;
export declare const VideoSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
