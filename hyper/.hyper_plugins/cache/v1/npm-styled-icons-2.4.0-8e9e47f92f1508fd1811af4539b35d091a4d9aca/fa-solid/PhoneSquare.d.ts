import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneSquare: StyledIcon<any>;
export declare const PhoneSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
