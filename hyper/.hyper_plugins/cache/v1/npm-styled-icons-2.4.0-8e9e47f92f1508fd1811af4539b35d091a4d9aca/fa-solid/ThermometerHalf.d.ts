import { StyledIcon, StyledIconProps } from '..';
export declare const ThermometerHalf: StyledIcon<any>;
export declare const ThermometerHalfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
