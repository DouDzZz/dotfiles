import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RulerHorizontal: StyledIcon<any>;
export declare const RulerHorizontalDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
