import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandsHelping: StyledIcon<any>;
export declare const HandsHelpingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
