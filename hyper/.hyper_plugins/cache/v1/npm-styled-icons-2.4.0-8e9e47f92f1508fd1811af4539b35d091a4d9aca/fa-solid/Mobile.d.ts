import { StyledIcon, StyledIconProps } from '..';
export declare const Mobile: StyledIcon<any>;
export declare const MobileDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
