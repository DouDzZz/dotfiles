import { StyledIcon, StyledIconProps } from '..';
export declare const Procedures: StyledIcon<any>;
export declare const ProceduresDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
