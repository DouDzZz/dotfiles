import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Helicopter: StyledIcon<any>;
export declare const HelicopterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
