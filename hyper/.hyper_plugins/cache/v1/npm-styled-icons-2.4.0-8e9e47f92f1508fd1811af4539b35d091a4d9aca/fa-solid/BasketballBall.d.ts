import { StyledIcon, StyledIconProps } from '..';
export declare const BasketballBall: StyledIcon<any>;
export declare const BasketballBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
