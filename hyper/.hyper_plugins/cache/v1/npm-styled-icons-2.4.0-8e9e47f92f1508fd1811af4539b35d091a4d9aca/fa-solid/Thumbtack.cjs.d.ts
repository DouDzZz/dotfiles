import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Thumbtack: StyledIcon<any>;
export declare const ThumbtackDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
