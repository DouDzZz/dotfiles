import { StyledIcon, StyledIconProps } from '..';
export declare const SignOutAlt: StyledIcon<any>;
export declare const SignOutAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
