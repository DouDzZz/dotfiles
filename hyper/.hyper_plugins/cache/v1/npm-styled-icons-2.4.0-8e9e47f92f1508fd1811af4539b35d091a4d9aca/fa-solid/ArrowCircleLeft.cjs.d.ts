import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowCircleLeft: StyledIcon<any>;
export declare const ArrowCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
