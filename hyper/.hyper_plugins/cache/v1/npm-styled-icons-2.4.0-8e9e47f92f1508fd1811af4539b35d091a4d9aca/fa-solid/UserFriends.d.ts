import { StyledIcon, StyledIconProps } from '..';
export declare const UserFriends: StyledIcon<any>;
export declare const UserFriendsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
