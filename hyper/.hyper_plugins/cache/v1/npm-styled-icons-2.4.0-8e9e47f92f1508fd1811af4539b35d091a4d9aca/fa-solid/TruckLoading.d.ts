import { StyledIcon, StyledIconProps } from '..';
export declare const TruckLoading: StyledIcon<any>;
export declare const TruckLoadingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
