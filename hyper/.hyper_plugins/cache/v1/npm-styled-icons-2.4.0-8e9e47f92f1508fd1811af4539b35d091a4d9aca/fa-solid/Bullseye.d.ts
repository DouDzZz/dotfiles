import { StyledIcon, StyledIconProps } from '..';
export declare const Bullseye: StyledIcon<any>;
export declare const BullseyeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
