import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const WindowMaximize: StyledIcon<any>;
export declare const WindowMaximizeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
