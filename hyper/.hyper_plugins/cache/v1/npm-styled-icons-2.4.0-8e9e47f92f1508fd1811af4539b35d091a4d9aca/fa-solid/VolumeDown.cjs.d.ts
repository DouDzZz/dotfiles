import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeDown: StyledIcon<any>;
export declare const VolumeDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
