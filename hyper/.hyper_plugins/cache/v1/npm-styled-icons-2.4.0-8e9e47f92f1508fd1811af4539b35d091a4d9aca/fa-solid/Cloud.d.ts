import { StyledIcon, StyledIconProps } from '..';
export declare const Cloud: StyledIcon<any>;
export declare const CloudDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
