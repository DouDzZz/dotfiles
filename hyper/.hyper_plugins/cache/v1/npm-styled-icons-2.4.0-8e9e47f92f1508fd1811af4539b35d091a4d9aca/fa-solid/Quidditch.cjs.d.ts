import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Quidditch: StyledIcon<any>;
export declare const QuidditchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
