import { StyledIcon, StyledIconProps } from '..';
export declare const Rss: StyledIcon<any>;
export declare const RssDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
