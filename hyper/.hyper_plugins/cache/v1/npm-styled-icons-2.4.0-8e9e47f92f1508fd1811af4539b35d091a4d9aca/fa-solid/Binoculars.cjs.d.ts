import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Binoculars: StyledIcon<any>;
export declare const BinocularsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
