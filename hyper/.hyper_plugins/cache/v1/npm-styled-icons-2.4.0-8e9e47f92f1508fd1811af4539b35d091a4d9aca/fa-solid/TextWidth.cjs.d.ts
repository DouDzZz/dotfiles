import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TextWidth: StyledIcon<any>;
export declare const TextWidthDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
