import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GlassMartini: StyledIcon<any>;
export declare const GlassMartiniDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
