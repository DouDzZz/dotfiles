import { StyledIcon, StyledIconProps } from '..';
export declare const Seedling: StyledIcon<any>;
export declare const SeedlingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
