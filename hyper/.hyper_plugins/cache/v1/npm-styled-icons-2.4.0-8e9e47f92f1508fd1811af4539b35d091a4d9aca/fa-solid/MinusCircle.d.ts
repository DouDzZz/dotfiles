import { StyledIcon, StyledIconProps } from '..';
export declare const MinusCircle: StyledIcon<any>;
export declare const MinusCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
