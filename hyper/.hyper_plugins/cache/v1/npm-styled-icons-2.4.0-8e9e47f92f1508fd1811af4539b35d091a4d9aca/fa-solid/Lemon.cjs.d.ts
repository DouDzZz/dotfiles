import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lemon: StyledIcon<any>;
export declare const LemonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
