import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Taxi: StyledIcon<any>;
export declare const TaxiDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
