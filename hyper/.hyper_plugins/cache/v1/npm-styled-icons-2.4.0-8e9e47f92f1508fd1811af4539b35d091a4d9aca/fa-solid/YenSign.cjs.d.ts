import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const YenSign: StyledIcon<any>;
export declare const YenSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
