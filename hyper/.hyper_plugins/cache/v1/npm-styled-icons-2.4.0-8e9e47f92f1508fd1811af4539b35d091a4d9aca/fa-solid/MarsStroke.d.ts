import { StyledIcon, StyledIconProps } from '..';
export declare const MarsStroke: StyledIcon<any>;
export declare const MarsStrokeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
