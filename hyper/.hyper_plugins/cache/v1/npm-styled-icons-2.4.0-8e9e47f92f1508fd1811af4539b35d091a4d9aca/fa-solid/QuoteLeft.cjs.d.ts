import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const QuoteLeft: StyledIcon<any>;
export declare const QuoteLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
