import { StyledIcon, StyledIconProps } from '..';
export declare const StoreAlt: StyledIcon<any>;
export declare const StoreAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
