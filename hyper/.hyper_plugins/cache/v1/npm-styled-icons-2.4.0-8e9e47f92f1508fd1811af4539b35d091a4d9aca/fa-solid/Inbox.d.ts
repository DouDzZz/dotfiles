import { StyledIcon, StyledIconProps } from '..';
export declare const Inbox: StyledIcon<any>;
export declare const InboxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
