import { StyledIcon, StyledIconProps } from '..';
export declare const EllipsisV: StyledIcon<any>;
export declare const EllipsisVDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
