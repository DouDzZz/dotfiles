import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LifeRing: StyledIcon<any>;
export declare const LifeRingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
