import { StyledIcon, StyledIconProps } from '..';
export declare const GasPump: StyledIcon<any>;
export declare const GasPumpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
