import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AngleUp: StyledIcon<any>;
export declare const AngleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
