import { StyledIcon, StyledIconProps } from '..';
export declare const HockeyPuck: StyledIcon<any>;
export declare const HockeyPuckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
