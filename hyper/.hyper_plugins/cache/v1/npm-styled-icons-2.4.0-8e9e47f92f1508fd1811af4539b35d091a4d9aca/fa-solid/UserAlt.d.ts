import { StyledIcon, StyledIconProps } from '..';
export declare const UserAlt: StyledIcon<any>;
export declare const UserAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
