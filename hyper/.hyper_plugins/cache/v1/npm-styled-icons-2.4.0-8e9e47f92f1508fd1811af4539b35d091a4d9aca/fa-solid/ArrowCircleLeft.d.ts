import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowCircleLeft: StyledIcon<any>;
export declare const ArrowCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
