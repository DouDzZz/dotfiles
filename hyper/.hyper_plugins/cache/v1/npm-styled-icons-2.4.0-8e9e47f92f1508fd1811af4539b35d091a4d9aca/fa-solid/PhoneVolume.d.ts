import { StyledIcon, StyledIconProps } from '..';
export declare const PhoneVolume: StyledIcon<any>;
export declare const PhoneVolumeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
