import { StyledIcon, StyledIconProps } from '..';
export declare const TruckMoving: StyledIcon<any>;
export declare const TruckMovingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
