import { StyledIcon, StyledIconProps } from '..';
export declare const Trademark: StyledIcon<any>;
export declare const TrademarkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
