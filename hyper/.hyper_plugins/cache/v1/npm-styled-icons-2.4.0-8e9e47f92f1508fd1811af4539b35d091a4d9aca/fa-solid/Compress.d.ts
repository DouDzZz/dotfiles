import { StyledIcon, StyledIconProps } from '..';
export declare const Compress: StyledIcon<any>;
export declare const CompressDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
