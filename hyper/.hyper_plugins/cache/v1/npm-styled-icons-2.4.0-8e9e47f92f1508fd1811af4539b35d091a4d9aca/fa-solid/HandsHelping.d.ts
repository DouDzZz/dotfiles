import { StyledIcon, StyledIconProps } from '..';
export declare const HandsHelping: StyledIcon<any>;
export declare const HandsHelpingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
