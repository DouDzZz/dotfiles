import { StyledIcon, StyledIconProps } from '..';
export declare const History: StyledIcon<any>;
export declare const HistoryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
