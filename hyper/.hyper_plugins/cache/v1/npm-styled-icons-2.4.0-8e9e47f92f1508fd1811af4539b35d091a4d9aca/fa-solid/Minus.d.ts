import { StyledIcon, StyledIconProps } from '..';
export declare const Minus: StyledIcon<any>;
export declare const MinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
