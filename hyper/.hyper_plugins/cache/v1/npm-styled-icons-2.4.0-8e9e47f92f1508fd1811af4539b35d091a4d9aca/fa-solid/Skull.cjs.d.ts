import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Skull: StyledIcon<any>;
export declare const SkullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
