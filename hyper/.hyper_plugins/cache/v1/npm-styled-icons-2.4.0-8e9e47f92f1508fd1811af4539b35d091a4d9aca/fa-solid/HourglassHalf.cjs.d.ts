import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HourglassHalf: StyledIcon<any>;
export declare const HourglassHalfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
