import { StyledIcon, StyledIconProps } from '..';
export declare const LongArrowAltUp: StyledIcon<any>;
export declare const LongArrowAltUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
