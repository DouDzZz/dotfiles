import { StyledIcon, StyledIconProps } from '..';
export declare const Microchip: StyledIcon<any>;
export declare const MicrochipDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
