import { StyledIcon, StyledIconProps } from '..';
export declare const Trash: StyledIcon<any>;
export declare const TrashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
