import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StepForward: StyledIcon<any>;
export declare const StepForwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
