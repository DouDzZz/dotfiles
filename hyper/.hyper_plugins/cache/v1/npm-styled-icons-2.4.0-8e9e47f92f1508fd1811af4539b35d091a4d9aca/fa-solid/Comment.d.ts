import { StyledIcon, StyledIconProps } from '..';
export declare const Comment: StyledIcon<any>;
export declare const CommentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
