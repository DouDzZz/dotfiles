import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Coins: StyledIcon<any>;
export declare const CoinsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
