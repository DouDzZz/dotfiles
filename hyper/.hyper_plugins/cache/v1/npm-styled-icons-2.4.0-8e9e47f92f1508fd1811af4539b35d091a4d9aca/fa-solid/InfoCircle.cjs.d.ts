import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const InfoCircle: StyledIcon<any>;
export declare const InfoCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
