import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Calculator: StyledIcon<any>;
export declare const CalculatorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
