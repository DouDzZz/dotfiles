import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowCircleRight: StyledIcon<any>;
export declare const ArrowCircleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
