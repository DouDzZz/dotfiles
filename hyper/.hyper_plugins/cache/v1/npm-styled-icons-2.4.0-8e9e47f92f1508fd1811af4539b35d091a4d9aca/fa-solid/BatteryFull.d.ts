import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryFull: StyledIcon<any>;
export declare const BatteryFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
