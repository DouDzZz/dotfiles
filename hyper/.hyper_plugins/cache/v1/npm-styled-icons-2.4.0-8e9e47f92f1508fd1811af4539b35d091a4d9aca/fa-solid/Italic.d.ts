import { StyledIcon, StyledIconProps } from '..';
export declare const Italic: StyledIcon<any>;
export declare const ItalicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
