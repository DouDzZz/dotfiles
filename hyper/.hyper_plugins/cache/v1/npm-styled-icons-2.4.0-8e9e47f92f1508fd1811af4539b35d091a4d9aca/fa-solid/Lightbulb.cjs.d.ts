import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Lightbulb: StyledIcon<any>;
export declare const LightbulbDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
