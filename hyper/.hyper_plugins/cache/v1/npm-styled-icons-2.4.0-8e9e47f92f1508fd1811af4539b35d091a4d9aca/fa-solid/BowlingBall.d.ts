import { StyledIcon, StyledIconProps } from '..';
export declare const BowlingBall: StyledIcon<any>;
export declare const BowlingBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
