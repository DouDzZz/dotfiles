import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Car: StyledIcon<any>;
export declare const CarDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
