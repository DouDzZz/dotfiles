import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronUp: StyledIcon<any>;
export declare const ChevronUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
