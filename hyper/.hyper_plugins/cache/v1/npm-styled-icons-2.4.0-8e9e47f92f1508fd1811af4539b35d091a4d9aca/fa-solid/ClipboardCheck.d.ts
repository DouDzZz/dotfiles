import { StyledIcon, StyledIconProps } from '..';
export declare const ClipboardCheck: StyledIcon<any>;
export declare const ClipboardCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
