import { StyledIcon, StyledIconProps } from '..';
export declare const Envelope: StyledIcon<any>;
export declare const EnvelopeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
