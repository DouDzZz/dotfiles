import { StyledIcon, StyledIconProps } from '..';
export declare const ShoppingBasket: StyledIcon<any>;
export declare const ShoppingBasketDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
