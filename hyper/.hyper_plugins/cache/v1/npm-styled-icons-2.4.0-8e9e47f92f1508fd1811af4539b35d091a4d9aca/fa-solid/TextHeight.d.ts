import { StyledIcon, StyledIconProps } from '..';
export declare const TextHeight: StyledIcon<any>;
export declare const TextHeightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
