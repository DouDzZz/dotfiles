import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subway: StyledIcon<any>;
export declare const SubwayDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
