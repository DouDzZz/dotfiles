import { StyledIcon, StyledIconProps } from '..';
export declare const Tablet: StyledIcon<any>;
export declare const TabletDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
