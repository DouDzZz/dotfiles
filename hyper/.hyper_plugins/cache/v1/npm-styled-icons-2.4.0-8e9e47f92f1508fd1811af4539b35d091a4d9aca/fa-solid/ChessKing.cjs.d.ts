import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessKing: StyledIcon<any>;
export declare const ChessKingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
