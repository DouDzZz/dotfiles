import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const DiceFour: StyledIcon<any>;
export declare const DiceFourDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
