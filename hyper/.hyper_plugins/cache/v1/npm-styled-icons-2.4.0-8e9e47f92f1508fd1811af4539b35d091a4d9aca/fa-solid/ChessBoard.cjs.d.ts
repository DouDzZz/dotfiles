import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessBoard: StyledIcon<any>;
export declare const ChessBoardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
