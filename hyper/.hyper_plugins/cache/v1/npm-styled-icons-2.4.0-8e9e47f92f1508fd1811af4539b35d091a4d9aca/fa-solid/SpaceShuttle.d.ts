import { StyledIcon, StyledIconProps } from '..';
export declare const SpaceShuttle: StyledIcon<any>;
export declare const SpaceShuttleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
