import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ToggleOn: StyledIcon<any>;
export declare const ToggleOnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
