import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bed: StyledIcon<any>;
export declare const BedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
