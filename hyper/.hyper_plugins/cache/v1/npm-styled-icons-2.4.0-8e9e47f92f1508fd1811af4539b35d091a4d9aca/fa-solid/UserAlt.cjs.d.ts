import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserAlt: StyledIcon<any>;
export declare const UserAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
