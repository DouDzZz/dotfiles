import { StyledIcon, StyledIconProps } from '..';
export declare const Skull: StyledIcon<any>;
export declare const SkullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
