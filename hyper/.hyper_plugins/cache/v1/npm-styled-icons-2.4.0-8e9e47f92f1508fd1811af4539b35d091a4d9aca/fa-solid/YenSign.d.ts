import { StyledIcon, StyledIconProps } from '..';
export declare const YenSign: StyledIcon<any>;
export declare const YenSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
