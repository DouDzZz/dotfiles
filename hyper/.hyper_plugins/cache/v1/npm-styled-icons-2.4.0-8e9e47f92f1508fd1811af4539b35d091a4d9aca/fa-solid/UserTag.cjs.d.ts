import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserTag: StyledIcon<any>;
export declare const UserTagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
