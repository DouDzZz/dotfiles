import { StyledIcon, StyledIconProps } from '..';
export declare const MicrophoneAltSlash: StyledIcon<any>;
export declare const MicrophoneAltSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
