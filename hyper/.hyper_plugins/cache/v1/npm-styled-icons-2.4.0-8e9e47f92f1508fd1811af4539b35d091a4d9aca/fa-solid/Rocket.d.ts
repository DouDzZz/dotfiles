import { StyledIcon, StyledIconProps } from '..';
export declare const Rocket: StyledIcon<any>;
export declare const RocketDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
