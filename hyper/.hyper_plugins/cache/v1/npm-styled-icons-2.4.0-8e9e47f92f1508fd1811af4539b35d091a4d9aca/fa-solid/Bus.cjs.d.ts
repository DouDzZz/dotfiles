import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bus: StyledIcon<any>;
export declare const BusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
