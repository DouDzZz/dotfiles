import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Subscript: StyledIcon<any>;
export declare const SubscriptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
