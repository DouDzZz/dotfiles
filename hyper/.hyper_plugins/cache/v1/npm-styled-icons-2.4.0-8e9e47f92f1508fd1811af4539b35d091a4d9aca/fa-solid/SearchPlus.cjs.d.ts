import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SearchPlus: StyledIcon<any>;
export declare const SearchPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
