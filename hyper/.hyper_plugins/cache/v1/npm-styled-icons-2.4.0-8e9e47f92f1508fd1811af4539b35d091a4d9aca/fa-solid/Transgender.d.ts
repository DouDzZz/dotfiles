import { StyledIcon, StyledIconProps } from '..';
export declare const Transgender: StyledIcon<any>;
export declare const TransgenderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
