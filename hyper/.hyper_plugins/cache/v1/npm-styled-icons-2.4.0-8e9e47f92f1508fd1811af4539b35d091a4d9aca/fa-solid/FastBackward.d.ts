import { StyledIcon, StyledIconProps } from '..';
export declare const FastBackward: StyledIcon<any>;
export declare const FastBackwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
