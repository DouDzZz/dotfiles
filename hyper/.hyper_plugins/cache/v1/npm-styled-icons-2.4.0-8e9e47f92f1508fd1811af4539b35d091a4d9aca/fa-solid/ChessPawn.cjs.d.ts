import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessPawn: StyledIcon<any>;
export declare const ChessPawnDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
