import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RssSquare: StyledIcon<any>;
export declare const RssSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
