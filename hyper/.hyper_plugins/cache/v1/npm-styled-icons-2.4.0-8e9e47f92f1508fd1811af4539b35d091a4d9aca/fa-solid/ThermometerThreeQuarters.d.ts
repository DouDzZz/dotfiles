import { StyledIcon, StyledIconProps } from '..';
export declare const ThermometerThreeQuarters: StyledIcon<any>;
export declare const ThermometerThreeQuartersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
