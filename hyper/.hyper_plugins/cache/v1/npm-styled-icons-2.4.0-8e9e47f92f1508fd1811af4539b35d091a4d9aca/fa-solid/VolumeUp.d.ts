import { StyledIcon, StyledIconProps } from '..';
export declare const VolumeUp: StyledIcon<any>;
export declare const VolumeUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
