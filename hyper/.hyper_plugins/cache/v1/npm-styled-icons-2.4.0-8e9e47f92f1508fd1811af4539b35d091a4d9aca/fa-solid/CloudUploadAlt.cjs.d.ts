import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CloudUploadAlt: StyledIcon<any>;
export declare const CloudUploadAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
