import { StyledIcon, StyledIconProps } from '..';
export declare const CalendarTimes: StyledIcon<any>;
export declare const CalendarTimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
