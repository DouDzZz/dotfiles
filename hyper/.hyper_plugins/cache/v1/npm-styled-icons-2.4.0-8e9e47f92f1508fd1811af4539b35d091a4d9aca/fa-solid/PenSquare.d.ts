import { StyledIcon, StyledIconProps } from '..';
export declare const PenSquare: StyledIcon<any>;
export declare const PenSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
