import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretSquareUp: StyledIcon<any>;
export declare const CaretSquareUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
