import { StyledIcon, StyledIconProps } from '..';
export declare const LongArrowAltDown: StyledIcon<any>;
export declare const LongArrowAltDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
