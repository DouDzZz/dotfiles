"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.Feather = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "Feather-title" }, props.title), react_1.default.createElement("path", { fill: "currentColor", d: "M512 0C504.81.01 98.51 22.01 71.47 287.42c-2.62 22.6-4.22 45.28-5.37 67.98l160.65-160.65c6.25-6.25 16.38-6.25 22.62 0s6.25 16.38 0 22.62l-240 240a31.9 31.9 0 0 0-9.38 22.67C.02 497.68 14.33 512 32 512c8.19 0 16.38-3.12 22.62-9.38l55.05-55.05c38.4-.5 76.76-2.63 114.91-7.05 11.58-1.18 22.54-3.29 33.21-5.84L256 384h101.86c12.61-10.63 24.12-22.45 34.76-35.07L384 288h50.19C502.8 163.6 512 .1 512 0z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { fill: "currentColor", d: "M512 0C504.81.01 98.51 22.01 71.47 287.42c-2.62 22.6-4.22 45.28-5.37 67.98l160.65-160.65c6.25-6.25 16.38-6.25 22.62 0s6.25 16.38 0 22.62l-240 240a31.9 31.9 0 0 0-9.38 22.67C.02 497.68 14.33 512 32 512c8.19 0 16.38-3.12 22.62-9.38l55.05-55.05c38.4-.5 76.76-2.63 114.91-7.05 11.58-1.18 22.54-3.29 33.21-5.84L256 384h101.86c12.61-10.63 24.12-22.45 34.76-35.07L384 288h50.19C502.8 163.6 512 .1 512 0z", key: "k0" })
        ]); },
    viewBox: '0 0 512 512',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.Feather.displayName = 'Feather';
exports.FeatherDimensions = { height: undefined, width: undefined };
var templateObject_1;
