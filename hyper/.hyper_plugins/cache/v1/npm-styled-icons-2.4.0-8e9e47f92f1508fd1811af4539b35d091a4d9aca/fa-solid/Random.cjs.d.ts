import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Random: StyledIcon<any>;
export declare const RandomDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
