import { StyledIcon, StyledIconProps } from '..';
export declare const BatteryEmpty: StyledIcon<any>;
export declare const BatteryEmptyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
