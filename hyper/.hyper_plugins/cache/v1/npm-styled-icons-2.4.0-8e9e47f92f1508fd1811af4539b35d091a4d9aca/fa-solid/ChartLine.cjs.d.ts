import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChartLine: StyledIcon<any>;
export declare const ChartLineDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
