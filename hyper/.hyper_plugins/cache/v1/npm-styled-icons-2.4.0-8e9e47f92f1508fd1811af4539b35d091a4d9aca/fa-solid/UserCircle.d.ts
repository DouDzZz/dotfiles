import { StyledIcon, StyledIconProps } from '..';
export declare const UserCircle: StyledIcon<any>;
export declare const UserCircleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
