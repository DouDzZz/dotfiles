import { StyledIcon, StyledIconProps } from '..';
export declare const FlagCheckered: StyledIcon<any>;
export declare const FlagCheckeredDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
