import { StyledIcon, StyledIconProps } from '..';
export declare const Screwdriver: StyledIcon<any>;
export declare const ScrewdriverDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
