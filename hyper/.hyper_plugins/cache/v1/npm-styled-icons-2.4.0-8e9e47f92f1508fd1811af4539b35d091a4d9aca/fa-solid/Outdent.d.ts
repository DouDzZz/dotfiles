import { StyledIcon, StyledIconProps } from '..';
export declare const Outdent: StyledIcon<any>;
export declare const OutdentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
