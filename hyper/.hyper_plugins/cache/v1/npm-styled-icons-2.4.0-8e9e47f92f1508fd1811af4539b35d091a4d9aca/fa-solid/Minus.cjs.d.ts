import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Minus: StyledIcon<any>;
export declare const MinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
