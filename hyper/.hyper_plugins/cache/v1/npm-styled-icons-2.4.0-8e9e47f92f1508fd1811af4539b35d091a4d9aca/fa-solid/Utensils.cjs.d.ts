import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Utensils: StyledIcon<any>;
export declare const UtensilsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
