import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FilePdf: StyledIcon<any>;
export declare const FilePdfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
