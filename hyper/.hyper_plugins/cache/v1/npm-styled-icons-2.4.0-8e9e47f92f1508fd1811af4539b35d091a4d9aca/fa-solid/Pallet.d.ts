import { StyledIcon, StyledIconProps } from '..';
export declare const Pallet: StyledIcon<any>;
export declare const PalletDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
