import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChessQueen: StyledIcon<any>;
export declare const ChessQueenDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
