import { StyledIcon, StyledIconProps } from '..';
export declare const Laptop: StyledIcon<any>;
export declare const LaptopDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
