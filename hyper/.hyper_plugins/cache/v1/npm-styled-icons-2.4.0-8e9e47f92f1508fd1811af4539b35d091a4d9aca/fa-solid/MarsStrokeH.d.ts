import { StyledIcon, StyledIconProps } from '..';
export declare const MarsStrokeH: StyledIcon<any>;
export declare const MarsStrokeHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
