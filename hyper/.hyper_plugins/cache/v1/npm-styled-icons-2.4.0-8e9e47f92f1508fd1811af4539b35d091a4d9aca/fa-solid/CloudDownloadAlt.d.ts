import { StyledIcon, StyledIconProps } from '..';
export declare const CloudDownloadAlt: StyledIcon<any>;
export declare const CloudDownloadAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
