import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowsAltV: StyledIcon<any>;
export declare const ArrowsAltVDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
