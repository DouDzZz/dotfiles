import { StyledIcon, StyledIconProps } from '..';
export declare const FileMedicalAlt: StyledIcon<any>;
export declare const FileMedicalAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
