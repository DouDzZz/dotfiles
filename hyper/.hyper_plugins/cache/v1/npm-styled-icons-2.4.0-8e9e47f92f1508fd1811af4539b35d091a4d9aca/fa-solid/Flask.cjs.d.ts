import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Flask: StyledIcon<any>;
export declare const FlaskDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
