import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThList: StyledIcon<any>;
export declare const ThListDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
