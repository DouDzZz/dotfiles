import { StyledIcon, StyledIconProps } from '..';
export declare const Users: StyledIcon<any>;
export declare const UsersDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
