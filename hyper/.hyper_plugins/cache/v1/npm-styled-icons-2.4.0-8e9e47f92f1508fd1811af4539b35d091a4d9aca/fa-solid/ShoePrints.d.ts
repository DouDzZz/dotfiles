import { StyledIcon, StyledIconProps } from '..';
export declare const ShoePrints: StyledIcon<any>;
export declare const ShoePrintsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
