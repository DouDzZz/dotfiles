import { StyledIcon, StyledIconProps } from '..';
export declare const Times: StyledIcon<any>;
export declare const TimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
