import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const RubleSign: StyledIcon<any>;
export declare const RubleSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
