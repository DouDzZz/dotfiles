import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Phone: StyledIcon<any>;
export declare const PhoneDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
