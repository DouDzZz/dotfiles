import { StyledIcon, StyledIconProps } from '..';
export declare const Stopwatch: StyledIcon<any>;
export declare const StopwatchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
