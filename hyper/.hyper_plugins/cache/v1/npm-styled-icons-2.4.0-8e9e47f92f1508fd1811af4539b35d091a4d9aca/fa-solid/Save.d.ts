import { StyledIcon, StyledIconProps } from '..';
export declare const Save: StyledIcon<any>;
export declare const SaveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
