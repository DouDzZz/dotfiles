import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Infinity: StyledIcon<any>;
export declare const InfinityDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
