import { StyledIcon, StyledIconProps } from '..';
export declare const DiceFive: StyledIcon<any>;
export declare const DiceFiveDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
