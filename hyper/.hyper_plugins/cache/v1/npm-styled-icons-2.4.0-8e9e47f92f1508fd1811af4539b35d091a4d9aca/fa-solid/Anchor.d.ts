import { StyledIcon, StyledIconProps } from '..';
export declare const Anchor: StyledIcon<any>;
export declare const AnchorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
