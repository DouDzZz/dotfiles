import { StyledIcon, StyledIconProps } from '..';
export declare const Weight: StyledIcon<any>;
export declare const WeightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
