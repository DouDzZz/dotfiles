import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const LessThanEqual: StyledIcon<any>;
export declare const LessThanEqualDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
