import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Registered: StyledIcon<any>;
export declare const RegisteredDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
