import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowsAltH: StyledIcon<any>;
export declare const ArrowsAltHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
