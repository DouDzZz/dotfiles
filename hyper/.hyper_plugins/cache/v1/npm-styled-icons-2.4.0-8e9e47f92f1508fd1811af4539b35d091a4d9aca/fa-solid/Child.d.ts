import { StyledIcon, StyledIconProps } from '..';
export declare const Child: StyledIcon<any>;
export declare const ChildDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
