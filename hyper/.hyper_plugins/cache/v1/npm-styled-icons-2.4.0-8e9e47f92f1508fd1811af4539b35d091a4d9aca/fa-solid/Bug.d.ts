import { StyledIcon, StyledIconProps } from '..';
export declare const Bug: StyledIcon<any>;
export declare const BugDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
