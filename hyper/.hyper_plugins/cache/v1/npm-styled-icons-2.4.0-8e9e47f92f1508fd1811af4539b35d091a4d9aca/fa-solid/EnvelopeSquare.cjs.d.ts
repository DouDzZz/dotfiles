import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EnvelopeSquare: StyledIcon<any>;
export declare const EnvelopeSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
