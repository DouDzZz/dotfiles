import { StyledIcon, StyledIconProps } from '..';
export declare const StepBackward: StyledIcon<any>;
export declare const StepBackwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
