import { StyledIcon, StyledIconProps } from '..';
export declare const AlignRight: StyledIcon<any>;
export declare const AlignRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
