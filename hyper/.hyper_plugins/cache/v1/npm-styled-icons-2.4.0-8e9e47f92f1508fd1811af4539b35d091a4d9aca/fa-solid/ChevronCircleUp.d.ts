import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronCircleUp: StyledIcon<any>;
export declare const ChevronCircleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
