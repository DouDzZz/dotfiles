import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SlidersH: StyledIcon<any>;
export declare const SlidersHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
