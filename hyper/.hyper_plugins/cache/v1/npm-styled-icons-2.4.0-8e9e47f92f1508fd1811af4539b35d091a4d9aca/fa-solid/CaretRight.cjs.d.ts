import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretRight: StyledIcon<any>;
export declare const CaretRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
