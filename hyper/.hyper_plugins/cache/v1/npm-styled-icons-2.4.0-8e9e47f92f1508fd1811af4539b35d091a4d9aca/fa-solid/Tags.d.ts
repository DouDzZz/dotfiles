import { StyledIcon, StyledIconProps } from '..';
export declare const Tags: StyledIcon<any>;
export declare const TagsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
