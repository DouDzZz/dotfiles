import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Broom: StyledIcon<any>;
export declare const BroomDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
