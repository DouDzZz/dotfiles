import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyCheck: StyledIcon<any>;
export declare const MoneyCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
