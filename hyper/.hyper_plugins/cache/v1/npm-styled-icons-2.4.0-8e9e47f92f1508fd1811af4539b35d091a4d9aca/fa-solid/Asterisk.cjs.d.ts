import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Asterisk: StyledIcon<any>;
export declare const AsteriskDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
