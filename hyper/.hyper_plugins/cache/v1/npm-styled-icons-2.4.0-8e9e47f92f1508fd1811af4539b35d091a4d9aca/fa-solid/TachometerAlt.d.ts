import { StyledIcon, StyledIconProps } from '..';
export declare const TachometerAlt: StyledIcon<any>;
export declare const TachometerAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
