import { StyledIcon, StyledIconProps } from '..';
export declare const LowVision: StyledIcon<any>;
export declare const LowVisionDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
