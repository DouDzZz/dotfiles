import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tablets: StyledIcon<any>;
export declare const TabletsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
