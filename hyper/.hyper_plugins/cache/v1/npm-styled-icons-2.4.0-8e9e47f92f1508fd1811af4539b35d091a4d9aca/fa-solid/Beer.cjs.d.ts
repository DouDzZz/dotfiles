import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Beer: StyledIcon<any>;
export declare const BeerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
