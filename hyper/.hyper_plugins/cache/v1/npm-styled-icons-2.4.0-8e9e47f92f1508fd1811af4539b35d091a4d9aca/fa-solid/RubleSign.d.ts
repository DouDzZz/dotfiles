import { StyledIcon, StyledIconProps } from '..';
export declare const RubleSign: StyledIcon<any>;
export declare const RubleSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
