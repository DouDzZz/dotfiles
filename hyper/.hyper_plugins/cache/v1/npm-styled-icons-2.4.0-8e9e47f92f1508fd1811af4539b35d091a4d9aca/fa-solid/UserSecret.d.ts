import { StyledIcon, StyledIconProps } from '..';
export declare const UserSecret: StyledIcon<any>;
export declare const UserSecretDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
