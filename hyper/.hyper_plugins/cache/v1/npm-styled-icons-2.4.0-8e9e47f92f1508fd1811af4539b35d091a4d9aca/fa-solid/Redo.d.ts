import { StyledIcon, StyledIconProps } from '..';
export declare const Redo: StyledIcon<any>;
export declare const RedoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
