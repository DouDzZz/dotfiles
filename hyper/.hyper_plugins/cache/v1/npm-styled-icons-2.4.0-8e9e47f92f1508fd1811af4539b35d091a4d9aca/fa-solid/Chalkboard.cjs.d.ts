import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Chalkboard: StyledIcon<any>;
export declare const ChalkboardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
