import { StyledIcon, StyledIconProps } from '..';
export declare const LessThan: StyledIcon<any>;
export declare const LessThanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
