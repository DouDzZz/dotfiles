import { StyledIcon, StyledIconProps } from '..';
export declare const AngleDoubleLeft: StyledIcon<any>;
export declare const AngleDoubleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
