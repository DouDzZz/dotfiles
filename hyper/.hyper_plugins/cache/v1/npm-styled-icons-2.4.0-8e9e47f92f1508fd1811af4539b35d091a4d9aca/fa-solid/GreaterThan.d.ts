import { StyledIcon, StyledIconProps } from '..';
export declare const GreaterThan: StyledIcon<any>;
export declare const GreaterThanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
