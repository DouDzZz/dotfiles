import { StyledIcon, StyledIconProps } from '..';
export declare const UserEdit: StyledIcon<any>;
export declare const UserEditDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
