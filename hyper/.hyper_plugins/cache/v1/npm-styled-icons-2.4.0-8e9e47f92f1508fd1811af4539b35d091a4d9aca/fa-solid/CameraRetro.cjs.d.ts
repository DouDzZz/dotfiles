import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CameraRetro: StyledIcon<any>;
export declare const CameraRetroDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
