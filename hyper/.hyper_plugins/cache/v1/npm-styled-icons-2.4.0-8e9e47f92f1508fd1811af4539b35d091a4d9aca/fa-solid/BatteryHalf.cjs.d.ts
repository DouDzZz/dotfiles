import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryHalf: StyledIcon<any>;
export declare const BatteryHalfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
