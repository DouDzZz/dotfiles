import { StyledIcon, StyledIconProps } from '..';
export declare const Robot: StyledIcon<any>;
export declare const RobotDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
