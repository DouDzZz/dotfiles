import { StyledIcon, StyledIconProps } from '..';
export declare const MicrophoneAlt: StyledIcon<any>;
export declare const MicrophoneAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
