import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThLarge: StyledIcon<any>;
export declare const ThLargeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
