import { StyledIcon, StyledIconProps } from '..';
export declare const ChartArea: StyledIcon<any>;
export declare const ChartAreaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
