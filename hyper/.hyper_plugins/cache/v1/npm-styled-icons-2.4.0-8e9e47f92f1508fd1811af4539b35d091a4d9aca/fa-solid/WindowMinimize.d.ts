import { StyledIcon, StyledIconProps } from '..';
export declare const WindowMinimize: StyledIcon<any>;
export declare const WindowMinimizeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
