import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PuzzlePiece: StyledIcon<any>;
export declare const PuzzlePieceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
