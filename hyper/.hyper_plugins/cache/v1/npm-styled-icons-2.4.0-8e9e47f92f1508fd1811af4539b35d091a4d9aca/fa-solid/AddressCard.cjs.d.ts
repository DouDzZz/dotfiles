import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AddressCard: StyledIcon<any>;
export declare const AddressCardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
