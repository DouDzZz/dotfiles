import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ArrowCircleDown: StyledIcon<any>;
export declare const ArrowCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
