import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserPlus: StyledIcon<any>;
export declare const UserPlusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
