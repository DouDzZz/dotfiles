import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SmokingBan: StyledIcon<any>;
export declare const SmokingBanDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
