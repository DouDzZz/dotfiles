import { StyledIcon, StyledIconProps } from '..';
export declare const StarHalf: StyledIcon<any>;
export declare const StarHalfDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
