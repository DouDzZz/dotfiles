import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TruckLoading: StyledIcon<any>;
export declare const TruckLoadingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
