import { StyledIcon, StyledIconProps } from '..';
export declare const Beer: StyledIcon<any>;
export declare const BeerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
