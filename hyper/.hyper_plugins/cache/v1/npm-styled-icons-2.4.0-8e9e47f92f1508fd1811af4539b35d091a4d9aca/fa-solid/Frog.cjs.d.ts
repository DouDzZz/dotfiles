import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Frog: StyledIcon<any>;
export declare const FrogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
