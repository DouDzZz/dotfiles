import { StyledIcon, StyledIconProps } from '..';
export declare const Comments: StyledIcon<any>;
export declare const CommentsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
