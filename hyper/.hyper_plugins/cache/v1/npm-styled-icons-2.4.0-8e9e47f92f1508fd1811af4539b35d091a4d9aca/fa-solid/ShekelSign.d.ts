import { StyledIcon, StyledIconProps } from '..';
export declare const ShekelSign: StyledIcon<any>;
export declare const ShekelSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
