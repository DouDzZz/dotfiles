import { StyledIcon, StyledIconProps } from '..';
export declare const Podcast: StyledIcon<any>;
export declare const PodcastDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
