import { StyledIcon, StyledIconProps } from '..';
export declare const MapMarker: StyledIcon<any>;
export declare const MapMarkerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
