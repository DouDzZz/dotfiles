import { StyledIcon, StyledIconProps } from '..';
export declare const TabletAlt: StyledIcon<any>;
export declare const TabletAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
