import { StyledIcon, StyledIconProps } from '..';
export declare const SortDown: StyledIcon<any>;
export declare const SortDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
