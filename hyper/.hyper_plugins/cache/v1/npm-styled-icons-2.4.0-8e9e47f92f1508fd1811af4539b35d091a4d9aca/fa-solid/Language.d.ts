import { StyledIcon, StyledIconProps } from '..';
export declare const Language: StyledIcon<any>;
export declare const LanguageDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
