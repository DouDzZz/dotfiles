import { StyledIcon, StyledIconProps } from '..';
export declare const SortAmountUp: StyledIcon<any>;
export declare const SortAmountUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
