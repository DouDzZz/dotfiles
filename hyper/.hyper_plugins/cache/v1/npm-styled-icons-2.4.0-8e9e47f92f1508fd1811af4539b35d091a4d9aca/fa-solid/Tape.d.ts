import { StyledIcon, StyledIconProps } from '..';
export declare const Tape: StyledIcon<any>;
export declare const TapeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
