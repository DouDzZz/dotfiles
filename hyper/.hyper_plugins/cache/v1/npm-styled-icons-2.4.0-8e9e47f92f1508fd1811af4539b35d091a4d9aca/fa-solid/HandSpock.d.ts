import { StyledIcon, StyledIconProps } from '..';
export declare const HandSpock: StyledIcon<any>;
export declare const HandSpockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
