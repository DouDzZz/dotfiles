import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ICursor: StyledIcon<any>;
export declare const ICursorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
