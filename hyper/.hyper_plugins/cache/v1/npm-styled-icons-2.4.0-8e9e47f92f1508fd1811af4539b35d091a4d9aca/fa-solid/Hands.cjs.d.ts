import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hands: StyledIcon<any>;
export declare const HandsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
