import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TruckMoving: StyledIcon<any>;
export declare const TruckMovingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
