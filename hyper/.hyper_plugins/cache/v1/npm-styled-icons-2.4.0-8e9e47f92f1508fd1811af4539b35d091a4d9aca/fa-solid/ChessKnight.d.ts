import { StyledIcon, StyledIconProps } from '..';
export declare const ChessKnight: StyledIcon<any>;
export declare const ChessKnightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
