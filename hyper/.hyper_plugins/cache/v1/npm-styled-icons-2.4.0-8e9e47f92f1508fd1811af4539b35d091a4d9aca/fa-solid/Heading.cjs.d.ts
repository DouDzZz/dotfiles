import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Heading: StyledIcon<any>;
export declare const HeadingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
