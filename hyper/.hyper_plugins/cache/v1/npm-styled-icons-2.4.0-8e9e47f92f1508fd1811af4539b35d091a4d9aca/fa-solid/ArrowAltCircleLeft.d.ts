import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowAltCircleLeft: StyledIcon<any>;
export declare const ArrowAltCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
