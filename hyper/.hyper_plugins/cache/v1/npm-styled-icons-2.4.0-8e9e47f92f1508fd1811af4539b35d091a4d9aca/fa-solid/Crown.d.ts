import { StyledIcon, StyledIconProps } from '..';
export declare const Crown: StyledIcon<any>;
export declare const CrownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
