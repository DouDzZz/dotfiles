import { StyledIcon, StyledIconProps } from '..';
export declare const ExternalLinkSquareAlt: StyledIcon<any>;
export declare const ExternalLinkSquareAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
