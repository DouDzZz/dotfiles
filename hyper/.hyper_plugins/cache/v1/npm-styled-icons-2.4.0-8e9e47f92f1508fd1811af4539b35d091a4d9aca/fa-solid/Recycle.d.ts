import { StyledIcon, StyledIconProps } from '..';
export declare const Recycle: StyledIcon<any>;
export declare const RecycleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
