import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const SortDown: StyledIcon<any>;
export declare const SortDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
