import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Vial: StyledIcon<any>;
export declare const VialDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
