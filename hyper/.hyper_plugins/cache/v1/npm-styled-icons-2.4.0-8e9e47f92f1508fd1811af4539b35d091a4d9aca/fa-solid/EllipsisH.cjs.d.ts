import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EllipsisH: StyledIcon<any>;
export declare const EllipsisHDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
