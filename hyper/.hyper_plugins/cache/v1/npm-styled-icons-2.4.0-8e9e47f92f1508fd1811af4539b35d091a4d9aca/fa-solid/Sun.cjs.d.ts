import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sun: StyledIcon<any>;
export declare const SunDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
