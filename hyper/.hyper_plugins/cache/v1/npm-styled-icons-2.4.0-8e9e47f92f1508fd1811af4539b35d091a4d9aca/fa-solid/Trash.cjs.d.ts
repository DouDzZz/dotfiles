import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Trash: StyledIcon<any>;
export declare const TrashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
