import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GolfBall: StyledIcon<any>;
export declare const GolfBallDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
