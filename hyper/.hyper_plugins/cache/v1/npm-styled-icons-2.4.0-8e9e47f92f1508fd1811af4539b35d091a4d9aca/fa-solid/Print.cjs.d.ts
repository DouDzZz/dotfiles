import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Print: StyledIcon<any>;
export declare const PrintDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
