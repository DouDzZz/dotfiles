import { StyledIcon, StyledIconProps } from '..';
export declare const ShareAlt: StyledIcon<any>;
export declare const ShareAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
