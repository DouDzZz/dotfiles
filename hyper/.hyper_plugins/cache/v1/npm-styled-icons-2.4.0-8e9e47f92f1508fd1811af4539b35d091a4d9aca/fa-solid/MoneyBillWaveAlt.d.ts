import { StyledIcon, StyledIconProps } from '..';
export declare const MoneyBillWaveAlt: StyledIcon<any>;
export declare const MoneyBillWaveAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
