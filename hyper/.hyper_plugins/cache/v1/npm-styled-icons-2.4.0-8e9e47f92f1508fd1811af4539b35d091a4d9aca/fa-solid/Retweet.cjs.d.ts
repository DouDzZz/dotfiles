import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Retweet: StyledIcon<any>;
export declare const RetweetDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
