import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FileVideo: StyledIcon<any>;
export declare const FileVideoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
