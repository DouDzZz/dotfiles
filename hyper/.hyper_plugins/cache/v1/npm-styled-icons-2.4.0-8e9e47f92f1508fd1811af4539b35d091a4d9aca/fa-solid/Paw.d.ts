import { StyledIcon, StyledIconProps } from '..';
export declare const Paw: StyledIcon<any>;
export declare const PawDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
