import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MapPin: StyledIcon<any>;
export declare const MapPinDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
