import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VenusDouble: StyledIcon<any>;
export declare const VenusDoubleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
