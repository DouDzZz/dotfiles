import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BellSlash: StyledIcon<any>;
export declare const BellSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
