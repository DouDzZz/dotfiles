import { StyledIcon, StyledIconProps } from '..';
export declare const PaintBrush: StyledIcon<any>;
export declare const PaintBrushDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
