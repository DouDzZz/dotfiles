import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CalendarAlt: StyledIcon<any>;
export declare const CalendarAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
