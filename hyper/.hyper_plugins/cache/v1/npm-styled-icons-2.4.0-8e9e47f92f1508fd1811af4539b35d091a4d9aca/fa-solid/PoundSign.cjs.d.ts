import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PoundSign: StyledIcon<any>;
export declare const PoundSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
