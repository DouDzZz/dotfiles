import { StyledIcon, StyledIconProps } from '..';
export declare const School: StyledIcon<any>;
export declare const SchoolDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
