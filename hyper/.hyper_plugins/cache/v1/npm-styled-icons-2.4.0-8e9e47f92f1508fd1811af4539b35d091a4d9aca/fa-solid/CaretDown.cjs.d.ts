import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretDown: StyledIcon<any>;
export declare const CaretDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
