import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tv: StyledIcon<any>;
export declare const TvDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
