import { StyledIcon, StyledIconProps } from '..';
export declare const HandPointLeft: StyledIcon<any>;
export declare const HandPointLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
