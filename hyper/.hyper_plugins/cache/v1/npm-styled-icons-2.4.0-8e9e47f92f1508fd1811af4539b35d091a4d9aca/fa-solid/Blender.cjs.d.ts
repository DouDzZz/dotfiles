import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Blender: StyledIcon<any>;
export declare const BlenderDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
