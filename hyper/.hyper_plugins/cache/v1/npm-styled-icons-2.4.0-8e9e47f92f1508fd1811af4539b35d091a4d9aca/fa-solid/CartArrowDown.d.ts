import { StyledIcon, StyledIconProps } from '..';
export declare const CartArrowDown: StyledIcon<any>;
export declare const CartArrowDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
