import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tree: StyledIcon<any>;
export declare const TreeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
