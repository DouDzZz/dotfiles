import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Boxes: StyledIcon<any>;
export declare const BoxesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
