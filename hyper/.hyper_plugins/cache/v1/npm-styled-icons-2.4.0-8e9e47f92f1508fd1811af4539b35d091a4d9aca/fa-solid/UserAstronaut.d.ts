import { StyledIcon, StyledIconProps } from '..';
export declare const UserAstronaut: StyledIcon<any>;
export declare const UserAstronautDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
