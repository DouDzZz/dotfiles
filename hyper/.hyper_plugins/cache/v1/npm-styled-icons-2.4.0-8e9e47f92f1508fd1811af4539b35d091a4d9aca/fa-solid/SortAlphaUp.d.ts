import { StyledIcon, StyledIconProps } from '..';
export declare const SortAlphaUp: StyledIcon<any>;
export declare const SortAlphaUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
