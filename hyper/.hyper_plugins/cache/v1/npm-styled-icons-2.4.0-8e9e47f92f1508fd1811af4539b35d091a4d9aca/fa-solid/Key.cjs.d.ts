import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Key: StyledIcon<any>;
export declare const KeyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
