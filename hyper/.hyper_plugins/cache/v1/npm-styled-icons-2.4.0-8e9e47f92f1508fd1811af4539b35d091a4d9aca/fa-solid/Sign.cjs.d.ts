import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Sign: StyledIcon<any>;
export declare const SignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
