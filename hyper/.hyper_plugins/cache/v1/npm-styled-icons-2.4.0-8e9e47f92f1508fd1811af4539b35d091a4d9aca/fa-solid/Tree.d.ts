import { StyledIcon, StyledIconProps } from '..';
export declare const Tree: StyledIcon<any>;
export declare const TreeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
