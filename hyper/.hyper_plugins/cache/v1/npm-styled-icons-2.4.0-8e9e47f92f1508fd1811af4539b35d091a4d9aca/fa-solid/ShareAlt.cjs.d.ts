import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShareAlt: StyledIcon<any>;
export declare const ShareAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
