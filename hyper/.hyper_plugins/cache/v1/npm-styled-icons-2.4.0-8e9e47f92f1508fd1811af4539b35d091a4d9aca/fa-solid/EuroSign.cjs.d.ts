import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const EuroSign: StyledIcon<any>;
export declare const EuroSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
