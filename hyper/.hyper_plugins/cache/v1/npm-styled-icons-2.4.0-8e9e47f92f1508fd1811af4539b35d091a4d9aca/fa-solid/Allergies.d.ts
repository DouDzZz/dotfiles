import { StyledIcon, StyledIconProps } from '..';
export declare const Allergies: StyledIcon<any>;
export declare const AllergiesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
