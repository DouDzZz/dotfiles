import { StyledIcon, StyledIconProps } from '..';
export declare const Cut: StyledIcon<any>;
export declare const CutDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
