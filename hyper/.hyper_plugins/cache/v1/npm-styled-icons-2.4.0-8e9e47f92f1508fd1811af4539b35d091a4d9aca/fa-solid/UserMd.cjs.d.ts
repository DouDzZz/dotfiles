import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserMd: StyledIcon<any>;
export declare const UserMdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
