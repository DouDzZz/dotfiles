import { StyledIcon, StyledIconProps } from '..';
export declare const BalanceScale: StyledIcon<any>;
export declare const BalanceScaleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
