import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Unlock: StyledIcon<any>;
export declare const UnlockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
