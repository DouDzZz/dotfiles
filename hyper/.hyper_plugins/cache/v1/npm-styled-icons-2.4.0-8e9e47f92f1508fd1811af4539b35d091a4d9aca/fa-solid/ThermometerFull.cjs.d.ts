import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThermometerFull: StyledIcon<any>;
export declare const ThermometerFullDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
