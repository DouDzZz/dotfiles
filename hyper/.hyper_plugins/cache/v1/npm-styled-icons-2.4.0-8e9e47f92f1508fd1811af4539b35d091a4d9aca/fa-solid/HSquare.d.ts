import { StyledIcon, StyledIconProps } from '..';
export declare const HSquare: StyledIcon<any>;
export declare const HSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
