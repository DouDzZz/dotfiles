import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserEdit: StyledIcon<any>;
export declare const UserEditDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
