import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const VolumeUp: StyledIcon<any>;
export declare const VolumeUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
