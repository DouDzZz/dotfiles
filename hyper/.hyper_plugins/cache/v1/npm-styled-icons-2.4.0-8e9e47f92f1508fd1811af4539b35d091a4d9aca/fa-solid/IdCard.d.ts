import { StyledIcon, StyledIconProps } from '..';
export declare const IdCard: StyledIcon<any>;
export declare const IdCardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
