import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserAstronaut: StyledIcon<any>;
export declare const UserAstronautDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
