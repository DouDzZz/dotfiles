import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Equals: StyledIcon<any>;
export declare const EqualsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
