import { StyledIcon, StyledIconProps } from '..';
export declare const Pause: StyledIcon<any>;
export declare const PauseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
