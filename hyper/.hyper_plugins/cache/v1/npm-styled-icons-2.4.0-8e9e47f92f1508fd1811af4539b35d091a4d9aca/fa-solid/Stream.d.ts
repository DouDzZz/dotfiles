import { StyledIcon, StyledIconProps } from '..';
export declare const Stream: StyledIcon<any>;
export declare const StreamDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
