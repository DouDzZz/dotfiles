import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CaretLeft: StyledIcon<any>;
export declare const CaretLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
