import { StyledIcon, StyledIconProps } from '..';
export declare const Road: StyledIcon<any>;
export declare const RoadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
