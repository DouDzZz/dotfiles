import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Anchor: StyledIcon<any>;
export declare const AnchorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
