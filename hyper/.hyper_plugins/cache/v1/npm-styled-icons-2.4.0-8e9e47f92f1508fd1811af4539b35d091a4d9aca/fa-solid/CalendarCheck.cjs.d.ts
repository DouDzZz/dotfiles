import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CalendarCheck: StyledIcon<any>;
export declare const CalendarCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
