import { StyledIcon, StyledIconProps } from '..';
export declare const Smile: StyledIcon<any>;
export declare const SmileDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
