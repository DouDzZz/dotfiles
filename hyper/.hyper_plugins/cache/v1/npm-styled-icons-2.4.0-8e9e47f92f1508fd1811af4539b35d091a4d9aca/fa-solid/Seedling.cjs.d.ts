import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Seedling: StyledIcon<any>;
export declare const SeedlingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
