import { StyledIcon, StyledIconProps } from '..';
export declare const Bicycle: StyledIcon<any>;
export declare const BicycleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
