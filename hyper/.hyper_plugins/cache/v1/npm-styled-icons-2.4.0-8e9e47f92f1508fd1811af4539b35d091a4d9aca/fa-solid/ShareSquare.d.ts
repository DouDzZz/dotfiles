import { StyledIcon, StyledIconProps } from '..';
export declare const ShareSquare: StyledIcon<any>;
export declare const ShareSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
