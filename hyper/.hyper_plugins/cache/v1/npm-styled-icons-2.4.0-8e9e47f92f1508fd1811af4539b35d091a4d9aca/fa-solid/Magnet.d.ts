import { StyledIcon, StyledIconProps } from '..';
export declare const Magnet: StyledIcon<any>;
export declare const MagnetDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
