import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignCenter: StyledIcon<any>;
export declare const AlignCenterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
