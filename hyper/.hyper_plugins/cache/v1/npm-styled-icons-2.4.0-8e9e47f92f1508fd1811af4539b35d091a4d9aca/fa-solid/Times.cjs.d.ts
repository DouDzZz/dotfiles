import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Times: StyledIcon<any>;
export declare const TimesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
