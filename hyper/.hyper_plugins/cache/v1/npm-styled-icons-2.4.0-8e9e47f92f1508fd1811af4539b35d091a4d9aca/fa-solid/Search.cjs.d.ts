import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Search: StyledIcon<any>;
export declare const SearchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
