import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BirthdayCake: StyledIcon<any>;
export declare const BirthdayCakeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
