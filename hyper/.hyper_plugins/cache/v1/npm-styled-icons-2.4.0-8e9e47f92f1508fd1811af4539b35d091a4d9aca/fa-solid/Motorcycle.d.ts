import { StyledIcon, StyledIconProps } from '..';
export declare const Motorcycle: StyledIcon<any>;
export declare const MotorcycleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
