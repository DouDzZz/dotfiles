import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const CommentAlt: StyledIcon<any>;
export declare const CommentAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
