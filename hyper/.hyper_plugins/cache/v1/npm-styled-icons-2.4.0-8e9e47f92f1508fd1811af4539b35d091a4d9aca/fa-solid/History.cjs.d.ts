import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const History: StyledIcon<any>;
export declare const HistoryDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
