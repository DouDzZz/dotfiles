import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MapMarker: StyledIcon<any>;
export declare const MapMarkerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
