import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserSlash: StyledIcon<any>;
export declare const UserSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
