import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Expand: StyledIcon<any>;
export declare const ExpandDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
