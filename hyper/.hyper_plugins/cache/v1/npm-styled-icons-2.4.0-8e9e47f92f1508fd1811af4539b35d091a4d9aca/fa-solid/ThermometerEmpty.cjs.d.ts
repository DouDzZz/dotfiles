import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ThermometerEmpty: StyledIcon<any>;
export declare const ThermometerEmptyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
