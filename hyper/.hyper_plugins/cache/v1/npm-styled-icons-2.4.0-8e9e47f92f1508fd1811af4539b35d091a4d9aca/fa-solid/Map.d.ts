import { StyledIcon, StyledIconProps } from '..';
export declare const Map: StyledIcon<any>;
export declare const MapDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
