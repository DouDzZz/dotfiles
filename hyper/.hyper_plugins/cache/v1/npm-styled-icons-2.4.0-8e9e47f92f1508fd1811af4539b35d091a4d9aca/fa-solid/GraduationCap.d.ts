import { StyledIcon, StyledIconProps } from '..';
export declare const GraduationCap: StyledIcon<any>;
export declare const GraduationCapDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
