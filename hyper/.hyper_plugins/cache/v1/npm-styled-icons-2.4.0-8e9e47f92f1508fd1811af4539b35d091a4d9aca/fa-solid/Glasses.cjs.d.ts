import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Glasses: StyledIcon<any>;
export declare const GlassesDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
