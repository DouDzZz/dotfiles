import { StyledIcon, StyledIconProps } from '..';
export declare const Gamepad: StyledIcon<any>;
export declare const GamepadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
