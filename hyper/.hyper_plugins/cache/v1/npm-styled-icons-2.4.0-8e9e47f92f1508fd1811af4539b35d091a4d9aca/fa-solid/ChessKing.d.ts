import { StyledIcon, StyledIconProps } from '..';
export declare const ChessKing: StyledIcon<any>;
export declare const ChessKingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
