import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const StreetView: StyledIcon<any>;
export declare const StreetViewDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
