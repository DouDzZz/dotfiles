import { StyledIcon, StyledIconProps } from '..';
export declare const SignInAlt: StyledIcon<any>;
export declare const SignInAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
