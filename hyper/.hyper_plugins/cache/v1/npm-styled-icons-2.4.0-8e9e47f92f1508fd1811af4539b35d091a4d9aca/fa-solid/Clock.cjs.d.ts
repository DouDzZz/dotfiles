import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Clock: StyledIcon<any>;
export declare const ClockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
