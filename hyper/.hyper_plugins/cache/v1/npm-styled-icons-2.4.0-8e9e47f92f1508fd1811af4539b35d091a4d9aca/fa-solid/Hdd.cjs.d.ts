import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hdd: StyledIcon<any>;
export declare const HddDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
