import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HospitalAlt: StyledIcon<any>;
export declare const HospitalAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
