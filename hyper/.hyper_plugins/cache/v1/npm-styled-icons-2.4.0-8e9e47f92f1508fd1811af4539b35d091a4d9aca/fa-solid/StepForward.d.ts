import { StyledIcon, StyledIconProps } from '..';
export declare const StepForward: StyledIcon<any>;
export declare const StepForwardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
