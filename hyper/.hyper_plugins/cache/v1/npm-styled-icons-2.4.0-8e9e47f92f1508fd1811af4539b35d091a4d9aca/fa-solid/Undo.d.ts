import { StyledIcon, StyledIconProps } from '..';
export declare const Undo: StyledIcon<any>;
export declare const UndoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
