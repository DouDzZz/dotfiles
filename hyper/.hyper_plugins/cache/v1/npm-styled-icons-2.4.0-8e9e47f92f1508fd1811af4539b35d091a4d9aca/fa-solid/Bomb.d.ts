import { StyledIcon, StyledIconProps } from '..';
export declare const Bomb: StyledIcon<any>;
export declare const BombDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
