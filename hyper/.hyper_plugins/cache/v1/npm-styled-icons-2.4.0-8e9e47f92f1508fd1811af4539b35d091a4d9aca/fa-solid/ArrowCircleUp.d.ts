import { StyledIcon, StyledIconProps } from '..';
export declare const ArrowCircleUp: StyledIcon<any>;
export declare const ArrowCircleUpDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
