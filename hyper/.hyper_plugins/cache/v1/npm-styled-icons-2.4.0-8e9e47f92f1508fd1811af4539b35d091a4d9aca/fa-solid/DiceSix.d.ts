import { StyledIcon, StyledIconProps } from '..';
export declare const DiceSix: StyledIcon<any>;
export declare const DiceSixDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
