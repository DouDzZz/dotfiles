import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Palette: StyledIcon<any>;
export declare const PaletteDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
