import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UniversalAccess: StyledIcon<any>;
export declare const UniversalAccessDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
