import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserCog: StyledIcon<any>;
export declare const UserCogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
