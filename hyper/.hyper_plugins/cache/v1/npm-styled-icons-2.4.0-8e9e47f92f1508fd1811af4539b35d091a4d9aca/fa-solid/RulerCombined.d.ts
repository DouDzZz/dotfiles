import { StyledIcon, StyledIconProps } from '..';
export declare const RulerCombined: StyledIcon<any>;
export declare const RulerCombinedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
