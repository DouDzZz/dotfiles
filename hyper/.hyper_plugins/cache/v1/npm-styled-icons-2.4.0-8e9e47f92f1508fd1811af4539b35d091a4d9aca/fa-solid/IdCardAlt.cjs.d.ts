import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const IdCardAlt: StyledIcon<any>;
export declare const IdCardAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
