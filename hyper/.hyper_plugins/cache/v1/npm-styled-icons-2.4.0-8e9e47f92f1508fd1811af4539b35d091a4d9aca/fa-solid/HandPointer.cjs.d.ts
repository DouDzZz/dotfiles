import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const HandPointer: StyledIcon<any>;
export declare const HandPointerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
