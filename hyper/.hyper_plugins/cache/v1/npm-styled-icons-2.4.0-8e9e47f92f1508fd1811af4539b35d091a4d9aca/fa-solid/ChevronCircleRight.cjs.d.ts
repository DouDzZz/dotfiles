import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronCircleRight: StyledIcon<any>;
export declare const ChevronCircleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
