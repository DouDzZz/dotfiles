import { StyledIcon, StyledIconProps } from '..';
export declare const HourglassStart: StyledIcon<any>;
export declare const HourglassStartDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
