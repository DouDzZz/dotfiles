import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Thermometer: StyledIcon<any>;
export declare const ThermometerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
