import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const GreaterThanEqual: StyledIcon<any>;
export declare const GreaterThanEqualDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
