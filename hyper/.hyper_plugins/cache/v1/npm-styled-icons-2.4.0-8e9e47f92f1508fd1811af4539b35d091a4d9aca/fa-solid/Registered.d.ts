import { StyledIcon, StyledIconProps } from '..';
export declare const Registered: StyledIcon<any>;
export declare const RegisteredDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
