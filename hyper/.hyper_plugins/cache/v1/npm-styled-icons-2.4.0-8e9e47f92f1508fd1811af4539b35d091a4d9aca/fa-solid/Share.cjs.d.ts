import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Share: StyledIcon<any>;
export declare const ShareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
