import { StyledIcon, StyledIconProps } from '..';
export declare const UndoAlt: StyledIcon<any>;
export declare const UndoAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
