import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryQuarter: StyledIcon<any>;
export declare const BatteryQuarterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
