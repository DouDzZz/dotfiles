import { StyledIcon, StyledIconProps } from '..';
export declare const Spinner: StyledIcon<any>;
export declare const SpinnerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
