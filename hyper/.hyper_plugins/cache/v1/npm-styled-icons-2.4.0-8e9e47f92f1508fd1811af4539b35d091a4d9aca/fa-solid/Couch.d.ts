import { StyledIcon, StyledIconProps } from '..';
export declare const Couch: StyledIcon<any>;
export declare const CouchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
