import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const PencilAlt: StyledIcon<any>;
export declare const PencilAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
