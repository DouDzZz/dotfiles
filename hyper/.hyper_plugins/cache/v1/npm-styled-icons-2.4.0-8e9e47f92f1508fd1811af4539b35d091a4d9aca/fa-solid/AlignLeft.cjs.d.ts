import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignLeft: StyledIcon<any>;
export declare const AlignLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
