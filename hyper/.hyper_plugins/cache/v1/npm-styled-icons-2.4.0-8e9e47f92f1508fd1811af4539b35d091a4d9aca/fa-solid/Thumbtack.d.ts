import { StyledIcon, StyledIconProps } from '..';
export declare const Thumbtack: StyledIcon<any>;
export declare const ThumbtackDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
