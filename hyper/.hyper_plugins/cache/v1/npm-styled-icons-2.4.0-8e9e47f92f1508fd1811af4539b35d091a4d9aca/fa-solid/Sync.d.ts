import { StyledIcon, StyledIconProps } from '..';
export declare const Sync: StyledIcon<any>;
export declare const SyncDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
