import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Trophy: StyledIcon<any>;
export declare const TrophyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
