import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Box: StyledIcon<any>;
export declare const BoxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
