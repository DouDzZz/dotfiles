import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Film: StyledIcon<any>;
export declare const FilmDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
