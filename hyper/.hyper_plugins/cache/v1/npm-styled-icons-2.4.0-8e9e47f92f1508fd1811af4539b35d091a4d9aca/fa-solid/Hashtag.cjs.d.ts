import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Hashtag: StyledIcon<any>;
export declare const HashtagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
