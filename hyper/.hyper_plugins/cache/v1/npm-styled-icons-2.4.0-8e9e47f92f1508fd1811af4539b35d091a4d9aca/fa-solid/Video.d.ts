import { StyledIcon, StyledIconProps } from '..';
export declare const Video: StyledIcon<any>;
export declare const VideoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
