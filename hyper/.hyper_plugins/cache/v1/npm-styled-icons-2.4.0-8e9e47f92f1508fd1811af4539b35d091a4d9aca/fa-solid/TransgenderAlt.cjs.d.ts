import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const TransgenderAlt: StyledIcon<any>;
export declare const TransgenderAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
