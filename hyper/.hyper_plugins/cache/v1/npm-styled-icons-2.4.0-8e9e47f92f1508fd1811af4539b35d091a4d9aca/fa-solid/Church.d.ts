import { StyledIcon, StyledIconProps } from '..';
export declare const Church: StyledIcon<any>;
export declare const ChurchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
