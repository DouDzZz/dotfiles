import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Italic: StyledIcon<any>;
export declare const ItalicDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
