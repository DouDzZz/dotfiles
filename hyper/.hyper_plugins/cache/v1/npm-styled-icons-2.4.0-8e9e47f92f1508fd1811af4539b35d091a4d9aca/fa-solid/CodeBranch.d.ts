import { StyledIcon, StyledIconProps } from '..';
export declare const CodeBranch: StyledIcon<any>;
export declare const CodeBranchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
