import { StyledIcon, StyledIconProps } from '..';
export declare const Smoking: StyledIcon<any>;
export declare const SmokingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
