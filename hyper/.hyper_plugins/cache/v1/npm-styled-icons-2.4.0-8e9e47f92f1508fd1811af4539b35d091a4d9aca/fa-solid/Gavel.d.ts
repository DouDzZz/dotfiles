import { StyledIcon, StyledIconProps } from '..';
export declare const Gavel: StyledIcon<any>;
export declare const GavelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
