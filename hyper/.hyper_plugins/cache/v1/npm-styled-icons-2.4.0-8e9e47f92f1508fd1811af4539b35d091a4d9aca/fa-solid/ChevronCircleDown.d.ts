import { StyledIcon, StyledIconProps } from '..';
export declare const ChevronCircleDown: StyledIcon<any>;
export declare const ChevronCircleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
