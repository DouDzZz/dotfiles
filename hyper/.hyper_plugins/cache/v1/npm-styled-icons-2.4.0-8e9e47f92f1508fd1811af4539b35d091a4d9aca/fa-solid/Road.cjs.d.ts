import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Road: StyledIcon<any>;
export declare const RoadDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
