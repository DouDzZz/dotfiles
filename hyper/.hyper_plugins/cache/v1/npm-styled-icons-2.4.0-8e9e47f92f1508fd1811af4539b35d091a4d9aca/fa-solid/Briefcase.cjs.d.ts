import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Briefcase: StyledIcon<any>;
export declare const BriefcaseDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
