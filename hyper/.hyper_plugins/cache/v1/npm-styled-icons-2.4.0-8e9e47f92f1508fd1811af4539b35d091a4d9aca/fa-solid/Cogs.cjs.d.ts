import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cogs: StyledIcon<any>;
export declare const CogsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
