import { StyledIcon, StyledIconProps } from '..';
export declare const ICursor: StyledIcon<any>;
export declare const ICursorDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
