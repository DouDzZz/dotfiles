import { StyledIcon, StyledIconProps } from '..';
export declare const Building: StyledIcon<any>;
export declare const BuildingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
