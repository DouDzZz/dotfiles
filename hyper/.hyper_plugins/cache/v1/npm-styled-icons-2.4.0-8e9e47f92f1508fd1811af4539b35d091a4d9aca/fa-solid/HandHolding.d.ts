import { StyledIcon, StyledIconProps } from '..';
export declare const HandHolding: StyledIcon<any>;
export declare const HandHoldingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
