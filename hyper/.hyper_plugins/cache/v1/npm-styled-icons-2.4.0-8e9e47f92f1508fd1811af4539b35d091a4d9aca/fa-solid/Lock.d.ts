import { StyledIcon, StyledIconProps } from '..';
export declare const Lock: StyledIcon<any>;
export declare const LockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
