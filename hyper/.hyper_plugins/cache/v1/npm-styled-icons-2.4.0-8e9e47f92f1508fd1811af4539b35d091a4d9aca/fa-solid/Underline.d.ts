import { StyledIcon, StyledIconProps } from '..';
export declare const Underline: StyledIcon<any>;
export declare const UnderlineDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
