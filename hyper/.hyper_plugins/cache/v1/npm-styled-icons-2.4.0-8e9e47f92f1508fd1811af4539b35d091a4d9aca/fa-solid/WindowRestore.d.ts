import { StyledIcon, StyledIconProps } from '..';
export declare const WindowRestore: StyledIcon<any>;
export declare const WindowRestoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
