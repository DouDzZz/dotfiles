import { StyledIcon, StyledIconProps } from '..';
export declare const DollyFlatbed: StyledIcon<any>;
export declare const DollyFlatbedDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
