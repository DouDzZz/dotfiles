import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Ruler: StyledIcon<any>;
export declare const RulerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
