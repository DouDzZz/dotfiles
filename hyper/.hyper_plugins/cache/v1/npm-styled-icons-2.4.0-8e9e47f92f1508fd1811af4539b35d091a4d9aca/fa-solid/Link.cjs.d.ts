import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Link: StyledIcon<any>;
export declare const LinkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
