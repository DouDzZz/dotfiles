import { StyledIcon, StyledIconProps } from '..';
export declare const ClipboardList: StyledIcon<any>;
export declare const ClipboardListDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
