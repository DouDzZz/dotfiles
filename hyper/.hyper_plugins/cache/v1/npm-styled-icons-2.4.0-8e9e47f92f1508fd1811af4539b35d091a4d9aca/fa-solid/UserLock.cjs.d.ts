import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserLock: StyledIcon<any>;
export declare const UserLockDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
