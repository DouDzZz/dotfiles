import { StyledIcon, StyledIconProps } from '..';
export declare const WonSign: StyledIcon<any>;
export declare const WonSignDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
