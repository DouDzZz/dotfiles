import { StyledIcon, StyledIconProps } from '..';
export declare const FileExcel: StyledIcon<any>;
export declare const FileExcelDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
