import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const BatteryEmpty: StyledIcon<any>;
export declare const BatteryEmptyDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
