import { StyledIcon, StyledIconProps } from '..';
export declare const LevelUpAlt: StyledIcon<any>;
export declare const LevelUpAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
