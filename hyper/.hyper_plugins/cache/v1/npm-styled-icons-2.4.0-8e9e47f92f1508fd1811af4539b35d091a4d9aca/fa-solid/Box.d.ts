import { StyledIcon, StyledIconProps } from '..';
export declare const Box: StyledIcon<any>;
export declare const BoxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
