import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Bullhorn: StyledIcon<any>;
export declare const BullhornDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
