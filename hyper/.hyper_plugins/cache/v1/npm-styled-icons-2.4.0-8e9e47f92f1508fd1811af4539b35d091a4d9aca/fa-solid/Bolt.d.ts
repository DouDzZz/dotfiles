import { StyledIcon, StyledIconProps } from '..';
export declare const Bolt: StyledIcon<any>;
export declare const BoltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
