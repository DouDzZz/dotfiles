import { StyledIcon, StyledIconProps } from '..';
export declare const PrescriptionBottleAlt: StyledIcon<any>;
export declare const PrescriptionBottleAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
