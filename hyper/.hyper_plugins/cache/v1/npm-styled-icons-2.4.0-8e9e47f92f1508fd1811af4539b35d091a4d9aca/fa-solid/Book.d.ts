import { StyledIcon, StyledIconProps } from '..';
export declare const Book: StyledIcon<any>;
export declare const BookDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
