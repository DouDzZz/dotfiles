import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MarsStrokeV: StyledIcon<any>;
export declare const MarsStrokeVDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
