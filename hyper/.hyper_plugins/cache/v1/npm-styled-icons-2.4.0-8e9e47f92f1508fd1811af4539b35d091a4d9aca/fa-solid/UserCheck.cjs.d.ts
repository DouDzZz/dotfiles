import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const UserCheck: StyledIcon<any>;
export declare const UserCheckDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
