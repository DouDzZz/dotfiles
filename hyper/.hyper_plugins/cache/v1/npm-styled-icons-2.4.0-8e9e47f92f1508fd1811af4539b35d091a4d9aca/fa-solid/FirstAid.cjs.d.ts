import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const FirstAid: StyledIcon<any>;
export declare const FirstAidDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
