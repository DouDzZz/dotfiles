import { StyledIcon, StyledIconProps } from '..';
export declare const ChalkboardTeacher: StyledIcon<any>;
export declare const ChalkboardTeacherDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
