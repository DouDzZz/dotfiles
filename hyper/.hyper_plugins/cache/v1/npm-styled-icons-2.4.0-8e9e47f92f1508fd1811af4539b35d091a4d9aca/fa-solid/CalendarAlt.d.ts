import { StyledIcon, StyledIconProps } from '..';
export declare const CalendarAlt: StyledIcon<any>;
export declare const CalendarAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
