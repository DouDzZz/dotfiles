import { StyledIcon, StyledIconProps } from '..';
export declare const Quidditch: StyledIcon<any>;
export declare const QuidditchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
