import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const AlignRight: StyledIcon<any>;
export declare const AlignRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
