import { StyledIcon, StyledIconProps } from '..';
export declare const Shower: StyledIcon<any>;
export declare const ShowerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
