import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ExclamationTriangle: StyledIcon<any>;
export declare const ExclamationTriangleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
