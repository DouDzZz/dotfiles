import { StyledIcon, StyledIconProps } from '..';
export declare const CalendarMinus: StyledIcon<any>;
export declare const CalendarMinusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
