import { StyledIcon, StyledIconProps } from '..';
export declare const MarsDouble: StyledIcon<any>;
export declare const MarsDoubleDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
