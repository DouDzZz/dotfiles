import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ClosedCaptioning: StyledIcon<any>;
export declare const ClosedCaptioningDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
