"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styled_components_1 = __importDefault(require("styled-components"));
exports.ThermometerFull = styled_components_1.default.svg.attrs({
    children: function (props) { return (props.title != null
        ? [react_1.default.createElement("title", { key: "ThermometerFull-title" }, props.title), react_1.default.createElement("path", { fill: "currentColor", d: "M224 96c0-53.019-42.981-96-96-96S32 42.981 32 96v203.347C12.225 321.756.166 351.136.002 383.333c-.359 70.303 56.787 128.176 127.089 128.664.299.002.61.003.909.003 70.698 0 128-57.304 128-128 0-32.459-12.088-62.09-32-84.653V96zm-96 368l-.576-.002c-43.86-.304-79.647-36.544-79.423-80.42.173-33.98 19.266-51.652 31.999-66.08V96c0-26.467 21.533-48 48-48s48 21.533 48 48v221.498c12.63 14.312 32 32.164 32 66.502 0 44.112-35.888 80-80 80zm64-80c0 35.346-28.654 64-64 64s-64-28.654-64-64c0-23.685 12.876-44.349 32-55.417V96c0-17.673 14.327-32 32-32s32 14.327 32 32v232.583c19.124 11.068 32 31.732 32 55.417z", key: "k0" })
        ]
        : [react_1.default.createElement("path", { fill: "currentColor", d: "M224 96c0-53.019-42.981-96-96-96S32 42.981 32 96v203.347C12.225 321.756.166 351.136.002 383.333c-.359 70.303 56.787 128.176 127.089 128.664.299.002.61.003.909.003 70.698 0 128-57.304 128-128 0-32.459-12.088-62.09-32-84.653V96zm-96 368l-.576-.002c-43.86-.304-79.647-36.544-79.423-80.42.173-33.98 19.266-51.652 31.999-66.08V96c0-26.467 21.533-48 48-48s48 21.533 48 48v221.498c12.63 14.312 32 32.164 32 66.502 0 44.112-35.888 80-80 80zm64-80c0 35.346-28.654 64-64 64s-64-28.654-64-64c0-23.685 12.876-44.349 32-55.417V96c0-17.673 14.327-32 32-32s32 14.327 32 32v232.583c19.124 11.068 32 31.732 32 55.417z", key: "k0" })
        ]); },
    viewBox: '0 0 256 512',
    height: function (props) { return (props.height !== undefined ? props.height : props.size); },
    width: function (props) { return (props.width !== undefined ? props.width : props.size); },
    // @ts-ignore - aria is not always defined on SVG in React TypeScript types
    'aria-hidden': function (props) { return (props.title == null ? 'true' : undefined); },
    focusable: 'false',
    role: function (props) { return (props.title != null ? 'img' : undefined); },
    "fill": "currentColor",
})(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"], ["\n  display: inline-block;\n  vertical-align: -.125em;\n  overflow: hidden;\n  ", ";\n"])), function (props) { return props.css; });
exports.ThermometerFull.displayName = 'ThermometerFull';
exports.ThermometerFullDimensions = { height: undefined, width: undefined };
var templateObject_1;
