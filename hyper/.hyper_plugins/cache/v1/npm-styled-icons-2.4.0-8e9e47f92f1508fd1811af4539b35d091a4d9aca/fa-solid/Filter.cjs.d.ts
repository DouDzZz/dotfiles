import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Filter: StyledIcon<any>;
export declare const FilterDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
