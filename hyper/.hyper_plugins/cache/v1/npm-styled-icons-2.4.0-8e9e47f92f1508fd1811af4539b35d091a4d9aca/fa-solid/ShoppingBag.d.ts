import { StyledIcon, StyledIconProps } from '..';
export declare const ShoppingBag: StyledIcon<any>;
export declare const ShoppingBagDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
