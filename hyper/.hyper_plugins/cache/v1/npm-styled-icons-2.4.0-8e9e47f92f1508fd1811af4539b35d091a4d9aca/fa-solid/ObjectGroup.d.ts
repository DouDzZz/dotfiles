import { StyledIcon, StyledIconProps } from '..';
export declare const ObjectGroup: StyledIcon<any>;
export declare const ObjectGroupDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
