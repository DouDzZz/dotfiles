import { StyledIcon, StyledIconProps } from '..';
export declare const CompactDisc: StyledIcon<any>;
export declare const CompactDiscDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
