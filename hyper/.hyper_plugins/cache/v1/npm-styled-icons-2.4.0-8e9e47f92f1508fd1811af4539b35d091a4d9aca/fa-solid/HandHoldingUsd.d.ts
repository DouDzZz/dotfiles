import { StyledIcon, StyledIconProps } from '..';
export declare const HandHoldingUsd: StyledIcon<any>;
export declare const HandHoldingUsdDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
