import { StyledIcon, StyledIconProps } from '..';
export declare const AlignLeft: StyledIcon<any>;
export declare const AlignLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
