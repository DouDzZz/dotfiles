import { StyledIcon, StyledIconProps } from '..';
export declare const ChartPie: StyledIcon<any>;
export declare const ChartPieDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
