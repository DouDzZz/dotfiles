import { StyledIcon, StyledIconProps } from '..';
export declare const Lemon: StyledIcon<any>;
export declare const LemonDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
