import { StyledIcon, StyledIconProps } from '..';
export declare const AngleRight: StyledIcon<any>;
export declare const AngleRightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
