import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Feather: StyledIcon<any>;
export declare const FeatherDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
