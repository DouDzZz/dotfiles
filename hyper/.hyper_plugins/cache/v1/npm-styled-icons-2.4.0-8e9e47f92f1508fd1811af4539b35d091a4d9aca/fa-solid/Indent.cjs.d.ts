import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Indent: StyledIcon<any>;
export declare const IndentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
