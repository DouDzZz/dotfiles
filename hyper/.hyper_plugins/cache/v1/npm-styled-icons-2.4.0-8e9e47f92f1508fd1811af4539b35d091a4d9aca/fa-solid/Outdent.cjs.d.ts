import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Outdent: StyledIcon<any>;
export declare const OutdentDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
