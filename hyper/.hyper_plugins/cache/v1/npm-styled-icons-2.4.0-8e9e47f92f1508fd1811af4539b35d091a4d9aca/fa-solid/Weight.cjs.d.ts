import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Weight: StyledIcon<any>;
export declare const WeightDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
