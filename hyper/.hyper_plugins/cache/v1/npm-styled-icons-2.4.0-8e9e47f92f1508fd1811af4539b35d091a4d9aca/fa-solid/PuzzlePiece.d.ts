import { StyledIcon, StyledIconProps } from '..';
export declare const PuzzlePiece: StyledIcon<any>;
export declare const PuzzlePieceDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
