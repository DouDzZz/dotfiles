import { StyledIcon, StyledIconProps } from '..';
export declare const MicrophoneSlash: StyledIcon<any>;
export declare const MicrophoneSlashDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
