import { StyledIcon, StyledIconProps } from '..';
export declare const Dna: StyledIcon<any>;
export declare const DnaDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
