import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ShoePrints: StyledIcon<any>;
export declare const ShoePrintsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
