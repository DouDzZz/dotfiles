import { StyledIcon, StyledIconProps } from '..';
export declare const EnvelopeSquare: StyledIcon<any>;
export declare const EnvelopeSquareDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
