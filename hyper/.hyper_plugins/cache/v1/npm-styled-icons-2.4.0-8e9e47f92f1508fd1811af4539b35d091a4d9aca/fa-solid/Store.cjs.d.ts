import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Store: StyledIcon<any>;
export declare const StoreDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
