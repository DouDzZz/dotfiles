import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Church: StyledIcon<any>;
export declare const ChurchDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
