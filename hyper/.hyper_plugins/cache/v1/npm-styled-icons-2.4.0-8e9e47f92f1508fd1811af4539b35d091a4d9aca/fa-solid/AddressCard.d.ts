import { StyledIcon, StyledIconProps } from '..';
export declare const AddressCard: StyledIcon<any>;
export declare const AddressCardDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
