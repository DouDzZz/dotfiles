import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Building: StyledIcon<any>;
export declare const BuildingDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
