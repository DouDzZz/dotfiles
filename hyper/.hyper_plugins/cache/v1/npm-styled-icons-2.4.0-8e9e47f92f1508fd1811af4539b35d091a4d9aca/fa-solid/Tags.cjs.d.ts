import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Tags: StyledIcon<any>;
export declare const TagsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
