import { StyledIcon, StyledIconProps } from '..';
export declare const Vial: StyledIcon<any>;
export declare const VialDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
