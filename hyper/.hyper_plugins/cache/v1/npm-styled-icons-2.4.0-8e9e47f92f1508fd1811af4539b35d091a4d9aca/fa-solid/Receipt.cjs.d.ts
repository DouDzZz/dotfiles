import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Receipt: StyledIcon<any>;
export declare const ReceiptDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
