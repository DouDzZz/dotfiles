import { StyledIcon, StyledIconProps } from '..';
export declare const TransgenderAlt: StyledIcon<any>;
export declare const TransgenderAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
