import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Snowflake: StyledIcon<any>;
export declare const SnowflakeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
