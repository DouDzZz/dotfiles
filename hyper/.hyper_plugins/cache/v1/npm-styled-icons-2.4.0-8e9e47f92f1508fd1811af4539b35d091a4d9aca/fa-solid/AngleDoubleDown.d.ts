import { StyledIcon, StyledIconProps } from '..';
export declare const AngleDoubleDown: StyledIcon<any>;
export declare const AngleDoubleDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
