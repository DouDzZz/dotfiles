import { StyledIcon, StyledIconProps } from '..';
export declare const EyeDropper: StyledIcon<any>;
export declare const EyeDropperDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
