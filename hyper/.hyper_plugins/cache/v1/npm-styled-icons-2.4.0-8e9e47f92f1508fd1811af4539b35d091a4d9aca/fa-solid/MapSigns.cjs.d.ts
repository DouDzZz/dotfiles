import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const MapSigns: StyledIcon<any>;
export declare const MapSignsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
