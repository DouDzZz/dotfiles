import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Toolbox: StyledIcon<any>;
export declare const ToolboxDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
