import { StyledIcon, StyledIconProps } from '..';
export declare const Info: StyledIcon<any>;
export declare const InfoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
