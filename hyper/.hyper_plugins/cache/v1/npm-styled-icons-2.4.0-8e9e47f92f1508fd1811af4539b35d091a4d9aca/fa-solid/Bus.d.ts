import { StyledIcon, StyledIconProps } from '..';
export declare const Bus: StyledIcon<any>;
export declare const BusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
