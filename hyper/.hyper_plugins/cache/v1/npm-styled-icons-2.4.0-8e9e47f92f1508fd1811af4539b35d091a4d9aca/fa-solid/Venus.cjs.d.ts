import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Venus: StyledIcon<any>;
export declare const VenusDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
