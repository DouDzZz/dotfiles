import { StyledIcon, StyledIconProps } from '..';
export declare const Link: StyledIcon<any>;
export declare const LinkDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
