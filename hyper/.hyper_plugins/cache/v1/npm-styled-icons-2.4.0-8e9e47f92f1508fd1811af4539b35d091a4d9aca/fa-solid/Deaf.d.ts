import { StyledIcon, StyledIconProps } from '..';
export declare const Deaf: StyledIcon<any>;
export declare const DeafDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
