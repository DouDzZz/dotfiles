import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const ChevronCircleLeft: StyledIcon<any>;
export declare const ChevronCircleLeftDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
