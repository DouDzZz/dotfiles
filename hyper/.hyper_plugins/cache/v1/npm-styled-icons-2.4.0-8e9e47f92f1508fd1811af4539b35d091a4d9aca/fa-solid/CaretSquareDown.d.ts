import { StyledIcon, StyledIconProps } from '..';
export declare const CaretSquareDown: StyledIcon<any>;
export declare const CaretSquareDownDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
