import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Crosshairs: StyledIcon<any>;
export declare const CrosshairsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
