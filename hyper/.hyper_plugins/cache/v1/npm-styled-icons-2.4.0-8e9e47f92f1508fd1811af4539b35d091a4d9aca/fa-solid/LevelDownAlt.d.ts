import { StyledIcon, StyledIconProps } from '..';
export declare const LevelDownAlt: StyledIcon<any>;
export declare const LevelDownAltDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
