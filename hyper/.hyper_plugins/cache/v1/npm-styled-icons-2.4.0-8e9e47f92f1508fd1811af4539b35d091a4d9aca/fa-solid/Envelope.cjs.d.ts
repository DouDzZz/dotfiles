import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Envelope: StyledIcon<any>;
export declare const EnvelopeDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
