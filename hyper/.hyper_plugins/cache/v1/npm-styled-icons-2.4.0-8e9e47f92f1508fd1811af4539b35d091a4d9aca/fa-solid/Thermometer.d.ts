import { StyledIcon, StyledIconProps } from '..';
export declare const Thermometer: StyledIcon<any>;
export declare const ThermometerDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
