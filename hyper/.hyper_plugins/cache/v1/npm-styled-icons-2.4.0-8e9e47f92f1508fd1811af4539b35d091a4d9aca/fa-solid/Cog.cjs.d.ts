import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Cog: StyledIcon<any>;
export declare const CogDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
