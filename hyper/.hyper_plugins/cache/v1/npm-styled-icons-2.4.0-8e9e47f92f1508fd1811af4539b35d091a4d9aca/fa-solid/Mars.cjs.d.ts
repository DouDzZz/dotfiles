import { StyledIcon, StyledIconProps } from '../index.cjs';
export declare const Mars: StyledIcon<any>;
export declare const MarsDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
