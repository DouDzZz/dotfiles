import { StyledIcon, StyledIconProps } from '..';
export declare const DiceTwo: StyledIcon<any>;
export declare const DiceTwoDimensions: {
    height: any;
    width: any;
};
export { StyledIcon, StyledIconProps };
