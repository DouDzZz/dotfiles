/// <reference types="react" />
import { StyledComponentClass, Interpolation, ThemedStyledProps } from 'styled-components';
import * as faRegular from './fa-regular';
import * as faSolid from './fa-solid';
import * as faBrands from './fa-brands';
import * as feather from './feather';
import * as material from './material';
import * as octicons from './octicons';
export interface AriaAttributes {
    'aria-hidden'?: string;
}
export interface StyledIconProps<T> extends React.SVGProps<SVGSVGElement>, AriaAttributes {
    css?: Interpolation<ThemedStyledProps<StyledIconProps<T>, T>>;
    size?: number | string;
    title?: string | null;
}
export interface StyledIcon<T = any> extends StyledComponentClass<StyledIconProps<T>, T> {
}
export { faRegular, faSolid, faBrands, feather, material, octicons };
