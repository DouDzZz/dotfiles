var styledIs = (function (exports,styledComponents) {
'use strict';

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var styledIf = function styledIf(method, condition) {
  return function () {
    for (var _len = arguments.length, names = Array(_len), _key = 0; _key < _len; _key++) {
      names[_key] = arguments[_key];
    }

    return function () {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return function (props) {
        return (method === "match" ? props[names[0]] === names[1] : names[method](function (name) {
          return Boolean(props[name]) === condition;
        })) && styledComponents.css.apply(undefined, toConsumableArray(handleFunctions(args, props)));
      };
    };
  };
};

var handleFunctions = function handleFunctions(args, props) {
  var css = "";
  for (var i = 1; i < args.length; i++) {
    if (typeof args[i] === "function") {
      var output = args[i](props);
      if (output.includes(":")) {
        css = css + output;
      }
    }
  }
  if (css) {
    var newArgs = args.slice(0);
    var argCss = args[0].slice(1);
    argCss.unshift(css + newArgs[0][0]);
    newArgs[0] = argCss;
    return newArgs;
  }
  return args;
};

var is = styledIf('every', true);
var isNot = styledIf('every', false);
var isOr = styledIf('some', true);
var isSomeNot = styledIf('some', false);
var match = styledIf("match");

exports.default = is;
exports.isNot = isNot;
exports.isOr = isOr;
exports.isSomeNot = isSomeNot;
exports.match = match;

return exports;

}({},styledComponents));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRoaXMgU291cmNlIENvZGUgRm9ybSBpcyBzdWJqZWN0IHRvIHRoZSB0ZXJtcyBvZiB0aGUgTW96aWxsYSBQdWJsaWNcbi8vIExpY2Vuc2UsIHYuIDIuMC4gSWYgYSBjb3B5IG9mIHRoZSBNUEwgd2FzIG5vdCBkaXN0cmlidXRlZCB3aXRoIHRoaXNcbi8vIGZpbGUsIFlvdSBjYW4gb2J0YWluIG9uZSBhdCBodHRwOi8vbW96aWxsYS5vcmcvTVBMLzIuMC8uXG5cbmltcG9ydCB7IGNzcyB9IGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3Qgc3R5bGVkSWYgPSAobWV0aG9kLCBjb25kaXRpb24pID0+ICguLi5uYW1lcykgPT4gKC4uLmFyZ3MpID0+IHByb3BzID0+IHtcbiAgcmV0dXJuIChcbiAgICAobWV0aG9kID09PSBcIm1hdGNoXCJcbiAgICAgID8gcHJvcHNbbmFtZXNbMF1dID09PSBuYW1lc1sxXVxuICAgICAgOiBuYW1lc1ttZXRob2RdKG5hbWUgPT4ge1xuICAgICAgICAgIHJldHVybiBCb29sZWFuKHByb3BzW25hbWVdKSA9PT0gY29uZGl0aW9uO1xuICAgICAgICB9KSkgJiYgY3NzKC4uLmhhbmRsZUZ1bmN0aW9ucyhhcmdzLCBwcm9wcykpXG4gICk7XG59O1xuXG5jb25zdCBoYW5kbGVGdW5jdGlvbnMgPSAoYXJncywgcHJvcHMpID0+IHtcbiAgbGV0IGNzcyA9IFwiXCI7XG4gIGZvciAobGV0IGkgPSAxOyBpIDwgYXJncy5sZW5ndGg7IGkrKykge1xuICAgIGlmICh0eXBlb2YgYXJnc1tpXSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICBjb25zdCBvdXRwdXQgPSBhcmdzW2ldKHByb3BzKTtcbiAgICAgIGlmIChvdXRwdXQuaW5jbHVkZXMoXCI6XCIpKSB7XG4gICAgICAgIGNzcyA9IGNzcyArIG91dHB1dDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgaWYgKGNzcykge1xuICAgIGNvbnN0IG5ld0FyZ3MgPSBhcmdzLnNsaWNlKDApO1xuICAgIGNvbnN0IGFyZ0NzcyA9IGFyZ3NbMF0uc2xpY2UoMSk7XG4gICAgYXJnQ3NzLnVuc2hpZnQoY3NzICsgbmV3QXJnc1swXVswXSk7XG4gICAgbmV3QXJnc1swXSA9IGFyZ0NzcztcbiAgICByZXR1cm4gbmV3QXJncztcbiAgfVxuICByZXR1cm4gYXJncztcbn07XG5cbmNvbnN0IGlzID0gc3R5bGVkSWYoJ2V2ZXJ5JywgdHJ1ZSk7XG5jb25zdCBpc05vdCA9IHN0eWxlZElmKCdldmVyeScsIGZhbHNlKTtcbmNvbnN0IGlzT3IgPSBzdHlsZWRJZignc29tZScsIHRydWUpO1xuY29uc3QgaXNTb21lTm90ID0gc3R5bGVkSWYoJ3NvbWUnLCBmYWxzZSk7XG5jb25zdCBtYXRjaCA9IHN0eWxlZElmKFwibWF0Y2hcIik7XG5cbmV4cG9ydCBkZWZhdWx0IGlzO1xuZXhwb3J0IHsgaXNOb3QsIGlzT3IsIGlzU29tZU5vdCwgbWF0Y2ggfTtcbiJdLCJuYW1lcyI6WyJzdHlsZWRJZiIsIm1ldGhvZCIsImNvbmRpdGlvbiIsIm5hbWVzIiwiYXJncyIsInByb3BzIiwiQm9vbGVhbiIsIm5hbWUiLCJjc3MiLCJoYW5kbGVGdW5jdGlvbnMiLCJpIiwibGVuZ3RoIiwib3V0cHV0IiwiaW5jbHVkZXMiLCJuZXdBcmdzIiwic2xpY2UiLCJhcmdDc3MiLCJ1bnNoaWZ0IiwiaXMiLCJpc05vdCIsImlzT3IiLCJpc1NvbWVOb3QiLCJtYXRjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBOzs7O0FBSUEsQUFFQSxJQUFNQSxXQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsTUFBRCxFQUFTQyxTQUFUO1NBQXVCO3NDQUFJQyxLQUFKO1dBQUE7OztXQUFjO3lDQUFJQyxJQUFKO1lBQUE7OzthQUFhLGlCQUFTO2VBRXhFLENBQUNILFdBQVcsT0FBWCxHQUNHSSxNQUFNRixNQUFNLENBQU4sQ0FBTixNQUFvQkEsTUFBTSxDQUFOLENBRHZCLEdBRUdBLE1BQU1GLE1BQU4sRUFBYyxnQkFBUTtpQkFDYkssUUFBUUQsTUFBTUUsSUFBTixDQUFSLE1BQXlCTCxTQUFoQztTQURGLENBRkosS0FJV00sd0RBQU9DLGdCQUFnQkwsSUFBaEIsRUFBc0JDLEtBQXRCLENBQVAsRUFMYjtPQURvRDtLQUFkO0dBQXZCO0NBQWpCOztBQVVBLElBQU1JLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ0wsSUFBRCxFQUFPQyxLQUFQLEVBQWlCO01BQ25DRyxNQUFNLEVBQVY7T0FDSyxJQUFJRSxJQUFJLENBQWIsRUFBZ0JBLElBQUlOLEtBQUtPLE1BQXpCLEVBQWlDRCxHQUFqQyxFQUFzQztRQUNoQyxPQUFPTixLQUFLTSxDQUFMLENBQVAsS0FBbUIsVUFBdkIsRUFBbUM7VUFDM0JFLFNBQVNSLEtBQUtNLENBQUwsRUFBUUwsS0FBUixDQUFmO1VBQ0lPLE9BQU9DLFFBQVAsQ0FBZ0IsR0FBaEIsQ0FBSixFQUEwQjtjQUNsQkwsTUFBTUksTUFBWjs7OztNQUlGSixHQUFKLEVBQVM7UUFDRE0sVUFBVVYsS0FBS1csS0FBTCxDQUFXLENBQVgsQ0FBaEI7UUFDTUMsU0FBU1osS0FBSyxDQUFMLEVBQVFXLEtBQVIsQ0FBYyxDQUFkLENBQWY7V0FDT0UsT0FBUCxDQUFlVCxNQUFNTSxRQUFRLENBQVIsRUFBVyxDQUFYLENBQXJCO1lBQ1EsQ0FBUixJQUFhRSxNQUFiO1dBQ09GLE9BQVA7O1NBRUtWLElBQVA7Q0FqQkY7O0FBb0JBLElBQU1jLEtBQUtsQixTQUFTLE9BQVQsRUFBa0IsSUFBbEIsQ0FBWDtBQUNBLElBQU1tQixRQUFRbkIsU0FBUyxPQUFULEVBQWtCLEtBQWxCLENBQWQ7QUFDQSxJQUFNb0IsT0FBT3BCLFNBQVMsTUFBVCxFQUFpQixJQUFqQixDQUFiO0FBQ0EsSUFBTXFCLFlBQVlyQixTQUFTLE1BQVQsRUFBaUIsS0FBakIsQ0FBbEI7QUFDQSxJQUFNc0IsUUFBUXRCLFNBQVMsT0FBVCxDQUFkOzs7Ozs7Ozs7Ozs7OzsifQ==