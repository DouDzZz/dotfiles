# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    .zshrc                                             :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/08 10:05:24 by edjubert          #+#    #+#              #
#    Updated: 2019/04/02 13:36:10 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#******************************************************************************#
#                                                                              #
#                                   OH MY ZSH                                  #
#                                                                              #
#******************************************************************************#

#----------------------------------   PATH   ----------------------------------#

# Path to your oh-my-zsh installation.
export ZSH="/Users/edjubert/.oh-my-zsh"
DOT="/sgoinfre/goinfre/Perso/edjubert/dotfiles/"


#----------------------------------  THEMES  ----------------------------------#

# ZSH_THEME="powerlevel9k/powerlevel9k"
# ZSH_THEME="lambda-mod"
ZSH_THEME="hyperzsh"
# ZSH_THEME="schminitz"
# ZSH_THEME="pi"
# ZSH_THEME="lambda-gitster"
# ZSH_THEME="spaceship"
# ZSH_THEME="zeta"
# ZSH_THEME="geometry"


#----------------------------------  PLUGINS  ---------------------------------#

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

COMPLETION_WAITING_DOTS="true"
plugins=(
	git
	zsh-autosuggestions
	zsh-syntax-highlighting
	command-not-found
	common-aliases
	copyfile
	dircycle
	history
	encode64
	git-extras
	git-flow-avh
	lol
	osx
	per-directory-history
	tig
	vi-mode
	vscode
	web-search
	wd
)
source $ZSH/oh-my-zsh.sh
source $DOT/zsh/.zsh/.per-directory-history.zsh
# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true"
# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13
# ZSH_CUSTOM=/path/to/new-custom-folder
# HIST_STAMPS="mm/dd/yyyy"
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"


#******************************************************************************#
#                                                                              #
#                                    ALIASES                                   #
#                                                                              #
#******************************************************************************#

bindkey -v
bindkey '^ ' autosuggest-accept

#******************************************************************************#
#                                                                              #
#                                    ALIASES                                   #
#                                                                              #
#******************************************************************************#

BROWSER="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"

alias gccFlags="gcc -Wall -Wextra -Werror"
alias code='/sgoinfre/goinfre/Perso/edjubert/applications/Visual\ Studio\ Code.app/Contents/MacOS/Electron'
alias vim="nvim"
alias lock="/Users/edjubert/.lock"
alias gg="git grep "
alias gs="git status"
alias checkout="git checkout "
alias commit="git commit "
alias stud42="$BROWSER stud42.fr"
alias intra42="$BROWSER profile.intra.42.fr"
alias djam="$BROWSER djamradio.com"
alias meuh="$BROWSER player.radiomeuh.com"
alias osx_helper="$DOT/scripts/.osx_helper"


#******************************************************************************#
#                                                                              #
#                               HOMEBREW 42 FIX                                #
#                                                                              #
#******************************************************************************#

# Load Homebrew config script

source $HOME/.brewconfig.zsh
export VAGRANT_HOME=/Volumes/Storage/goinfre/edjubert/vagrant


#******************************************************************************#
#                                                                              #
#                            TMUX RESTORE SESSION                              #
#                                                                              #
#******************************************************************************#

if [ "$TMUX" = "" ]; then 
	tmux new-session -A -s spawn; 
fi



#******************************************************************************#
#                                                                              #
#                              POWERLEVEL9K CONFIG                             #
#                                                                              #
#******************************************************************************#

# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs disk_usage)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs battery history date time)
# POWERLEVEL9K_DATE_FORMAT=%D{%d.%m.%Y}
# POWERLEVEL9K_DIR_PATH_ABSOLUTE=false
# POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
# POWERLEVEL9K_SHORTEN_STRATEGY="truncate_to_unique"
# POWERLEVEL9K_BATTERY_STAGES="▁▂▃▄▅▆▇█"
# POWERLEVEL9K_BATTERY_LEVEL_BACKGROUND=(
	# red1
	# orangered1
	# darkorange
	# orange1
	# gold1
	# yellow1
	# yellow2
	# greenyellow
	# chartreuse1
	# chartreuse2
	# green1
# )
#POWERLEVEL9K_BATTERY_STAGES=(
	# $'\u2581 '
	# $'\u2582 '
	# $'\u2583 '
	# $'\u2584 '
	# $'\u2585 '
	# $'\u2586 '
	# $'\u2587 '
	# $'\u2588 '
# )
#POWERLEVEL9K_BATTERY_STAGES=(
#   $'▏    ▏' $'▎    ▏' $'▍    ▏' $'▌    ▏' $'▋    ▏' $'▊    ▏' $'▉    ▏' $'█    ▏'
#   $'█▏   ▏' $'█▎   ▏' $'█▍   ▏' $'█▌   ▏' $'█▋   ▏' $'█▊   ▏' $'█▉   ▏' $'██   ▏'
#   $'██   ▏' $'██▎  ▏' $'██▍  ▏' $'██▌  ▏' $'██▋  ▏' $'██▊  ▏' $'██▉  ▏' $'███  ▏'
#   $'███  ▏' $'███▎ ▏' $'███▍ ▏' $'███▌ ▏' $'███▋ ▏' $'███▊ ▏' $'███▉ ▏' $'████ ▏'
#   $'████ ▏' $'████▎▏' $'████▍▏' $'████▌▏' $'████▋▏' $'████▊▏' $'████▉▏' $'█████▏' )

